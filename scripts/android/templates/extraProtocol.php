package <?=PACKAGE_PREFIX?><?=$info['package']?>;
/*
该文件由脚本自动生成，不要手动修改
 */
<?foreach($info['import'] as $key=>$val){?>
import <?=PACKAGE_PREFIX.".$val.$key"; ?>;
<?}?>
import java.util.ArrayList;

public class <?=$info['name']; ?>{
<?
$hasList=false;
$hasListVal="String";
foreach($info['fields'] as $key=>$val){
if(strpos($key,'?')>0){
    $hasList=trim($key,'?');
    $hasListVal=$val;?>
    public ArrayList<<?=$val?>> <?=$hasList?>=new ArrayList<>();
<?}elseif(strpos($key,'[]')>0){
    $hasList=trim($key,'[]');?>
    public ArrayList<String> <?=$hasList?>=new ArrayList<>();
<?}else{?>
    public <?=$val;?> <?=$key;?>;
<?}?>
<?}
?>
}