
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@Table(name = "usercollectlistResponse")
public class usermessagelistResponse extends Model {

    @Column(name = "status")
    public STATUS status;

    public List<MESSAGE> data;

    @Column(name = "paginated")
    public PAGINATED paginated;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }
        JSONArray subItemArray;

        STATUS status = new STATUS();
        status.fromJson(jsonObject);
        this.status = status;

        subItemArray = jsonObject.optJSONObject("data").optJSONArray("list");
        data = new ArrayList<>();
        if (null != subItemArray && subItemArray.length() > 0) {
            for (int i = 0; i < subItemArray.length(); i++) {
                JSONObject subItemObject = subItemArray.getJSONObject(i);
                MESSAGE subItem = new MESSAGE();
                subItem.fromJson(subItemObject);
                this.data.add(subItem);
            }
        }

        PAGINATED paginated = new PAGINATED();
        paginated.fromJson(jsonObject.optJSONObject("data").optJSONObject("pageInfo"));
        this.paginated = paginated;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();
        JSONArray itemJSONArray = new JSONArray();
        if (null != status) {
            localItemObject.put("status", status.toJson());
        }

        for (int i = 0; i < data.size(); i++) {
            MESSAGE itemData = data.get(i);
            JSONObject itemJSONObject = itemData.toJson();
            itemJSONArray.put(itemJSONObject);
        }
        localItemObject.put("data", itemJSONArray);
        if (null != paginated) {
            localItemObject.put("paginated", paginated.toJson());
        }
        return localItemObject;
    }

}
