package com.xz.shiku.fragment.order;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.AppConst;
import com.xz.btc.BTCManager;
import com.xz.btc.activity.AlixPayActivity;
import com.xz.btc.protocol.ADDRESS;
import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.BONUS;
import com.xz.btc.protocol.GOODORDER;
import com.xz.btc.protocol.ORDER_INFO;
import com.xz.btc.protocol.PAYMENT;
import com.xz.btc.protocol.SHIPPING;
import com.xz.btc.protocol.STATUS;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.external.view.XListViewCart;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.adapter.order.OrderDetailsAdapter;
import com.xz.shiku.controller.OrderController;
import com.xz.shiku.fragment.address.AddressListFragment;
import com.xz.shiku.fragment.payment.PaymentFragment;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.utils.Utils;
import com.xz.tframework.view.MyDialog;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderTrackFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderTrackFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderTrackFragment extends BackHandledFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    private List<OrderController.OrderListViewItem> mOrderListViewItems;

    @InjectView(R.id.tv_order_shipping_carrier)
    TextView mShippingCarrier;
    @InjectView(R.id.tv_order_shipping_tracking_number)
    TextView mTrackingNumber;
    @InjectView(R.id.tv_order_shipping_rates)
    TextView mShippingRates;
    @InjectView(R.id.lv_order_details)
    ListView mOrderDetails;
    @InjectView(R.id.wv_order_shipping_tracking_map)
    WebView mTrackingMap;

    private GOODORDER mOrder;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrderTrackFragment newInstance(String param1, String param2, List<OrderController.OrderListViewItem> viewItems) {
        PopActivity.gShowNavigationBar = true;

        titleResId = R.string.title_fragment_order_track;
        topRightText = "";
        topRightTextResId = 0;

        OrderTrackFragment fragment = new OrderTrackFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        fragment.mOrderListViewItems = viewItems;

        return fragment;
    }

    public OrderTrackFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_ordertrack, null);

        ButterKnife.inject(this, view);

        OrderDetailsAdapter adapter = new OrderDetailsAdapter(getActivity(), mOrderListViewItems);
        mOrderDetails.setAdapter(adapter);

        mOrder = mOrderListViewItems.size() > 0 ? mOrderListViewItems.get(0).mOrder : null;
        initUI(mOrder);

        return view;
    }

    private void initUI(GOODORDER order) {
        if (order != null) {
            mShippingCarrier.setText(order.order_info.shipping_carrier);
            mTrackingNumber.setText(String.format("快递单号：%s", order.order_info.shipping_tracking_number));
            mShippingRates.setText(String.format("备注说明：￥%s 元", Utils.tryParseDouble(order.order_info.shipping_remark, 0D)));

            mTrackingMap.getSettings().setJavaScriptEnabled(true);
            mTrackingMap.getSettings().setDomStorageEnabled(true);
            mTrackingMap.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });
            mTrackingMap.loadUrl(AppConst.URL_SHIPPING_TRACKING_MAP);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and mAddressName
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {

    }
}
