package com.xz.shiku.fragment.search;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.FILTER;
import com.xz.btc.protocol.SIMPLEGOODS;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.SearchResultActivity;
import com.xz.shiku.adapter.search.SearchResultAdapter;
import com.xz.shiku.controller.SearchController;
import com.xz.shiku.controller.ShoppingCartController;
import com.xz.shiku.fragment.CartFragment;
import com.xz.shiku.fragment.product.ProductDetailsFragment;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.utils.GridViewScrollListener;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class SearchResultFragment extends BackHandledFragment implements GridViewScrollListener.OnLoadMoreListener {

    private SearchResultAdapter searchResultAdapter;
    private FILTER mSearchFilter;
    private int mSearchBy;
    private int mLastOrderBy;
    private boolean mIsAscending;

    @InjectView(R.id.shopping_cart_badge)
    LinearLayout mShoppingCartBadge;
    @InjectViews({R.id.tv_gp1, R.id.tv_gp2, R.id.tv_gp3, R.id.tv_gp4})
    List<TextView> tvGrips;
    @InjectView(R.id.gvProductList)
    GridView mProducts;

    public SearchResultFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tf_search_result, container, false);

        ButterKnife.inject(this, view);

        mSearchFilter = new FILTER();
        Intent intent = getActivity().getIntent();
        mSearchBy = intent.getIntExtra(SearchResultActivity.SEARCH_ARG_BY, SearchResultActivity.KEYWORD);
        switch (mSearchBy) {
            case SearchResultActivity.KEYWORD:
                mSearchFilter.keywords = intent.getStringExtra(SearchResultActivity.SEARCH_ARG_KEYWORD);
                break;
            case SearchResultActivity.CATEGORY:
                mSearchFilter.category_id = intent.getStringExtra(SearchResultActivity.SEARCH_ARG_CATEGORY_ID);
                break;
        }

        mSearchFilter.sort_by = "sales,id";
        mSearchFilter.sort_order = "desc,desc";
        mLastOrderBy = R.id.tv_gp1;
        mIsAscending = true;

        mProducts.setOnScrollListener(new GridViewScrollListener(true, true, this));

        SearchController.getInstance().addResponseListener(this);
        SearchController.getInstance().getProduct(getActivity(), mSearchFilter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ShoppingCartController.getInstance().updateShoppingCartBadge(mShoppingCartBadge);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        SearchController.getInstance().removeResponseListener(this);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }


    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.SEARCH)) {
            if (SearchController.getInstance().getLastResponseStatus().getIsSuccess()) {
                if (SearchController.getInstance().getIsFirstPage()) {
                    searchResultAdapter = new SearchResultAdapter(getActivity(),
                            SearchController.getInstance().getProductResult());
                    mProducts.setAdapter(searchResultAdapter);
                } else {
                    searchResultAdapter.notifyDataSetChanged();

                    if (!SearchController.getInstance().getHasMore()) {
                        ToastView.showMessage(getActivity(),
                                getActivity().getResources().getString(R.string.no_more_pages));
                    }
                }
            }
        }
    }

    @Override
    public void loadMore(int page, int totalItemsCount) {
        if (SearchController.getInstance().getHasMore()) {
            SearchController
                    .getInstance()
                    .getMoreProduct(getActivity(), mSearchFilter);
        }
    }

    @OnClick(R.id.backbtn)
    public void onBack() {
        getActivity().finish();
    }

    @OnClick(R.id.index_search_edit)
    public void onSearch() {
        SearchFragment searchFragment = new SearchFragment();
        startActivityWithFragment(searchFragment);
    }

    @OnClick(R.id.back_to_top)
    public void onBackToTop() {
        mProducts.smoothScrollToPosition(0);
    }

    @OnClick({R.id.tv_gp1, R.id.tv_gp2, R.id.tv_gp3, R.id.tv_gp4})
    public void onSort(View v) {
        final int id = v.getId();
        if (mLastOrderBy != id) {
            ButterKnife.apply(tvGrips, new ButterKnife.Action<TextView>() {
                @Override
                public void apply(TextView view, int index) {
                    if (view.getId() == id) {
                        view.setTextColor(getResources().getColor(R.color.bg_Main));
                    } else {
                        view.setTextColor(getResources().getColor(R.color.search_order_text));
                    }
                }
            });
            mLastOrderBy = id;
            mIsAscending = false;
        }

        switch (v.getId()) {
            case R.id.tv_gp1:
                mSearchFilter.sort_by = "sales,id";
                mSearchFilter.sort_order = mIsAscending ? "asc,asc" : "desc,desc";
                mIsAscending = !mIsAscending;
                break;
            case R.id.tv_gp2:
                mSearchFilter.sort_by = "sales";
                mSearchFilter.sort_order = mIsAscending ? "asc" : "desc";
                mIsAscending = !mIsAscending;
                break;
            case R.id.tv_gp3:
                mSearchFilter.sort_by = "price";
                mSearchFilter.sort_order = mIsAscending ? "asc" : "desc";
                mIsAscending = !mIsAscending;
                break;
            case R.id.tv_gp4:
                mSearchFilter.sort_by = "id";
                mSearchFilter.sort_order = mIsAscending ? "asc" : "desc";
                mIsAscending = !mIsAscending;
                break;
        }

        SearchController.getInstance().getProduct(getActivity(), mSearchFilter);
    }

    @OnItemClick(R.id.gvProductList)
    public void onShowProductDetails(AdapterView<?> parent, View view, int position, long id) {
        ProductDetailsFragment productDetailsFragment = ProductDetailsFragment.newInstance(
                ((SIMPLEGOODS) parent.getItemAtPosition(position)).id);
        startActivityWithFragment(productDetailsFragment);
    }

    @OnClick(R.id.gotoCart)
    public void onGoToCart() {
        CartFragment fragment = CartFragment.newInstance(CartFragment.SHOW_IN_POPUP);
        startActivityWithFragment(fragment);
    }
}
