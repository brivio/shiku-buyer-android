
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "SESSION")
public class SESSION extends Model {

    @Column(name = "token")
    public String token;

    protected static SESSION instance;

    public static SESSION getInstance() {
        if (instance == null) {
            instance = new SESSION();
        }

        return instance;
    }

    public static void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        SESSION session = SESSION.getInstance();

        session.token = jsonObject.optString("token");
    }

    public static JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("token", getInstance().token);

        return jsonObject;
    }
}
