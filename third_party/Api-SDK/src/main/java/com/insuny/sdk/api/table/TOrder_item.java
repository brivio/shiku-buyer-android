package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TOrder_item{
    public String id;
    public String item_id;
    public String order_id;
    public String orderid;
    public String express_type;
    public String title;
    public String price;
    public String num;
    public String attr;
    public String is_refund;
    public String remark;
    public String mid;
    public String mname;
    public String add_time;
    public String status;
}