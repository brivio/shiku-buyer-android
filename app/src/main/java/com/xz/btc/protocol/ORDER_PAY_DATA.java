package com.xz.btc.protocol;

import com.xz.btc.AppConst;
import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "ORDER_PAY_DATA")
public class ORDER_PAY_DATA extends Model {
    public int payWith = 0;
    public WXPayResult wxPayResult;
    public AlipayResult aliPayResult;

    /**
     * Unused fields
     */
    @Column(name = "pay_wap")
    public String pay_wap;

    @Column(name = "pay_online")
    public String pay_online;

    @Column(name = "upop_tn")
    public String upop_tn;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        payWith = jsonObject.optInt("pays", 0);
        JSONObject jsonResult;
        switch (payWith) {
            case AppConst.PAY_TYPE_ALIPAY:
                jsonResult = jsonObject.optJSONObject("ali_pay_result");
                if (jsonResult != null) {
                    AlipayResult result = new AlipayResult();
                    result.fromJson(jsonResult);
                    aliPayResult = result;
                }
                break;
            case AppConst.PAY_TYPE_WXPAY:
                jsonResult = jsonObject.optJSONObject("wx_pay_result");
                if (jsonResult != null) {
                    WXPayResult result = new WXPayResult();
                    result.fromJson(jsonResult);
                    wxPayResult = result;
                }
                break;
        }
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }

    public static class WXPayResult {
        public String appId;
        public String noncestr;
        public String packageName;
        public String partnerId;
        public String prepayId;
        public String sign;
        public long timeStamp;

        public void fromJson(JSONObject jsonObject) {
            if (jsonObject != null) {
                appId = jsonObject.optString("appid");
                noncestr = jsonObject.optString("noncestr");
                packageName = jsonObject.optString("package");
                partnerId = jsonObject.optString("partnerid");
                prepayId = jsonObject.optString("prepayid");
                sign = jsonObject.optString("sign");
                timeStamp = jsonObject.optLong("timeStamp");
            }
        }
    }

    public static class AlipayResult {
        public String orderString;

        public void fromJson(JSONObject jsonObject) {
            if (jsonObject != null) {
                orderString = jsonObject.optString("orderString");
            }
        }
    }
}
