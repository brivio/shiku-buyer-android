package com.xz.tframework.service;

/**
 * Created by txj on 15/4/14.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.baidu.frontia.api.FrontiaPushMessageReceiver;
import com.xz.btc.AppConst;
import com.xz.btc.BTCManager;
import com.xz.btc.activity.BTCMainActivity;

import java.util.List;

public class PushMessageReceiver extends FrontiaPushMessageReceiver {

	private SharedPreferences shared;
	private SharedPreferences.Editor editor;

	@Override
	public void onBind(Context context, int errorCode, String appid, String userId, String channelId, String requestId) {
		shared = context.getSharedPreferences("userInfo", 0);
		editor = shared.edit();
		if (errorCode == 0) {
			editor.putString("UUID", userId);
			editor.commit();
            BTCManager.setBaiduUUID(context, userId, AppConst.AppId, AppConst.AppKey);
		}
	}

	@Override
	public void onUnbind(Context context, int i, String s) {

	}

	@Override
	public void onSetTags(Context context, int i, List<String> strings, List<String> strings2, String s) {

	}

	@Override
	public void onDelTags(Context context, int i, List<String> strings, List<String> strings2, String s) {

	}

	@Override
	public void onListTags(Context context, int i, List<String> strings, String s) {

	}

	@Override
	public void onMessage(Context context, String message, String customContentString) {
		String messageString = "透传消息 message=" + message + " customContentString=" + customContentString;
		//System.out.println("messageString:"+messageString);
	}

	@Override
	public void onNotificationClicked(Context context, String title, String description, String customContentString) {
		String notifyString = "通知点击 title=" + title + " description=" + description + " customContent=" + customContentString;
		//System.out.println("notifyString:"+notifyString);
		
		Intent responseIntent = null;
        responseIntent = new Intent(BTCMainActivity.ACTION_PUSHCLICK);
        responseIntent.setClass(context, BTCMainActivity.class);
        responseIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if(customContentString != null)
        {
            responseIntent.putExtra(BTCMainActivity.CUSTOM_CONTENT, customContentString);
        }
        context.startActivity(responseIntent);
	}
}
