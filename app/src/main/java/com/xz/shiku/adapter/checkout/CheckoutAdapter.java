package com.xz.shiku.adapter.checkout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xz.btc.protocol.CartItem;
import com.xz.shiku.R;
import com.xz.shiku.controller.ShoppingCartController;
import com.xz.tframework.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CheckoutAdapter extends BaseAdapter {
    private static final int TYPE_MAX_COUNT = 3;
    private List<ShoppingCartController.ShoppingCartListViewItem> mData = new ArrayList<>();

    private LayoutInflater inflater;
    private Context context;
    private CartItem current_header = new CartItem();
    private double totalPrice = 0;

    private ArrayList<Integer> cartIds = new ArrayList<>();

    public CheckoutAdapter(Context context, List<ShoppingCartController.ShoppingCartListViewItem> list) {
        this.context = context;
        this.mData = list;
        inflater = LayoutInflater.from(context);
        for (ShoppingCartController.ShoppingCartListViewItem item : list) {
            if (item.mItemType == ShoppingCartController.ItemType.HEADER) {
                totalPrice += item.mCartItem.subtotal;
            } else if (item.mItemType == ShoppingCartController.ItemType.ITEM) {
                cartIds.add(item.mProduct.id);
            }
        }
    }

    public ArrayList<Integer> getCartIds() {
        return cartIds;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final ShoppingCartController.ShoppingCartListViewItem cartItem = mData.get(position);
        switch (cartItem.mItemType) {
            case ITEM:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = inflater.inflate(R.layout.item_checkout, null);
                    holder.image = (ImageView) convertView.findViewById(R.id.shop_car_item_image);
                    holder.text = (TextView) convertView.findViewById(R.id.shop_car_item_text);
                    holder.property = (TextView) convertView.findViewById(R.id.shop_car_item_property);
                    holder.price = (TextView) convertView.findViewById(R.id.shop_car_item_price);
                    holder.amount = (TextView) convertView.findViewById(R.id.shop_car_item_amount);
                    convertView.setTag(R.id.tag_first, holder);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                }

                if (cartItem.mProduct.image != null) {
                    Utils.getImage(context, holder.image, cartItem.mProduct.image);
                }
                holder.text.setText(cartItem.mProduct.name);
                holder.property.setText("特级");
                holder.price.setText(String.format("￥%.2f", cartItem.mProduct.price));
                holder.amount.setText(String.format("x%d", cartItem.mProduct.quantity));
                break;
            case HEADER:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = inflater.inflate(R.layout.item_checkout_header, null);
                    holder.text = (TextView) convertView.findViewById(R.id.shop_car_item_text);
                    convertView.setTag(R.id.tag_first, holder);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                }
                holder.text.setText(cartItem.mCartItem.shop_name);
                current_header = cartItem.mCartItem;
                break;
            case FOOTER:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = inflater.inflate(R.layout.item_checkout_footer, null);
                    holder.carriage = (TextView) convertView.findViewById(R.id.tv_total_yf);
                    holder.price = (TextView) convertView.findViewById(R.id.tv_real_pay);
                    holder.amount = (TextView) convertView.findViewById(R.id.tv_total_count);
                    convertView.setTag(R.id.tag_first, holder);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                }
                holder.carriage.setText(String.format("￥%.2f", 0.0));
                holder.price.setText(String.format("￥%.2f", current_header.subtotal));
                holder.amount.setText(String.format("共%d件商品", current_header.products.size()));
                break;
        }

        return convertView;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    class ViewHolder {
        private ImageView image;
        private TextView text;
        private TextView property;
        private TextView price;
        private TextView amount;
        private TextView carriage;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

}
