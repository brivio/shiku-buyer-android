package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import com.insuny.sdk.api.table.TItem_cate;
import com.insuny.sdk.api.table.TItem_img;
import com.insuny.sdk.api.table.TMember_shop;
import java.util.ArrayList;

public class TItem{
    public String id;
    public String mid;
    public String mname;
    public String cate_id;
    public String title;
    public String sn;
    public String price;
    public String rprice;
    public String net_content;
    public String days;
    public String auth;
    public String auth_img;
    public String license;
    public String brand;
    public String storage;
    public String new_year;
    public String new_month;
    public String new_days;
    public String qual_safe;
    public String qual_char;
    public String qual_auth;
    public String qual_comp;
    public String qual_orga;
    public String qual_attr;
    public String qual_status;
    public String weight;
    public String stock;
    public String areas;
    public String years;
    public String sales;
    public String express;
    public String img;
    public String url;
    public String tags;
    public String info;
    public String app_info;
    public String app_status;
    public String add_time;
    public String update_time;
    public String is_hots;
    public String is_new;
    public String ordid;
    public String favs;
    public String comments;
    public String hits;
    public String rates;
    public String status;
    public String seo_titile;
    public String seo_keys;
    public String seo_desc;
    public TItem_cate item_cate;
    public ArrayList<TItem_img> item_img=new ArrayList<>();
    public TMember_shop member_shop;
    public String is_like;
    public String storage_label;
    public String days_label;
}