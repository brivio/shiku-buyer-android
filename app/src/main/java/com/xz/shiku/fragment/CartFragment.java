package com.xz.shiku.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.CartItemProduct;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.external.view.XListView;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.adapter.CartAdapter;
import com.xz.shiku.controller.ShoppingCartController;
import com.xz.shiku.controller.UserController;
import com.xz.shiku.fragment.checkout.CheckoutFragment;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class CartFragment extends BaseShikuFragment implements CartAdapter.OnCartActionListener {
    //region 控件声明
    @InjectView(R.id.backbtn)
    TextView mBack;
    @InjectView(R.id.top_menu_text_title)
    TextView mTitle;
    @InjectView(R.id.totalprice)
    TextView mTotal;
    @InjectView(R.id.toprightbtn)
    TextView mTopRight;
    @InjectView(R.id.ll_empty)
    LinearLayout mEmptyCartLayout;
    @InjectView(R.id.shop_car_isnot)
    FrameLayout mShoppingCartLayout;
    @InjectView(R.id.shop_car_list)
    XListView mShoppingCart;
    @InjectView(R.id.cb_choice)
    CheckBox mCheckAll;
    //endregion

    public static final String SHOW_IN_MAIN = "in_main_activity";
    public static final String SHOW_IN_POPUP = "in_popup_activity";

    private static final String ARG_SHOW_IN = "arg_show_in";
    private String mShowIn;
    private CartAdapter mShoppingCartAdapter;
    private boolean scrollFlag;

    public static CartFragment newInstance(String showIn) {
        if (showIn.equals(SHOW_IN_POPUP)) {
            PopActivity.gShowNavigationBar = false;
        }

        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SHOW_IN, showIn);
        fragment.setArguments(args);
        return fragment;
    }

    public CartFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mShowIn = getArguments().getString(ARG_SHOW_IN);
        } else {
            mShowIn = SHOW_IN_MAIN;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_cart, null);

        ButterKnife.inject(this, view);

        if (mShowIn.equals(SHOW_IN_MAIN)) {
            ButterKnife.findById(view, R.id.product_detail_top_layout).setBackgroundResource(R.color.bg_Main);
            ((TextView) ButterKnife.findById(view, R.id.top_menu_text_title)).setTextColor(Color.WHITE);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (UserController.getInstance().isUserReady()) {
            ShoppingCartController.getInstance().addResponseListener(this);
            ShoppingCartController.getInstance().getItems(getActivity());
        } else {
            initUI(false);
        }
    }

    private void initUI(boolean showCartLayout) {
        mTitle.setText(R.string.shopcar_shopcar);
        mTopRight.setText("");

        if (mShowIn.equals(SHOW_IN_MAIN)) {
            mBack.setText("");
        }

        if (showCartLayout) {
            mEmptyCartLayout.setVisibility(View.GONE);
            mShoppingCartLayout.setVisibility(View.VISIBLE);

            mShoppingCart.setPullRefreshEnable(false);
            mShoppingCart.setPullLoadEnable(false);
            mShoppingCart.setXListViewListener(this);
            mShoppingCart.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    switch (scrollState) {
                        // 当不滚动时
                        case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:// 是当屏幕停止滚动时
                            scrollFlag = false;
                            break;
                        case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:// 滚动时
                            scrollFlag = true;
                            break;
                        case AbsListView.OnScrollListener.SCROLL_STATE_FLING:// 是当用户由于之前划动屏幕并抬起手指，屏幕产生惯性滑动时
                            scrollFlag = true;
                            break;
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                }
            });
        } else {
            mEmptyCartLayout.setVisibility(View.VISIBLE);
            mShoppingCartLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        ShoppingCartController.getInstance().removeResponseListener(this);
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.CART_LIST)) {
            if (ShoppingCartController.getInstance().getLastResponseStatus().getIsSuccess()) {
                List<ShoppingCartController.ShoppingCartListViewItem> items =
                        ShoppingCartController.getInstance().getShoppingCart();
                if (items != null && items.size() > 0) {
                    initUI(true);

                    mShoppingCartAdapter = new CartAdapter(getActivity(), items);
                    mShoppingCartAdapter.setOnCartActionListener(this);
                    mShoppingCart.setAdapter(mShoppingCartAdapter);
                    updateShoppingCartCount();
                } else {
                    initUI(false);
                }
            }
        } else if (url.endsWith(ApiInterface.CART_REMOVE)) {
            if (ShoppingCartController.getInstance().getLastResponseStatus().getIsSuccess()) {
                if (mShoppingCartAdapter != null) {
                    int position = (int) mShoppingCart.getTag(mShoppingCart.getId());
                    mShoppingCartAdapter.remove(position);
                    updateShoppingCartCount();
                }
            }
        } else if (url.endsWith(ApiInterface.CART_UPDATE)) {
            if (ShoppingCartController.getInstance().getLastResponseStatus().getIsSuccess()) {
                if (mShoppingCartAdapter != null) {
                    int position = (int) mShoppingCart.getTag(mShoppingCart.getId());
                    mShoppingCartAdapter.update(position);
                    updateShoppingCartCount();
                }
            }
        }
    }

    private void updateShoppingCartCount() {
        int count = ShoppingCartController.getInstance().getProductCount();
        if (count > 0) {
            mTitle.setText(String.format("%s（%d）", getString(R.string.shopcar_shopcar), count));
        } else {
            mTitle.setText(R.string.shopcar_shopcar);
        }

        if (mShowIn.equals(SHOW_IN_MAIN)) {
            LinearLayout shoppingCartBadge = (LinearLayout) getActivity().findViewById(
                    R.id.shopping_cart_badge);
            if (count > 0) {
                shoppingCartBadge.setVisibility(View.VISIBLE);
                ((TextView) shoppingCartBadge.getChildAt(0)).setText(
                        String.format("%d", count));
            } else {
                shoppingCartBadge.setVisibility(View.GONE);
            }
        }
    }

    @OnClick(R.id.cb_choice)
    public void onCheckAll(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        if (mShoppingCartAdapter != null) {
            mShoppingCartAdapter.checkAll(checked);
        }
    }

    @OnClick(R.id.backbtn)
    public void onBack() {
        if (mShowIn.equals(SHOW_IN_POPUP)) {
            getActivity().finish();
        }
    }

    @OnClick(R.id.btn_checkout)
    public void onCheckout() {
        if (mShoppingCartAdapter.isCheck()) {
            CheckoutFragment fragment = CheckoutFragment.newInstance();
            fragment.setCartItems(mShoppingCartAdapter.getCheckItems());
            startActivityWithFragment(fragment);
        } else {
            toast("没有选中的商品");
        }
    }

    /**
     * CartAdapter回调方法
     */
    @Override
    public void onCheckedChanged(double subtotal, boolean isAllChecked) {
        mCheckAll.setChecked(isAllChecked);
        mTotal.setText(String.format("￥%.2f", subtotal));
    }

    @Override
    public void onRemoveCartItem(int position, CartItemProduct product) {
        mShoppingCart.setTag(mShoppingCart.getId(), position);
        ShoppingCartController.getInstance().remove(getActivity(), product.id);
    }

    @Override
    public void onUpdateCartItem(int position, CartItemProduct product, int newQuantity) {
        mShoppingCart.setTag(mShoppingCart.getId(), position);
        ShoppingCartController.getInstance().update(getActivity(), product.item_id, newQuantity);
    }
}