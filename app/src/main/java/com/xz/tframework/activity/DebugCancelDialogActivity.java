package com.xz.tframework.activity;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.xz.shiku.R;

public class DebugCancelDialogActivity extends Activity {

    private TextView title;
    private TextView message;
    private TextView positive;
    private TextView negative;

    public static Handler parentHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.tf_dialog_layout);

        title = (TextView) findViewById(R.id.dialog_title);
        message = (TextView) findViewById(R.id.dialog_message);
        title.setVisibility(View.GONE);
        Resources resource = (Resources) getBaseContext().getResources();
        String mes=resource.getString(R.string.exit_debug_mode);
        message.setText(mes);

        positive = (TextView) findViewById(R.id.yes);
        negative = (TextView) findViewById(R.id.no);

        positive.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Message msg = new Message();
                msg.what = 1;
                parentHandler.handleMessage(msg);
                finish();
            }
        });

        negative.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

}
