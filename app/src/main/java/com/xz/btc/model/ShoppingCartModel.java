package com.xz.btc.model;

import android.content.Context;
import android.view.Gravity;

import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.external.androidquery.util.AQUtility;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.model.BeeCallback;
import com.xz.tframework.view.MyProgressDialog;
import com.xz.tframework.view.ToastView;
import com.xz.shiku.R;
import com.xz.btc.protocol.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ShoppingCartModel extends BaseModel {

    public ArrayList<GOODS_GROUPED_LIST> goods_grouped_lists = new ArrayList<GOODS_GROUPED_LIST>();
    public TOTAL total;
    public int goods_num;

    // 结算（提交订单前的订单预览）
    public ADDRESS address;
    public TreeMap<Integer, GOODS_LIST> select_googds_list = new TreeMap<Integer, GOODS_LIST>();
    private ArrayList<GOODS_GROUPED_LIST> balance_goods_list = new ArrayList<GOODS_GROUPED_LIST>();
    public ArrayList<PAYMENT> payment_list = new ArrayList<PAYMENT>();
    public ArrayList<SHIPPING> shipping_list = new ArrayList<SHIPPING>();

    public String orderInfoJsonString;

    private static ShoppingCartModel instance;
    public int order_id;

    public TOTAL getTotal() {

        TOTAL total1 = new TOTAL();
        int subtoalcount = 0, totalcount = 0;
        double totalprice = 0, market_price = 0, saving = 0, subtotalprice = 0;
        GOODS_LIST tempHeader;
        for (GOODS_LIST gl : select_googds_list.values()) {
            if (gl.isHeaderItem) {
                subtoalcount = 0;
                subtotalprice = 0;
                tempHeader = gl;
            }
            if (!gl.isFooterItem && !gl.isHeaderItem) {
                totalcount++;
                subtoalcount++;
                subtotalprice += gl.goods_number * gl.goods_price;
                totalprice += gl.goods_number * gl.goods_price;
                saving = market_price - totalcount;
            }
            if (gl.isFooterItem) {
                gl.goods_number = subtoalcount;
                gl.goods_price = subtotalprice;
            }
        }
        total1.goods_amount = totalcount + "";
        total1.goods_price = totalprice;
        return total1;
    }

    public ShoppingCartModel(Context context) {
        super(context);
        instance = this;
    }

    // 获取购物车列表
    public void cartList() {
        cartlistRequest request = new cartlistRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                ShoppingCartModel.this.callback(url, jo, status);
                try {
                    cartlistResponse response = new cartlistResponse();
                    response.fromJson(jo);
                    if (null != jo) {
                        if (response.status.succeed == 1) {
                            CART_LIST_DATA data = response.data;

                            total = data.total;
                            ArrayList<GOODS_GROUPED_LIST> goods_lists = data.goods_grouped_lists;

                            goods_grouped_lists.clear();
                            ShoppingCartModel.this.goods_num = 0;
                            if (null != goods_lists && goods_lists.size() > 0) {
                                goods_grouped_lists.clear();
                                for (int i = 0; i < goods_lists.size(); i++) {
                                    GOODS_GROUPED_LIST goods_list_Item = goods_lists.get(i);
                                    goods_grouped_lists.add(goods_list_Item);
                                    ShoppingCartModel.this.goods_num += Integer.valueOf(goods_list_Item.total.goods_amount);
                                }
                            }
                            ShoppingCartModel.this.OnMessageResponse(url, jo, status);
                        }
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        };

        request.session = SESSION.getInstance();

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("json", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        cb.url(ApiInterface.CART_LIST).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext, mContext.getResources().getString(R.string.hold_on));
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }

    // 在首页获取购物车列表，存成单件
    public void homeCartList() {
        cartlistRequest request = new cartlistRequest();

        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                try {
                    cartlistResponse response = new cartlistResponse();
                    response.fromJson(jo);
                    if (null != jo) {
                        if (response.status.succeed == 1) {
                            CART_LIST_DATA data = response.data;

                            total = data.total;
                            ArrayList<GOODS_GROUPED_LIST> goods_lists = data.goods_grouped_lists;

                            goods_grouped_lists.clear();
                            ShoppingCartModel.this.goods_num = 0;
                            if (null != goods_lists && goods_lists.size() > 0) {
                                goods_grouped_lists.clear();
                                for (int i = 0; i < goods_lists.size(); i++) {
                                    GOODS_GROUPED_LIST goods_list_Item = goods_lists.get(i);
                                    goods_grouped_lists.add(goods_list_Item);
                                    ShoppingCartModel.this.goods_num += Integer.valueOf(goods_list_Item.total.goods_amount);
                                }
                            }
                            ShoppingCartModel.this.OnMessageResponse(url, jo, status);
                        }
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        };

        request.session = SESSION.getInstance();

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("json", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cb.url(ApiInterface.CART_LIST).type(JSONObject.class).params(params);
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }


    public void deleteGoods(int rec_id) {
        cartdeleteRequest request = new cartdeleteRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                ShoppingCartModel.this.callback(url, jo, status);
                try {
                    cartdeleteResponse response = new cartdeleteResponse();
                    response.fromJson(jo);
                    if (jo != null) {
                        if (response.status.succeed == 1) {

                            ShoppingCartModel.this.OnMessageResponse(url, jo, status);

                        }
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        };
        request.session = SESSION.getInstance();
        request.rec_id = rec_id;
        Map<String, String> params = new HashMap<String, String>();

        params.put("json", request.toJson().toString());

        cb.url(ApiInterface.CART_REMOVE).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext, mContext.getResources().getString(R.string.hold_on));
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }

    public void updateGoods(int rec_id, int new_number) {
        cartupdateRequest request = new cartupdateRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                ShoppingCartModel.this.callback(url, jo, status);
                try {
                    cartdeleteResponse response = new cartdeleteResponse();
                    response.fromJson(jo);
                    if (jo != null) {
                        if (response.status.succeed == 1) {

                        }
                        ShoppingCartModel.this.OnMessageResponse(url, jo, status);
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        };

        request.session = SESSION.getInstance();
        request.rec_id = rec_id;
        request.new_number = new_number;
        Map<String, String> params = new HashMap<String, String>();

        params.put("json", request.toJson().toString());

        cb.url(ApiInterface.CART_UPDATE).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext, mContext.getResources().getString(R.string.hold_on));
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }

    public void checkOrder() {
        flowcheckOrderRequest request = new flowcheckOrderRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                ShoppingCartModel.this.callback(url, jo, status);
                try {
                    flowcheckOrderResponse response = new flowcheckOrderResponse();
                    response.fromJson(jo);
                    if (jo != null) {

                        if (response.status.succeed == 1) {

                            CHECK_ORDER_DATA check_order_data = response.data;
                            address = check_order_data.consignee;
                            ArrayList<GOODS_LIST> goods = check_order_data.goods_list;

                            if (null != goods && goods.size() > 0) {

                                balance_goods_list.clear();
//                                balance_goods_list.addAll(goods);

                            }

                            orderInfoJsonString = jo.toString();
                            ArrayList<SHIPPING> shipping_lists = check_order_data.shipping_list;
                            if (null != shipping_lists && shipping_lists.size() > 0) {
                                shipping_list.clear();
                                shipping_list.addAll(shipping_lists);

                            }

                            ArrayList<PAYMENT> payments = check_order_data.payment_list;

                            if (null != payments && payments.size() > 0) {
                                payment_list.clear();
                                ;
                                payment_list.addAll(payments);

                            }

                        }

                        ShoppingCartModel.this.OnMessageResponse(url, jo, status);
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        };

        request.session = SESSION.getInstance();
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("json", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cb.url(ApiInterface.FLOW_CHECKORDER).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext, mContext.getResources().getString(R.string.hold_on));
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }

    // 订单生成
    public void flowDone(String pay_id, String shipping_id, String bonus, String score, String inv_type, String inv_payee, String inv_content) {
        flowdoneRequest request = new flowdoneRequest();

        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                ShoppingCartModel.this.callback(url, jo, status);
                try {
                    flowdoneResponse response = new flowdoneResponse();
                    response.fromJson(jo);
                    if (jo != null) {
                        if (response.status.succeed == 1) {
                            FLOW_DONE_DATA data = response.data;
                            order_id = data.order_id;
                            ShoppingCartModel.this.OnMessageResponse(url, jo, status);
                        }
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        };

        request.session = SESSION.getInstance();
        request.pay_id = pay_id;
        request.shipping_id = shipping_id;
        request.bonus = bonus;
        request.integral = score;
        if (!inv_content.equals("-1")) {
            request.inv_content = inv_content;
        }
        if (!inv_type.equals("-1")) {
            request.inv_type = inv_type;
        }
        request.inv_payee = inv_payee;
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("json", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cb.url(ApiInterface.FLOW_DONE).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext, mContext.getResources().getString(R.string.hold_on));
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }

    public void score(String score) {
        validateintegralRequest request = new validateintegralRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                ShoppingCartModel.this.callback(url, jo, status);
                try {
                    validateintegralResponse response = new validateintegralResponse();
                    response.fromJson(jo);
                    if (jo != null) {


                        if (response.status.succeed == 1) {
                            VALIDATE_INTEGRAL_DATA data = response.data;
                            String bonus = data.bouns;
                            String bonus_formated = data.bonus_formated;
                            ShoppingCartModel.this.OnMessageResponse(url, jo, status);
                        }
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        };

        request.session = SESSION.getInstance();
        request.integral = score;

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("json", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cb.url(ApiInterface.VALIDATE_INTEGRAL).type(JSONObject.class).params(params);
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }

    // 验证红包
    public void bonus(String bonus_sn) {
        validatebonusRequest request = new validatebonusRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                //ShoppingCartModel.this.callback(url, jo, status);
                try {
                    validatebonusResponse response = new validatebonusResponse();
                    response.fromJson(jo);
                    if (jo != null) {

                        if (response.status.succeed == 1) {
                            VALIDATE_BONUS_DATA data = response.data;
                            String bonus = data.bouns;
                            String bonus_formated = data.bonus_formated;
                            ShoppingCartModel.this.OnMessageResponse(url, jo, status);

                        }

                        if (response.status.error_code == 101) {
                            //Toast toast = Toast.makeText(mContext, "红包输入错误", 0);
                            ToastView toast = new ToastView(mContext, "红包输入错误");
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        };

        request.session = SESSION.getInstance();
        request.bonus_sn = bonus_sn;

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("json", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cb.url(ApiInterface.VALIDATE_BONUS).type(JSONObject.class).params(params);
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }

}
