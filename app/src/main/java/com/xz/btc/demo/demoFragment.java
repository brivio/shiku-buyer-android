package com.xz.btc.demo;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.xz.shiku.R;
import com.xz.tframework.fragment.BaseFragment;
import com.xz.tframework.utils.Utils;
import com.xz.tframework.view.abview.AbSlidingPlayView;
import com.xz.tframework.view.iosdialog.wheel.wheelcity.OnWheelChangedListener;
import com.xz.tframework.view.iosdialog.wheel.wheelcity.WheelView;
import com.xz.tframework.view.iosdialog.wheel.wheelcity.adapters.AbstractWheelTextAdapter;
import com.xz.tframework.view.iosdialog.wheel.wheelcity.adapters.AddressData;
import com.xz.tframework.view.iosdialog.wheel.wheelcity.adapters.ArrayWheelAdapter;
import com.xz.tframework.view.iosdialog.wheel.wheelview.JudgeDate;
import com.xz.tframework.view.iosdialog.wheel.wheelview.ScreenInfo;
import com.xz.tframework.view.iosdialog.wheel.wheelview.WheelMain;
import com.xz.tframework.view.iosdialog.widget.ActionSheetDialog;
import com.xz.tframework.view.iosdialog.widget.MyAlertDialog;
import com.xz.tframework.view.scaleview.HackyViewPager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link demoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link demoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class demoFragment extends BaseFragment implements View.OnClickListener{

    /*日期地址弹出菜单************************************/
    private String textprovence,textcity,textdistrict;
    WheelMain wheelMain;
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    /*END日期地址弹出菜单************************************/

    /*商品详情图片切换************************************/
    private ArrayList<View> allListView;
    private HackyViewPager viewPager;
    private ArrayList<View> pointView;
    LinearLayout linearLayoutcirclept;
    int position;
    ArrayList<String> images;
    /*END商品详情图片切换************************************/

    /**
     * 首页轮播的界面的资源
     */
    private AbSlidingPlayView abSlidingPlayView;
    private ArrayList<View> allListAbSlidingPlayView;


    private ViewFlipper viewFlipper;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment demo.
     */
    // TODO: Rename and change types and number of parameters
    public static demoFragment newInstance(String param1, String param2) {
        demoFragment fragment = new demoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public demoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_demo, container, false);
        /*日期地址弹出菜单************************************/
        ((TextView)v.findViewById(R.id.rqxz)).setOnClickListener(this);
        ((TextView)v.findViewById(R.id.dzxz)).setOnClickListener(this);
        ((TextView)v.findViewById(R.id.tpxz)).setOnClickListener(this);
        ((TextView)v.findViewById(R.id.lb)).setOnClickListener(this);
        /*END日期地址弹出菜单************************************/

         /*商品详情图片切换************************************/
        linearLayoutcirclept=(LinearLayout)v.findViewById(R.id.cycelPointContainer);
        viewPager = (HackyViewPager)v.findViewById(R.id.iv_baby);
        images=new ArrayList<String>();
        images.add("http://www.linuxidc.com/upload/2011_05/11050305525473.png");
        images.add("http://www.linuxidc.com/upload/2011_05/11050305525473.png");
        images.add("http://www.linuxidc.com/upload/2011_05/11050305525473.png");
        initViewPager();
         /*END商品详情图片切换************************************/

        abSlidingPlayView = (AbSlidingPlayView) v.findViewById(R.id.viewPager_menu);
        //设置播放方式为顺序播放
        abSlidingPlayView.setPlayType(1);
        //设置播放间隔时间
        abSlidingPlayView.setSleepTime(3000);
        initAbSlidingPlayView();


        viewFlipper = (ViewFlipper)v.findViewById(R.id.flipper_scrollTitle);
        initNoticeView();
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            //日期选择
            case R.id.rqxz:
//                LayoutInflater inflater = LayoutInflater.from(getActivity());
//                final View timepickerview = inflater.inflate(R.layout.tf_iosdialog_timepicker,
//                        null);
//                ScreenInfo screenInfo = new ScreenInfo(getActivity());
//                wheelMain = new WheelMain(timepickerview);
//                wheelMain.screenheight = screenInfo.getHeight();
//                String time = "2015-09-09";
//                Calendar calendar = Calendar.getInstance();
//                if (JudgeDate.isDate(time, "yyyy-MM-dd")) {
//                    try {
//                        calendar.setTime(dateFormat.parse(time));
//                    } catch (ParseException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//                }
//                int year = calendar.get(Calendar.YEAR);
//                int month = calendar.get(Calendar.MONTH);
//                int day = calendar.get(Calendar.DAY_OF_MONTH);
//                wheelMain.initDateTimePicker(year, month, day);
//                new AlertDialog.Builder(getActivity())
//                        .setTitle("选择时间")
//                        .setView(timepickerview)
//                        .setPositiveButton("确定",
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog,
//                                                        int which) {
//                                        //txttime.setText(wheelMain.getTime());
//                                    }
//                                })
//                        .setNegativeButton("取消",
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog,
//                                                        int which) {
//                                    }
//                                }).show();
                LayoutInflater inflater1 = LayoutInflater.from(getActivity());
                final View timepickerview1 = inflater1.inflate(R.layout.tf_iosdialog_timepicker,
                        null);
                ScreenInfo screenInfo1 = new ScreenInfo(getActivity());
                wheelMain = new WheelMain(timepickerview1);
                wheelMain.screenheight = screenInfo1.getHeight();
                String time1 = "2010-01-01";
                Calendar calendar1 = Calendar.getInstance();
                if (JudgeDate.isDate(time1, "yyyy-MM-dd")) {
                    try {
                        calendar1.setTime(dateFormat.parse(time1));
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                int year1 = calendar1.get(Calendar.YEAR);
                int month1 = calendar1.get(Calendar.MONTH);
                int day1 = calendar1.get(Calendar.DAY_OF_MONTH);
                wheelMain.initDateTimePicker(year1, month1, day1);
                final MyAlertDialog dialog = new MyAlertDialog(getActivity())
                        .builder()
                        .setTitle("选自时间")
                                // .setMsg("再连续登陆15天，就可变身为QQ达人。退出QQ可能会使你现有记录归零，确定退出？")
                                // .setEditText("1111111111111")
                        .setView(timepickerview1)
                        .setNegativeButton("取消", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                dialog.setPositiveButton("保存", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getActivity().getApplicationContext(),
                                wheelMain.getTime(), Toast.LENGTH_LONG).show();
                    }
                });
                dialog.show();
                break;
            case R.id.dzxz:
                View vview = dialogm();
                final MyAlertDialog dialog1 = new MyAlertDialog(getActivity())
                        .builder()
                        .setTitle("选择地区")
                                // .setMsg("再连续登陆15天，就可变身为QQ达人。退出QQ可能会使你现有记录归零，确定退出？")
                                // .setEditText("1111111111111")
                        .setView(vview)
                        .setNegativeButton("取消", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                dialog1.setPositiveButton("确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //tv_area.setText(textprovence+textcity+textdistrict);
                        //Toast.makeText(getActivity().getApplicationContext(), textprovence+textcity+textdistrict, Toast.LENGTH_LONG).show();
                    }
                });
                dialog1.show();
                break;
            case R.id.tpxz:
                new ActionSheetDialog(getActivity())
                        .builder()
                        .setCancelable(true)
                        .setCanceledOnTouchOutside(true)
                        .addSheetItem("用相机更换头像", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {

                                    }
                                })
                        .addSheetItem("去相册选择头像", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {

                                    }
                                }).show();
                break;
            case R.id.lb:
                new ActionSheetDialog(getActivity())
                        .builder()
                        .setTitle("请选择操作")
                        .setCancelable(false)
                        .setCanceledOnTouchOutside(false)
                        .addSheetItem("条目一", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(getActivity(),
                                                "item" + which, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                })
                        .addSheetItem("条目二", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(getActivity(),
                                                "item" + which, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                })
                        .addSheetItem("条目三", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(getActivity(),
                                                "item" + which, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                })
                        .addSheetItem("条目四", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(getActivity(),
                                                "item" + which, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                })
                        .addSheetItem("条目五", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(getActivity(),
                                                "item" + which, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                })
                        .addSheetItem("条目六", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(getActivity(),
                                                "item" + which, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                })
                        .addSheetItem("条目七", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(getActivity(),
                                                "item" + which, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                })
                        .addSheetItem("条目八", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(getActivity(),
                                                "item" + which, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                })
                        .addSheetItem("条目九", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(getActivity(),
                                                "item" + which, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                })
                        .addSheetItem("条目十", ActionSheetDialog.SheetItemColor.Blue,
                                new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(getActivity(),
                                                "item" + which, Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                }).show();
                break;
        }
    }

    /*日期地址弹出菜单************************************/
    private View dialogm() {
        View contentView = LayoutInflater.from(getActivity()).inflate(
                R.layout.wheelcity_cities_layout, null);
        final WheelView country = (WheelView) contentView
                .findViewById(R.id.wheelcity_country);
        country.setVisibleItems(3);
        CountryAdapter adapter=new CountryAdapter(getActivity());
        adapter.setTextColor(getResources().getColor(R.color.actionsheet_gray));
        country.setViewAdapter(adapter);

        final String cities[][] = AddressData.CITIES;
        final String ccities[][][] = AddressData.COUNTIES;
        final WheelView city = (WheelView) contentView
                .findViewById(R.id.wheelcity_city);
        city.setVisibleItems(0);

        // 地区选择
        final WheelView ccity = (WheelView) contentView
                .findViewById(R.id.wheelcity_ccity);
        ccity.setVisibleItems(0);// 不限城市

        country.addChangingListener(new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                updateCities(city, cities, newValue);

                textprovence =AddressData.PROVINCES[country.getCurrentItem()];
                textcity=AddressData.CITIES[country.getCurrentItem()][city.getCurrentItem()];
                textdistrict=AddressData.COUNTIES[country.getCurrentItem()][city.getCurrentItem()][ccity.getCurrentItem()];
//                cityTxt = AddressData.PROVINCES[country.getCurrentItem()]
//                        + " | "
//                        + AddressData.CITIES[country.getCurrentItem()][city
//                        .getCurrentItem()]
//                        + " | "
//                        + AddressData.COUNTIES[country.getCurrentItem()][city
//                        .getCurrentItem()][ccity.getCurrentItem()];
            }
        });

        city.addChangingListener(new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                updatecCities(ccity, ccities, country.getCurrentItem(),
                        newValue);

                textprovence =AddressData.PROVINCES[country.getCurrentItem()];
                textcity=AddressData.CITIES[country.getCurrentItem()][city.getCurrentItem()];
                textdistrict=AddressData.COUNTIES[country.getCurrentItem()][city.getCurrentItem()][ccity.getCurrentItem()];
//                cityTxt = AddressData.PROVINCES[country.getCurrentItem()]
//                        + " | "
//                        + AddressData.CITIES[country.getCurrentItem()][city
//                        .getCurrentItem()]
//                        + " | "
//                        + AddressData.COUNTIES[country.getCurrentItem()][city
//                        .getCurrentItem()][ccity.getCurrentItem()];
            }
        });

        ccity.addChangingListener(new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {

                textprovence = AddressData.PROVINCES[country.getCurrentItem()];
                textcity=AddressData.CITIES[country.getCurrentItem()][city.getCurrentItem()];
                textdistrict=AddressData.COUNTIES[country.getCurrentItem()][city.getCurrentItem()][ccity.getCurrentItem()];
//                cityTxt = AddressData.PROVINCES[country.getCurrentItem()]
//                        + " | "
//                        + AddressData.CITIES[country.getCurrentItem()][city
//                        .getCurrentItem()]
//                        + " | "
//                        + AddressData.COUNTIES[country.getCurrentItem()][city
//                        .getCurrentItem()][ccity.getCurrentItem()];
            }
        });

        country.setCurrentItem(11);// 设置北京
        city.setCurrentItem(1);
        ccity.setCurrentItem(1);
        return contentView;
    }
    /**
     * Updates the city wheel
     */
    private void updateCities(WheelView city, String cities[][], int index) {
        ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<String>(getActivity(),
                cities[index]);

        adapter.setTextSize(18);
        adapter.setTextColor(getResources().getColor(R.color.actionsheet_gray));
        city.setViewAdapter(adapter);
        city.setCurrentItem(0);
    }

    /**
     * Updates the ccity wheel
     */
    private void updatecCities(WheelView city, String ccities[][][], int index,
                               int index2) {
        ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<String>(getActivity(),
                ccities[index][index2]);
        adapter.setTextSize(18);
        adapter.setTextColor(getResources().getColor(R.color.actionsheet_gray));
        city.setViewAdapter(adapter);
        city.setCurrentItem(0);
    }

    /**
     * Adapter for countries
     */
    private class CountryAdapter extends AbstractWheelTextAdapter {
        // Countries names
        private String countries[] = AddressData.PROVINCES;

        /**
         * Constructor
         */
        protected CountryAdapter(Context context) {
            super(context, R.layout.wheelcity_country_layout, NO_RESOURCE);
            setItemTextResource(R.id.wheelcity_country_name);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);
            return view;
        }

        @Override
        public int getItemsCount() {
            return countries.length;
        }

        @Override
        protected CharSequence getItemText(int index) {
            return countries[index];
        }

    }

    /*END日期地址弹出菜单************************************/



    /*商品详情图片切换************************************/
    private void initViewPager() {

        if (allListView != null) {
            allListView.clear();
            allListView = null;
        }
        allListView = new ArrayList<View>();
        pointView = new ArrayList<View>();
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < images.size(); i++) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.tf_item_pic, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.pic_item);
            Utils.getImage(getActivity(), imageView, images.get(i));
//            imageView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View arg0) {
//                    //挑战到查看大图界面
//                    Intent intent = new Intent(ProductDetailsActivity.this, ShowBigPictrue.class);
//                    intent.putExtra("position", position);
//                    startActivity(intent);
//                }
//            });
            allListView.add(view);

            View pointview = new View(getActivity());

            if (i == 0) {
                pointview.setBackground(getResources().getDrawable(R.drawable.tf_layoutwithstorkemainroundfill));
            } else {
                pointview.setBackground(getResources().getDrawable(R.drawable.tf_layoutwithstorkeroundwhitefill));
            }
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(20, 20);
            lp.setMargins(0, 0, 10, 0);
            pointview.setLayoutParams(lp);
            pointview.setTag(i);
            pointView.add(pointview);
            linearLayoutcirclept.addView(pointview);
        }


        ViewPagerAdapter adapter = new ViewPagerAdapter();
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                position = arg0;
            }

            int ptcurr = 0;

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                if (ptcurr != arg0) {
                    pointView.get(ptcurr).setBackground(getResources().getDrawable(R.drawable.tf_layoutwithstorkeroundwhitefill));
                    pointView.get(arg0).setBackground(getResources().getDrawable(R.drawable.tf_layoutwithstorkemainroundfill));
                    ptcurr = arg0;
                }
//                if (ptcurr>arg0)
//                {
//
//                }
//                else if (ptcurr<arg0)
//                {
//
//                }
//
//                pointView.get(arg0==0?pointView.size()-1:arg0-1).setBackground(getResources().getDrawable(R.drawable.tf_layoutwithstorkeroundwhitefill));
//
//                pointView.get(arg0).setBackground(getResources().getDrawable(R.drawable.tf_layoutwithstorkemainroundfill));
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
        viewPager.setAdapter(adapter);


    }
    private class ViewPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return allListView.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == (View) arg1;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = allListView.get(position);
            container.addView(view);
            return view;
        }

    }
    /*END商品详情图片切换************************************/


    private void initAbSlidingPlayView() {
        if (allListAbSlidingPlayView != null) {
            allListAbSlidingPlayView.clear();
            allListAbSlidingPlayView = null;
        }
        allListAbSlidingPlayView = new ArrayList<>();

        for (int i = 0; i < images.size(); i++) {
            //导入ViewPager的布局
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.tf_item_pic, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.pic_item);
            Utils.getImage(getActivity(), imageView, images.get(i));
            allListAbSlidingPlayView.add(view);
        }
        abSlidingPlayView.setPageLineHorizontalGravity(Gravity.CENTER_HORIZONTAL);
        abSlidingPlayView.addViews(allListAbSlidingPlayView);
        //开始轮播
        abSlidingPlayView.startPlay();
        /*
        viewPager.setOnItemClickListener(new AbOnItemClickListener() {
            @Override
            public void onClick(int position) {
                //跳转到详情界面
        Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
        startActivity(intent);
            }
        });
        */
    }


    private void initNoticeView() {
        viewFlipper.removeAllViews();
        for (int i = 0; i < images.size(); i++) {
            TextView textView = new TextView(getActivity());
            textView.setText("公告"+i);
            textView.setTextSize(11);
            textView.setTextColor(getResources().getColor(R.color.text_color));
            textView.setLines(1);
            textView.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            //textView.setOnClickListener(new NoticeTitleOnClickListener(getActivity(), i + text));
            //LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
            //LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            viewFlipper.addView(textView);
        }
    }
}
