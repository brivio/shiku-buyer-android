
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "PAGINATION")
public class PAGINATION extends Model {

    @Column(name = "count")
    public int count;

    @Column(name = "page")
    public int page;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.count = jsonObject.optInt("count");
        this.page = jsonObject.optInt("page");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("perPage", count);
        localItemObject.put("page", page);

        return localItemObject;
    }
}
