package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TMember{
    public String id;
    public String deviceid;
    public String member_type;
    public String username;
    public String tele;
    public String email;
    public String idcard;
    public String card_fr;
    public String card_bg;
    public String password;
    public String sex;
    public String img;
    public String birthday;
    public String province;
    public String city;
    public String area;
    public String province_id;
    public String city_id;
    public String area_id;
    public String town;
    public String address;
    public String likes;
    public String favs;
    public String orders;
    public String score;
    public String money;
    public String total_price;
    public String reg_time;
    public String reg_ip;
    public String reg_dev;
    public String last_time;
    public String last_ip;
    public String last_dev;
    public String last_sys;
    public String last_lng;
    public String last_lat;
    public String status;
    public String employ;
}