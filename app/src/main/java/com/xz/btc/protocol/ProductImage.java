
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "ProductImage")
public class ProductImage extends Model {

    @Column(name = "feature")
    public String feature;      //大图

    @Column(name = "thumb")
    public String thumb;        //中图

    @Column(name = "type")
    public int type;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.feature = jsonObject.optString("img");
        this.thumb = jsonObject.optString("img_thumb");
        this.type = jsonObject.optInt("type");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("img", feature);
        localItemObject.put("img_thumb", thumb);
        localItemObject.put("type", type);

        return localItemObject;
    }
}
