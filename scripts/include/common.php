<?php
function array_keys_exists($val, $array)
{
    if (is_array($val)) {
        $flag = false;
        foreach ($val as $k => $v) {
            if (array_key_exists($v, $array)) {
                $flag = true;
                break;
            }
        }
        return $flag;
    } else {
        return array_key_exists($val, $array);
    }
}

function del_files($path, $extension = "*")
{
    if (empty($path)) {
        _print_r(__line__ . ':$path不能为空');
        exit();
    }
    $path = $path . "\\";
    $res = scandir($path);
    foreach ($res as $key => $val) {
        if ($val == '.' || $val == '..')
            continue;
        unlink($path . $val);
        if (is_dir($path . $val)) {
            del_files($path . $val);
        }
    }
}

function del_dir($path, $del_self = false)
{
    if (empty($path)) {
        _print_r(__line__ . ':$path不能为空');
        exit();
    }
    if (!is_dir($path)) {
        return false;
    }
    //print_r("rmdir /s /q \"$path\"");exit();
    exec("rmdir /s /q \"$path\"");
    //del_files($path);
    //rmdir($path);    
    !$del_self && mkdir($path);
}

function create_path($path, $not_include = array())
{
    $res = explode('/', $path);
    $str = "";

    foreach ($res as $key => $val) {
        $str .= $val . '/';

        if (in_array($str, $not_include)) {
            return false;
        }

        if (!file_exists($str)) {
            mkdir($str);
        }
    }
    return true;
}

function zip($path, $zip_name, $width_dir = true, $only_dir = array())
{
    //获取文件数量
    @unlink($zip_name);
    $path = rtrim($path, '/');
    _print_r("准备压缩[$path]……\n");
    $entry_list = get_entry_list($path);
    $total = count($entry_list);

    $zip = new ZipArchive();
    $zip->open($zip_name, ZIPARCHIVE::CREATE);

    $i = 0;
    foreach ($entry_list as $key => $val) {
        if ($width_dir) {
            $local_name = basename($path) . $val;
        } else {
            $local_name = $val;
        }
        if (is_file($path . $val)) {
            $zip->addFile($path . $val, $local_name);
        } else {
            $zip->addEmptyDir($path . $val, $local_name);
        }
        $i++;
        show_progress("压缩[$path]", ($i / $total) * 100);
    }
    $zip->close();
}

function get_entry_list($path = "./", $top_dir = "")
{
    $entry_list = array();
    $path = rtrim($path) . '/';
    if (empty($top_dir)) {
        $top_dir = $path;
    }
    $res = scandir($path);
    foreach ($res as $key => $val) {
        if (!in_array($val, array('.', '..'))) {

            $entry_list[] = substr($path . $val, strlen($top_dir) - 1);
            if (is_dir($path . $val)) {
                $entry_list = array_merge($entry_list, get_entry_list($path . $val, $top_dir));
            }
        }
    }
    return $entry_list;
}

function show_progress($title, $percent)
{
    _print_r(sprintf("$title: [%-25s] %d%%\r", str_repeat('>', $percent / 4), $percent));
}

function check_path($str)
{
    return rtrim(str_replace('\\', '/', $str), '/');
}

function is_url($str)
{
    $res = parse_url(trim($str));
    return !empty($res['host']);
}

function http_post($url, $data = array())
{
    //对空格进行转义
    $url = str_replace(' ', '+', $url);

    $ch = curl_init();
    //设置选项，包括URL
    curl_setopt($ch, CURLOPT_URL, "$url");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3); //定义超时3秒钟
    // POST数据
    curl_setopt($ch, CURLOPT_POST, 1);
    // 把post的变量加上
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    //执行并获取url地址的内容
    $output = curl_exec($ch);
    //释放curl句柄
    curl_close($ch);
    return $output;

}

function http_get($url, $no_body = false, $opt = array())
{
    $options = array(
        CURLOPT_TIMEOUT        => 30,
        CURLOPT_HEADER         => true,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_NOBODY         => $no_body,
    );

    $ch = curl_init();
    $options[CURLOPT_URL] = $url;
    curl_setopt_array($ch, $options);

    $result = curl_exec($ch);

    curl_close($ch);
    return $result;
}

function _print_r($str, $return = false)
{
    global $config;
    if (!defined('CONSOLE_CHARSET')) {
        define('CONSOLE_CHARSET', 'UTF-8');
    }
    $str = iconv('UTF-8', CONSOLE_CHARSET, $str);
    if ($return) {
        return $str;
    } else {
        fwrite(STDOUT, $str);
        // file_put_contents($config['out_tmp_path'].'/console.log',trim($str,PHP_EOL).PHP_EOL,FILE_APPEND);
    }
}

function parse_uri($url)
{
    $res = parse_url($url);
    $list = explode('&', $res['query']);
    foreach ($list as $item) {
        $kv = explode('=', $item);
        $res['_query'][$kv[0]] = $kv[1];
    }
    return $res;
}

function strtofloat($str)
{
    return floatval(preg_replace("/[^0-9\.]+/u", '', $str));
}

function safestr($str)
{
    return preg_replace("/[^0-9a-zA-Z\x{4e00}-\x{9fa5}\`\~\!\@\#\$\%\^\&\*\(\)\_\+\-\=\;\'\,\.\/\{\}\:\"\<\>\?]+/u", '', $str);
}

function get_path_last($path)
{
    $res = explode('/', $path);
    return $res[count($res) - 1];
}

function getBaseClass($name)
{
    return 'T' . ucfirst(trim($name, '/'));
}

function getExtraClass($name)
{
    return 'Ex' . ucfirst(trim($name, '@'));
}
