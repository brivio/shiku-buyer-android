
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "userUpdateSettingsRequest")
public class userUpdateSettingsRequest extends Model {

    @Column(name = "type")
    public int type;

    @Column(name = "info")
    public String info;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        throw new JSONException("Not implemented");
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        try {
            localItemObject.put("type", type);
            localItemObject.put("info", info);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }
}
