
package com.xz.btc.protocol;

import android.os.Parcel;
import android.os.Parcelable;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "ADDRESS")
public class ADDRESS extends Model implements Parcelable {

    @Column(name = "ADDRESS_id", unique = true)
    public int id;

    @Column(name = "is_default")
    public boolean is_default;

    @Column(name = "consignee")
    public String consignee;

    @Column(name = "tel")
    public String tel;

    @Column(name = "zipcode")
    public String zipcode;

    @Column(name = "country")
    public String country;

    @Column(name = "city")
    public String city;

    @Column(name = "address")
    public String address;

    @Column(name = "province")
    public String province;

    @Column(name = "district")
    public String district;

    @Column(name = "status")
    public int status;

    /**
     * Unused fields below.
     */
    @Column(name = "default_address")
    public int default_address;

    @Column(name = "sign_building")
    public String sign_building;

    @Column(name = "city_name")
    public String city_name;

    @Column(name = "country_name")
    public String country_name;

    @Column(name = "province_name")
    public String province_name;

    @Column(name = "district_name")
    public String district_name;

    @Column(name = "email")
    public String email;

    @Column(name = "best_time")
    public String best_time;

    @Column(name = "mobile")
    public String mobile;

    public ADDRESS() {

    }

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optInt("id");
        this.is_default = jsonObject.optInt("is_default", 0) == 1;
        this.consignee = jsonObject.optString("name");
        this.tel = jsonObject.optString("tele");
        this.zipcode = jsonObject.optString("zipcode");
        this.country = jsonObject.optString("country");
        this.city = jsonObject.optString("city");
        this.address = jsonObject.optString("address");
        this.province = jsonObject.optString("province");
        this.district = jsonObject.optString("area");
        this.status = jsonObject.optInt("status");
    }

    public JSONObject toJson() {
        return toJson(true);
    }

    public JSONObject toJson(boolean includingId) {
        JSONObject localItemObject = new JSONObject();

        try {
            if (includingId) {
                localItemObject.put("id", id);
            }
            localItemObject.put("is_default", is_default ? 1 : 0);
            localItemObject.put("name", consignee);
            localItemObject.put("tele", tel);
            localItemObject.put("zipcode", zipcode);
            localItemObject.put("country", country);
            localItemObject.put("city", city);
            localItemObject.put("address", address);
            localItemObject.put("province", province);
            localItemObject.put("area", district);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }

    ADDRESS(Parcel in) {
        this.id = in.readInt();
        this.is_default = in.readByte() != 0;
        this.consignee = in.readString();
        this.tel = in.readString();
        this.zipcode = in.readString();
        this.country = in.readString();
        this.city = in.readString();
        this.address = in.readString();
        this.province = in.readString();
        this.district = in.readString();
        this.status = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeByte((byte) (is_default ? 1 : 0));
        dest.writeString(consignee);
        dest.writeString(tel);
        dest.writeString(zipcode);
        dest.writeString(country);
        dest.writeString(city);
        dest.writeString(address);
        dest.writeString(province);
        dest.writeString(district);
        dest.writeInt(status);
    }

    public static final Parcelable.Creator<ADDRESS> CREATOR =
            new Creator<ADDRESS>() {
                @Override
                public ADDRESS createFromParcel(Parcel source) {
                    return new ADDRESS(source);
                }

                @Override
                public ADDRESS[] newArray(int size) {
                    return new ADDRESS[size];
                }
            };
}
