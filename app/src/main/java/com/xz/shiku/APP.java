package com.xz.shiku;

import com.xz.tframework.fragment.BackHandledFragment;

import java.util.Stack;

/**
 * Created by txj on 15/5/11.
 */
public class APP {

    Stack<BackHandledFragment> fragments;
    Stack<BackHandledFragment> parentfragments;

    /**
     * 内部类实现单例模式
     * 延迟加载，减少内存开销
     */
    private static class SingletonHolder {
        private static APP instance = new APP();
    }

    /**
     * 私有的构造函数
     */
    private APP() {
        fragments = new Stack<BackHandledFragment>();
        parentfragments = new Stack<BackHandledFragment>();
    }

    public static APP getInstance() {
        return SingletonHolder.instance;
    }

    public Stack<BackHandledFragment> getFragments() {
        return fragments;
    }

    public Stack<BackHandledFragment> getParentfragments() {
        return parentfragments;
    }

    public BackHandledFragment getLastParentfragment() {
        return parentfragments.get(parentfragments.size() - 1);
    }
}
