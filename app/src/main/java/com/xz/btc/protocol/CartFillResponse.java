
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "CartFillResponse")
public class CartFillResponse extends Model {

    @Column(name = "status")
    public STATUS status;

    @Column(name = "data")
    public CartList data;

    public void fromJson(JSONObject jsonObject) {
        if (null == jsonObject) {
            return;
        }

        STATUS status = new STATUS();
        CartList data = new CartList();

        try {
            status.fromJson(jsonObject);
            data.fromJson(jsonObject.optJSONArray("data"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.status = status;
        this.data = data;
    }

    public JSONObject toJson() throws JSONException {
        return new JSONObject();
    }
}
