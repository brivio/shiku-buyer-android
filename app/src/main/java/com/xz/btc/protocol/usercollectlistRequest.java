
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

@Table(name = "usercollectlistRequest")
public class usercollectlistRequest extends Model {

    @Column(name = "session")
    public SESSION session;

    @Column(name = "pagination")
    public PAGINATION pagination;

    @Column(name = "rec_id")
    public int rec_id;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        SESSION session = new SESSION();
        session.fromJson(jsonObject.optJSONObject("session"));
        this.session = session;

        PAGINATION pagination = new PAGINATION();
        pagination.fromJson(jsonObject.optJSONObject("pagination"));

        this.pagination = pagination;
        this.rec_id = jsonObject.optInt("rec_id");
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();
        String tempKey;

        try {
            if (null != pagination) {
                JSONObject jsonPagination = pagination.toJson();
                Iterator iPagination = jsonPagination.keys();
                while (iPagination.hasNext()) {
                    tempKey = (String) iPagination.next();
                    localItemObject.put(tempKey, jsonPagination.get(tempKey));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }
}
