
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "userEditInfoRequest")
public class userEditInfoRequest extends Model {
    @Column(name = "user")
    public USER user;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }
    }

    public JSONObject toJson() throws JSONException {
        return user.toJson();
    }
}
