package com.xz.btc.model;

/**
 * Created by txj on 15/4/14.
 * 测试用例，模拟服务器
 */

import com.xz.external.androidquery.callback.AjaxCallback;
import com.xz.tframework.model.BeeCallback;
import com.xz.btc.protocol.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MockServer {
    private static MockServer instance;

    public static MockServer getInstance() {
        if (instance == null) {
            instance = new MockServer();
        }
        return instance;
    }

    public static <K> void ajax(AjaxCallback<K> callback) {
        try {
            JSONObject responseJsonObject = new JSONObject();
            if (callback.getUrl() == ApiInterface.HOME_DATA) {

                STATUS status = new STATUS();
                status.succeed = 1;
                status.error_code = 0;
                status.error_desc = "";
                responseJsonObject.put("status", status.toJson());

                JSONObject dataJsonObject = new JSONObject();
                JSONArray itemJsonArray = new JSONArray();
                for (int i = 0; i < 3; i++) {
                    PLAYER player = new PLAYER();
                    player.description = "月光光照大床";
                    player.url = "www.baidu.com";
                    player.photo = new PHOTO();

                    player.photo.url = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                    itemJsonArray.put(player.toJson());
                }

                dataJsonObject.put("player", itemJsonArray);

                JSONArray item2JsonArray = new JSONArray();
                for (int i = 0; i < 3; i++) {
                    SIMPLEGOODS good = new SIMPLEGOODS();
                    good.name = "商品" + i;
                    good.shop_price = i * 100 + "";
                    good.img = new PHOTO();
                    good.img.url = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                    item2JsonArray.put(good.toJson());
                }
                dataJsonObject.put("promote_goods", item2JsonArray);
                JSONArray item3JsonArray = new JSONArray();
                for (int i = 0; i < 8; i++) {
                    HOME_CATEGORY good = new HOME_CATEGORY();
                    good.img = new PHOTO();
                    good.name = "类目" + i;

                    good.img.small = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                    good.img.url = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                    item3JsonArray.put(good.toJson());
                }

                dataJsonObject.put("categories", item3JsonArray);
                responseJsonObject.put("data", dataJsonObject);

            } else if (callback.getUrl() == ApiInterface.USER_COLLECT_LIST) {
                STATUS status = new STATUS();
                status.succeed = 1;
                status.error_code = 0;
                status.error_desc = "";
                responseJsonObject.put("status", status.toJson());

                JSONArray dataJsonObject = new JSONArray();
                for (int i = 0; i < 8; i++) {
                    COLLECT_LIST item = new COLLECT_LIST();

                    item.rec_id = i + "";
                    item.username = "zhangdaye" + i;
                    item.shopname = "店铺" + i;
                    item.yjhl = i * 2.3 + "mg/平方米";
                    item.mcl = i + "kg";
                    item.price = i * 10;
                    item.img = new PHOTO();
                    item.img.small = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                    dataJsonObject.put(item.toJson());
                }
                PAGINATED p = new PAGINATED();
                p.total = 1;
                p.more = 1;
                p.count = 1;

                responseJsonObject.put("paginated", p.toJson());
                responseJsonObject.put("data", dataJsonObject);

            } else if (callback.getUrl() == ApiInterface.USER_MESSAGE_LIST) {
                STATUS status = new STATUS();
                status.succeed = 1;
                status.error_code = 0;
                status.error_desc = "";
                responseJsonObject.put("status", status.toJson());

                JSONArray dataJsonObject = new JSONArray();
                for (int i = 0; i < 8; i++) {
                    MESSAGE item = new MESSAGE();

                    item.id = i;
                    item.content = "消息标题" + i;
                    item.custom_data = "消息内容" + i;
                    item.time = "2014-07-08 12:0" + i;
                    item.img = new PHOTO();
                    item.img.small = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                    dataJsonObject.put(item.toJson());
                }
                PAGINATED p = new PAGINATED();
                p.total = 1;
                p.more = 1;
                p.count = 1;

                responseJsonObject.put("paginated", p.toJson());
                responseJsonObject.put("data", dataJsonObject);

            } else if (callback.getUrl() == ApiInterface.USER_INTEGRAL_LIST) {
                STATUS status = new STATUS();
                status.succeed = 1;
                status.error_code = 0;
                status.error_desc = "";
                responseJsonObject.put("status", status.toJson());

                JSONArray dataJsonObject = new JSONArray();
                for (int i = 0; i < 8; i++) {
                    INTEGRAL item = new INTEGRAL();

                    item.id = i;
                    item.content = "消息标题" + i;
                    item.custom_data = "消息内容" + i;
                    item.time = "2014-07-08 12:0" + i;
                    item.img = new PHOTO();
                    item.img.small = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                    dataJsonObject.put(item.toJson());
                }
                PAGINATED p = new PAGINATED();
                p.total = 1;
                p.more = 1;
                p.count = 1;

                responseJsonObject.put("paginated", p.toJson());
                responseJsonObject.put("data", dataJsonObject);

            } else if (callback.getUrl() == ApiInterface.CATEGORY) {
                STATUS status = new STATUS();
                status.succeed = 1;
                status.error_code = 0;
                status.error_desc = "";
                responseJsonObject.put("status", status.toJson());

                JSONArray dataJsonObject = new JSONArray();
                for (int i = 0; i < 8; i++) {
                    CATEGORY item = new CATEGORY();
                    item.id = i + "";
                    item.name = "分类" + i;
                    //item.img = new PHOTO();
                    //item.img.small = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                    ArrayList<CATEGORY> ch = new ArrayList<CATEGORY>();
                    for (int j = 0; j < 4; j++) {
                        CATEGORY subitem = new CATEGORY();
                        subitem.id = j + "";
                        subitem.name = i + "子分类" + j;
                        //subitem.img = new PHOTO();
                        //subitem.img.small = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                        ch.add(subitem);
                    }
                    //item.children = ch;
                    dataJsonObject.put(item.toJson());
                }
//            PAGINATED p=new PAGINATED();
//            p.total=1;
//            p.more=1;
//            p.count=1;

//            responseJsonObject.put("paginated", p.toJson());
                responseJsonObject.put("data", dataJsonObject);

            } else if (callback.getUrl() == ApiInterface.CART_LIST) {
                STATUS status = new STATUS();
                status.succeed = 1;
                status.error_code = 0;
                status.error_desc = "";
                responseJsonObject.put("status", status.toJson());


                CART_LIST_DATA cart_list_data = new CART_LIST_DATA();
                ArrayList<GOODS_GROUPED_LIST> goods_grouped_lists = new ArrayList<GOODS_GROUPED_LIST>();

                for (int i = 1; i <= 6; i++) {

                    GOODS_GROUPED_LIST goods_grouped_list = new GOODS_GROUPED_LIST();

                    ArrayList<GOODS_LIST> l = new ArrayList<GOODS_LIST>();
                    for (int j = 1; j <= i; j++) {
                        GOODS_LIST goods_list = new GOODS_LIST();
                        goods_list.rec_id = j + "";
                        goods_list.goods_name = "商品" + i + j;
                        goods_list.goods_number = j;
                        goods_list.img = new PHOTO();
                        goods_list.img.small = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                        goods_list.goods_price = j;
                        goods_list.formated_goods_price = "￥：" + j;
                        goods_list.market_price = j * 2;
                        goods_list.formated_goods_price = "￥：" + j * 2;
                        l.add(goods_list);
                    }

                    TOTAL t = new TOTAL();
                    t.real_goods_count = 10;
                    t.saving = 100 + "";
                    t.goods_price = 12;
                    t.goods_amount = 11 + "";
                    t.market_price = 18 + "";
                    t.save_rate = "1";
                    t.virtual_goods_count = 1000;

                    goods_grouped_list.goods_list = l;
                    goods_grouped_list.total = t;
                    goods_grouped_list.name = "菜园子" + i;
                    goods_grouped_list.rec_id = i + "";

                    goods_grouped_lists.add(goods_grouped_list);
                }
                TOTAL t = new TOTAL();
                t.real_goods_count = 10;
                t.saving = 100 + "";
                t.goods_price = 12;
                t.goods_amount = 11 + "";
                t.market_price = 18 + "";
                t.save_rate = "1";
                t.virtual_goods_count = 1000;
                cart_list_data.total = t;
                cart_list_data.goods_grouped_lists = goods_grouped_lists;


                responseJsonObject.put("data", cart_list_data.toJson());
            } else if (callback.getUrl() == ApiInterface.ADDRESS_LIST) {
                STATUS status = new STATUS();
                status.succeed = 1;
                status.error_code = 0;
                status.error_desc = "";
                responseJsonObject.put("status", status.toJson());

                JSONArray dataJsonObject = new JSONArray();

                for (int j = 0; j < 3; j++) {
                    ADDRESS address = new ADDRESS();
                    address.province = "浙江省";
                    address.city = "杭州市";
                    address.district = "西湖区";
                    address.address = "详细地址测试数据" + j;
                    address.consignee = "收件人" + j;
                    address.mobile = "12345423" + j;
                    dataJsonObject.put(address.toJson());
                }

                responseJsonObject.put("data", dataJsonObject);
            } else if (callback.getUrl() == ProtocolConst.SHOPHELP) {
                STATUS status = new STATUS();
                status.succeed = 1;
                status.error_code = 0;
                status.error_desc = "";
                responseJsonObject.put("status", status.toJson());

                JSONArray dataJsonObject = new JSONArray();

                for (int i = 0; i < 3; i++) {
                    SHOPHELP shophelp = new SHOPHELP();
                    shophelp.name = "支付与配送";
                    for (int j = 0; j < 3; j++) {
                        ARTICLE article = new ARTICLE();
                        article.short_title = "月光光照大床";
                        article.id = j + "";
                        article.title = "月光光照大床";
                        shophelp.article.add(article);
                    }
                    dataJsonObject.put(shophelp.toJson());
                }

                responseJsonObject.put("data", dataJsonObject);
            } else if (callback.getUrl() == ProtocolConst.GOODSDETAIL) {
                STATUS status = new STATUS();
                status.succeed = 1;
                status.error_code = 0;
                status.error_desc = "";
                responseJsonObject.put("status", status.toJson());

                JSONObject dataJsonObject = new JSONObject();
                GOODS goods = new GOODS();

                for (int i = 0; i < 5; i++) {
                    PHOTO photo = new PHOTO();
                    photo.url = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                    goods.pictures.add(photo);
                }
                responseJsonObject.put("data", goods.toJson());

            } else if (callback.getUrl() == ApiInterface.ORDER_LIST) {
                STATUS status = new STATUS();
                status.succeed = 1;
                status.error_code = 0;
                status.error_desc = "";
                responseJsonObject.put("status", status.toJson());

                JSONArray dataJsonObject = new JSONArray();

                for (int i = 1; i < 8; i++) {
                    GOODORDER item = new GOODORDER();

                    item.order_time = i + "";
                    item.total_fee = "" + i * 1;
                    item.order_id = "" + i;
                    item.order_sn = "sn20151112" + i;
                    //item.goods_list=new ArrayList<GOODS_GROUPED_LIST>();
//                item.goods_list==new GOODS_GROUPED_LIST();
//                for (int j=0;j<=i;j++)
//                {
                    GOODS_GROUPED_LIST goods_grouped_list = new GOODS_GROUPED_LIST();

                    ArrayList<GOODS_LIST> l = new ArrayList<GOODS_LIST>();
                    for (int k = 0; k <= i; k++) {
                        GOODS_LIST goods_list = new GOODS_LIST();
                        goods_list.rec_id = k + "";
                        goods_list.goods_name = "商品" + i + k;
                        goods_list.goods_number = k;
                        goods_list.img = new PHOTO();
                        goods_list.img.small = "http://gi2.md.alicdn.com/bao/uploaded/i2/TB1wQmPHpXXXXX3apXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
                        goods_list.goods_price = k;
                        goods_list.formated_goods_price = "￥：" + k;
                        goods_list.market_price = k * 2;
                        goods_list.formated_goods_price = "￥：" + k * 2;
                        l.add(goods_list);
                    }

                    TOTAL t = new TOTAL();
                    t.real_goods_count = 10;
                    t.saving = 100 + "";
                    t.goods_price = 12;
                    t.goods_amount = 11 + "";
                    t.market_price = 18 + "";
                    t.save_rate = "1";
                    t.virtual_goods_count = 1000;

                    goods_grouped_list.goods_list = l;
                    goods_grouped_list.total = t;
                    goods_grouped_list.name = "菜园子" + i;
                    goods_grouped_list.rec_id = i + "";

                    item.goods_grouped_list = goods_grouped_list;
//                }

                    dataJsonObject.put(item.toJson());
                }
                PAGINATED p = new PAGINATED();
                p.total = 1;
                p.more = 1;
                p.count = 1;

                responseJsonObject.put("paginated", p.toJson());
                responseJsonObject.put("data", dataJsonObject);
            }

            ((BeeCallback) callback).callback(callback.getUrl(), responseJsonObject, callback.getStatus());

        } catch (JSONException e) {
            // TODO: handle exception
        }
    }
}
