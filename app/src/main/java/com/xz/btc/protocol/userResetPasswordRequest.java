
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "userResetPasswordRequest")
public class userResetPasswordRequest extends Model {

    @Column(name = "uid")
    public String uid;

    @Column(name = "oldpassword")
    public String oldpassword;

    @Column(name = "newpassword")
    public String newpassword;

    public int type;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        throw new JSONException("Not implemented");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("tele", uid);
        localItemObject.put("oldpassword", oldpassword);
        localItemObject.put("newpassword", newpassword);
        localItemObject.put("type", type);

        return localItemObject;
    }
}
