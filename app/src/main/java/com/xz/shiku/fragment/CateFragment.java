package com.xz.shiku.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.CATEGORY;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.activity.SearchResultActivity;
import com.xz.shiku.adapter.category.Category1Adapter;
import com.xz.shiku.adapter.category.Category2Adapter;
import com.xz.shiku.controller.SearchController;
import com.xz.shiku.fragment.search.SearchFragment;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.Optional;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.xz.shiku.fragment.CateFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.xz.shiku.fragment.CateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CateFragment extends BackHandledFragment implements AdapterView.OnItemClickListener {
    public static final String MAIN = "main_activity";
    public static final String POPUP = "popup_activity";

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SHOW_IN = "show_in";
    private static final String ARG_PARAM2 = "param2";

    private String mShowIn;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    Category1Adapter mCategoryAdapter;
    Category2Adapter mSubcategoryAdapter;

    @InjectView(R.id.gvCategory1)
    ListView mCategories;
    @InjectView(R.id.gvCategory2)
    GridView mSubcategories;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param showIn Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CateFragment newInstance(String showIn, String param2) {
        CateFragment fragment = new CateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SHOW_IN, showIn);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        if (showIn.equalsIgnoreCase(POPUP)) {
            PopActivity.gShowNavigationBar = false;
        }

        return fragment;
    }

    public CateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mShowIn = getArguments().getString(ARG_SHOW_IN);
            mParam2 = getArguments().getString(ARG_PARAM2);
        } else {
            mShowIn = MAIN;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_category, container, false);

        if (mShowIn.equalsIgnoreCase(MAIN)) {
            v.findViewById(R.id.top_in_pop).setVisibility(View.GONE);
        } else {
            v.findViewById(R.id.top_in_main).setVisibility(View.GONE);
        }

        ButterKnife.inject(this, v);
        ButterKnife.findById(v, R.id.tv_scan).setVisibility(View.INVISIBLE);

        SearchController.getInstance().addResponseListener(this);
        SearchController.getInstance().getCategory(getActivity());

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        SearchController.getInstance().removeResponseListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.CATEGORY)) {
            if (SearchController.getInstance().getLastResponseStatus().getIsSuccess()) {
                mCategoryAdapter = new Category1Adapter(getActivity(), SearchController.getInstance().getCategoryResult());
                mCategories.setAdapter(mCategoryAdapter);

                mSubcategoryAdapter = new Category2Adapter(getActivity(), SearchController
                        .getInstance()
                        .getCategoryResult()
                        .get(0)
                        .subcategories);
                mSubcategories.setAdapter(mSubcategoryAdapter);
            } else {
                ToastView.showMessage(getActivity(), SearchController
                        .getInstance()
                        .getLastResponseStatus()
                        .getErrorMessage());
            }
        }
    }

    @OnItemClick({R.id.gvCategory1, R.id.gvCategory2})
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.gvCategory1) {
            mCategoryAdapter.setSelection(position);

            mSubcategoryAdapter.updateAdapter(SearchController
                    .getInstance()
                    .getCategoryResult()
                    .get(position)
                    .subcategories);
        } else if (parent.getId() == R.id.gvCategory2) {
            CATEGORY category = (CATEGORY) parent.getItemAtPosition(position);

            Intent intent = new Intent(getActivity(), SearchResultActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(SearchResultActivity.SEARCH_ARG_BY, SearchResultActivity.CATEGORY);
            bundle.putString(SearchResultActivity.SEARCH_ARG_CATEGORY_ID, category.id);
            bundle.putString(SearchResultActivity.SEARCH_ARG_CATEGORY_NAME, category.name);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    @OnClick(R.id.index_search_edit)
    public void onSearch() {
        SearchFragment searchFragment = new SearchFragment();
        startActivityWithFragment(searchFragment);
    }

    @Optional
    @OnClick(R.id.backbtn)
    public void onBack() {
        getActivity().finish();
    }
}
