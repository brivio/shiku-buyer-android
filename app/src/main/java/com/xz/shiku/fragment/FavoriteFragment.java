package com.xz.shiku.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.COLLECT_LIST;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.adapter.FavoriteAdapter;
import com.xz.shiku.controller.FavoriteController;
import com.xz.shiku.controller.UserController;
import com.xz.shiku.fragment.product.ProductDetailsFragment;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.SwipeListView;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.xz.shiku.fragment.FavoriteFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.xz.shiku.fragment.FavoriteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoriteFragment extends BackHandledFragment implements AbOnItemClickListener, AdapterView.OnItemClickListener {
    public static final String SHOW_IN_MAIN = "in_main_activity";
    public static final String SHOW_IN_POPUP = "in_popup_activity";
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SHOW_IN = "arg_show_in";

    private String mShowIn;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    @InjectView(R.id.collect_list)
    SwipeListView mFavorites;
    @InjectView(R.id.ll_empty)
    LinearLayout mEmptyLayout;

    private FavoriteAdapter mFavoriteAdapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param showIn Parameter 1.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoriteFragment newInstance(String showIn) {
        if (showIn.equals(SHOW_IN_POPUP)) {
            PopActivity.gShowNavigationBar = true;
            FavoriteFragment.titleResId = R.string.title_fragment_favorite;
            FavoriteFragment.topRightText = "";
        }

        FavoriteFragment fragment = new FavoriteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SHOW_IN, showIn);
        fragment.setArguments(args);
        return fragment;
    }

    public FavoriteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mShowIn = getArguments().getString(ARG_SHOW_IN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v;

        if (mShowIn.equals(SHOW_IN_MAIN)) {
            v = inflater.inflate(R.layout.layout_favorite_with_navbar, container, false);
            ((TextView) v.findViewById(R.id.top_menu_text_title)).setText(R.string.title_fragment_favorite);
            ((TextView) v.findViewById(R.id.top_menu_text_title)).setTextColor(Color.WHITE);
            ((TextView) v.findViewById(R.id.backbtn)).setText("");
            ((TextView) v.findViewById(R.id.toprightbtn)).setText("");
            v.findViewById(R.id.product_detail_top_layout).setBackgroundResource(R.color.bg_Main);
        } else {
            v = inflater.inflate(R.layout.layout_favorite, container, false);
        }

        ButterKnife.inject(this, v);

        ((TextView) v.findViewById(R.id.ll_empty_icon)).setText(R.string.iconlove);
        ((TextView) v.findViewById(R.id.ll_empty_text)).setText(R.string.label_for_empty_favorite_text);
        ((TextView) v.findViewById(R.id.ll_empty_subtext)).setText(R.string.label_for_empty_favorite_subtext);

        if (UserController.getInstance().isUserReady()) {
            FavoriteController.getInstance().addResponseListener(this);
            FavoriteController.getInstance().getFavorites(getActivity());
        } else {
            initUI(false);
        }

        return v;
    }

    private void initUI(boolean showFavoriteLayout) {
        if (showFavoriteLayout) {
            mEmptyLayout.setVisibility(View.GONE);
            mFavorites.setPullLoadEnable(false);
            mFavorites.setPullRefreshEnable(true);
            mFavorites.setXListViewListener(this);
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        FavoriteController.getInstance().removeResponseListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.FAVORITE_LIST)) {
            List<COLLECT_LIST> favorites = FavoriteController.getInstance().getFavoritesResult();
            if (FavoriteController.getInstance().getIsFirstPage()) {
                if (favorites != null && favorites.size() > 0) {
                    initUI(true);

                    mFavorites.setRefreshTime();
                    mFavorites.stopRefresh();
                    if (FavoriteController.getInstance().getHasMore()) {
                        mFavorites.setPullLoadEnable(true);
                    }
                    mFavoriteAdapter = new FavoriteAdapter(getActivity(), favorites, mFavorites.getRightViewWidth(), this);
                    mFavorites.setAdapter(mFavoriteAdapter);
                } else {
                    initUI(false);
                }
            } else {
                mFavorites.stopLoadMore();
                mFavoriteAdapter.notifyDataSetChanged();
            }
        } else if (url.endsWith(ApiInterface.FAVORITE_REMOVE)) {
            List<COLLECT_LIST> favorites = FavoriteController.getInstance().getFavoritesResult();
            if (favorites != null && favorites.size() > 0) {
                mFavoriteAdapter.notifyDataSetChanged();
            } else {
                initUI(false);
            }
        }
    }

    public void onRefresh() {
        FavoriteController.getInstance().getFavorites(getActivity());
    }

    @Override
    public void onLoadMore() {
        if (FavoriteController.getInstance().getHasMore()) {
            FavoriteController.getInstance().getMoreFavorites(getActivity());
        }
    }

    @OnItemClick(R.id.collect_list)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ProductDetailsFragment productDetailsFragment = ProductDetailsFragment.newInstance(
                ((COLLECT_LIST) parent.getItemAtPosition(position)).productId + "");
        startActivityWithFragment(productDetailsFragment);
    }

    public void onClick(int position) {
        COLLECT_LIST favItem = (COLLECT_LIST) mFavoriteAdapter.getItem(position);
        if (favItem != null) {
            FavoriteController.getInstance().removeFromFavorites(getActivity(), favItem.id);
        }
    }
}
