/**
 * Created by txj on 15/4/14.
 */
package com.xz.tframework.model;

import android.content.Context;

import com.xz.external.androidquery.callback.AjaxCallback;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Stack;

/**
 * @author mc374
 *
 */
public class DebugMessageModel extends BaseModel {

	public static Stack<BeeCallback> messageList = new Stack<BeeCallback>();
    public static ArrayList<BeeCallback> sendingmessageList = new ArrayList<BeeCallback>();
	
    
	public DebugMessageModel(Context context) {
		super(context);
		 
	}
	
	public static void addMessage(BeeCallback msg)
	{
		
		messageList.push(msg);
        sendingmessageList.add(msg);
        
	}

    public static void finishMessage(BeeCallback msg)
    {
        long currentTimestamp = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日HH时mm分ss秒");
        msg.endTimeStamp = sdf.format(new Date(currentTimestamp));

        sendingmessageList.remove(msg);
    }

    public static boolean isSendingMessage(String url)
    {
        for (int i = 0; i < sendingmessageList.size(); i++)
        {
        	AjaxCallback msg = sendingmessageList.get(i);
            if(msg.getUrl().endsWith(url) )
            {
                return true;
            }
        }

        return false;
    }

}
