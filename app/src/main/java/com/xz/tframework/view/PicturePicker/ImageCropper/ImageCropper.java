package com.xz.tframework.view.PicturePicker.ImageCropper;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.xz.shiku.R;
import com.xz.tframework.view.PicturePicker.ImageCropper.view.ClipImageLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageCropper extends Activity {
    private static final int CROP = 2;
    private ClipImageLayout mClipImageLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tf_activity_image_cropper);
        mClipImageLayout = (ClipImageLayout) findViewById(R.id.id_clipImageLayout);

        byte[] b = getIntent().getByteArrayExtra("bitmap");
        if(b!=null)
        {
            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
            mClipImageLayout.setSourceImgBitmap(bitmap);
        }
        else{
            Log.e("图片裁剪错误", "传递的图像为空");
        }

        final Activity acti=this;
        Button btn_clip_cancel=(Button)findViewById(R.id.btn_clip_cancel);
        Button btn_clip_ok=(Button)findViewById(R.id.btn_clip_ok);

        btn_clip_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Bitmap bitmap = mClipImageLayout.clip();

                String path = null;
//判断是否有存储设备（SD卡以及自带的存储设备等）
                if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

                }
//Environment.getExternalStorageDirectory()此方法获得存储设备路径
                path = Environment.getExternalStorageDirectory() + "/personal.png";
                File tempFile = new File(path);
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(tempFile);
                    //1.格式；2.质量；3输出流
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (null != fos) {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                /*ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] datas = baos.toByteArray();
*/
                Intent intent = new Intent();
                intent.putExtra("imguri", path);

                // 设置结果，并进行传送
                acti.setResult(RESULT_OK, intent);
                acti.finish();           }
        });

        btn_clip_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                acti.finish();
            }
        });
    }
}
