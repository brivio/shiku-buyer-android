
package com.xz.btc.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeData {

    public List<AdItem> adList1;
    public List<AdItem> adList2;
    public List<AdItem> adList3;
    public List<AdItem> adList4;
    public List<AdItem> adList5;


    public void fromJson(JSONObject jsonObject) {
        if (null == jsonObject) {
            return;
        }

        String[] adlistNames = {"ad_list", "ad_list2", "ad_list3", "ad_list4", "ad_list5"};
        List<AdItem>[] adlists = new List[adlistNames.length];

        JSONArray subItemArray;
        AdItem adItem;

        for (int i = 0; i < adlists.length; i++) {
            subItemArray = jsonObject.optJSONArray(adlistNames[i]);
            adlists[i] = new ArrayList<>();
            if (subItemArray != null && subItemArray.length() > 0) {
                for (int j = 0; j < subItemArray.length(); j++) {
                    adItem = new AdItem();
                    try {
                        adItem.fromJson(subItemArray.getJSONObject(j));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    adlists[i].add(adItem);
                }
            }
        }

        adList1 = adlists[0];
        adList2 = adlists[1];
        adList3 = adlists[2];
        adList4 = adlists[3];
        adList5 = adlists[4];
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }
}
