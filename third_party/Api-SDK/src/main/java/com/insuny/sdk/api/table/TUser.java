package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TUser{
    public String id;
    public String deviceid;
    public String username;
    public String openid;
    public String email;
    public String tele;
    public String sign;
    public String password;
    public String sex;
    public String img;
    public String birthday;
    public String url;
    public String province;
    public String city;
    public String area;
    public String province_id;
    public String city_id;
    public String area_id;
    public String address;
    public String cover;
    public String intro;
    public String likes;
    public String favs;
    public String orders;
    public String quans;
    public String score;
    public String reg_time;
    public String reg_ip;
    public String reg_dev;
    public String last_time;
    public String last_ip;
    public String last_dev;
    public String last_sys;
    public String last_lng;
    public String last_lat;
    public String msg_express;
    public String msg_sys;
    public String msg_sales;
    public String is_vip;
    public String level;
    public String status;
}