package com.xz.shiku.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xz.btc.protocol.HOME_CATEGORY;
import com.xz.shiku.R;
import com.xz.tframework.adapter.TBaseAdapter;
import com.xz.tframework.utils.Utils;
import com.xz.tframework.view.CircleImageView;

import java.util.ArrayList;

/**
 * Created by txj on 15/2/17.
 */
public class GVUserAdapter extends TBaseAdapter {

    public class GVUser2Holder extends TCellHolder
    {
        TextView textView;
        CircleImageView imageView;
    }
    public GVUserAdapter(Context c, ArrayList dataList) {
        super(c, dataList);
    }
    @Override
    protected TCellHolder createCellHolder(View cellView) {
        GVUser2Holder holder = new GVUser2Holder();
        holder.textView = (TextView)cellView.findViewById(R.id.titleview);
        holder.imageView = (CircleImageView)cellView.findViewById(R.id.imageview);
        return holder;
    }

    @Override
    protected View bindData(int position, View cellView, ViewGroup parent, TCellHolder h) {

        GVUser2Holder hh=(GVUser2Holder)h;
        HOME_CATEGORY s=(HOME_CATEGORY)dataList.get(position);
        hh.textView.setText(s.name);
        Utils.getImage(mContext,hh.imageView,s.img.small);

//        CATEGORY categoryItem = (CATEGORY)dataList.get(position);
//        CategoryHolder holder = (CategoryHolder)h;
//        holder.categoryName.setText(categoryItem.name);
//        if (categoryItem.children.size() > 0)
//        {
//            holder.rightArrow.setVisibility(View.VISIBLE);
//        }
//        else
//        {
//            holder.rightArrow.setVisibility(View.GONE);
//        }
        return cellView;
    }

    @Override
    public View createCellView() {
        View cellView = mInflater.inflate(R.layout.item_home_sec2,null);
        return cellView;
    }
}
