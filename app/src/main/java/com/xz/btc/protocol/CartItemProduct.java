
package com.xz.btc.protocol;

import org.json.JSONException;
import org.json.JSONObject;

public class CartItemProduct {

    /**
     * The unique ID in the shopping cart.
     */
    public int id;
    /**
     * The unique ID of this product.
     */
    public int item_id;
    public String name;
    public String image;
    public int quantity;
    public double price;
    public String attr;
    public int shop_id;
    public String shop_name;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        id = jsonObject.optInt("id");
        item_id = jsonObject.optInt("item_id");
        name = jsonObject.optString("item_title");
        image = jsonObject.optString("item_img");
        quantity = jsonObject.optInt("num");
        price = jsonObject.optDouble("price");
        attr = jsonObject.optString("attr");
        shop_id = jsonObject.optInt("mid");
        shop_name = jsonObject.optString("mname");
    }

    public JSONObject toJson() throws JSONException {
        return new JSONObject();
    }
}
