package com.xz.tframework.activity;

/**
 * Created by txj on 15/4/14.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.xz.tframework.model.ActivityManagerModel;
import com.xz.tframework.model.BusinessMessage;
import com.xz.shiku.R;

import org.json.JSONException;

@SuppressLint("NewApi")
public class BaseActivity extends Activity implements Handler.Callback
{
    public Handler mHandler;

    public BaseActivity()
    {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
         
        super.onCreate(savedInstanceState);
        mHandler = new Handler(this);
        ActivityManagerModel.addLiveActivity(this);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        ActivityManagerModel.addVisibleActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        ActivityManagerModel.removeVisibleActivity(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        ActivityManagerModel.addForegroundActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ActivityManagerModel.removeForegroundActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityManagerModel.removeLiveActivity(this);
    }

    @Override
    public boolean handleMessage(Message msg) {
        return false;
    }

    public void OnMessageResponse(BusinessMessage response)
            throws JSONException
    {
        if (response.messageState == BusinessMessage.FAILURE_MESSAGE)
        {
        }
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.tf_push_right_in,
                R.anim.tf_push_right_out);
    }
    public void startActivityForResult(Intent intent, int requestCode)
    {
        super.startActivityForResult(intent,requestCode);
        overridePendingTransition(R.anim.tf_push_right_in,
                R.anim.tf_push_right_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.tf_push_left_in,R.anim.tf_push_left_out);
    }
}
