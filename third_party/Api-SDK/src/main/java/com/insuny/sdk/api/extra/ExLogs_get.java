package com.insuny.sdk.api.extra;
/*
该文件由脚本自动生成，不要手动修改
 */
import com.insuny.sdk.api.extra.ExLogs_class_item;
import com.insuny.sdk.api.extra.ExLogs_get_list;
import java.util.ArrayList;

public class ExLogs_get{
    public ArrayList<ExLogs_class_item> class_list=new ArrayList<>();
    public String id;
    public String img;
    public String title;
    public ExLogs_get_list logs_list;
}