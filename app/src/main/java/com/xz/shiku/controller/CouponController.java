package com.xz.shiku.controller;

import android.content.Context;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.Coupon;
import com.xz.btc.protocol.GetCouponsRequest;
import com.xz.btc.protocol.GetCouponsResponse;
import com.xz.btc.protocol.PAGINATED;
import com.xz.btc.protocol.PAGINATION;
import com.xz.btc.protocol.SESSION;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.view.ToastView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by XH on 2015/6/13.
 */
public class CouponController extends BaseModel {
    public static final int PER_PAGE = 10;

    private static CouponController mInstance;

    public static CouponController getInstance() {
        if (mInstance == null) {
            mInstance = new CouponController(TFrameworkApp.getInstance());
        }

        return mInstance;
    }

    private List<Coupon> mCoupons;
    private PAGINATED mLastSearchPagination;

    protected CouponController(Context context) {
        super(context);
    }

    public List<Coupon> getCouponsResult() {
        return mCoupons;
    }

    public boolean getIsFirstPage() {
        return (mLastSearchPagination != null &&
                mLastSearchPagination.page == 1);
    }

    public boolean getHasMore() {
        boolean hasMore = false;

        if (mLastSearchPagination != null) {
            hasMore = mLastSearchPagination.more > 0;
        }

        return hasMore;
    }

    public void getCoupons(Context context) {
        mContext = context;

        GetCouponsRequest request = new GetCouponsRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = 1;
        pagination.count = PER_PAGE;

        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.COUPON_LIST, params, true);
    }

    public void getMoreCoupons(Context context) {
        if (!getHasMore()) {
            return;
        }

        mContext = context;

        GetCouponsRequest request = new GetCouponsRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = mLastSearchPagination.page + 1;
        pagination.count = PER_PAGE;

        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.COUPON_LIST, params, false);
    }

    @Override
    protected void onSuccess(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.COUPON_LIST)) {
            GetCouponsResponse response = new GetCouponsResponse();
            try {
                response.fromJson(jo);

                mLastSearchPagination = response.paginated;

                if (mLastSearchPagination.page == 1) {
                    if (mCoupons == null) {
                        mCoupons = new ArrayList<>();
                    } else {
                        mCoupons.clear();
                    }
                }

                if (response.data != null && response.data.size() > 0) {
                    mCoupons.addAll(response.data);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        super.onSuccess(url, jo, status);
    }

    @Override
    protected void onFail(String url, JSONObject jo, AjaxStatus status) {
        ToastView.showMessage(mContext, getLastResponseStatus().getErrorMessage());
    }
}
