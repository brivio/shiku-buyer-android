
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "addressdeleteRequest")
public class addressdeleteRequest extends Model {

    @Column(name = "address_id")
    public String address_id;

    @Column(name = "session")
    public SESSION session;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.address_id = jsonObject.optString("address_id");
        SESSION session = new SESSION();
        session.fromJson(jsonObject.optJSONObject("session"));
        this.session = session;
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        try {
            localItemObject.put("id", address_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }
}
