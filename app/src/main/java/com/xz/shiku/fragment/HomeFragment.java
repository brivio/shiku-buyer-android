package com.xz.shiku.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.protocol.AdItem;
import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.HomeData;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.adapter.Home_Sec2Adapter;
import com.xz.shiku.adapter.Home_Sec4Adapter;
import com.xz.shiku.controller.SearchController;
import com.xz.shiku.fragment.search.SearchFragment;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.view.abview.AbSlidingPlayView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class HomeFragment extends BaseShikuFragment {

    private boolean hadIntercept;

    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @InjectView(R.id.main_adlist_1)
    AbSlidingPlayView mAdlist1;
    @InjectView(R.id.main_adlist_2)
    GridView mAdlist2;
    @InjectView(R.id.main_adlist_3)
    TextView mAdlist3;
    @InjectView(R.id.main_adlist_3_image)
    ImageView mAdlist3Image;
    @InjectView(R.id.main_adlist_4)
    ListView mAdlist4;
    @InjectView(R.id.main_adlist_5_image)
    ImageView mAdlist5Image;
    @InjectView(R.id.main_adlist_5_text)
    TextView mAdlist5Text;

    Home_Sec2Adapter mAdlist2Adapter;
    Home_Sec4Adapter mAdlist4Adapter;

    /**
     * 首页轮播的界面的资源
     */
    ArrayList<View> allListAbSlidingPlayView;

    public HomeFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_home, container, false);

        ButterKnife.inject(this, v);

        //设置播放方式为顺序播放
        mAdlist1.setPlayType(1);
        mAdlist1.setPageLineHorizontalGravity(Gravity.CENTER_HORIZONTAL);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.bg_Main);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                SearchController.getInstance().getHomeData(getActivity());
            }
        });

        SearchController.getInstance().addResponseListener(this);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        }, 200);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SearchController.getInstance().getHomeData(getActivity());

            }
        }, 1200);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        SearchController.getInstance().removeResponseListener(this);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.HOME_DATA)) {
            initUI(SearchController.getInstance().getHomeDataResult());
            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    private void initUI(HomeData data) {
        // Ad list 1
        initAbSlidingPlayView(data.adList1);

        // Ad list 2 and 4
        initAdapterView(data.adList2, data.adList4);

        // Ad list 3
        AdItem adItem = data.adList3.size() >= 1 ? data.adList3.get(0) : null;
        if (adItem != null) {
            mAdlist3.setText(adItem.title);
            UILUtil.getInstance().getImage(getActivity(), mAdlist3Image, adItem.image);
            mAdlist3Image.setTag(adItem);
        }

        // Ad list 5
        adItem = data.adList5.size() >= 1 ? data.adList5.get(0) : null;
        if (adItem != null) {
            mAdlist5Text.setText(adItem.title);
            UILUtil.getInstance().getImage(getActivity(), mAdlist5Image, adItem.image);
        }
    }

    private void initAbSlidingPlayView(List<AdItem> adlist) {
        if (mAdlist1.getCount() > 0) {
            mAdlist1.removeAllViews();
        }
        if (allListAbSlidingPlayView != null) {
            allListAbSlidingPlayView.clear();
            allListAbSlidingPlayView = null;
        }
        allListAbSlidingPlayView = new ArrayList<>(adlist.size());
        LayoutInflater inflater = LayoutInflater.from(getActivity());

        for (int i = 0; i < adlist.size(); i++) {
            //导入ViewPager的布局
            View view = inflater.inflate(R.layout.tf_item_pic, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.pic_item);
            UILUtil.getInstance().getImage(getActivity(), imageView, adlist.get(i).image);
            allListAbSlidingPlayView.add(view);
        }
        mAdlist1.addViews(allListAbSlidingPlayView);
        //开始轮播
        mAdlist1.startPlay();
    }

    private void initAdapterView(List<AdItem> adList2, List<AdItem> adList4) {
        mAdlist2Adapter = new Home_Sec2Adapter(getActivity(), adList2);
        mAdlist2.setAdapter(mAdlist2Adapter);

        mAdlist4Adapter = new Home_Sec4Adapter(getActivity(), adList4);
        mAdlist4.setAdapter(mAdlist4Adapter);
    }

    @OnClick(R.id.index_search_edit)
    public void onSearch() {
        SearchFragment searchFragment = new SearchFragment();
        startActivityWithFragment(searchFragment);
    }
}