package com.xz.tframework;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.baidu.frontia.FrontiaApplication;
import com.xz.external.activeandroid.app.Application;
import com.xz.shiku.R;
import com.xz.tframework.activity.DebugCancelDialogActivity;
import com.xz.tframework.activity.DebugTabActivity;
import com.xz.tframework.utils.CustomExceptionHandler;

import java.io.File;


//需要定制的文件
/*

com.xz.tframework.model.BaseModel
创建的activity是MainActivity extends ActionBarActivity这样的。把后面的ActionBarActivity改成FragmentActivity，然后导包，把下面报错的地方删掉运行就不报错了。。。
*
* */

/**
 * Created by txj on 15/4/14.
 */
public class TFrameworkApp extends Application implements View.OnClickListener {
    private static TFrameworkApp instance;
    private ImageView bugImage;
    public Context currContext;

    private WindowManager wManager;
    private boolean flag = true;

    public Handler messageHandler;

    public static TFrameworkApp getInstance() {
        if (instance == null) {
            instance = new TFrameworkApp();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FrontiaApplication.initFrontiaApplication(this);
        initConfig();
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + TFrameworkConst.LOG_DIR_PATH;
        File storePath = new File(path);
        storePath.mkdirs();
        Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(
                path, null));
        instance = this;
    }

    void initConfig() {
    }

    public void showBug(final Context context) {
        TFrameworkApp.getInstance().currContext = context;
        wManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams wmParams = new WindowManager.LayoutParams();
        wmParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        wmParams.format = PixelFormat.RGBA_8888;
        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        wmParams.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        wmParams.x = 0;
        wmParams.y = 0;

        wmParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        if (bugImage != null) { //判断bugImage是否存在，如果存在则移除，必须加在 new ImageView(context) 前面
            wManager.removeView(bugImage);
        }

        bugImage = new ImageView(context);
        bugImage.setImageResource(R.drawable.tf_bug);

        wManager.addView(bugImage, wmParams);
        bugImage.setOnClickListener(this);

        bugImage.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                DebugCancelDialogActivity.parentHandler = messageHandler;
                Intent intent = new Intent(TFrameworkApp.getInstance().currContext, DebugCancelDialogActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                flag = false;
                return false;
            }
        });

        messageHandler = new Handler() {

            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    wManager.removeView(bugImage);
                    bugImage = null; // 必须要把bugImage清空，否则再次进入debug模式会与102行冲突
                }
            }
        };
    }

    public void onClick(View v) {
        if (flag != false) {
            Intent intent = new Intent(TFrameworkApp.getInstance().currContext, DebugTabActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        flag = true;
    }
}

