package com.xz.shiku.wxapi;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.xz.btc.AppConst;
import com.xz.shiku.R;
import com.xz.tframework.view.ToastView;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

    private static final String TAG = WXPayEntryActivity.class.getSimpleName();
    private IWXAPI api;
    private boolean mIsPaid = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wx_pay_result);


        api = WXAPIFactory.createWXAPI(this, AppConst.WX_APP_ID);

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        String payWith = intent.getStringExtra("payWith");
        Boolean isPaid = intent.getBooleanExtra("isPaid", false);

        if (payWith != null) {
            // 其他支付方式
            setPayResult(isPaid);
        } else {
            // 微信支付
            api.handleIntent(intent, this);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent(intent);
    }

    // 微信发送请求到第三方应用时，会回调到该方法
    @Override
    public void onReq(BaseReq req) {
        Log.w(TAG, "onReq: " + req);

        switch (req.getType()) {
            case ConstantsAPI.COMMAND_GETMESSAGE_FROM_WX:
                break;
            case ConstantsAPI.COMMAND_SHOWMESSAGE_FROM_WX:
                break;
            default:
                break;
        }
    }

    // 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法
    @Override
    public void onResp(BaseResp resp) {
        Log.d(TAG, "onResp from WeChat, errCode = " + resp.errCode);

        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            switch (resp.errCode) {
                case BaseResp.ErrCode.ERR_OK:
                    setPayResult(true);
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    finish();
                    break;
                case BaseResp.ErrCode.ERR_COMM:
                case BaseResp.ErrCode.ERR_AUTH_DENIED:
                case BaseResp.ErrCode.ERR_SENT_FAILED:
                case BaseResp.ErrCode.ERR_UNSUPPORT:
                    Log.w(TAG, "Failed to pay, erroMsg = " + resp.errStr);
                    setPayResult(false);
                    break;
            }

            if (mWXPayEntryListener != null) {
                mWXPayEntryListener.onPaid(resp.errCode == BaseResp.ErrCode.ERR_OK);
            }
        } else if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
            switch (resp.errCode) {
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                case BaseResp.ErrCode.ERR_AUTH_DENIED:
                    finish();
                    break;
                case BaseResp.ErrCode.ERR_OK:
                    break;
            }
        }
    }

    private void setPayResult(boolean isSuccessful) {
        mIsPaid = isSuccessful;

        /*
        if (isSuccessful) {
            ((TextView) findViewById(R.id.tv_pay_result_message)).setText("恭喜您，支付成功！");
            findViewById(R.id.icv_pay_result_success).setVisibility(View.VISIBLE);
            findViewById(R.id.icv_pay_result_failure).setVisibility(View.GONE);
        } else {
            ((TextView) findViewById(R.id.tv_pay_result_message)).setText("支付失败了唉……");
            findViewById(R.id.icv_pay_result_success).setVisibility(View.GONE);
            findViewById(R.id.icv_pay_result_failure).setVisibility(View.VISIBLE);
        }
        */

        if (mIsPaid) {
            ToastView.showMessage(this, "支付成功");
        } else {
            ToastView.showMessage(this, "支付失败");
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 200);
    }

    static OnWXPayEntryListener mWXPayEntryListener;

    public static void SetWXPayEntryListener(OnWXPayEntryListener l) {
        mWXPayEntryListener = l;
    }

    public interface OnWXPayEntryListener {
        public void onPaid(boolean isPaid);
    }
}