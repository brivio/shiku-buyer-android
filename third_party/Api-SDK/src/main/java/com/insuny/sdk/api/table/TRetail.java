package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TRetail{
    public String id;
    public String orderid;
    public String mid;
    public String item_id;
    public String item_name;
    public String nums;
    public String price;
    public String express;
    public String remark;
    public String add_time;
    public String status;
    public String pays;
    public String pays_data;
    public String pays_sn;
    public String pays_time;
    public String pays_price;
    public String pays_status;
}