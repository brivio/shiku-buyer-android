package com.xz.shiku.fragment.integral;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.model.IntegralModel;
import com.xz.btc.protocol.ApiInterface;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.adapter.integral.IntegralAdapter;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.SwipeListView;
import com.xz.tframework.view.ToastView;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import org.json.JSONObject;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link IntegralFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link IntegralFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IntegralFragment extends BackHandledFragment implements View.OnClickListener, AbOnItemClickListener, AdapterView.OnItemClickListener {
    private boolean hadIntercept;
    IntegralModel dataModel;
    private IntegralAdapter adapter;
    private SwipeListView listView;
    LinearLayout ll_empty;
    private int position;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IntegralFragment newInstance(String param1, String param2) {
        title = "积分数量";
        topRightText = "";
        IntegralFragment fragment = new IntegralFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public IntegralFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_integral, container, false);
        ((TextView) v.findViewById(R.id.wydh)).setOnClickListener(this);
        listView = (SwipeListView) v.findViewById(R.id.collect_list);
        ll_empty = (LinearLayout) v.findViewById(R.id.ll_empty);

        listView.setPullLoadEnable(true);
        listView.setRefreshTime();
        listView.setXListViewListener(this);

        if (null == dataModel) {
            dataModel = new IntegralModel(getActivity());
        }

        dataModel.addResponseListener(this);
        dataModel.getList();
//        ((TextView) v.findViewById(R.id.ll_empty_icon)).setText(getResources().getString(R.string.iconlove));
//        ((TextView) v.findViewById(R.id.ll_empty_text)).setText("您还没有喜欢的商品");
//        ((TextView) v.findViewById(R.id.ll_empty_subtext)).setText("主人快给我挑点宝贝吧");
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }


    public void setCollectList() {
        Resources resource = (Resources) getResources();
        String nof = resource.getString(R.string.no_favorite);
        String com = resource.getString(R.string.collect_compile);

        if (dataModel.mList.size() == 0) {
            ToastView toast = new ToastView(getActivity(), nof);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            if (adapter != null) {
                adapter.list = dataModel.mList;
                adapter.notifyDataSetChanged();
            }

//            ll_empty.setVisibility(View.VISIBLE);
//            listView.setVisibility(View.GONE);

        } else {
            listView.setVisibility(View.VISIBLE);
//            ll_empty.setVisibility(View.GONE);
            if (adapter == null) {
                adapter = new IntegralAdapter(getActivity(), dataModel.mList, listView.getRightViewWidth(), this);
                listView.setAdapter(adapter);
            } else {
                adapter.list = dataModel.mList;
                adapter.notifyDataSetChanged();
            }
            //adapter.parentHandler = messageHandler;
        }

    }


    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_INTEGRAL_LIST)) {
            listView.setRefreshTime();
            listView.stopRefresh();
            listView.stopLoadMore();
            if (dataModel.paginated.more == 0) {
                listView.setPullLoadEnable(false);
            } else {
                listView.setPullLoadEnable(true);
            }
            setCollectList();
        } else if (url.endsWith(ApiInterface.USER_INTEGRAL_DELETE)) {
            dataModel.mList.remove(position);
            adapter.list = dataModel.mList;
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dataModel.removeResponseListener(this);
    }

    public void onRefresh() {
        dataModel.getList();
    }

    @Override
    public void onLoadMore() {
        dataModel.getListMore();
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
//        Bundle bundle = new Bundle();
//        COLLECT_LIST c = (COLLECT_LIST) parent.getAdapter().getItem(position);
//        bundle.putString("productid", c.goods_id);
//        intent.putExtras(bundle);
//        startActivity(intent);
    }

    public void onClick(int position) {
        dataModel.itemDelete(String.format("%d", position));
        this.position = position;
        //ToastView.showMsg(getActivity(),"aaa");
    }

    public void onClick(View v) {
        Intent intent;
        Bundle bundle;
        switch (v.getId()) {
            case R.id.wydh:
                IntegralExchangeFragment fragment = IntegralExchangeFragment.newInstance("", "");
                startActivityWithFragment(fragment);
                break;
        }
    }
}
