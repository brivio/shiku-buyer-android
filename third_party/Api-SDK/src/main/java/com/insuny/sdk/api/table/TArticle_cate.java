package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TArticle_cate{
    public String id;
    public String type;
    public String name;
    public String alias;
    public String img;
    public String pid;
    public String spid;
    public String ordid;
    public String status;
    public String seo_title;
    public String seo_keys;
    public String seo_desc;
}