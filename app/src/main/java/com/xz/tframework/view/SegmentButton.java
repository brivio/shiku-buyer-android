package com.xz.tframework.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.xz.shiku.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 分段控件
 *
 * @ClassName: SegmentButton
 * @Description: Txj
 * @date: 2015年3月12日 下午11:13:55
 */
public class SegmentButton extends LinearLayout implements View.OnClickListener {

    public OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(int position, Button button) {

        }
    };

    public void setOnCheckedChangeListener(
            OnCheckedChangeListener onCheckedChangeListener) {
        this.onCheckedChangeListener = onCheckedChangeListener;
    }

    private Context mContext;
    private List<Button> mSegments;
    private int mSegmentCount;

    private String buttonContent;
    private String[] contentStr;
    private int position = 0;
    private int textPressColor;
    private int textNormalColor;
    private float textSize;

    public SegmentButton(Context context) {
        super(context);

        mContext = context;
    }

    public SegmentButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SegmentButton,
                0, 0);
        try {
            textPressColor = a.getColor(R.styleable.SegmentButton_textPressColor, R.color.white);
            textNormalColor = a.getColor(R.styleable.SegmentButton_textNormalColor, R.color.bg_Main);
            textSize = a.getFloat(R.styleable.SegmentButton_textSize, 16.0F);
            buttonContent = a.getString(R.styleable.SegmentButton_buttonContent);
        } finally {
            a.recycle();
        }

        if (buttonContent != null && buttonContent.length() > 0) {
            contentStr = buttonContent.split(";");
            if (contentStr != null) {
                mSegmentCount = contentStr.length;
            } else {
                mSegmentCount = 0;
            }
        } else {
            mSegmentCount = 0;
        }

        initView();
    }

    private void initView() {
        mSegments = new ArrayList<>();

        if (mSegmentCount == 1) {
            Button button = new Button(mContext);
            button.setBackgroundResource(R.drawable.tf_layoutwithstorkemainround);
            button.setTextSize(textSize);
            button.setTextColor(textPressColor);
            button.setGravity(Gravity.CENTER);
            button.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT));
            button.setTag(0);

            this.addView(button);
            button.setOnClickListener(this);
            mSegments.add(button);
        } else {
            for (int i = 0; i < mSegmentCount; i++) {
                Button button = new Button(mContext);
                if (i == 0) {
                    button.setBackgroundResource(R.drawable.segment_left_press);
                    button.setTextColor(textPressColor);
                } else if (i == mSegmentCount - 1) {
                    button.setBackgroundResource(R.drawable.segment_right_normal);
                    button.setTextColor(textNormalColor);
                } else {
                    button.setBackgroundResource(R.drawable.segment_middle_normal);
                    button.setTextColor(textNormalColor);
                }

                button.setTextSize(textSize);
                button.setGravity(Gravity.CENTER);
                button.setLayoutParams(new LayoutParams(0,
                        LayoutParams.MATCH_PARENT, 1));
                button.setTag(i);
                this.addView(button);
                button.setOnClickListener(this);
                mSegments.add(button);
            }
        }

        for (int i = 0; i < mSegmentCount; i++) {
            mSegments.get(i).setText(contentStr[i]);
        }
    }

    public int getPosition() {
        return position;
    }

    @Override
    public void onClick(View v) {
        setPressedButton((Integer) v.getTag());
    }

    public void setPressedButton(int p) {
        position = p;

        for (int i = 0; i < mSegmentCount; i++) {
            if (i == 0) {
                if (position == i) {
                    mSegments.get(i).setBackgroundResource(R.drawable.segment_left_press);
                    mSegments.get(i).setTextColor(textPressColor);
                    position = i;
                    onCheckedChangeListener.onCheckedChanged(i,
                            mSegments.get(i));
                } else {
                    mSegments.get(i).setBackgroundResource(R.drawable.segment_left_normal);
                    mSegments.get(i).setTextColor(textNormalColor);
                }
            } else if (i == mSegmentCount - 1) {
                if (position == i) {
                    mSegments.get(i).setBackgroundResource(R.drawable.segment_right_press);
                    mSegments.get(i).setTextColor(textPressColor);
                    position = i;
                    onCheckedChangeListener.onCheckedChanged(i,
                            mSegments.get(i));
                } else {
                    mSegments.get(i).setBackgroundResource(R.drawable.segment_right_normal);
                    mSegments.get(i).setTextColor(textNormalColor);
                }
            } else {
                if (position == i) {
                    mSegments.get(i).setBackgroundResource(R.drawable.segment_middle_press);
                    mSegments.get(i).setTextColor(textPressColor);
                    position = i;
                    onCheckedChangeListener.onCheckedChanged(i,
                            mSegments.get(i));
                } else {
                    mSegments.get(i).setBackgroundResource(R.drawable.segment_middle_normal);
                    mSegments.get(i).setTextColor(textNormalColor);
                }
            }
        }
    }

    public Button getSegmentButton(int position) {
        return mSegments.get(position);
    }

    public interface OnCheckedChangeListener {
        public void onCheckedChanged(int position, Button button);
    }
}
