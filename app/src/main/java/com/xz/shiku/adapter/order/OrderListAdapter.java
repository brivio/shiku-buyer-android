package com.xz.shiku.adapter.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xz.btc.protocol.GOODORDER;
import com.xz.btc.protocol.ORDER_GOODS_LIST;
import com.xz.shiku.R;
import com.xz.shiku.controller.OrderController;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class OrderListAdapter extends BaseAdapter {

    private List<OrderController.OrderListViewItem> mOrderListItems;
    private LayoutInflater mInflater;
    private Context mContext;

    public OrderListAdapter(Context context, List<OrderController.OrderListViewItem> listItems) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mOrderListItems = listItems;
    }

    public List<OrderController.OrderListViewItem> getOrderItems(int fromPosition, int count) {
        List<OrderController.OrderListViewItem> items = new ArrayList<>(count);

        for (int i = 0; i < count; i++) {
            items.add(mOrderListItems.get(fromPosition + i));
        }

        return items;
    }

    public void updateOrderStatus(int fromPosition) {
        int count = mOrderListItems.get(fromPosition).mOrder.goods_list.size() + 2;
        for (int i = 0; i < count; i++) {
            mOrderListItems.remove(fromPosition);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mOrderListItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mOrderListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return mOrderListItems.get(position).mItemType.getValue();
    }

    @Override
    public int getViewTypeCount() {
        return OrderController.ItemType.values().length;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        OrderController.OrderListViewItem item = mOrderListItems.get(position);
        ViewHolder holder;

        switch (item.mItemType) {
            case ITEM:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = mInflater.inflate(R.layout.item_checkout, parent, false);

                    holder.mProductImage = (ImageView) convertView.findViewById(R.id.shop_car_item_image);
                    holder.mProductTitle = (TextView) convertView.findViewById(R.id.shop_car_item_text);
                    holder.mProductPromotion = (TextView) convertView.findViewById(R.id.shop_car_item_property);
                    holder.mProductPrice = (TextView) convertView.findViewById(R.id.shop_car_item_price);
                    holder.mProductQuantity = (TextView) convertView.findViewById(R.id.shop_car_item_amount);

                    convertView.setTag(R.id.tag_first, holder);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                }

                ORDER_GOODS_LIST goods = (ORDER_GOODS_LIST) item.mTag;

                UILUtil.getInstance().getImage(mContext, holder.mProductImage, goods.goods_info.picture);

                holder.mProductTitle.setText(goods.goods_info.name);
                holder.mProductPromotion.setText(goods.goods_info.origin);
                holder.mProductPrice.setText(String.format("￥%.2f", Utils.tryParseDouble(goods.goods_info.shop_price, 0D)));
                holder.mProductQuantity.setText(String.format("×%d", Utils.tryParseInteger(goods.goods_number, 0)));
                break;
            case HEADER:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = mInflater.inflate(R.layout.item_orderlist_header, parent, false);

                    holder.mShopTitle = (TextView) convertView.findViewById(R.id.shop_car_item_text);
                    holder.mOrderStatus = (TextView) convertView.findViewById(R.id.shop_car_item_change);

                    convertView.setTag(R.id.tag_first, holder);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                }

                holder.mShopTitle.setText(item.mOrder.order_info.shop_title);
                holder.mOrderStatus.setText(OrderController.
                        OrderStatus.mapStringToValue(item.mOrder.order_info.status).getDescription());
                break;
            case FOOTER:
                OnClickListener listener;
                if (convertView == null) {
                    holder = new ViewHolder();
                    listener = new OnClickListener();
                    convertView = mInflater.inflate(R.layout.item_orderlist_footer, parent, false);

                    holder.mShippingRates = (TextView) convertView.findViewById(R.id.tv_total_yf);
                    holder.mOrderTotal = (TextView) convertView.findViewById(R.id.tv_real_pay);
                    holder.mOrderAmount = (TextView) convertView.findViewById(R.id.tv_total_count);

                    holder.mActionToView = (TextView) convertView.findViewById(R.id.action_to_view);
                    holder.mActionToClose = (TextView) convertView.findViewById(R.id.action_to_close);
                    holder.mActionToPay = (TextView) convertView.findViewById(R.id.action_to_pay);
                    holder.mActionToRemind = (TextView) convertView.findViewById(R.id.action_to_remind);
                    holder.mActionToTrack = (TextView) convertView.findViewById(R.id.action_to_track);
                    holder.mActionToConfirm = (TextView) convertView.findViewById(R.id.action_to_confirm);
                    holder.mActionToArchive = (TextView) convertView.findViewById(R.id.action_to_archive);

                    holder.mActionToView.setOnClickListener(listener);
                    holder.mActionToClose.setOnClickListener(listener);
                    holder.mActionToPay.setOnClickListener(listener);
                    holder.mActionToRemind.setOnClickListener(listener);
                    holder.mActionToTrack.setOnClickListener(listener);
                    holder.mActionToConfirm.setOnClickListener(listener);
                    holder.mActionToArchive.setOnClickListener(listener);

                    convertView.setTag(R.id.tag_first, holder);
                    convertView.setTag(holder.mActionToView.getId(), listener);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                    listener = (OnClickListener) convertView.getTag(holder.mActionToView.getId());
                }

                int orderAmount = 0;
                for (ORDER_GOODS_LIST g : item.mOrder.goods_list) {
                    orderAmount += Utils.tryParseInteger(g.goods_number, 0);
                }

                holder.mOrderAmount.setText(String.format("共%d件商品", orderAmount));
                holder.mShippingRates.setText(String.format("￥%.2f", item.mOrder.order_info.shipping_rates));
                holder.mOrderTotal.setText(String.format("￥%.2f", item.mOrder.order_info.total));
                setupOrderActions(holder, OrderController.OrderStatus.mapStringToValue(item.mOrder.order_info.status));
                listener.setPositionAndOrder(position, item.mOrder);
                break;
        }

        return convertView;
    }

    private void setupOrderActions(ViewHolder holder, OrderController.OrderStatus status) {
        holder.mActionToClose.setVisibility(View.GONE);
        holder.mActionToPay.setVisibility(View.GONE);
        holder.mActionToRemind.setVisibility(View.GONE);
        holder.mActionToTrack.setVisibility(View.GONE);
        holder.mActionToConfirm.setVisibility(View.GONE);
        holder.mActionToArchive.setVisibility(View.GONE);

        int padding;
        switch (status) {
            case UNPAID:
                holder.mActionToClose.setVisibility(View.VISIBLE);
                holder.mActionToPay.setVisibility(View.VISIBLE);
                break;
            case PAID:
                holder.mActionToRemind.setVisibility(View.VISIBLE);
                break;
            case SHIPPED:
                padding = mContext.getResources().getDimensionPixelSize(R.dimen.order_action_padding);
                holder.mActionToTrack.setBackgroundResource(R.drawable.tf_layoutwithstorkemainround);
                holder.mActionToTrack.setPadding(padding, padding, padding, padding);
                holder.mActionToTrack.setTextColor(mContext.getResources().getColor(R.color.bg_Main));
                holder.mActionToConfirm.setVisibility(View.VISIBLE);
                holder.mActionToTrack.setVisibility(View.VISIBLE);
                break;
            case DELIVERED:
                padding = mContext.getResources().getDimensionPixelSize(R.dimen.order_action_padding);
                holder.mActionToTrack.setBackgroundResource(R.drawable.tf_layoutwithstorkeround);
                holder.mActionToTrack.setPadding(padding, padding, padding, padding);
                holder.mActionToTrack.setTextColor(mContext.getResources().getColor(R.color.bg_Main_split));
                holder.mActionToTrack.setVisibility(View.VISIBLE);
                break;
        }
    }

    class OnClickListener implements View.OnClickListener {
        int mPosition;
        GOODORDER mOrder;

        @Override
        public void onClick(View v) {
            if (mListener == null) {
                return;
            }

            int viewId = v.getId();
            switch (viewId) {
                case R.id.action_to_view:
                    mListener.onView(mPosition, mOrder);
                    break;
                case R.id.action_to_close:
                    mListener.onClose(mPosition, mOrder);
                    break;
                case R.id.action_to_pay:
                    mListener.onPay(mPosition, mOrder);
                    break;
                case R.id.action_to_remind:
                    mListener.onRemind(mPosition, mOrder);
                    break;
                case R.id.action_to_track:
                    mListener.onTrack(mPosition, mOrder);
                    break;
                case R.id.action_to_confirm:
                    mListener.onConfirm(mPosition, mOrder);
                    break;
                case R.id.action_to_archive:
                    mListener.onArchive(mPosition, mOrder);
                    break;
            }
        }

        public void setPositionAndOrder(int position, GOODORDER order) {
            mPosition = position;
            mOrder = order;
        }
    }

    static class ViewHolder {
        ImageView mProductImage;
        TextView mShopTitle;
        TextView mOrderStatus;
        TextView mProductTitle;
        TextView mProductPromotion;
        TextView mProductPrice;
        TextView mProductQuantity;
        TextView mOrderAmount;
        TextView mShippingRates;
        TextView mOrderTotal;

        TextView mActionToView;
        TextView mActionToClose;
        TextView mActionToPay;
        TextView mActionToRemind;
        TextView mActionToTrack;
        TextView mActionToConfirm;
        TextView mActionToArchive;
    }

    public void setOnOrderActionListener(OnOrderActionListener l) {
        mListener = l;
    }

    private OnOrderActionListener mListener;

    public interface OnOrderActionListener {
        void onView(int position, GOODORDER order);

        void onClose(int position, GOODORDER order);

        void onPay(int position, GOODORDER order);

        void onRemind(int position, GOODORDER order);

        void onTrack(int position, GOODORDER order);

        void onConfirm(int position, GOODORDER order);

        void onArchive(int position, GOODORDER order);
    }
}
