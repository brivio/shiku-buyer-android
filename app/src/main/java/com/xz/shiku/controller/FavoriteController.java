package com.xz.shiku.controller;

import android.content.Context;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.COLLECT_LIST;
import com.xz.btc.protocol.PAGINATED;
import com.xz.btc.protocol.PAGINATION;
import com.xz.btc.protocol.SESSION;
import com.xz.btc.protocol.usercollectcreateRequest;
import com.xz.btc.protocol.usercollectdeleteRequest;
import com.xz.btc.protocol.usercollectlistRequest;
import com.xz.btc.protocol.usercollectlistResponse;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.view.ToastView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by XH on 2015/6/13.
 */
public class FavoriteController extends BaseModel {
    public static final int PER_PAGE = 10;

    private static FavoriteController mInstance;

    public static FavoriteController getInstance() {
        if (mInstance == null) {
            mInstance = new FavoriteController(TFrameworkApp.getInstance());
        }

        return mInstance;
    }

    private List<COLLECT_LIST> mFavorites;
    private PAGINATED mLastSearchPagination;
    private Object mTag;

    protected FavoriteController(Context context) {
        super(context);
    }

    public List<COLLECT_LIST> getFavoritesResult() {
        return mFavorites;
    }

    public boolean getIsFirstPage() {
        return (mLastSearchPagination != null &&
                mLastSearchPagination.page == 1);
    }

    public boolean getHasMore() {
        boolean hasMore = false;

        if (mLastSearchPagination != null) {
            hasMore = mLastSearchPagination.more > 0;
        }

        return hasMore;
    }

    public void getFavorites(Context context) {
        mContext = context;

        usercollectlistRequest request = new usercollectlistRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = 1;
        pagination.count = PER_PAGE;

        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.FAVORITE_LIST, params, true);
    }

    public void getMoreFavorites(Context context) {
        if (!getHasMore()) {
            return;
        }

        mContext = context;

        usercollectlistRequest request = new usercollectlistRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = mLastSearchPagination.page + 1;
        pagination.count = PER_PAGE;

        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.FAVORITE_LIST, params, true);
    }

    public void addToFavorites(Context context, int productId) {
        mContext = context;

        usercollectcreateRequest request = new usercollectcreateRequest();
        request.goods_id = productId;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.FAVORITE_ADD, params, true);
    }

    public void removeFromFavorites(Context context, int favId) {
        mContext = context;

        usercollectdeleteRequest request = new usercollectdeleteRequest();
        request.rec_id = String.format("%d", favId);

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        mTag = favId;

        ajax(ApiInterface.FAVORITE_REMOVE, params, true);
    }

    @Override
    protected void onSuccess(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.FAVORITE_LIST)) {
            usercollectlistResponse response = new usercollectlistResponse();
            try {
                response.fromJson(jo);

                mLastSearchPagination = response.paginated;

                if (mLastSearchPagination.page == 1) {
                    if (mFavorites == null) {
                        mFavorites = new ArrayList<>();
                    } else {
                        mFavorites.clear();
                    }
                }

                if (response.data != null && response.data.size() > 0) {
                    mFavorites.addAll(response.data);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (url.endsWith(ApiInterface.FAVORITE_REMOVE)) {
            int favId = (int) mTag;
            for (Iterator<COLLECT_LIST> iterator = mFavorites.iterator(); iterator.hasNext(); ) {
                COLLECT_LIST favItem = iterator.next();
                if (favItem.id == favId) {
                    iterator.remove();
                    break;
                }
            }
        }

        super.onSuccess(url, jo, status);
    }

    @Override
    protected void onFail(String url, JSONObject jo, AjaxStatus status) {
        ToastView.showMessage(mContext, getLastResponseStatus().getErrorMessage());
    }
}
