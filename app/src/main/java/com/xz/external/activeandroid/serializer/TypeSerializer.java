package com.xz.external.activeandroid.serializer;

/**
 * Created by txj on 15/4/14.
 */

public abstract class TypeSerializer {
	public abstract Class<?> getDeserializedType();

	public abstract Class<?> getSerializedType();

	public abstract Object serialize(Object data);

	public abstract Object deserialize(Object data);
}