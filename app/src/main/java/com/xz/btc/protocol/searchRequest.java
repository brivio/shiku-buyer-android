
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

@Table(name = "searchRequest")
public class searchRequest extends Model {

    @Column(name = "pagination")
    public PAGINATION pagination;

    @Column(name = "filter")
    public FILTER filter;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        PAGINATION pagination = new PAGINATION();
        pagination.fromJson(jsonObject.optJSONObject("pagination"));

        FILTER filter = new FILTER();
        filter.fromJson(jsonObject.optJSONObject("filter"));
        this.filter = filter;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();
        String tempKey;

        if (null != filter) {
            JSONObject jsonFilter = filter.toJson();
            Iterator iFilter = jsonFilter.keys();
            while (iFilter.hasNext()) {
                tempKey = (String)iFilter.next();
                localItemObject.put(tempKey, jsonFilter.get(tempKey));
            }
        }

        if (null != pagination) {
            JSONObject jsonPagination = pagination.toJson();
            Iterator iPagination = jsonPagination.keys();
            while (iPagination.hasNext()) {
                tempKey = (String)iPagination.next();
                localItemObject.put(tempKey, jsonPagination.get(tempKey));
            }
        }

        return localItemObject;
    }

}
