
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "USER")
public class USER extends Model {

    @Column(name = "USER_id", unique = true)
    public int id;

    @Column(name = "name")
    public String name;

    @Column(name = "tele")
    public String tele;

    @Column(name = "email")
    public String email;

    @Column(name = "sign")
    public String sign;

    @Column(name = "avatar")
    public String avatar;

    @Column(name = "sex")
    public int sex;

    @Column(name = "score")
    public int score;

    @Column(name = "status")
    public int status;

    @Column(name = "token")
    public String token;

    @Column(name = "coupon")
    public int coupon;

    @Column(name = "favourite")
    public int fav;

    @Column(name = "province")
    public String province;

    @Column(name = "city")
    public String city;

    @Column(name = "county")
    public String county;

    @Column(name = "address")
    public String address;

    @Column(name = "level")
    public int level;

    @Column(name = "msg_express")
    public boolean msg_express;

    @Column(name = "msg_sales")
    public boolean msg_sales;

    @Column(name = "msg_sys")
    public boolean msg_sys;

    @Column(name = "order_fahuo")
    public int order_fahuo;

    @Column(name = "order_fukuan")
    public int order_fukuan;

    @Column(name = "order_shouhuo")
    public int order_shouhuo;

    @Column(name = "weibo")
    public boolean weibo;

    @Column(name = "weixin")
    public boolean weixin;

    @Column(name = "qq")
    public boolean qq;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optInt("id", -1);
        this.name = jsonObject.optString("username");
        this.tele = jsonObject.optString("tele");
        this.email = jsonObject.optString("email");
        this.sign = jsonObject.optString("sign");
        this.avatar = jsonObject.optString("img");
        this.sex = jsonObject.optInt("sex");
        this.score = jsonObject.optInt("score");
        this.status = jsonObject.optInt("status");
        this.token = jsonObject.optString("token");
        this.coupon = jsonObject.optInt("quans");
        this.fav = jsonObject.getInt("favs");
        this.province = jsonObject.optString("province");
        this.city = jsonObject.optString("city");
        this.county = jsonObject.optString("area");
        this.address = jsonObject.optString("address");
        this.level = jsonObject.optInt("level");
        this.msg_express = jsonObject.optInt("msg_express", 0) == 1;
        this.msg_sales = jsonObject.optInt("msg_sales", 0) == 1;
        this.msg_sys = jsonObject.optInt("msg_sys", 0) == 1;
        this.order_fahuo = jsonObject.optInt("order_fahuo");
        this.order_fukuan = jsonObject.optInt("order_fukuan");
        this.order_shouhuo = jsonObject.optInt("order_shouhuo");
        this.weibo = jsonObject.optInt("weibo", 0) == 1;
        this.weixin = jsonObject.optInt("weixin", 0) == 1;
        this.qq = jsonObject.optInt("qq", 0) == 1;
    }

    public JSONObject toJson() throws JSONException {
        return toJson(true);
    }

    public JSONObject toJson(boolean updateableOnly) throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("username", name);
        localItemObject.put("email", email);
        localItemObject.put("sign", sign);
        localItemObject.put("sex", sex);

        if (!updateableOnly) {
            localItemObject.put("id", id);
            localItemObject.put("tele", tele);
            localItemObject.put("province", province);
            localItemObject.put("city", city);
            localItemObject.put("area", county);
            localItemObject.put("address", address);
        }

        return localItemObject;
    }
}
