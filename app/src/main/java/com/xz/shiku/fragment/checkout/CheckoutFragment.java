package com.xz.shiku.fragment.checkout;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.insuny.sdk.api.ApiClient;
import com.insuny.sdk.api.request.OrderAddRequest;
import com.insuny.sdk.api.response.OrderAddResponse;
import com.xz.btc.protocol.ADDRESS;
import com.xz.btc.protocol.ORDER_INFO;
import com.xz.btc.protocol.PAYMENT;
import com.xz.btc.protocol.SHIPPING;
import com.xz.shiku.R;
import com.xz.shiku.adapter.checkout.CheckoutAdapter;
import com.xz.shiku.controller.ShoppingCartController;
import com.xz.shiku.fragment.BaseShikuFragment;
import com.xz.shiku.fragment.address.AddressListFragment;
import com.xz.shiku.fragment.payment.PaymentFragment;
import com.xz.tframework.utils.Utils;
import com.xz.tframework.view.MyDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class CheckoutFragment extends BaseShikuFragment implements AddressListFragment.OnSelectAddress {

    private PAYMENT payment;
    private SHIPPING shipping;
    private MyDialog mDialog;
    private ORDER_INFO order_info;
    private final static int REQUEST_ADDRESS_LIST = 1;
    private final static int REQUEST_PAYMENT = 2;
    private final static int REQUEST_Distribution = 3;
    private final static int REQUEST_BONUS = 4;
    private final static int REQUEST_INVOICE = 5;
    private final static int REQUEST_RedEnvelope = 6;
    private final static int REQUEST_ALIPAY = 7;
    private final static int REQUEST_Pay_Web = 8;
    private final static int REQUEST_UPPay = 10;

    private List<ShoppingCartController.ShoppingCartListViewItem> mCartItems;
    private OrderAddRequest orderAddRequest = new OrderAddRequest();
    //region 控件声明
    @InjectView(R.id.checkout_cart_items)
    ListView cart_items_lv;

    //endregion
    public static CheckoutFragment newInstance() {
        title = "确认订单";
        topRightText = "";
        return new CheckoutFragment();
    }

    public CheckoutFragment setCartItems(List<ShoppingCartController.ShoppingCartListViewItem> items) {
        mCartItems = items;
        return this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        myView = inflater.inflate(R.layout.layout_checkout, null);
        ButterKnife.inject(this, myView);

        CheckoutAdapter checkoutAdapter = new CheckoutAdapter(getActivity(), mCartItems);
        cart_items_lv.setAdapter(checkoutAdapter);

        setTextView(R.id.checkout_totalGoodsPrice, String.format("¥%.2f", checkoutAdapter.getTotalPrice()));
        orderAddRequest.items = checkoutAdapter.getCartIds();

        return myView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ADDRESS_LIST) {
            if (data != null) {
//                shoppingCartModel.checkOrder();
            }
        } else if (requestCode == REQUEST_PAYMENT) {
            if (data != null) {
                String paymentString = data.getStringExtra("payment");
                try {
                    JSONObject paymentJSONObject = new JSONObject(paymentString);
                    payment = new PAYMENT();
                    payment.fromJson(paymentJSONObject);
//                    pay_type.setText(payment.pay_name);
                } catch (JSONException e) {

                }

            }
        } else if (requestCode == REQUEST_Distribution) {
            if (data != null) {
                String shippingString = data.getStringExtra("shipping");
                try {
                    JSONObject shippingJSONObject = new JSONObject(shippingString);
                    shipping = new SHIPPING();
//                    shipping.fromJson(shippingJSONObject);
//                    dis_type.setText(shipping.shipping_name);
//                    fees.setText(shipping.format_shipping_fee);
                } catch (JSONException e) {

                }
            }
        } else if (requestCode == REQUEST_BONUS) {
            if (data != null) {
//                scoreNum = data.getStringExtra("input");
//                Resources resource = (Resources)getActivity().getBaseContext().getResources();
//                String use=resource.getString(R.string.use);
//                String inte=resource.getString(R.string.score);
////                score_num.setText(use+scoreNum+inte);
//
//                scoreChangedMoney = data.getStringExtra("bonus");
//                scoreChangedMoneyFormated = data.getStringExtra("bonus_formated");
//
//                coupon.setText("-"+scoreChangedMoneyFormated);
//                refreshTotalPrice();
            }
        } else if (requestCode == REQUEST_INVOICE) {
            if (data != null) {
//                inv_type = data.getIntExtra("inv_type",0);
//                inv_content = data.getIntExtra("inv_content",0);
//                inv_payee = data.getStringExtra("inv_payee");
//                invoice_message.setText(inv_payee);
            }
        } else if (requestCode == REQUEST_RedEnvelope) {
            if (data != null) {
//                String bonusJSONString = data.getStringExtra("bonus");
//
//                if (null != bonusJSONString) {
//                    try {
//                        JSONObject jsonObject = new JSONObject(bonusJSONString);
//                        selectedBONUS = new  BONUS();
//                        selectedBONUS.fromJson(jsonObject);
//                        redPaper_name.setText(selectedBONUS.type_name + "[" + selectedBONUS.bonus_money_formated + "]");
//                        bonus_text.setText("-" + selectedBONUS.bonus_money_formated);
//                        refreshTotalPrice();
//                    } catch (JSONException e) {
//
//                    }
//                }

            }
        } else if (requestCode == REQUEST_UPPay) {
            if (data == null) {
                return;
            }
        /*
         * 支付控件返回字符串:success、fail、cancel
         *      分别代表支付成功，支付失败，支付取消
         */
            String str = data.getExtras().getString("pay_result");
            if (str.equalsIgnoreCase("success")) {
                Resources resource = getResources();
                String exit = resource.getString(R.string.pay_success);
                String exiten = resource.getString(R.string.continue_shopping_or_not);
                final MyDialog mDialog = new MyDialog(getActivity(), exit, exiten);
                mDialog.show();
                mDialog.positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        mDialog.dismiss();
//                        Intent it = new Intent(C1_CheckOutActivity.this, EcmobileMainActivity.class);
//                        startActivity(it);
//                        finish();

                    }
                });
                mDialog.negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        mDialog.dismiss();
//                        Intent intent = new Intent(C1_CheckOutActivity.this, E4_HistoryActivity.class);
//                        intent.putExtra("flag", "await_ship");
//                        startActivity(intent);
//                        finish();

                    }
                });
            } else if (str.equalsIgnoreCase("fail") || str.equals("cancel")) {
//                ToastView toast = new ToastView(C1_CheckOutActivity.this, getResources().getString(R.string.pay_failed));
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//                Intent intent = new Intent(C1_CheckOutActivity.this, E4_HistoryActivity.class);
//                intent.putExtra("flag", "await_pay");
//                startActivity(intent);
//                finish();
            }
        } else if (requestCode == REQUEST_ALIPAY) {
            if (data == null) {
                return;
            }
            String str = data.getExtras().getString("pay_result");
            if (str.equalsIgnoreCase("success")) {
                Resources resource = getResources();
                String exit = resource.getString(R.string.pay_success);
                String exiten = resource.getString(R.string.continue_shopping_or_not);
                final MyDialog mDialog = new MyDialog(getActivity(), exit, exiten);
                mDialog.show();
                mDialog.positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        mDialog.dismiss();
//                        Intent it = new Intent(C1_CheckOutActivity.this, EcmobileMainActivity.class);
//                        startActivity(it);
//                        finish();

                    }
                });
                mDialog.negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        mDialog.dismiss();
//                        Intent intent = new Intent(C1_CheckOutActivity.this, E4_HistoryActivity.class);
//                        intent.putExtra("flag", "await_ship");
//                        startActivity(intent);
//                        finish();

                    }
                });
            } else if (str.equalsIgnoreCase("fail")) {
//                ToastView toast = new ToastView(C1_CheckOutActivity.this, getResources().getString(R.string.pay_failed));
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//                Intent intent = new Intent(C1_CheckOutActivity.this, E4_HistoryActivity.class);
//                intent.putExtra("flag", "await_pay");
//                startActivity(intent);
//                finish();
            }
        } else if (requestCode == REQUEST_Pay_Web) {
            if (data == null) {
                return;
            }
            String str = data.getExtras().getString("pay_result");
            if (str.equalsIgnoreCase("success")) {
                Resources resource = getResources();
                String exit = resource.getString(R.string.pay_success);
                String exiten = resource.getString(R.string.continue_shopping_or_not);
                final MyDialog mDialog = new MyDialog(getActivity(), exit, exiten);
                mDialog.show();
                mDialog.positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        mDialog.dismiss();
//                        Intent it = new Intent(C1_CheckOutActivity.this, EcmobileMainActivity.class);
//                        startActivity(it);
//                        finish();

                    }
                });
                mDialog.negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        mDialog.dismiss();
//                        Intent intent = new Intent(C1_CheckOutActivity.this, E4_HistoryActivity.class);
//                        intent.putExtra("flag", "await_ship");
//                        startActivity(intent);
//                        finish();

                    }
                });
            } else if (str.equalsIgnoreCase("fail")) {
//                ToastView toast = new ToastView(getActivity(), getResources().getString(R.string.pay_failed));
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//                Intent intent = new Intent(getActivity(), E4_HistoryActivity.class);
//                intent.putExtra("flag", "await_pay");
//                startActivity(intent);
//                getActivity().finish();
            } else {

                Resources resource = getResources();
                String exit = resource.getString(R.string.pay_finished);
                String exiten = resource.getString(R.string.is_pay_success);
                final MyDialog mDialog = new MyDialog(getActivity(), exit, exiten);
                mDialog.show();
                mDialog.positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        mDialog.dismiss();
//                        Intent it = new Intent(C1_CheckOutActivity.this, EcmobileMainActivity.class);
//                        startActivity(it);
//                        finish();

                    }
                });
                mDialog.negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
//                        Intent intent = new Intent(C1_CheckOutActivity.this, E4_HistoryActivity.class);
//                        intent.putExtra("flag", "await_pay");
//                        startActivity(intent);
//                        getActivity().finish();

                    }
                });

            }
        }

    }

    @Override
    public void onSelect(ADDRESS address) {
        setTextView(R.id.item_address_user, address.consignee);
        setTextView(R.id.item_address_tele, address.tel);
        setTextView(R.id.item_address_address, address.address);

        hideView(R.id.ll_user_address);
        showView(R.id.ll_user_select_address);

        orderAddRequest.addr_name = address.consignee;
        orderAddRequest.addr_address = address.address;
        orderAddRequest.addr_province = address.province;
        orderAddRequest.addr_city = address.city;
        orderAddRequest.addr_area = address.district;
        orderAddRequest.addr_tele = address.tel;
        orderAddRequest.addr_zipcode = address.zipcode;
    }

    @OnClick({R.id.ll_user_address, R.id.ll_user_select_address})
    protected void OnSelectAddress() {
        AddressListFragment addressListFragment = AddressListFragment.newInstance(false);
        addressListFragment.mOnSelectAddressListener = this;
        startActivityWithFragment(addressListFragment);
    }

    @OnClick(R.id.tv_qrtj)
    protected void OnOrderConfirm() {
        if (orderAddRequest.addr_name == null) {
            toast("请选择收货地址");
            return;
        }
        apiClient.doOrderAdd(orderAddRequest, new ApiClient.OnSuccessListener() {
            @Override
            public void callback(Object object) {
                ORDER_INFO order_info = new ORDER_INFO();
                order_info.fromJson(Utils.jsonEncode(((OrderAddResponse) object).data));

                PaymentFragment fragment = PaymentFragment.newInstance(order_info);
                startActivityWithFragment(fragment);
                getActivity().finish();
            }
        });
    }

}
