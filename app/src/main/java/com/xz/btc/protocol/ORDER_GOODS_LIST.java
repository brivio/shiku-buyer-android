
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "ORDER_GOODS_LIST")
public class ORDER_GOODS_LIST extends Model {

    @Column(name = "goods_id")
    public String goods_id;

    @Column(name = "goods_number")
    public String goods_number;

    @Column(name = "name")
    public String name;

    @Column(name = "img")
    public PHOTO img;

    @Column(name = "formated_shop_price")
    public String formated_shop_price;

    @Column(name = "subtotal")
    public String subtotal;

    public SIMPLEGOODS goods_info;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }


        this.goods_number = jsonObject.optString("num");

        SIMPLEGOODS goods_info = new SIMPLEGOODS();
        goods_info.fromJson(jsonObject.optJSONObject("item"));
        this.goods_info = goods_info;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();
        JSONArray itemJSONArray = new JSONArray();
        localItemObject.put("goods_number", goods_number);
        localItemObject.put("goods_id", goods_id);
        localItemObject.put("name", name);
        if (null != img) {
            localItemObject.put("img", img.toJson());
        }
        localItemObject.put("formated_shop_price", formated_shop_price);
        localItemObject.put("subtotal", subtotal);
        return localItemObject;
    }

}
