
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@Table(name = "HOME_CATEGORY")
public class HOME_CATEGORY  extends Model
{

     @Column(name = "HOME_CATEGORY_id")
     public String id;

     @Column(name = "name")
     public String name;

    @Column(name = "img")
    public PHOTO img;

     public ArrayList<SIMPLEGOODS> goods = new ArrayList<SIMPLEGOODS>();

     public void  fromJson(JSONObject jsonObject)  throws JSONException
     {
          if(null == jsonObject){
            return ;
           }

          JSONArray subItemArray;

          this.id = jsonObject.optString("id");

          this.name = jsonObject.optString("name");

         PHOTO  img = new PHOTO();
         img.fromJson(jsonObject.optJSONObject("img"));
         this.img = img;

          subItemArray = jsonObject.optJSONArray("goods");
          if(null != subItemArray)
           {
              for(int i = 0;i < subItemArray.length();i++)
               {
                  JSONObject subItemObject = subItemArray.getJSONObject(i);
                  SIMPLEGOODS subItem = new SIMPLEGOODS();
                  subItem.fromJson(subItemObject);
                  this.goods.add(subItem);
               }
           }

          return ;
     }

     public JSONObject toJson() throws JSONException
     {
          JSONObject localItemObject = new JSONObject();
          JSONArray itemJSONArray = new JSONArray();
          localItemObject.put("id", id);
          localItemObject.put("name", name);

         if(null!=img)
         {
             localItemObject.put("img", img.toJson());
         }
          for(int i =0; i< goods.size(); i++)
          {
              SIMPLEGOODS itemData =goods.get(i);
              JSONObject itemJSONObject = itemData.toJson();
              itemJSONArray.put(itemJSONObject);
          }
          localItemObject.put("goods", itemJSONArray);
          return localItemObject;
     }

}
