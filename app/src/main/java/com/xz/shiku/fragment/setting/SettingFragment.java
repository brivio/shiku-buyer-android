package com.xz.shiku.fragment.setting;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.USER;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.controller.UserController;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.model.BusinessResponse;
import com.xz.tframework.view.MySwitch;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingFragment extends BackHandledFragment implements BusinessResponse {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    @InjectView(R.id.status_switch)
    MySwitch mTrackingInfo;
    @InjectView(R.id.status_switch_cxyh)
    MySwitch mPromotions;
    @InjectView(R.id.status_switch_xttz)
    MySwitch mSystemNotifications;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingFragment newInstance(String param1, String param2) {
        PopActivity.gShowNavigationBar = true;

        titleResId = R.string.title_fragment_setting;
        topRightText = "";

        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_setting, container, false);
        ButterKnife.inject(this, v);

        if (UserController.getInstance().isUserReady()) {
            initUI(UserController.getInstance().getUser());

            UserController.getInstance().addResponseListener(this);
        }

        return v;
    }

    private void initUI(USER user) {
        if (user != null) {
            mTrackingInfo.setChecked(user.msg_express);
            mPromotions.setChecked(user.msg_sales);
            mSystemNotifications.setChecked(user.msg_sys);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        UserController.getInstance().removeResponseListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_SETTINGS_UPDATE)) {
            if (UserController.getInstance().getLastResponseStatus().getIsSuccess()) {
                ToastView.showMessage(getActivity(), "设置成功");
            } else {
                ToastView.showMessage(getActivity(), "设置失败");
            }
        }
    }

    @OnClick(R.id.rl_yjfk)
    public void onFeedback() {
        FeedbackFragment messageFragment = FeedbackFragment.newInstance("", "");
        startActivityWithFragment(messageFragment);
    }

    @OnClick(R.id.signout)
    public void onSignout() {
        UserController.getInstance().signout();
        getActivity().finish();
    }

    @OnClick(R.id.status_switch)
    public void onCheckedTrackingInfo() {
        UserController.getInstance().updateUserSettings(getActivity(),
                UserController.SETTING_TRACKING_INFORMATION,
                mTrackingInfo.isChecked() ? "1" : "0");
    }

    @OnClick(R.id.status_switch_cxyh)
    public void onCheckedPromotions() {
        UserController.getInstance().updateUserSettings(getActivity(),
                UserController.SETTING_PROMOTIONS,
                mPromotions.isChecked() ? "1" : "0");
    }

    @OnClick(R.id.status_switch_xttz)
    public void onCheckedSystemNotifications() {
        UserController.getInstance().updateUserSettings(getActivity(),
                UserController.SETTING_SYSTEM_NOTIFICATION,
                mSystemNotifications.isChecked() ? "1" : "0");
    }
}
