package com.xz.external.imagezoom.utils;

public interface IDisposable {

	void dispose();
}
