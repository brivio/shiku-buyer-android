
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "SIMPLEGOODS")
public class SIMPLEGOODS extends Model {

    @Column(name = "SIMPLEGOODS_id", unique = true)
    public String id;

    @Column(name = "shop_price")
    public String shop_price;

    @Column(name = "market_price")
    public String market_price;

    @Column(name = "name")
    public String name;

    @Column(name = "img")
    public PHOTO img;

    @Column(name = "picture")
    public String picture;

    @Column(name = "brief")
    public String brief;

    @Column(name = "promote_price")
    public String promote_price;

    @Column(name = "goods_id")
    public String goods_id;

    @Column(name = "origin")
    public String origin;

    @Column(name = "sales")
    public String sales;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optString("id");
        this.name = jsonObject.optString("title");
        this.shop_price = jsonObject.optString("price");
        this.origin = jsonObject.optString("address");
        this.sales = jsonObject.optString("sales");
        this.picture = jsonObject.optString("img");

        this.market_price = jsonObject.optString("market_price");
        this.brief = jsonObject.optString("brief");
        this.promote_price = jsonObject.optString("promote_price");
        this.goods_id = jsonObject.optString("goods_id");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("id", id);
        localItemObject.put("shop_price", shop_price);
        localItemObject.put("market_price", market_price);
        localItemObject.put("name", name);
        if (null != img) {
            localItemObject.put("img", img.toJson());
        }
        localItemObject.put("brief", brief);
        localItemObject.put("promote_price", promote_price);
        localItemObject.put("goods_id", goods_id);

        return localItemObject;
    }
}
