package com.xz.shiku.controller;

import android.content.Context;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.MESSAGE;
import com.xz.btc.protocol.PAGINATED;
import com.xz.btc.protocol.PAGINATION;
import com.xz.btc.protocol.SESSION;
import com.xz.btc.protocol.usermessagedeleteRequest;
import com.xz.btc.protocol.usermessagelistRequest;
import com.xz.btc.protocol.usermessagelistResponse;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.view.ToastView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by XH on 2015/6/13.
 */
public class MessageController extends BaseModel {
    public static final int PER_PAGE = 10;

    private static MessageController mInstance;

    public static MessageController getInstance() {
        if (mInstance == null) {
            mInstance = new MessageController(TFrameworkApp.getInstance());
        }

        return mInstance;
    }

    private List<MESSAGE> mMessages;
    private PAGINATED mLastSearchPagination;
    private Object mTag;

    protected MessageController(Context context) {
        super(context);
    }

    public List<MESSAGE> getMessageResult() {
        return mMessages;
    }

    public boolean getIsFirstPage() {
        return (mLastSearchPagination != null &&
                mLastSearchPagination.page == 1);
    }

    public boolean getHasMore() {
        boolean hasMore = false;

        if (mLastSearchPagination != null) {
            hasMore = mLastSearchPagination.more > 0;
        }

        return hasMore;
    }

    public void getMessages(Context context) {
        mContext = context;

        usermessagelistRequest request = new usermessagelistRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = 1;
        pagination.count = PER_PAGE;

        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.USER_MESSAGE_LIST, params, true);
    }

    public void getMoreMessages(Context context) {
        if (!getHasMore()) {
            return;
        }

        mContext = context;

        usermessagelistRequest request = new usermessagelistRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = mLastSearchPagination.page + 1;
        pagination.count = PER_PAGE;

        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.USER_MESSAGE_LIST, params, true);
    }

    public void delete(Context context, int msgId) {
        mContext = context;

        usermessagedeleteRequest request = new usermessagedeleteRequest();
        request.rec_id = String.format("%d", msgId);

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        mTag = msgId;

        ajax(ApiInterface.USER_MESSAGE_DELETE, params, true);
    }

    @Override
    protected void onSuccess(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_MESSAGE_LIST)) {
            usermessagelistResponse response = new usermessagelistResponse();
            try {
                response.fromJson(jo);

                mLastSearchPagination = response.paginated;

                if (mLastSearchPagination.page == 1) {
                    if (mMessages == null) {
                        mMessages = new ArrayList<>();
                    } else {
                        mMessages.clear();
                    }
                }

                if (response.data != null && response.data.size() > 0) {
                    mMessages.addAll(response.data);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (url.endsWith(ApiInterface.USER_MESSAGE_DELETE)) {
            int msgId = (int) mTag;
            for (Iterator<MESSAGE> iterator = mMessages.iterator(); iterator.hasNext(); ) {
                MESSAGE msgItem = iterator.next();
                if (msgItem.id == msgId) {
                    iterator.remove();
                    break;
                }
            }
        }

        super.onSuccess(url, jo, status);
    }

    @Override
    protected void onFail(String url, JSONObject jo, AjaxStatus status) {
        ToastView.showMessage(mContext, getLastResponseStatus().getErrorMessage());
    }
}
