package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TItem_brand{
    public String id;
    public String title;
    public String img;
    public String address;
    public String tele;
    public String abst;
    public String info;
    public String hits;
    public String add_time;
    public String ordid;
    public String status;
    public String seo_title;
    public String seo_keys;
    public String seo_desc;
}