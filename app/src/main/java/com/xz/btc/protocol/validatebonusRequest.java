
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "validatebonusRequest")
public class validatebonusRequest  extends Model
{

     @Column(name = "session")
     public SESSION   session;

     @Column(name = "bonus_sn")
     public String bonus_sn;

     public void  fromJson(JSONObject jsonObject)  throws JSONException
     {
          if(null == jsonObject){
            return ;
           }

          JSONArray subItemArray;
          SESSION  session = new SESSION();
          session.fromJson(jsonObject.optJSONObject("session"));
          this.session = session;

          this.bonus_sn = jsonObject.optString("bonus_sn");
          return ;
     }

     public JSONObject toJson() throws JSONException
     {
          JSONObject localItemObject = new JSONObject();
          JSONArray itemJSONArray = new JSONArray();
          if(null != session)
          {
            localItemObject.put("session", session.toJson());
          }
          localItemObject.put("bonus_sn", bonus_sn);
          return localItemObject;
     }

}
