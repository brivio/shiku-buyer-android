<?php
return [
    'db'      => [
        'db_host'   => '127.0.0.1',
        'db_name'   => 'shiku',
        'db_user'   => 'root',
        'db_pwd'    => 'root',
        'db_prefix' => 'ins_',
    ],
    'url'     => 'http://115.28.219.106/shiku/buyerApi',
    'tables'  => [
        'item'        => [
            'item_cate'   => '/item_cate',
            'item_img?'   => '/item_img',
            'member_shop' => '/member_shop',
            'is_like','storage_label', 'days_label',
        ],
        'member_shop' => [
            'img', 'item_count', 'item_stock', 'follows','is_follow',
        ],
        'logs'        => [
            'class_name',
            'data_name',
            'item_name',
            'logs_name[]',
            'title_name',
        ],
    ],
    'modules' => [
        'pageInfo'        => [
            'page', 'perPage', 'totalPage'
        ],
        'SESSION'         => [
            'uid', 'token', 'username', 'member_type'
        ],
        'logs_class_item' => [
            'info', 'name', 'count', 'id'
        ],
        'logs_get_list'   => [
            'list?' => '/logs',
            '@pageInfo'
        ],
        'logs_get'        => [
            'class_list?' => '@logs_class_item',
            'id',
            'img',
            'title',
            'logs_list'   => '@logs_get_list'
        ],
    ],
    'api'     => [
        'item/follow'      => [
            'request'  => ['id'],
            'response' => [],
        ],
        'item/get'         => [
            'request'  => ['id'],
            'response' => '/item',
        ],
        'item/unfollow'    => [
            'request'  => ['id'],
            'response' => [],
        ],
        'logs/get'         => [
            'request'  => ['item_id', 'perPage', 'page'],
            'response' => '@logs_get'
        ],
        'order/add'        => [
            'request'  => ['items?' => 'Integer', 'quan_id', 'exress_type', 'pays', 'score',
                'addr_tele', 'addr_name', 'addr_province', 'addr_city', 'addr_area',
                'addr_address', 'addr_zipcode', 'remark'],
            'response' => '/order'
        ],
        'user/login'       => [
            'request'  => ['tele', 'password'],
            'response' => '/user'
        ],
        'user_like/add'    => [
            'request'  => ['item_id'],
            'response' => [],
        ],
        'user_like/delete' => [
            'request'  => ['item_id'],
            'response' => [],
        ],
    ]
];