package com.xz.tframework.adapter;

/**
 * Created by txj on 15/4/14.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

public abstract class TBaseAdapter extends BaseAdapter {
    protected LayoutInflater mInflater = null;
    protected Context mContext;

    public ArrayList dataList = new ArrayList();
    protected static final int TYPE_ITEM = 0;
    protected static final int TYPE_FOOTER = 1;
    protected static final int TYPE_HEADER = 2;

    public class TCellHolder {
        public int position;
    }

    public TBaseAdapter(Context c, ArrayList dataList) {
        mContext = c;
        mInflater = LayoutInflater.from(c);
        this.dataList = dataList;
    }

    protected abstract TCellHolder createCellHolder(View cellView);

    protected abstract View bindData(int position, View cellView, ViewGroup parent, TCellHolder h);

    public abstract View createCellView();

    /* (non-Javadoc)
     * @see android.widget.BaseAdapter#getViewTypeCount()
     */
    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }


    @Override
    public int getCount() {
        int count = dataList != null ? dataList.size() : 0;
        return count;
    }

    /* (non-Javadoc)
     * @see com.miyoo.lottery.adapter.HummerBaseAdapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        if (0 <= position && position < getCount()) {
            return dataList.get(position);
        } else {
            return null;
        }
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int position) {

        return 0;
    }

    protected View dequeueReuseableCellView(int position, View convertView,
                                            ViewGroup parent) {
        return null;
    }

    public void update(int newState) {
        notifyDataSetInvalidated();
    }


    /* (non-Javadoc)
     * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View cellView, ViewGroup parent) {

        TCellHolder holder = null;
        if (cellView == null) {
            cellView = createCellView();
            holder = createCellHolder(cellView);
            if (null != holder) {
                cellView.setTag(holder);
            }

        } else {
            holder = (TCellHolder) cellView.getTag();
            if (holder == null) {
                Log.v("lottery", "error");
            }
        }
        if (null != holder) {
            holder.position = position;
        }

        bindData(position, cellView, parent, holder);
        return cellView;
    }
}
