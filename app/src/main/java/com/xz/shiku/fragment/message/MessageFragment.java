package com.xz.shiku.fragment.message;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.MESSAGE;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.adapter.message.MessageAdapter;
import com.xz.shiku.controller.MessageController;
import com.xz.shiku.controller.UserController;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.SwipeListView;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MessageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MessageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MessageFragment extends BackHandledFragment implements AbOnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    MessageAdapter mMessageAdapter;
    @InjectView(R.id.collect_list)
    SwipeListView mMessages;
    @InjectView(R.id.ll_empty)
    LinearLayout mEmptyLayout;
    @InjectView(R.id.ll_empty_icon)
    TextView mEmptyIcon;
    @InjectView(R.id.ll_empty_text)
    TextView mEmptyText;
    @InjectView(R.id.ll_empty_subtext)
    TextView mEmptySubtext;

    private int position;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MessageFragment newInstance(String param1, String param2) {
        PopActivity.gShowNavigationBar = true;
        titleResId = R.string.title_fragment_message;
        topRightText = "";

        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MessageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_favorite, container, false);

        ButterKnife.inject(this, v);

        mEmptyIcon.setText(getResources().getString(R.string.iconmessage));
        mEmptyText.setText("您还没有任何消息");
        mEmptySubtext.setText("");

        if (UserController.getInstance().isUserReady()) {
            MessageController.getInstance().addResponseListener(this);
            MessageController.getInstance().getMessages(getActivity());
        } else {
            initUI(false);
        }

        return v;
    }

    private void initUI(boolean showMessageLayout) {
        if (showMessageLayout) {
            mEmptyLayout.setVisibility(View.GONE);
            mMessages.setPullLoadEnable(true);
            mMessages.setRefreshTime();
            mMessages.setXListViewListener(this);
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        MessageController.getInstance().removeResponseListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_MESSAGE_LIST)) {
            List<MESSAGE> messages = MessageController.getInstance().getMessageResult();
            if (MessageController.getInstance().getIsFirstPage()) {
                if (messages != null && messages.size() > 0) {
                    initUI(true);

                    mMessages.setRefreshTime();
                    mMessages.stopRefresh();

                    if (MessageController.getInstance().getHasMore()) {
                        mMessages.setPullLoadEnable(true);
                    } else {
                        mMessages.setPullLoadEnable(false);
                    }

                    mMessageAdapter = new MessageAdapter(getActivity(), messages,
                            mMessages.getRightViewWidth(), this);
                    mMessages.setAdapter(mMessageAdapter);
                } else {
                    initUI(false);
                }
            } else {
                mMessages.stopLoadMore();
                mMessageAdapter.notifyDataSetChanged();
            }
        } else if (url.endsWith(ApiInterface.USER_MESSAGE_DELETE)) {
            List<MESSAGE> messages = MessageController.getInstance().getMessageResult();
            if (messages != null && messages.size() > 0) {
                mMessageAdapter.notifyDataSetChanged();
            } else {
                initUI(false);
            }
        }
    }

    @Override
    public void onRefresh() {
        MessageController.getInstance().getMessages(getActivity());
    }

    @Override
    public void onLoadMore() {
        MessageController.getInstance().getMoreMessages(getActivity());
    }

    @Override
    public void onClick(int position) {
        MESSAGE message = (MESSAGE) mMessageAdapter.getItem(position);
        if (message != null) {
            MessageController.getInstance().delete(getActivity(), message.id);
        }
    }
}
