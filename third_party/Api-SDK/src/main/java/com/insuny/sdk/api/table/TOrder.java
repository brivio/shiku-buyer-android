package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TOrder{
    public String id;
    public String orderid;
    public String total;
    public String express;
    public String express_name;
    public String express_sn;
    public String express_remark;
    public String express_time;
    public String quan_id;
    public String quan_price;
    public String prices;
    public String score;
    public String remark;
    public String uid;
    public String uname;
    public String mid;
    public String mname;
    public String service_id;
    public String pays;
    public String pays_data;
    public String pays_time;
    public String pays_status;
    public String pays_price;
    public String pays_sn;
    public String addr_name;
    public String addr_tele;
    public String addr_province;
    public String addr_city;
    public String addr_area;
    public String addr_address;
    public String addr_zipcode;
    public String status;
    public String add_time;
    public String check_time;
    public String check_uid;
}