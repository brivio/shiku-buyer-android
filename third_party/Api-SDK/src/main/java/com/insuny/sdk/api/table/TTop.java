package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TTop{
    public String id;
    public String title;
    public String img;
    public String url;
    public String city_id;
    public String city_name;
    public String remark;
    public String stime;
    public String etime;
    public String add_time;
    public String update_time;
    public String uid;
    public String uname;
    public String is_hots;
    public String is_focus;
    public String ordid;
    public String status;
}