package com.xz.tframework.model;

import com.xz.external.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

public interface BusinessResponse {
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) throws JSONException;
}
