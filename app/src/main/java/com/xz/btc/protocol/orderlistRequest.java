
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

@Table(name = "orderlistRequest")
public class orderlistRequest extends Model {

    @Column(name = "session")
    public SESSION session;

    @Column(name = "type")
    public String type;

    @Column(name = "pagination")
    public PAGINATION pagination;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        SESSION session = new SESSION();
        session.fromJson(jsonObject.optJSONObject("session"));
        this.session = session;

        this.type = jsonObject.optString("status");
        PAGINATION pagination = new PAGINATION();
        pagination.fromJson(jsonObject.optJSONObject("pagination"));
        this.pagination = pagination;
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();
        String tempKey;

        try {
            if (type != null && type.length() > 0) {
                localItemObject.put("status", type);
            }

            if (null != pagination) {
                JSONObject jsonPagination = pagination.toJson();
                Iterator iPagination = jsonPagination.keys();
                while (iPagination.hasNext()) {
                    tempKey = (String) iPagination.next();
                    localItemObject.put(tempKey, jsonPagination.get(tempKey));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }

}
