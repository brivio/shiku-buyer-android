package com.xz.shiku.fragment.user;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserRegisterStep1Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserRegisterStep1Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserRegisterStep1Fragment extends BackHandledFragment {
    public static final String ACTION_REGISTER = "register";
    public static final String ACTION_RESET_PASSWORD = "reset_password";

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ACTION_TYPE = "action_type";

    private String mActionType;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    @InjectView(R.id.validcode)
    EditText mMobile;
    @InjectView(R.id.password)
    EditText mVerificationCode;
    @InjectView(R.id.btnnext)
    Button mNext;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param action The action that this fragment is take.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserRegisterStep1Fragment newInstance(String action) {
        if (action.equals(ACTION_REGISTER)) {
            titleResId = R.string.title_fragment_user_register_step1;
            topRightText = "";
        } else if (action.equals(ACTION_RESET_PASSWORD)) {
            titleResId = R.string.title_fragment_user_register_step1_alternative;
            topRightText = "";
        }

        UserRegisterStep1Fragment fragment = new UserRegisterStep1Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_ACTION_TYPE, action);
        fragment.setArguments(args);
        return fragment;
    }

    public UserRegisterStep1Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mActionType = getArguments().getString(ARG_ACTION_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_user_register1, container, false);
        ButterKnife.inject(this, v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.btnnext)
    public void onNext() {
        if (mMobile.length() != 11) {
            ToastView.showMessage(getActivity(), "您输入的手机号有误，请重新输入");
            return;
        }

        if (mActionType.equals(ACTION_RESET_PASSWORD)) {
            UserRestPswFragment userRestPswFragment = UserRestPswFragment.newInstance(
                    mMobile.getText().toString());
            addFragment(getActivity(), userRestPswFragment, this);
        } else {
            UserRegisterStep2Fragment userRegisterStep2Fragment = UserRegisterStep2Fragment.newInstance(
                    mMobile.getText().toString());
            addFragment(getActivity(), userRegisterStep2Fragment, this);
        }
    }

    @OnClick(R.id.sendVerificationCode)
    public void onSendVerificationCode() {
        Toast.makeText(getActivity(), "验证码已发送", Toast.LENGTH_SHORT)
                .show();
    }

    @OnTextChanged(value = {R.id.validcode, R.id.password}, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onTextChanged() {
        mNext.setEnabled(mMobile.length() > 0 && mVerificationCode.length() > 0);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
    }
}
