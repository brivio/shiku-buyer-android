package com.xz.btc;

import com.xz.external.androidquery.util.AQUtility;
import com.xz.shiku.BuildConfig;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.utils.UILUtil;

public class BtcApp extends TFrameworkApp {
    @Override
    public void onCreate() {
        super.onCreate();

        AQUtility.setDebug(BuildConfig.DEBUG);

        UILUtil.getInstance().init();
    }
}