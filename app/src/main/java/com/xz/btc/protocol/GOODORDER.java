
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@Table(name = "GOODORDER")
public class GOODORDER extends Model {

    @Column(name = "order_time")
    public String order_time;

    @Column(name = "total_fee")
    public String total_fee;

    @Column(name = "formated_integral_money")
    public String formated_integral_money;

    @Column(name = "formated_bonus")
    public String formated_bonus;

    @Column(name = "order_sn")
    public String order_sn;

    @Column(name = "order_id")
    public String order_id;

    @Column(name = "formated_shipping_fee")
    public String formated_shipping_fee;

    public GOODS_GROUPED_LIST goods_grouped_list = new GOODS_GROUPED_LIST();

    public ORDER_INFO order_info;
    public List<ORDER_GOODS_LIST> goods_list = new ArrayList<>();

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        JSONArray subItemArray;

        ORDER_INFO orderInfo = new ORDER_INFO();
        orderInfo.fromJson(jsonObject);
        this.order_info = orderInfo;

        subItemArray = jsonObject.optJSONArray("items");
        if (null != subItemArray) {
            for (int i = 0; i < subItemArray.length(); i++) {
                JSONObject subItemObject = subItemArray.getJSONObject(i);
                ORDER_GOODS_LIST subItem = new ORDER_GOODS_LIST();
                subItem.fromJson(subItemObject);
                this.goods_list.add(subItem);
            }
        }
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("order_time", order_time);
        localItemObject.put("total_fee", total_fee);

//     for(int i =0; i< goods_list.size(); i++)
//     {
//         GOODS_GROUPED_LIST itemData =goods_list.get(i);
        GOODS_GROUPED_LIST itemData = goods_grouped_list;
        JSONObject itemJSONObject = itemData.toJson();
//     itemJSONArray.put(itemJSONObject);
//     }
        localItemObject.put("goods_grouped_list", itemJSONObject);
        localItemObject.put("formated_integral_money", formated_integral_money);
        localItemObject.put("formated_bonus", formated_bonus);
        localItemObject.put("order_sn", order_sn);
        localItemObject.put("order_id", order_id);
        localItemObject.put("formated_shipping_fee", formated_shipping_fee);
        if (null != order_info) {
            localItemObject.put("order_info", order_info.toJson());
        }

        return localItemObject;
    }
}
