package com.xz.tframework.activity;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.xz.external.view.XListView;
import com.xz.tframework.adapter.ActivitylifeCycleAdapter;
import com.xz.tframework.model.ActivityManagerModel;
import com.xz.shiku.R;

/**
 * Created by txj on 15/4/14.
 */
public class ActivityLifeCycleActivity extends BaseActivity{
    XListView listView;
    ActivityManagerModel dataModel;
    ActivitylifeCycleAdapter listAdapter;
    TextView activityAll;
    TextView activityVisible;
    TextView activityForeground;
    TextView topview_title;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tf_activity_lifecycle_activity);

        topview_title = (TextView)findViewById(R.id.navigationbar_title);
        Resources resource = (Resources) getBaseContext().getResources();
        String lif=resource.getString(R.string.life_cycle);
        topview_title.setText(lif);


        listView = (XListView)findViewById(R.id.activitylist);
        dataModel = new ActivityManagerModel(this);
        listAdapter = new ActivitylifeCycleAdapter(this,dataModel.liveActivityList);

        listView.setAdapter(listAdapter);
        listView.setPullRefreshEnable(false);
        listView.setPullLoadEnable(false);

        activityAll = (TextView)findViewById(R.id.activity_all);
        activityAll.setTextColor(ColorStateList.valueOf(Color.RED));
        activityAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                activityAll.setTextColor(ColorStateList.valueOf(Color.RED));
                activityVisible.setTextColor(ColorStateList.valueOf(Color.BLACK));
                activityForeground.setTextColor(ColorStateList.valueOf(Color.BLACK));

                listAdapter.dataList = ActivityLifeCycleActivity.this.dataModel.liveActivityList;
                listAdapter.notifyDataSetChanged();
            }
        });
        activityVisible = (TextView)findViewById(R.id.activity_visible);
        activityVisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                activityAll.setTextColor(ColorStateList.valueOf(Color.BLACK));
                activityVisible.setTextColor(ColorStateList.valueOf(Color.RED));
                activityForeground.setTextColor(ColorStateList.valueOf(Color.BLACK));

                listAdapter.dataList = ActivityLifeCycleActivity.this.dataModel.visibleActivityList;
                listAdapter.notifyDataSetChanged();
            }
        });
        activityForeground = (TextView)findViewById(R.id.activity_foreground);
        activityForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                activityAll.setTextColor(ColorStateList.valueOf(Color.BLACK));
                activityVisible.setTextColor(ColorStateList.valueOf(Color.BLACK));
                activityForeground.setTextColor(ColorStateList.valueOf(Color.RED));

                listAdapter.dataList = ActivityLifeCycleActivity.this.dataModel.foregroundActivityList;
                listAdapter.notifyDataSetChanged();
            }
        });

    }
}
