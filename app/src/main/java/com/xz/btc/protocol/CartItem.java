
package com.xz.btc.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CartItem {

    public String shop_name;
    public String shop_id;
    public double subtotal;
    public List<CartItemProduct> products;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        shop_id = jsonObject.optString("mid");
        shop_name = jsonObject.optString("mname");
        subtotal = jsonObject.optDouble("amount");
        products = new ArrayList<>();

        JSONArray jsonArray = jsonObject.optJSONArray("item_list");
        if (jsonArray != null && jsonArray.length() > 0) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject subItemObject = jsonArray.getJSONObject(i);
                CartItemProduct product = new CartItemProduct();
                product.fromJson(subItemObject);
                products.add(product);
            }
        }
    }

    public JSONObject toJson() throws JSONException {
        return new JSONObject();
    }

}
