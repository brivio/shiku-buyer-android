
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "ProductVariant")
public class ProductVariant extends Model {

    @Column(name = "variant_id")
    public int id;

    @Column(name = "name")
    public String name;

    @Column(name = "cate_id")
    public int cate_id;

    @Column(name = "nums")
    public int nums;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optInt("id");
        this.name = jsonObject.optString("name");
        this.cate_id = jsonObject.optInt("cate_id");
        this.nums = jsonObject.optInt("nums");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("id", id);
        localItemObject.put("name", name);
        localItemObject.put("cate_id", cate_id);
        localItemObject.put("nums", nums);

        return localItemObject;
    }
}
