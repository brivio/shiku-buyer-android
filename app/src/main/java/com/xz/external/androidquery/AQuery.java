/**
 * Created by txj on 15/4/14.
 */

package com.xz.external.androidquery;

import android.app.Activity;
import android.content.Context;
import android.view.View;

/**
 * The tf_main class of AQuery. All methods are actually inherited from AbstractAQuery.
 *
 */
public class AQuery extends AbstractAQuery<AQuery>{

	
	public AQuery(Activity act) {
		super(act);
	}
	
	public AQuery(View view) {
		super(view);
	}
	
	public AQuery(Context context) {
		super(context);
	}
	
	public AQuery(Activity act, View root){
		super(act, root);
	}
	

}


