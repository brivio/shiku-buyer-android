package com.xz.shiku.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.xz.shiku.APP;
import com.xz.shiku.R;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.tinterface.BackHandledInterface;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;

public class PopActivity extends FragmentActivity implements BackHandledInterface {

    public static Boolean gShowNavigationBar = true;
    public static BackHandledFragment gCurrentFragment;
    public static PopActivityListener gPopActivityListener;

    private BackHandledFragment mBackHandedFragment;
    private boolean hadIntercept;

    @Optional
    @InjectView(R.id.top_menu_text_title)
    TextView mTitle;
    @Optional
    @InjectView(R.id.toprightbtn)
    TextView mTopRightButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(gShowNavigationBar ? R.layout.layout_pop_with_navigation_bar :
                R.layout.layout_pop_without_navigation_bar);

        ButterKnife.inject(this);

        addFragment(gCurrentFragment);
    }

    @Override
    public void setSelectedFragment(BackHandledFragment selectedFragment) {
        this.mBackHandedFragment = selectedFragment;
    }

    @Override
    public void onBackPressed() {
        if (mBackHandedFragment == null || !mBackHandedFragment.onBackPressed()) {
            if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }
    }

    /**
     * 添加Fragment *
     */
    public void addFragment(BackHandledFragment fragment) {
        if (gShowNavigationBar) {
            if (fragment.titleResId != 0) {
                mTitle.setText(fragment.titleResId);
            } else {
                mTitle.setText(fragment.title);
            }

            if (fragment.topRightTextResId != 0) {
                mTopRightButton.setText(fragment.topRightTextResId);
            } else {
                mTopRightButton.setText(fragment.topRightText);
            }
        }

        APP.getInstance().getFragments().push(fragment);
        APP.getInstance().getParentfragments().push(fragment);
        this.gCurrentFragment = fragment;
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        ft.add(R.id.container, fragment);
        ft.commitAllowingStateLoss();
        showFragment(fragment);
    }

    /**
     * 显示Fragment *
     */
    public void showFragment(Fragment fragment) {
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        // 设置Fragment的切换动画
        ft.setCustomAnimations(R.anim.tf_push_right_in, R.anim.tf_push_left_out);
        ft.show(fragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * 弹出Fragment *
     */
    public void popFragment(Fragment fragment) {
        hideFragment(fragment);
        removeFragment(fragment);
        BackHandledFragment pref = APP.getInstance().getLastParentfragment();
        mTitle.setText(pref.title);
        mTopRightButton.setText(pref.topRightText);
        showFragment(pref);
    }

    /**
     * 隐藏Fragment *
     */
    public void hideFragment(Fragment fragment) {
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        // 设置Fragment的切换动画
        ft.setCustomAnimations(R.anim.tf_push_right_in, R.anim.tf_push_left_out);
        ft.hide(fragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * 删除Fragment *
     */
    public void removeFragment(Fragment fragment) {
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        ft.hide(fragment);
        ft.remove(fragment);
        ft.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public interface PopActivityListener {
        public void onTopRightClick();
    }

    @Optional
    @OnClick(R.id.backbtn)
    public void onBack() {
        //当前需要销毁的fragment
        BackHandledFragment o = APP.getInstance().getFragments().pop();
        if (gCurrentFragment.equals(o)) {
            APP.getInstance().getParentfragments().pop();
            if (APP.getInstance().getParentfragments().size() > 0) {
                gCurrentFragment = APP.getInstance().getLastParentfragment();
            } else {
                gCurrentFragment = null;
            }
            finish();
        } else {
            popFragment(o);
        }
    }

    @Optional
    @OnClick(R.id.toprightbtn)
    public void onTopRightClick(View v) {
        if (gPopActivityListener != null) {
            gPopActivityListener.onTopRightClick();
        }
    }

}
