package com.xz.tframework.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.xz.shiku.R;

/**
 * Created by txj on 15/4/14.
 */
public class CrashLogDetailActivity extends BaseActivity
{
    TextView crashTimeTextView;
    TextView crashDetailTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tf_crash_log_detail_activity);

        crashTimeTextView = (TextView)findViewById(R.id.crash_time);
        crashDetailTextView = (TextView)findViewById(R.id.crash_detail);
        String crashTime = getIntent().getStringExtra("crash_time");
        if (null != crashTime)
        {
            crashTimeTextView.setText(crashTime);
        }

        String crashContent = getIntent().getStringExtra("crash_content");
        if (null != crashContent)
        {
            crashDetailTextView.setText(crashContent);
        }
    }
}
