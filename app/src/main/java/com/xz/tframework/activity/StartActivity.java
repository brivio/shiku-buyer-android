package com.xz.tframework.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

import com.umeng.analytics.MobclickAgent;
import com.xz.btc.activity.BTCMainActivity;
import com.xz.btc.activity.GalleryImageActivity;
import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.SESSION;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.BuildConfig;
import com.xz.shiku.R;
import com.xz.shiku.controller.ShoppingCartController;
import com.xz.shiku.controller.UserController;
import com.xz.tframework.model.BusinessResponse;
import com.xz.tframework.utils.PropertiesUtil;
import com.xz.tframework.utils.SharedPrefsUtil;

import org.json.JSONObject;

public class StartActivity extends Activity implements BusinessResponse {

    private static final int DURATION_SKIP = 100;
    private static final int DURATION_2000 = 2000;
    private static final int MSG_ANIMATION_IS_OVER = 1;
    private static final int MSG_USER_IS_RESTORED = 2;
    private static final int MSG_SHOPPING_CART_IS_RESTORED = 3;

    private boolean mIsAppReady;

    private Handler mHandler = new Handler() {
        int mActionCount = 0;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_ANIMATION_IS_OVER:
                    break;
                case MSG_USER_IS_RESTORED:
                case MSG_SHOPPING_CART_IS_RESTORED:
                    mActionCount++;
                    if (mActionCount == 2) {
                        mIsAppReady = true;
                    }
                    break;
            }
            if (mIsAppReady) {
                start();
            }
        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final View startView = View.inflate(this, R.layout.splash, null);
        setContentView(startView);

        MobclickAgent.openActivityDurationTrack(false);
        MobclickAgent.onError(this);

        mIsAppReady = false;

        //渐变
        AlphaAnimation aa = new AlphaAnimation(0.3f, 1.0f);
        aa.setDuration(BuildConfig.DEBUG ? DURATION_SKIP : DURATION_2000);
        startView.setAnimation(aa);
        aa.setAnimationListener(new AnimationListener() {

                                    @Override
                                    public void onAnimationStart(Animation animation) {
                                        initializeApp();
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        mHandler.sendEmptyMessage(MSG_ANIMATION_IS_OVER);
                                    }
                                }
        );
    }

    private void start() {
        Intent intent;
        if (SharedPrefsUtil.getInstance(getApplicationContext()).getIsFirstRun() || PropertiesUtil.getInstance().getShowGallery()) {
            intent = new Intent(this, GalleryImageActivity.class);
            SharedPrefsUtil.getInstance(getApplicationContext()).setFirstRunComplete();
        } else {
            intent = new Intent(this, BTCMainActivity.class);
        }

        startActivity(intent);
        finish();
    }

    private void initializeApp() {
        // 初始化用户信息
        String session = SharedPrefsUtil.getInstance(getApplicationContext()).getSession();

        if (session != null) {
            // 若为已登录用户，初始化购物车
            SESSION.getInstance().token = session;

            UserController.getInstance().addResponseListener(this);
            UserController.getInstance().getUser(this);
            ShoppingCartController.getInstance().addResponseListener(this);
            ShoppingCartController.getInstance().getItems(this, false);
        } else {
            mIsAppReady = true;
        }
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_INFO)) {
            mHandler.sendEmptyMessage(MSG_USER_IS_RESTORED);
        } else if (url.endsWith(ApiInterface.CART_LIST)) {
            mHandler.sendEmptyMessage(MSG_SHOPPING_CART_IS_RESTORED);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UserController.getInstance().removeResponseListener(this);
        ShoppingCartController.getInstance().removeResponseListener(this);
    }
}
