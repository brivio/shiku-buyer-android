
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@Table(name = "GOODS_GROUPED_LIST")
public class GOODS_GROUPED_LIST extends Model {

    public ArrayList<GOODS_LIST> goods_list = new ArrayList<GOODS_LIST>();

    @Column(name = "total")
    public TOTAL total;

    @Column(name = "name")
    public String name;

    @Column(name = "rec_id")
    public String rec_id;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        JSONArray subItemArray;
        TOTAL total = new TOTAL();
        total.fromJson(jsonObject.optJSONObject("total"));
        this.total = total;

        subItemArray = jsonObject.optJSONArray("goods_list");
        if (null != subItemArray) {
            for (int i = 0; i < subItemArray.length(); i++) {
                JSONObject subItemObject = subItemArray.getJSONObject(i);
                GOODS_LIST subItem = new GOODS_LIST();
                subItem.fromJson(subItemObject);
                this.goods_list.add(subItem);
            }
        }

        this.name = jsonObject.optString("name");
        this.rec_id = jsonObject.optString("rec_id");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();
        JSONArray itemJSONArray = new JSONArray();
        if (null != total) {
            localItemObject.put("total", total.toJson());
        }

        for (int i = 0; i < goods_list.size(); i++) {
            GOODS_LIST itemData = goods_list.get(i);
            JSONObject itemJSONObject = itemData.toJson();
            itemJSONArray.put(itemJSONObject);
        }
        localItemObject.put("goods_list", itemJSONArray);
        localItemObject.put("name", name);
        localItemObject.put("rec_id", rec_id);

        return localItemObject;
    }
}
