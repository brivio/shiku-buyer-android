package com.xz.shiku.adapter.farmer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xz.btc.protocol.Farmer;
import com.xz.shiku.R;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import java.text.DecimalFormat;
import java.util.List;

public class FarmerAdapter extends BaseAdapter {

    private Context mContext;
    private List<Farmer> mFarmers;
    private LayoutInflater mInflater;
    private int mRightWidth = 0;
    private AbOnItemClickListener abOnItemClickListener;

    public FarmerAdapter(Context context, List<Farmer> farmers, int rightWidth, AbOnItemClickListener l) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mFarmers = farmers;
        mRightWidth = rightWidth;
        abOnItemClickListener = l;
    }

    @Override
    public int getCount() {
        return mFarmers != null ? mFarmers.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mFarmers != null ? mFarmers.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        OnFarmerActionListener listener;
        ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            listener = new OnFarmerActionListener();

            view = mInflater.inflate(R.layout.item_farmer, parent, false);

            holder.rl_left = (RelativeLayout) view.findViewById(R.id.rl_left);
            holder.rl_right = (RelativeLayout) view.findViewById(R.id.rl_right);
            holder.icon = (ImageView) view.findViewById(R.id.imageview);
            holder.username = (TextView) view.findViewById(R.id.username);
            holder.shopname = (TextView) view.findViewById(R.id.shopname);
            holder.yjhl = (TextView) view.findViewById(R.id.yjhl);
            holder.mcl = (TextView) view.findViewById(R.id.mcl);
            holder.delbtn = (TextView) view.findViewById(R.id.activity_favorite_item_delbtn);
            holder.delbtn.setOnClickListener(listener);

            LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            holder.rl_left.setLayoutParams(lp1);
            LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(mRightWidth,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            holder.rl_right.setLayoutParams(lp2);

            view.setTag(holder);
            view.setTag(holder.delbtn.getId(), listener);
        } else {
            holder = (ViewHolder) view.getTag();
            listener = (OnFarmerActionListener) view.getTag(holder.delbtn.getId());

            if (view.getScrollX() > 0) {
                view.scrollTo(0, 0);
            }
        }

        Farmer farmer = mFarmers.get(position);

        if (farmer.image != null && farmer.image.length() > 0) {
            UILUtil.getInstance().getImage(mContext, holder.icon, farmer.image);
        } else {
            holder.icon.setImageResource(R.drawable.ic_placeholder_black);
        }
        holder.username.setText(farmer.name);
        holder.shopname.setText(String.format("店铺名称：%s", farmer.farmName));
        holder.yjhl.setText(new DecimalFormat("有机含量：#.## mg/平方米").format(farmer.organicContent));
        holder.mcl.setText(new DecimalFormat("亩产量：#.## KG").format(farmer.yield));
        listener.setPosition(position);

        return view;
    }

    class OnFarmerActionListener implements View.OnClickListener {

        int mPosition;

        @Override
        public void onClick(View view) {
            if (abOnItemClickListener != null) {
                abOnItemClickListener.onClick(mPosition);
            }
        }

        public void setPosition(int position) {
            mPosition = position;
        }
    }

    static class ViewHolder {
        RelativeLayout rl_left;
        RelativeLayout rl_right;

        ImageView icon;
        TextView username;
        TextView shopname;
        TextView yjhl;
        TextView mcl;
        TextView delbtn;
    }
}
