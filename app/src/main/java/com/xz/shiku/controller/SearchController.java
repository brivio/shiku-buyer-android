package com.xz.shiku.controller;

import android.content.Context;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.CATEGORY;
import com.xz.btc.protocol.FILTER;
import com.xz.btc.protocol.GOODS;
import com.xz.btc.protocol.GetHomeDataResponse;
import com.xz.btc.protocol.HomeData;
import com.xz.btc.protocol.PAGINATED;
import com.xz.btc.protocol.PAGINATION;
import com.xz.btc.protocol.SESSION;
import com.xz.btc.protocol.SIMPLEGOODS;
import com.xz.btc.protocol.categoryResponse;
import com.xz.btc.protocol.goodsRequest;
import com.xz.btc.protocol.goodsResponse;
import com.xz.btc.protocol.searchRequest;
import com.xz.btc.protocol.searchResponse;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.view.ToastView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchController extends BaseModel {

    public static final int PER_PAGE = 10;

    private static SearchController mInstance;

    public static SearchController getInstance() {
        if (mInstance == null) {
            mInstance = new SearchController(TFrameworkApp.getInstance());
        }

        return mInstance;
    }

    private List<CATEGORY> mCategories;
    private List<SIMPLEGOODS> mProducts;
    private PAGINATED mLastSearchPagination;
    private GOODS mProduct;
    private HomeData mHomeData;

    protected SearchController(Context context) {
        super(context);
    }

    public List<CATEGORY> getCategoryResult() {
        return mCategories;
    }

    public List<SIMPLEGOODS> getProductResult() {
        return mProducts;
    }

    public GOODS getProductDetailsResult() {
        return mProduct;
    }

    public HomeData getHomeDataResult() {
        return mHomeData;
    }

    public boolean getIsFirstPage() {
        return (mLastSearchPagination != null &&
                mLastSearchPagination.page == 1);
    }

    public boolean getHasMore() {
        boolean hasMore = false;

        if (mLastSearchPagination != null) {
            hasMore = mLastSearchPagination.more > 0;
        }

        return hasMore;
    }

    public void getCategory(Context context) {
        mContext = context;

        Map<String, String> params = new HashMap<>();
        params.put("data", new JSONArray().toString());

        ajax(ApiInterface.CATEGORY, params, true);
    }

    public void getProduct(Context context, FILTER filter) {
        mContext = context;
        mLastSearchPagination = null;

        searchRequest request = new searchRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = 1;
        pagination.count = PER_PAGE;

        request.filter = filter;
        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();
        try {
            params.put("data", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ajax(ApiInterface.SEARCH, params, true);
    }

    public void getMoreProduct(Context context, FILTER filter) {
        if (!getHasMore()) {
            return;
        }

        mContext = context;

        searchRequest request = new searchRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = mLastSearchPagination.page + 1;
        pagination.count = PER_PAGE;

        request.filter = filter;
        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();
        try {
            params.put("data", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ajax(ApiInterface.SEARCH, params, true);
    }

    public void getProductDetails(Context context, int productId) {
        mContext = context;

        goodsRequest request = new goodsRequest();
        request.goods_id = productId;

        Map<String, String> params = new HashMap<>();
        params.put("data", request.toJson().toString());
        params.put("token", SESSION.getInstance().token);

        ajax(ApiInterface.PRODUCT_DETAILS, params, true);
    }

    public void getHomeData(Context context) {
        mContext = context;

        Map<String, String> params = new HashMap<>();
        params.put("token", SESSION.getInstance().token);

        ajax(ApiInterface.HOME_DATA, params, false);
    }

    @Override
    protected void onSuccess(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.CATEGORY)) {
            try {
                categoryResponse response = new categoryResponse();
                response.fromJson(jo);

                mCategories = response.data;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (url.endsWith(ApiInterface.SEARCH)) {
            try {
                searchResponse response = new searchResponse();
                response.fromJson(jo);

                mLastSearchPagination = response.paginated;

                if (mLastSearchPagination.page == 1) {
                    if (mProducts == null) {
                        mProducts = new ArrayList<>();
                    } else {
                        mProducts.clear();
                    }
                }
                if (response.data != null && response.data.size() > 0) {
                    mProducts.addAll(response.data);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (url.endsWith(ApiInterface.PRODUCT_DETAILS)) {
            goodsResponse response = new goodsResponse();
            try {
                response.fromJson(jo);

                mProduct = response.data;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (url.endsWith(ApiInterface.HOME_DATA)) {
            GetHomeDataResponse response = new GetHomeDataResponse();
            response.fromJson(jo);

            mHomeData = response.data;
        }

        super.onSuccess(url, jo, status);
    }

    @Override
    protected void onFail(String url, JSONObject jo, AjaxStatus status) {
        ToastView.showMessage(mContext, getLastResponseStatus().getErrorMessage());

        super.onFail(url, jo, status);
    }
}
