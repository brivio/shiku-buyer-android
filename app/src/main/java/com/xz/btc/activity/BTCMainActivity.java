package com.xz.btc.activity;
/**
 * Created by txj on 15/4/14.
 */

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.KeyEvent;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.model.BeeQuery;
import com.xz.tframework.tinterface.BackHandledInterface;
import com.xz.tframework.view.ToastView;
import com.xz.btc.BTCManager;
import com.xz.shiku.R;
import com.umeng.analytics.MobclickAgent;

public class BTCMainActivity extends FragmentActivity implements BackHandledInterface {
    public static final String RESPONSE_METHOD = "method";
    public static final String RESPONSE_CONTENT = "content";
    public static final String RESPONSE_ERRCODE = "errcode";
    public static final String ACTION_MESSAGE = "com.baiud.pushdemo.action.MESSAGE";
    public static final String ACTION_RESPONSE = "bccsclient.action.RESPONSE";
    public static final String ACTION_PUSHCLICK = "bccsclient.action.PUSHCLICK";
    public static final String ACTION_SHOW_MESSAGE = "bccsclient.action.SHOW_MESSAGE";
    public static final String EXTRA_MESSAGE = "message";
    public static final String CUSTOM_CONTENT = "CustomContent";
    // 在百度开发者中心查询应用的API Key
    public static String API_KEY;

    protected static final String ACTION_LOGIN = "com.baidu.pushdemo.action.LOGIN";
    protected static final String EXTRA_ACCESS_TOKEN = "access_token";

    private BackHandledFragment mBackHandedFragment;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tf_main);

        Intent intent = new Intent();
        intent.setAction("com.Framework.NetworkStateService");
        intent.setPackage(getPackageName());
        startService(intent);

        if (getIntent().getStringExtra(CUSTOM_CONTENT) != null) {
            pushIntent(getIntent().getStringExtra(CUSTOM_CONTENT));
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // 如果要统计Push引起的用户使用应用情况，请实现本方法，且加上这一个语句
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();

        if (ACTION_RESPONSE.equals(action)) {

        } else if (ACTION_LOGIN.equals(action)) {

        } else if (ACTION_MESSAGE.equals(action)) {

        } else if (ACTION_PUSHCLICK.equals(action)) {
            String message = intent.getStringExtra(CUSTOM_CONTENT);
            pushIntent(message);
        }
    }

    public void pushIntent(String message) {
        if (message != null) {
//        	try
//            {
//                JSONObject jsonObject = new JSONObject(message);
//                String actionString = jsonObject.optString("a");
//                if (0 == actionString.compareTo("s")) {
//                    String parameter = jsonObject.optString("k");
//                    if (null != parameter && parameter.length() > 0) {
//                    	try {
//							parameter = URLDecoder.decode(parameter, "UTF-8");
//						} catch (UnsupportedEncodingException e1) {
//
//							e1.printStackTrace();
//						}
//                        Intent it = new Intent(this, B1_ProductListActivity.class);
//                        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        FILTER filter = new FILTER();
//                        filter.keywords =parameter;
//                        try {
//                            it.putExtra(B1_ProductListActivity.FILTER,filter.toJson().toString());
//                        } catch (JSONException e) {
//                        	e.printStackTrace();
//                        }
//                        startActivity(it);
//                    }
//                } else if(0 == actionString.compareTo("w")) {
//                	String parameter = jsonObject.optString("u");
//                	if (null != parameter && parameter.length() > 0) {
//                		try {
//							parameter = URLDecoder.decode(parameter, "UTF-8");
//						} catch (UnsupportedEncodingException e1) {
//
//							e1.printStackTrace();
//						}
//                        Intent it = new Intent(this, BannerWebActivity.class);
//                        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        it.putExtra("url",parameter);
//                        startActivity(it);
//                    }
//                }
//            } catch (JSONException e) {
//
//            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        API_KEY = BTCManager.getPushKey(this);
        PushManager.activityStarted(this);
        PushManager.startWork(getApplicationContext(), PushConstants.LOGIN_TYPE_API_KEY, API_KEY);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (BTCManager.getUmengKey(this) != null) {
            MobclickAgent.onResume(this, BTCManager.getUmengKey(this), "");
        }
    }

    private boolean isExit = false;

    //退出操作
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isExit == false) {
                isExit = true;
                Resources resource = (Resources) getBaseContext().getResources();
                String exit = resource.getString(R.string.again_exit);
                ToastView toast = new ToastView(getApplicationContext(), exit);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                handler.sendEmptyMessageDelayed(0, 3000);
                if (BeeQuery.environment() == BeeQuery.ENVIROMENT_DEVELOPMENT) {
                    TFrameworkApp.getInstance().showBug(this);
                }

                return true;
            } else {
                Intent intent = new Intent();
                intent.setAction("com.TFramework.NetworkStateService");
                stopService(intent);
                finish();
                ToastView.hide();
                return false;
            }
        }
        return true;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };


    @Override
    protected void onStop() {
        super.onStop();
        PushManager.activityStoped(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BTCManager.getUmengKey(this) != null) {
            MobclickAgent.onPause(this);
        }
    }

    @Override
    public void setSelectedFragment(BackHandledFragment selectedFragment) {
        this.mBackHandedFragment = selectedFragment;
    }

    @Override
    public void onBackPressed() {
        if (mBackHandedFragment == null || !mBackHandedFragment.onBackPressed()) {
            if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                super.onBackPressed();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }
    }
}
