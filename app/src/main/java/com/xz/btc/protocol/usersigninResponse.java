
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "usersigninResponse")
public class usersigninResponse extends Model {

    @Column(name = "status")
    public STATUS status;

    @Column(name = "data")
    public SIGNIN_DATA data;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        STATUS status = new STATUS();
        status.fromJson(jsonObject);
        this.status = status;

        SIGNIN_DATA data = new SIGNIN_DATA();
        data.fromJson(jsonObject.optJSONObject("data"));
        this.data = data;
    }

    public JSONObject toJson() throws JSONException {
        throw new JSONException("Not implemented");
    }
}
