package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TMessage{
    public String id;
    public String from_uid;
    public String from_uname;
    public String to_uid;
    public String to_uname;
    public String add_time;
    public String title;
    public String info;
    public String status;
}