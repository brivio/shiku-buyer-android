package com.xz.shiku.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xz.btc.protocol.AdItem;
import com.xz.shiku.R;
import com.xz.tframework.utils.UILUtil;

import java.util.List;

public class Home_Sec4Adapter extends BaseAdapter {

    List<AdItem> mAdlist;
    Context mContext;
    public Home_Sec4Adapter(Context c, List<AdItem> adlist) {
        mContext = c;
        mAdlist= adlist;
    }

    @Override
    public int getCount() {
        return mAdlist != null ? mAdlist.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mAdlist != null ? mAdlist.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_home_sec4, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.titleview);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageview);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        AdItem adItem = mAdlist.get(position);

        UILUtil.getInstance().getImage(mContext, holder.imageView, adItem.image);

        return convertView;
    }

    static class ViewHolder {
        TextView textView;
        TextView priceView;
        ImageView imageView;
    }
}
