package com.xz.shiku.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.ProductOptions;
import com.xz.btc.protocol.ProductVariant;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.adapter.search.ProductVariantsAdapter;
import com.xz.shiku.controller.ShoppingCartController;
import com.xz.tframework.model.BusinessResponse;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.view.NumberPicker;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ProductOptionsActivity extends Activity implements BusinessResponse {

    public static final String ARG_PRODUCT_OPTIONS = "product_options";

    ProductVariantsAdapter mProductVariantsAdapter;
    ProductOptions mProductOptions;

    @InjectView(R.id.activity_product_details_price)
    TextView mProductPrice;
    @InjectView(R.id.product_stock)
    TextView mProductStock;
    @InjectView(R.id.product_feature_image)
    ImageView mProductImage;
    @InjectView(R.id.product_variants)
    GridView mProductVariants;
    @InjectView(R.id.product_quantity)
    NumberPicker mProductQuantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.tf_push_up_in, R.anim.tf_stay_still);

        mProductOptions = new ProductOptions();
        mProductOptions.fromJson(getIntent().getStringExtra(ARG_PRODUCT_OPTIONS));

        setContentView(R.layout.activity_product_options);

        ButterKnife.inject(this);

        initUI(mProductOptions);

        ShoppingCartController.getInstance().addResponseListener(this);
    }

    private void initUI(ProductOptions options) {
        UILUtil.getInstance().getImage(this, mProductImage, options.feature_img);

        mProductPrice.setText(String.format("%.2f", options.price));
        mProductStock.setText(String.format("%d", options.stock));

        if (options.variants != null && options.variants.size() > 0) {
            mProductVariantsAdapter = new ProductVariantsAdapter(this, options.variants);
            mProductVariants.setAdapter(mProductVariantsAdapter);
        }

        mProductQuantity.setValue(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.tf_stay_still, R.anim.tf_push_up_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShoppingCartController.getInstance().removeResponseListener(this);
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) throws JSONException {
        if (url.endsWith(ApiInterface.CART_ADD)) {
            if (ShoppingCartController.getInstance().getLastResponseStatus().getIsSuccess()) {
                ShoppingCartController.getInstance().getItems(this, false);
            }
        } else if (url.endsWith(ApiInterface.CART_LIST)) {
            if (ShoppingCartController.getInstance().getLastResponseStatus().getIsSuccess()) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    @OnClick(R.id.backbtn)
    public void onBack() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.action_to_confirm)
    public void onAddToCart() {
        ProductVariant variant = null;
        if (mProductVariantsAdapter != null) {
            variant = mProductVariantsAdapter.getSelectedItem();
        }

        ShoppingCartController.getInstance().add(this,
                mProductOptions.id,
                mProductQuantity.getValue(),
                variant);
    }
}
