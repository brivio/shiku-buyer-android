package com.insuny.sdk.api.request;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class OrderAddRequest{
    public ArrayList<Integer> items=new ArrayList<>();
    public String quan_id;
    public String exress_type;
    public String pays;
    public String score;
    public String addr_tele;
    public String addr_name;
    public String addr_province;
    public String addr_city;
    public String addr_area;
    public String addr_address;
    public String addr_zipcode;
    public String remark;
}