package com.xz.shiku.controller;

import android.content.Context;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.GOODORDER;
import com.xz.btc.protocol.ORDER_GOODS_LIST;
import com.xz.btc.protocol.ORDER_PAY_DATA;
import com.xz.btc.protocol.PAGINATED;
import com.xz.btc.protocol.PAGINATION;
import com.xz.btc.protocol.SESSION;
import com.xz.btc.protocol.ordercancelRequest;
import com.xz.btc.protocol.orderconfirmReceivedRequest;
import com.xz.btc.protocol.orderlistRequest;
import com.xz.btc.protocol.orderlistResponse;
import com.xz.btc.protocol.orderpayRequest;
import com.xz.btc.protocol.orderpayResponse;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.view.ToastView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderController extends BaseModel {
    public static final int PER_PAGE = 10;

    private static OrderController mInstance;

    public static OrderController getInstance() {
        if (mInstance == null) {
            mInstance = new OrderController(TFrameworkApp.getInstance());
        }

        return mInstance;
    }

    private List<GOODORDER> mOrders;
    private List<OrderListViewItem> mOrderListViewData;
    private PAGINATED mLastSearchPagination;

    protected OrderController(Context context) {
        super(context);
    }

    public List<GOODORDER> getOrderResult() {
        return mOrders;
    }

    public List<OrderListViewItem> getOrderListViewData() {
        return mOrderListViewData;
    }

    public boolean getIsFirstPage() {
        return (mLastSearchPagination != null &&
                mLastSearchPagination.page == 1);
    }

    public boolean getHasMore() {
        boolean hasMore = false;

        if (mLastSearchPagination != null) {
            hasMore = mLastSearchPagination.more > 0;
        }

        return hasMore;
    }

    public void getOrder(Context context, String orderStatus) {
        mContext = context;

        orderlistRequest request = new orderlistRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = 1;
        pagination.count = PER_PAGE;

        request.pagination = pagination;
        request.type = orderStatus;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.ORDER_LIST, params, true);
    }

    public void getMoreOrder(Context context, String orderStatus) {
        if (!getHasMore()) {
            return;
        }

        mContext = context;

        orderlistRequest request = new orderlistRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = mLastSearchPagination.page + 1;
        pagination.count = PER_PAGE;

        request.pagination = pagination;
        request.type = orderStatus;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.ORDER_LIST, params, true);
    }

    public void close(Context context, int orderId) {
        mContext = context;

        ordercancelRequest request = new ordercancelRequest();
        request.order_id = orderId;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.ORDER_CANCEL, params, false);
    }

    public void confirm(Context context, int orderId) {
        mContext = context;

        orderconfirmReceivedRequest request = new orderconfirmReceivedRequest();
        request.order_id = orderId;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.ORDER_AFFIRMRECEIVED, params, false);
    }

    ORDER_PAY_DATA mPayResult;

    public ORDER_PAY_DATA getPayResult() {
        return mPayResult;
    }

    public void payWith(Context context, int orderId, int paymentType) {
        mContext = context;

        orderpayRequest request = new orderpayRequest();
        request.order_id = orderId;
        request.pay_with = paymentType;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.ORDER_PAY, params, true);
    }

    @Override
    protected void onSuccess(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.ORDER_LIST)) {
            orderlistResponse response = new orderlistResponse();
            try {
                response.fromJson(jo);

                mLastSearchPagination = response.paginated;

                if (mLastSearchPagination.page == 1) {
                    if (mOrders == null) {
                        mOrders = new ArrayList<>();
                    } else {
                        mOrders.clear();
                    }
                    if (mOrderListViewData == null) {
                        mOrderListViewData = new ArrayList<>();
                    } else {
                        mOrderListViewData.clear();
                    }
                }
                if (response.data != null && response.data.size() > 0) {
                    mOrders.addAll(response.data);

                    for (GOODORDER order : mOrders) {
                        mOrderListViewData.add(new OrderListViewItem(ItemType.HEADER, order));

                        for (ORDER_GOODS_LIST goods : order.goods_list) {
                            mOrderListViewData.add(new OrderListViewItem(ItemType.ITEM, order, goods));
                        }

                        mOrderListViewData.add(new OrderListViewItem(ItemType.FOOTER, order));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (url.endsWith(ApiInterface.ORDER_PAY)) {
            orderpayResponse response = new orderpayResponse();

            try {
                response.fromJson(jo);

                mPayResult = response.data;
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        super.onSuccess(url, jo, status);
    }

    @Override
    protected void onFail(String url, JSONObject jo, AjaxStatus status) {
        ToastView.showMessage(mContext, getLastResponseStatus().getErrorMessage());

        super.onFail(url, jo, status);
    }

    public enum OrderStatus {
        ALL("", "全部订单"),
        UNPAID("0", "待付款"),
        PAID("1", "待发货"),
        SHIPPED("2", "待收货"),
        DELIVERED("4", "已完成"),
        CLOSED("5", "已关闭"),
        COMPLETED("6", "已完成");

        private String mStatusString;
        private String mStatusDescription;

        OrderStatus(String status, String description) {
            mStatusString = status;
            mStatusDescription = description;
        }

        public String getStringValue() {
            return mStatusString;
        }

        public String getDescription() {
            return mStatusDescription;
        }

        public static OrderStatus mapStringToValue(String status) {
            for (OrderStatus value : OrderStatus.values()) {
                if (status.equals(value.getStringValue())) {
                    return value;
                }
            }

            return ALL;
        }
    }

    public enum ItemType {
        HEADER(0x00),
        ITEM(0x01),
        FOOTER(0x02);

        private int mIntValue;

        ItemType(int typeInt) {
            mIntValue = typeInt;
        }

        public int getValue() {
            return mIntValue;
        }
    }

    public static class OrderListViewItem {
        public ItemType mItemType;
        public GOODORDER mOrder;
        public Object mTag;

        OrderListViewItem(ItemType type, GOODORDER order) {
            this(type, order, null);
        }

        OrderListViewItem(ItemType type, GOODORDER order, Object tag) {
            mItemType = type;
            mOrder = order;
            mTag = tag;
        }
    }
}
