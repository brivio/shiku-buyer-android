package com.xz.shiku.fragment.address;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.protocol.ADDRESS;
import com.xz.btc.protocol.ApiInterface;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.adapter.address.AddressAdapter;
import com.xz.shiku.controller.AddressController;
import com.xz.shiku.controller.UserController;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.SwipeListView;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class AddressListFragment extends BackHandledFragment implements AbOnItemClickListener, AdapterView.OnItemClickListener {
    private static final String ARG_MODE = "param2";

    private boolean mIsInEditMode;

    @InjectView(R.id.ll_empty)
    LinearLayout mEmptyLayout;
    @InjectView(R.id.ll_empty_icon)
    TextView mEmptyIcon;
    @InjectView(R.id.ll_empty_text)
    TextView mEmptyText;
    @InjectView(R.id.ll_empty_subtext)
    TextView mEmptySubtext;
    @InjectView(R.id.swlist)
    SwipeListView mAddresses;

    AddressAdapter mAddressAdapter;
    public OnSelectAddress mOnSelectAddressListener;

    public static AddressListFragment newInstance(boolean editMode) {
        PopActivity.gShowNavigationBar = true;
        titleResId = editMode ?
                R.string.title_fragment_address_list_in_edit_mode :
                R.string.title_fragment_address_list;
        topRightText = "";

        AddressListFragment fragment = new AddressListFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_MODE, editMode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mIsInEditMode = getArguments().getBoolean(ARG_MODE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_addresslist, container, false);

        ButterKnife.inject(this, v);

        mEmptyIcon.setText(getResources().getString(R.string.iconlove));
        mEmptyText.setText("暂无收货地址");
        mEmptySubtext.setText("主人快去添加收货地址吧");

        if (UserController.getInstance().isUserReady()) {
            AddressController.getInstance().addResponseListener(this);
        } else {
            initUI(false);
        }

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (UserController.getInstance().isUserReady()) {
            AddressController.getInstance().getAdresses(getActivity());
        }
    }

    private void initUI(boolean showAddressLayout) {
        if (showAddressLayout) {
            mEmptyLayout.setVisibility(View.GONE);
            mAddresses.setPullRefreshEnable(true);
            mAddresses.setPullLoadEnable(false);
            mAddresses.setXListViewListener(this);
            mAddresses.setOnItemClickListener(this);
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        AddressController.getInstance().removeResponseListener(this);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.ADDRESS_LIST)) {
            List<ADDRESS> addresses = AddressController.getInstance().getAddressesResult();
            if (AddressController.getInstance().getIsFirstPage()) {
                if (addresses != null && addresses.size() > 0) {
                    initUI(true);

                    mAddresses.setRefreshTime();
                    mAddresses.stopRefresh();
                    if (AddressController.getInstance().getHasMore()) {
                        mAddresses.setPullLoadEnable(true);
                    }

                    mAddressAdapter = new AddressAdapter(getActivity(), addresses, mAddresses.getRightViewWidth(), this);
                    mAddresses.setAdapter(mAddressAdapter);
                } else {
                    initUI(false);
                }
            } else {
                mAddresses.stopLoadMore();
                mAddressAdapter.notifyDataSetChanged();
            }
        } else if (url.endsWith(ApiInterface.ADDRESS_DELETE)) {
            List<ADDRESS> addresses = AddressController.getInstance().getAddressesResult();
            if (addresses != null && addresses.size() > 0) {
                mAddressAdapter.notifyDataSetChanged();
            } else {
                initUI(false);
            }
        }
    }

    @Override
    public void onRefresh() {
        AddressController.getInstance().getAdresses(getActivity());
    }

    @Override
    public void onLoadMore() {
        if (AddressController.getInstance().getHasMore()) {
            AddressController.getInstance().getMoreAdresses(getActivity());
        }
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ADDRESS address = (ADDRESS) mAddressAdapter.getItem(position - 1);
        if (mIsInEditMode) {
            AddressEditFragment addressEditFragment = AddressEditFragment.newInstance(address, "1");
            startActivityWithFragment(addressEditFragment);
        } else {
            if (mOnSelectAddressListener != null) {
                mOnSelectAddressListener.onSelect(address);
            }
            getActivity().finish();
        }
    }

    public void onClick(int position) {
        ADDRESS address = (ADDRESS) mAddressAdapter.getItem(position);
        if (address != null) {
            AddressController.getInstance().delete(getActivity(), address.id);
        }
    }

    @OnClick(R.id.btn_add_address)
    public void onAddAddress() {
        AddressEditFragment addressEditFragment = AddressEditFragment.newInstance(null, "");
        startActivityWithFragment(addressEditFragment);
    }

    public interface OnSelectAddress {
        void onSelect(ADDRESS address);
    }
}
