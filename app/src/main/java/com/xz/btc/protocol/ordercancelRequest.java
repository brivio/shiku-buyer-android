
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "ordercancelRequest")
public class ordercancelRequest extends Model {

    @Column(name = "session")
    public SESSION session;

    @Column(name = "order_id")
    public int order_id;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        SESSION session = new SESSION();
        session.fromJson(jsonObject.optJSONObject("session"));
        this.session = session;

        this.order_id = jsonObject.optInt("order_id");
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        try {
            localItemObject.put("id", order_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }
}
