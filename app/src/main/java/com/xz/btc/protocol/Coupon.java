package com.xz.btc.protocol;

/**
 * Created by txj on 15/3/6.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.xz.tframework.utils.Utils;

import org.json.JSONObject;

import java.util.Date;

public class Coupon implements Parcelable {

    public int id;
    public String image;
    public String title;
    public double minimum;
    public double discount;
    public String beginDate;
    public String endDate;
    public int type;
    public int status;
    public boolean isUsed;
    public String usedDate;

    public Coupon() {

    }

    public void fromJson(JSONObject jsonObject) {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optInt("id");
        this.image = jsonObject.optString("img");
        this.title = jsonObject.optString("title");
        this.minimum = jsonObject.optDouble("man_price");
        this.discount = jsonObject.optDouble("man_sale");
        this.beginDate = jsonObject.optString("stime");
        this.endDate = jsonObject.optString("etime");
        this.type = jsonObject.optInt("type");
        this.status = jsonObject.optInt("status");
        this.isUsed = jsonObject.optInt("used_status", 0) == 1;
        this.usedDate = jsonObject.optString("used_time");
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }

    public boolean getIsValid() {
        boolean isValid = true;

        if (isUsed) {
            isValid = false;
        } else {
            Date validUntil = Utils.tryParseDate(endDate, "yyyy-MM-dd");
            if (validUntil == null || validUntil.before(new Date())) {
                isValid = false;
            }
        }

        return isValid;
    }

    Coupon(Parcel in) {
        this.id = in.readInt();
        this.image = in.readString();
        this.title = in.readString();
        this.minimum = in.readDouble();
        this.discount = in.readDouble();
        this.beginDate = in.readString();
        this.endDate = in.readString();
        this.type = in.readInt();
        this.status = in.readInt();
        this.isUsed = in.readByte() == 1;
        this.usedDate = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(image);
        dest.writeString(title);
        dest.writeDouble(minimum);
        dest.writeDouble(discount);
        dest.writeString(beginDate);
        dest.writeString(endDate);
        dest.writeInt(type);
        dest.writeInt(status);
        dest.writeByte((byte) (isUsed ? 1 : 0));
        dest.writeString(usedDate);
    }

    public static final Parcelable.Creator<Coupon> CREATOR =
            new Creator<Coupon>() {
                @Override
                public Coupon createFromParcel(Parcel source) {
                    return new Coupon(source);
                }

                @Override
                public Coupon[] newArray(int size) {
                    return new Coupon[size];
                }
            };
}
