
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

@Table(name = "addresslistRequest")
public class addresslistRequest extends Model {

    @Column(name = "session")
    public SESSION session;

    @Column(name = "country")
    public String country;

    @Column(name = "pagination")
    public PAGINATION pagination;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        SESSION session = new SESSION();
        session.fromJson(jsonObject.optJSONObject("session"));
        this.session = session;
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();
        String tempKey;

        try {
            localItemObject.put("country", country);

            if (null != pagination) {
                JSONObject jsonPagination = pagination.toJson();
                Iterator iPagination = jsonPagination.keys();
                while (iPagination.hasNext()) {
                    tempKey = (String) iPagination.next();
                    localItemObject.put(tempKey, jsonPagination.get(tempKey));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }
}