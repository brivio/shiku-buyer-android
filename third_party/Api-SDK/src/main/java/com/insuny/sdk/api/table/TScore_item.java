package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TScore_item{
    public String id;
    public String cate_id;
    public String title;
    public String type;
    public String img;
    public String score;
    public String stock;
    public String user_num;
    public String buy_num;
    public String desc;
    public String ordid;
    public String status;
}