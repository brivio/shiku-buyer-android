package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TLogs{
    public String id;
    public String mid;
    public String item_id;
    public String class_id;
    public String data;
    public String data_id;
    public String img;
    public String info;
    public String check_mid;
    public String check_time;
    public String add_time;
    public String status;
    public String class_name;
    public String data_name;
    public String item_name;
    public ArrayList<String> logs_name=new ArrayList<>();
    public String title_name;
}