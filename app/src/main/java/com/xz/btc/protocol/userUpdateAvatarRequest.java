
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "userUpdateAvatarRequest")
public class userUpdateAvatarRequest extends Model {

    @Column(name = "file")
    public String file;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        throw new JSONException("Not implemented");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("file", file);

        return localItemObject;
    }
}
