package com.xz.external.activeandroid.query;

/**
 * Created by txj on 15/4/14.
 */

public interface Sqlable {
	public String toSql();
}