package com.xz.shiku.adapter.taste;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xz.btc.protocol.TasteItem;
import com.xz.shiku.R;
import com.xz.tframework.utils.UILUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TasteAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<TasteItem> mTaste;
    private Set<Integer> mCheckedPositions;

    public TasteAdapter(Context context, List<TasteItem> taste) {
        mContext = context;
        mTaste = taste;
        mInflater = LayoutInflater.from(context);
        mCheckedPositions = new HashSet<>();
    }

    public boolean isChecked(int position) {
        return mCheckedPositions.contains(position);
    }

    public void toggleCheckState(int position) {
        if (isChecked(position)) {
            mCheckedPositions.remove(position);
        } else {
            mCheckedPositions.add(position);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mTaste != null ? mTaste.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mTaste != null ? mTaste.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return mTaste != null ? mTaste.get(position).id : -1;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = mInflater.inflate(R.layout.item_taste, parent, false);

            holder.mImage = (ImageView) view.findViewById(R.id.item_image);
            holder.mCheck = (ImageView) view.findViewById(R.id.item_check);
            holder.mText = (TextView) view.findViewById(R.id.item_text);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        TasteItem tasteItem = mTaste.get(position);

        if (!tasteItem.image.endsWith("no_picture.gif")) {
            UILUtil.getInstance().getImage(mContext, holder.mImage, tasteItem.image);
        } else {
            holder.mImage.setImageResource(R.drawable.ic_placeholder_black);
        }
        holder.mCheck.setVisibility(isChecked(position) ? View.VISIBLE : View.INVISIBLE);
        holder.mText.setText(tasteItem.title);

        return view;
    }

    static class ViewHolder {
        ImageView mImage;
        ImageView mCheck;
        TextView mText;
    }
}
