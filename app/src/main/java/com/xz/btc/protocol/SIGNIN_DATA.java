
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "SIGNIN_DATA")
public class SIGNIN_DATA extends Model {

    @Column(name = "session")
    public SESSION session;

    @Column(name = "user")
    public USER user;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        SESSION.fromJson(jsonObject);
        this.session = SESSION.getInstance();

        USER user = new USER();
        user.fromJson(jsonObject);
        this.user = user;
    }

    public JSONObject toJson() throws JSONException {
        throw new JSONException("Not implemented");
    }
}
