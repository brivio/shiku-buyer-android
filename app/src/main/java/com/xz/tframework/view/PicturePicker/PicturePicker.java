package com.xz.tframework.view.PicturePicker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import com.xz.shiku.R;
import com.xz.tframework.view.PicturePicker.ImageCropper.ImageCropper;
import com.xz.tframework.view.PicturePicker.ImageCropper.ImageTools;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by XH on 2015/5/21.
 */
public class PicturePicker {
    private static final int TAKE_PICTURE = 0;
    private static final int CHOOSE_PICTURE = 1;
    private static final int CROP = 2;
    private static final int CROP_PICTURE = 3;
    private static final int SCALE_RATIO = 5;
    private static final int RESULT_OK = -1;
    private static final int MAX_PICTURE_SIZE = 500;

    private static final String TEMP_PICTURE_FILE_NAME = "pp_pic.jpg";
    private static final String TEMP_PICTURE_URI_NAME = "imguri";

    private Activity mActivity;
    private Fragment mFragment;
    private Context mContext;
    private boolean mNeedCrop;
    private OnPickListener mListener;

    public PicturePicker(Fragment fragment, boolean needCrop) {
        mActivity = fragment.getActivity();
        mFragment = fragment;
        mContext = fragment.getActivity();
        mNeedCrop = needCrop;
        mListener = (OnPickListener) fragment;
    }

    public PicturePicker(Activity activity, boolean needCrop) {
        mActivity = activity;
        mContext = activity;
        mNeedCrop = needCrop;
        mListener = (OnPickListener) activity;
    }

    public void show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.picture_picker_title));
        builder.setNegativeButton(mContext.getResources().getString(R.string.cancel), null);
        builder.setItems(new String[]{
                mContext.getResources().getString(R.string.picture_picker_take_picture),
                mContext.getResources().getString(R.string.picture_picker_choose_picture)
        }, new DialogInterface.OnClickListener() {

            //类型码
            int requestCode;

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case TAKE_PICTURE:
                        requestCode = mNeedCrop ? CROP_PICTURE : TAKE_PICTURE;

                        //删除上一次截图的临时文件
                        ImageTools.deletePhotoAtPathAndName(Environment.getExternalStorageDirectory().getAbsolutePath(), TEMP_PICTURE_FILE_NAME);

                        Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        Uri imageUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), TEMP_PICTURE_FILE_NAME));

                        //指定照片保存路径（SD卡），imageUri为一个临时文件，每次拍照后这个图片都会被替换
                        openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(openCameraIntent, requestCode);
                        break;
                    case CHOOSE_PICTURE:
                        requestCode = mNeedCrop ? CROP_PICTURE : CHOOSE_PICTURE;

                        Intent openAlbumIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        openAlbumIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                        startActivityForResult(openAlbumIntent, requestCode);
                        break;
                }
            }
        });
        builder.create().show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Bitmap photo;
            Bitmap zoomedBitmap;
            switch (requestCode) {
                case TAKE_PICTURE:
                    //将保存在本地的图片取出并缩小后显示在界面上
                    photo = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/" + TEMP_PICTURE_FILE_NAME);
                    zoomedBitmap = ImageTools.zoomBitmap(
                            photo,
                            photo.getWidth() / SCALE_RATIO,
                            photo.getHeight() / SCALE_RATIO);
                    photo.recycle();

                    //将处理过的图片保存到本地
                    ImageTools.savePhotoToSDCard(zoomedBitmap,
                            Environment.getExternalStorageDirectory().getAbsolutePath(), TEMP_PICTURE_FILE_NAME);

                    if (mListener != null) {
                        mListener.onPick(zoomedBitmap);
                    }
                    zoomedBitmap.recycle();
                    break;
                case CHOOSE_PICTURE:
                    ContentResolver resolver = mActivity.getContentResolver();
                    //照片的原始资源地址
                    Uri originalUri = data.getData();
                    try {
                        //使用ContentProvider通过URI获取原始图片
                        photo = MediaStore.Images.Media.getBitmap(resolver, originalUri);
                        if (photo != null) {
                            //为防止原始图片过大导致内存溢出，这里先缩小原图显示，然后释放原始Bitmap占用的内存
                            zoomedBitmap = ImageTools.zoomBitmap(
                                    photo,
                                    photo.getWidth() / SCALE_RATIO,
                                    photo.getHeight() / SCALE_RATIO);
                            //释放原始图片占用的内存，防止out of memory异常发生
                            photo.recycle();

                            if (mListener != null) {
                                mListener.onPick(zoomedBitmap);
                            }
                            zoomedBitmap.recycle();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case CROP_PICTURE:
                    photo = null;
                    if (data != null) {
                        Uri photoUri = data.getData();
                        try {
                            photo = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), photoUri);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (photo == null) {
                        photo = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/" + TEMP_PICTURE_FILE_NAME);
                    }

                    if (photo != null) {
                        crop(photo);
                    }
                    break;
                case CROP:
                    String uri;
                    if (data != null) {
                        uri = data.getStringExtra(TEMP_PICTURE_URI_NAME);

                        photo = BitmapFactory.decodeFile(uri);

                        if (mListener != null) {
                            mListener.onPick(photo);
                        }

                        photo.recycle();
                    }
                    break;
            }
        }
    }

    private void crop(Bitmap smallBitmap) {
        Bitmap zoomedBitmap = null;
        int height = smallBitmap.getHeight();
        int width = smallBitmap.getWidth();

        do {
            if (zoomedBitmap != null) {
                zoomedBitmap.recycle();
            }

            height /= SCALE_RATIO;
            width /= SCALE_RATIO;

            zoomedBitmap = ImageTools.zoomBitmap(smallBitmap, width, height);
        } while (zoomedBitmap.getByteCount() / 1024 > MAX_PICTURE_SIZE * SCALE_RATIO);

        smallBitmap.recycle();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        zoomedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        zoomedBitmap.recycle();

        Intent intent = new Intent(mContext, ImageCropper.class);
        intent.putExtra("bitmap", byteArray);
        startActivityForResult(intent, CROP);
    }

    public interface OnPickListener {
        public void onPick(Bitmap photo);
    }

    private void startActivityForResult(Intent intent, int requestCode) {
        if (mFragment != null) {
            mFragment.startActivityForResult(intent, requestCode);
        } else {
            mActivity.startActivityForResult(intent, requestCode);
        }
    }
}
