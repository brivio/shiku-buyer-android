buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:1.2.3'
    }
}

allprojects {
    repositories {
        jcenter()
    }
    project.ext {
        compileSdkVersion = 22
        buildToolsVersion = "22.0.1"
    }
}