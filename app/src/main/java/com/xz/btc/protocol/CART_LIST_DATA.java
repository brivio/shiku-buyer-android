
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@Table(name = "CART_LIST_DATA")
public class CART_LIST_DATA  extends Model
{

     @Column(name = "total")
     public TOTAL total;

     public ArrayList<GOODS_GROUPED_LIST> goods_grouped_lists = new ArrayList<GOODS_GROUPED_LIST>();

     public void  fromJson(JSONObject jsonObject)  throws JSONException
     {
          if(null == jsonObject){
            return ;
           }

          JSONArray subItemArray;
          TOTAL  total = new TOTAL();
          total.fromJson(jsonObject.optJSONObject("total"));
          this.total = total;

          subItemArray = jsonObject.optJSONArray("goods_grouped_lists");
          if(null != subItemArray)
           {
              for(int i = 0;i < subItemArray.length();i++)
               {
                  JSONObject subItemObject = subItemArray.getJSONObject(i);
                   GOODS_GROUPED_LIST subItem = new GOODS_GROUPED_LIST();
                  subItem.fromJson(subItemObject);
                  this.goods_grouped_lists.add(subItem);
               }
           }

          return ;
     }

     public JSONObject toJson() throws JSONException
     {
          JSONObject localItemObject = new JSONObject();
          JSONArray itemJSONArray = new JSONArray();
          if(null != total)
          {
            localItemObject.put("total", total.toJson());
          }

          for(int i =0; i< goods_grouped_lists.size(); i++)
          {
              GOODS_GROUPED_LIST itemData =goods_grouped_lists.get(i);
              JSONObject itemJSONObject = itemData.toJson();
              itemJSONArray.put(itemJSONObject);
          }
          localItemObject.put("goods_grouped_lists", itemJSONArray);
          return localItemObject;
     }

}
