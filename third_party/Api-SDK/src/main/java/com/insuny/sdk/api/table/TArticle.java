package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TArticle{
    public String id;
    public String cate_id;
    public String title;
    public String colors;
    public String author;
    public String tags;
    public String img;
    public String intro;
    public String info;
    public String comments;
    public String hits;
    public String ordid;
    public String add_time;
    public String last_time;
    public String status;
    public String seo_title;
    public String seo_keys;
    public String seo_desc;
}