package com.xz.shiku.adapter.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xz.shiku.R;
import com.xz.btc.protocol.CATEGORY;
import com.xz.tframework.utils.UILUtil;

import java.util.List;

/**
 * Created by txj on 15/2/17.
 */
public class Category2Adapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context mContext;
    public List<CATEGORY> mSubcategories;

    public void updateAdapter(List<CATEGORY> subcategories) {
        mSubcategories = subcategories;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView text;
        ImageView icon;
    }

    public Category2Adapter(Context context, List<CATEGORY> subcategories) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mSubcategories = subcategories;
    }

    @Override
    public int getCount() {
        return mSubcategories.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = this.mInflater.inflate(R.layout.item_category2, null);

            holder.icon = (ImageView) view.findViewById(R.id.category2_item_image);
            holder.text = (TextView) view.findViewById(R.id.category2_item_text);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.icon.setImageResource(R.drawable.product);
        UILUtil.getInstance().getImage(mContext, holder.icon, mSubcategories.get(position).img);
        holder.text.setText(mSubcategories.get(position).name);

        return view;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Object getItem(int i) {
        return mSubcategories.get(i);
    }
}
