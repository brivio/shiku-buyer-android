package com.xz.shiku.adapter.coupon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xz.btc.protocol.Coupon;
import com.xz.shiku.R;

import java.text.DecimalFormat;
import java.util.List;

public class CouponAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<Coupon> mCoupons;
    private int mRightWidth = 0;

    public CouponAdapter(Context context, List<Coupon> coupons, int rightWidth) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mCoupons = coupons;
        mRightWidth = rightWidth;
    }

    @Override
    public int getCount() {
        return mCoupons != null ? mCoupons.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mCoupons != null ? mCoupons.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            view = mInflater.inflate(R.layout.item_coupon, parent, false);

            holder.rl_left = (RelativeLayout) view.findViewById(R.id.rl_left);
            holder.rl_right = (RelativeLayout) view.findViewById(R.id.rl_right);
            holder.mTitle = (TextView) view.findViewById(R.id.coupon_title);
            holder.mImage = (ImageView) view.findViewById(R.id.imageview);
            holder.mValidPeriod = (TextView) view.findViewById(R.id.coupon_valid_period);
            holder.mDiscount = (TextView) view.findViewById(R.id.coupon_discount);
            holder.mMinimum = (TextView) view.findViewById(R.id.coupon_minimum);
            holder.mCouponDetails = (RelativeLayout) view.findViewById(R.id.coupon_details);

            LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            holder.rl_left.setLayoutParams(lp1);
            LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(mRightWidth,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            holder.rl_right.setLayoutParams(lp2);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Coupon coupon = mCoupons.get(position);

        holder.mTitle.setText(coupon.title);
        holder.mValidPeriod.setText(String.format("有效期至：%s", coupon.endDate));
        holder.mDiscount.setText(new DecimalFormat("￥#.##").format(coupon.discount));
        holder.mMinimum.setText(new DecimalFormat("满#.##使用").format(coupon.minimum));

        if (coupon.getIsValid()) {
            holder.mImage.setImageResource(R.drawable.ic_char_shi);
            holder.mCouponDetails.setBackgroundResource(R.drawable.ic_coupon);
        } else {
            holder.mImage.setImageResource(R.drawable.ic_char_shi_white);
            holder.mCouponDetails.setBackgroundResource(R.drawable.ic_coupon);
        }

        return view;
    }

    static class ViewHolder {
        RelativeLayout rl_left;
        RelativeLayout rl_right;

        ImageView mImage;
        TextView mTitle;
        TextView mValidPeriod;
        TextView mDiscount;
        TextView mMinimum;
        RelativeLayout mCouponDetails;
    }
}
