#import <Foundation/Foundation.h>

@interface TAd : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *board_id;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSString *content;
@property(strong, nonatomic) NSString *extimg;
@property(strong, nonatomic) NSString *extval;
@property(strong, nonatomic) NSString *desc;
@property(strong, nonatomic) NSString *start_time;
@property(strong, nonatomic) NSString *end_time;
@property(strong, nonatomic) NSString *clicks;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TAdboard : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *tpl;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *width;
@property(strong, nonatomic) NSString *height;
@property(strong, nonatomic) NSString *description;
@property(strong, nonatomic) NSString *status;

@end
@interface TAdmin : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *username;
@property(strong, nonatomic) NSString *password;
@property(strong, nonatomic) NSString *role_id;
@property(strong, nonatomic) NSString *last_ip;
@property(strong, nonatomic) NSString *last_time;
@property(strong, nonatomic) NSString *email;
@property(strong, nonatomic) NSString *status;

@end
@interface TAdmin_auth : NSObject

@property(strong, nonatomic) NSString *role_id;
@property(strong, nonatomic) NSString *menu_id;

@end
@interface TAdmin_log : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *admin_id;
@property(strong, nonatomic) NSString *admin_name;
@property(strong, nonatomic) NSString *admin_time;
@property(strong, nonatomic) NSString *actions;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *ip;

@end
@interface TAdmin_role : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TApp_ad : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *board_id;
@property(strong, nonatomic) NSString *app_type;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *price;
@property(strong, nonatomic) NSString *nums;
@property(strong, nonatomic) NSString *content;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *desc;
@property(strong, nonatomic) NSString *start_time;
@property(strong, nonatomic) NSString *end_time;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TApply_service : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *service_id;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *status;

@end
@interface TArea : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *city_id;
@property(strong, nonatomic) NSString *city_name;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *update_time;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TArticle : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *colors;
@property(strong, nonatomic) NSString *author;
@property(strong, nonatomic) NSString *tags;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *intro;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *comments;
@property(strong, nonatomic) NSString *hits;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *last_time;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *seo_title;
@property(strong, nonatomic) NSString *seo_keys;
@property(strong, nonatomic) NSString *seo_desc;

@end
@interface TArticle_cate : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *alias;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *pid;
@property(strong, nonatomic) NSString *spid;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *seo_title;
@property(strong, nonatomic) NSString *seo_keys;
@property(strong, nonatomic) NSString *seo_desc;

@end
@interface TArticle_comment : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *article_id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *status;

@end
@interface TArticle_page : NSObject

@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *seo_title;
@property(strong, nonatomic) NSString *seo_keys;
@property(strong, nonatomic) NSString *seo_desc;
@property(strong, nonatomic) NSString *last_time;

@end
@interface TAttr : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *class_id;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TAttr_val : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *attr_id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *ordid;

@end
@interface TAuto_user : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *users;

@end
@interface TBadword : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *word_type;
@property(strong, nonatomic) NSString *badword;
@property(strong, nonatomic) NSString *replaceword;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TCart : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *mname;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *attr;
@property(strong, nonatomic) NSString *num;
@property(strong, nonatomic) NSString *price;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TCity : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *pid;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *spid;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *update_time;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TClass : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *classid;
@property(strong, nonatomic) NSString *spid;
@property(strong, nonatomic) NSString *num;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *is_del;

@end
@interface TExpress : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *code;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *status;

@end
@interface TFeedback : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TFlink : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TFlink_cate : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface THots : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *last_time;
@property(strong, nonatomic) NSString *hits;

@end
@interface TItem : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *mname;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *sn;
@property(strong, nonatomic) NSString *price;
@property(strong, nonatomic) NSString *rprice;
@property(strong, nonatomic) NSString *net_content;
@property(strong, nonatomic) NSString *days;
@property(strong, nonatomic) NSString *auth;
@property(strong, nonatomic) NSString *auth_img;
@property(strong, nonatomic) NSString *license;
@property(strong, nonatomic) NSString *brand;
@property(strong, nonatomic) NSString *storage;
@property(strong, nonatomic) NSString *new_year;
@property(strong, nonatomic) NSString *new_month;
@property(strong, nonatomic) NSString *new_days;
@property(strong, nonatomic) NSString *qual_safe;
@property(strong, nonatomic) NSString *qual_char;
@property(strong, nonatomic) NSString *qual_auth;
@property(strong, nonatomic) NSString *qual_comp;
@property(strong, nonatomic) NSString *qual_orga;
@property(strong, nonatomic) NSString *qual_attr;
@property(strong, nonatomic) NSString *qual_status;
@property(strong, nonatomic) NSString *weight;
@property(strong, nonatomic) NSString *stock;
@property(strong, nonatomic) NSString *areas;
@property(strong, nonatomic) NSString *years;
@property(strong, nonatomic) NSString *sales;
@property(strong, nonatomic) NSString *express;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSString *tags;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *app_info;
@property(strong, nonatomic) NSString *app_status;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *update_time;
@property(strong, nonatomic) NSString *is_hots;
@property(strong, nonatomic) NSString *is_new;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *favs;
@property(strong, nonatomic) NSString *comments;
@property(strong, nonatomic) NSString *hits;
@property(strong, nonatomic) NSString *rates;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *seo_titile;
@property(strong, nonatomic) NSString *seo_keys;
@property(strong, nonatomic) NSString *seo_desc;

@end
@interface TItem_brand : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *address;
@property(strong, nonatomic) NSString *tele;
@property(strong, nonatomic) NSString *abst;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *hits;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *seo_title;
@property(strong, nonatomic) NSString *seo_keys;
@property(strong, nonatomic) NSString *seo_desc;

@end
@interface TItem_cate : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *pid;
@property(strong, nonatomic) NSString *spid;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *update_time;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *maxs;
@property(strong, nonatomic) NSString *unit_years;
@property(strong, nonatomic) NSString *unit_areas;
@property(strong, nonatomic) NSString *license;
@property(strong, nonatomic) NSString *rprice;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *is_hots;
@property(strong, nonatomic) NSString *seo_title;
@property(strong, nonatomic) NSString *seo_keys;
@property(strong, nonatomic) NSString *seo_desc;

@end
@interface TItem_comment : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *likes;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *reply_info;
@property(strong, nonatomic) NSString *reply_time;
@property(strong, nonatomic) NSString *reply_name;
@property(strong, nonatomic) NSString *reply_id;
@property(strong, nonatomic) NSString *is_message;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TItem_favs : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TItem_img : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *type;

@end
@interface TItem_setting : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *status;

@end
@interface TItem_sku : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *nums;

@end
@interface TItem_sql : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *nums;
@property(strong, nonatomic) NSString *prices;

@end
@interface TItem_tag : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *tag_id;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TLogs : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *class_id;
@property(strong, nonatomic) NSString *data;
@property(strong, nonatomic) NSString *data_id;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *check_mid;
@property(strong, nonatomic) NSString *check_time;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *status;

@end
@interface TLogs_class : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *pid;
@property(strong, nonatomic) NSString *spid;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TMember : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *deviceid;
@property(strong, nonatomic) NSString *member_type;
@property(strong, nonatomic) NSString *username;
@property(strong, nonatomic) NSString *tele;
@property(strong, nonatomic) NSString *email;
@property(strong, nonatomic) NSString *idcard;
@property(strong, nonatomic) NSString *card_fr;
@property(strong, nonatomic) NSString *card_bg;
@property(strong, nonatomic) NSString *password;
@property(strong, nonatomic) NSString *sex;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *birthday;
@property(strong, nonatomic) NSString *province;
@property(strong, nonatomic) NSString *city;
@property(strong, nonatomic) NSString *area;
@property(strong, nonatomic) NSString *province_id;
@property(strong, nonatomic) NSString *city_id;
@property(strong, nonatomic) NSString *area_id;
@property(strong, nonatomic) NSString *town;
@property(strong, nonatomic) NSString *address;
@property(strong, nonatomic) NSString *likes;
@property(strong, nonatomic) NSString *favs;
@property(strong, nonatomic) NSString *orders;
@property(strong, nonatomic) NSString *score;
@property(strong, nonatomic) NSString *money;
@property(strong, nonatomic) NSString *total_price;
@property(strong, nonatomic) NSString *reg_time;
@property(strong, nonatomic) NSString *reg_ip;
@property(strong, nonatomic) NSString *reg_dev;
@property(strong, nonatomic) NSString *last_time;
@property(strong, nonatomic) NSString *last_ip;
@property(strong, nonatomic) NSString *last_dev;
@property(strong, nonatomic) NSString *last_sys;
@property(strong, nonatomic) NSString *last_lng;
@property(strong, nonatomic) NSString *last_lat;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *employ;

@end
@interface TMember_favs : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TMember_give : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *mname;
@property(strong, nonatomic) NSString *total;
@property(strong, nonatomic) NSString *money;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *status;

@end
@interface TMember_service : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *rates;

@end
@interface TMember_shop : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *is_company;
@property(strong, nonatomic) NSString *company;
@property(strong, nonatomic) NSString *voice;
@property(strong, nonatomic) NSString *voice_file;
@property(strong, nonatomic) NSString *pid;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *years;
@property(strong, nonatomic) NSString *areas;
@property(strong, nonatomic) NSString *exp;
@property(strong, nonatomic) NSString *organic;
@property(strong, nonatomic) NSString *service_id;
@property(strong, nonatomic) NSString *tech_id;
@property(strong, nonatomic) NSString *province;
@property(strong, nonatomic) NSString *city;
@property(strong, nonatomic) NSString *area;
@property(strong, nonatomic) NSString *town;
@property(strong, nonatomic) NSString *province_id;
@property(strong, nonatomic) NSString *city_id;
@property(strong, nonatomic) NSString *area_id;
@property(strong, nonatomic) NSString *rates;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *status;

@end
@interface TMember_tags : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *member_id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TMember_tech : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *years;
@property(strong, nonatomic) NSString *company;
@property(strong, nonatomic) NSString *province;
@property(strong, nonatomic) NSString *city;
@property(strong, nonatomic) NSString *area;
@property(strong, nonatomic) NSString *address;

@end
@interface TMenu : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *pid;
@property(strong, nonatomic) NSString *module_name;
@property(strong, nonatomic) NSString *action_name;
@property(strong, nonatomic) NSString *data;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *often;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *display;

@end
@interface TMessage : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *from_uid;
@property(strong, nonatomic) NSString *from_uname;
@property(strong, nonatomic) NSString *to_uid;
@property(strong, nonatomic) NSString *to_uname;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *status;

@end
@interface TMessage_tpl : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *is_sys;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *alias;
@property(strong, nonatomic) NSString *content;

@end
@interface TOauth : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *code;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *config;
@property(strong, nonatomic) NSString *desc;
@property(strong, nonatomic) NSString *author;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TOrder : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *orderid;
@property(strong, nonatomic) NSString *total;
@property(strong, nonatomic) NSString *express;
@property(strong, nonatomic) NSString *express_name;
@property(strong, nonatomic) NSString *express_sn;
@property(strong, nonatomic) NSString *express_remark;
@property(strong, nonatomic) NSString *express_time;
@property(strong, nonatomic) NSString *prices;
@property(strong, nonatomic) NSString *score;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *mname;
@property(strong, nonatomic) NSString *service_id;
@property(strong, nonatomic) NSString *pays;
@property(strong, nonatomic) NSString *pays_data;
@property(strong, nonatomic) NSString *pays_time;
@property(strong, nonatomic) NSString *pays_status;
@property(strong, nonatomic) NSString *pays_price;
@property(strong, nonatomic) NSString *pays_sn;
@property(strong, nonatomic) NSString *addr_name;
@property(strong, nonatomic) NSString *addr_tele;
@property(strong, nonatomic) NSString *addr_province;
@property(strong, nonatomic) NSString *addr_city;
@property(strong, nonatomic) NSString *addr_area;
@property(strong, nonatomic) NSString *addr_address;
@property(strong, nonatomic) NSString *addr_zipcode;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *check_time;
@property(strong, nonatomic) NSString *check_uid;

@end
@interface TOrder_item : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *order_id;
@property(strong, nonatomic) NSString *orderid;
@property(strong, nonatomic) NSString *express_type;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *price;
@property(strong, nonatomic) NSString *num;
@property(strong, nonatomic) NSString *attr;
@property(strong, nonatomic) NSString *is_refund;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *mname;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *status;

@end
@interface TOrder_refund : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *order_id;
@property(strong, nonatomic) NSString *gid;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *admin_id;
@property(strong, nonatomic) NSString *admin_name;
@property(strong, nonatomic) NSString *status;

@end
@interface TQuan : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *stime;
@property(strong, nonatomic) NSString *etime;
@property(strong, nonatomic) NSString *price;
@property(strong, nonatomic) NSString *max;
@property(strong, nonatomic) NSString *used_time;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *status;

@end
@interface TRetail : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *orderid;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *item_name;
@property(strong, nonatomic) NSString *nums;
@property(strong, nonatomic) NSString *price;
@property(strong, nonatomic) NSString *express;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *pays;
@property(strong, nonatomic) NSString *pays_data;
@property(strong, nonatomic) NSString *pays_sn;
@property(strong, nonatomic) NSString *pays_time;
@property(strong, nonatomic) NSString *pays_price;
@property(strong, nonatomic) NSString *pays_status;

@end
@interface TScore_item : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *score;
@property(strong, nonatomic) NSString *stock;
@property(strong, nonatomic) NSString *user_num;
@property(strong, nonatomic) NSString *buy_num;
@property(strong, nonatomic) NSString *desc;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TScore_item_cate : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *ordid;

@end
@interface TScore_log : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *action;
@property(strong, nonatomic) NSString *score;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TScore_order : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *order_sn;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *item_name;
@property(strong, nonatomic) NSString *item_num;
@property(strong, nonatomic) NSString *consignee;
@property(strong, nonatomic) NSString *address;
@property(strong, nonatomic) NSString *zip;
@property(strong, nonatomic) NSString *mobile;
@property(strong, nonatomic) NSString *order_score;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *status;

@end
@interface TSession : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *session_id;
@property(strong, nonatomic) NSString *session_expire;
@property(strong, nonatomic) NSString *session_data;

@end
@interface TSetting : NSObject

@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *data;
@property(strong, nonatomic) NSString *remark;

@end
@interface TTags : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TTaste : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSString *is_hot;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TTop : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSString *city_id;
@property(strong, nonatomic) NSString *city_name;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *stime;
@property(strong, nonatomic) NSString *etime;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *update_time;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *is_hots;
@property(strong, nonatomic) NSString *is_focus;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end
@interface TTopic : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *cate_id;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *prices;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *stime;
@property(strong, nonatomic) NSString *etime;
@property(strong, nonatomic) NSString *maxs;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *admin_id;
@property(strong, nonatomic) NSString *status;

@end
@interface TTopic_cate : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *pid;
@property(strong, nonatomic) NSString *spid;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *update_time;
@property(strong, nonatomic) NSString *mid;
@property(strong, nonatomic) NSString *maxs;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *is_hots;
@property(strong, nonatomic) NSString *seo_title;
@property(strong, nonatomic) NSString *seo_keys;
@property(strong, nonatomic) NSString *seo_desc;

@end
@interface TTopic_item : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *topic_id;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TUser : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *deviceid;
@property(strong, nonatomic) NSString *username;
@property(strong, nonatomic) NSString *openid;
@property(strong, nonatomic) NSString *email;
@property(strong, nonatomic) NSString *tele;
@property(strong, nonatomic) NSString *sign;
@property(strong, nonatomic) NSString *password;
@property(strong, nonatomic) NSString *sex;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *birthday;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSString *province;
@property(strong, nonatomic) NSString *city;
@property(strong, nonatomic) NSString *area;
@property(strong, nonatomic) NSString *province_id;
@property(strong, nonatomic) NSString *city_id;
@property(strong, nonatomic) NSString *area_id;
@property(strong, nonatomic) NSString *address;
@property(strong, nonatomic) NSString *cover;
@property(strong, nonatomic) NSString *intro;
@property(strong, nonatomic) NSString *likes;
@property(strong, nonatomic) NSString *favs;
@property(strong, nonatomic) NSString *orders;
@property(strong, nonatomic) NSString *quans;
@property(strong, nonatomic) NSString *score;
@property(strong, nonatomic) NSString *reg_time;
@property(strong, nonatomic) NSString *reg_ip;
@property(strong, nonatomic) NSString *reg_dev;
@property(strong, nonatomic) NSString *last_time;
@property(strong, nonatomic) NSString *last_ip;
@property(strong, nonatomic) NSString *last_dev;
@property(strong, nonatomic) NSString *last_sys;
@property(strong, nonatomic) NSString *last_lng;
@property(strong, nonatomic) NSString *last_lat;
@property(strong, nonatomic) NSString *msg_express;
@property(strong, nonatomic) NSString *msg_sys;
@property(strong, nonatomic) NSString *msg_sales;
@property(strong, nonatomic) NSString *is_vip;
@property(strong, nonatomic) NSString *level;
@property(strong, nonatomic) NSString *status;

@end
@interface TUser_address : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *province;
@property(strong, nonatomic) NSString *city;
@property(strong, nonatomic) NSString *area;
@property(strong, nonatomic) NSString *province_id;
@property(strong, nonatomic) NSString *city_id;
@property(strong, nonatomic) NSString *area_id;
@property(strong, nonatomic) NSString *address;
@property(strong, nonatomic) NSString *zipcode;
@property(strong, nonatomic) NSString *tele;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *is_default;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *country;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TUser_bind : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *keyid;
@property(strong, nonatomic) NSString *info;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *status;

@end
@interface TUser_follow : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *follow_uid;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *mutually;

@end
@interface TUser_level : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *min;
@property(strong, nonatomic) NSString *max;

@end
@interface TUser_like : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *item_id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TUser_log : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *data_id;
@property(strong, nonatomic) NSString *data_type;
@property(strong, nonatomic) NSString *data_content;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TUser_msgtip : NSObject

@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *num;

@end
@interface TUser_stat : NSObject

@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *action;
@property(strong, nonatomic) NSString *num;
@property(strong, nonatomic) NSString *last_time;

@end
@interface TUser_taste : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *taste_id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TUser_trend : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *uid;
@property(strong, nonatomic) NSString *uname;
@property(strong, nonatomic) NSString *data_id;
@property(strong, nonatomic) NSString *data_type;
@property(strong, nonatomic) NSString *data_content;
@property(strong, nonatomic) NSString *add_time;

@end
@interface TVers : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSString *vers;
@property(strong, nonatomic) NSString *remark;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *admin_id;
@property(strong, nonatomic) NSString *admin_name;

@end
@interface TWx_ad : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *board_id;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *price;
@property(strong, nonatomic) NSString *nums;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSString *img;
@property(strong, nonatomic) NSString *desc;
@property(strong, nonatomic) NSString *start_time;
@property(strong, nonatomic) NSString *end_time;
@property(strong, nonatomic) NSString *add_time;
@property(strong, nonatomic) NSString *ordid;
@property(strong, nonatomic) NSString *status;

@end