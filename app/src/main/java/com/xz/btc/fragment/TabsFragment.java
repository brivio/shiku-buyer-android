package com.xz.btc.fragment;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xz.shiku.R;
import com.xz.shiku.controller.ShoppingCartController;
import com.xz.shiku.fragment.CartFragment;
import com.xz.shiku.fragment.CateFragment;
import com.xz.shiku.fragment.FavoriteFragment;
import com.xz.shiku.fragment.HomeFragment;
import com.xz.shiku.fragment.UserFragment;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;

public class TabsFragment extends Fragment {

    @InjectViews({
            R.id.tab_one_image,
            R.id.tab_two_image,
            R.id.tab_three_image,
            R.id.tab_four_image,
            R.id.tab_five_image})
    List<ImageView> mTabImages;

    @InjectViews({
            R.id.tab_one_text,
            R.id.tab_two_text,
            R.id.tab_three_text,
            R.id.tab_four_text,
            R.id.tab_five_text})
    List<TextView> mTabTexts;

    @InjectView(R.id.shopping_cart_badge)
    LinearLayout mShoppingCartBadge;

    int[] mTabImageIds = {
            R.drawable.ic_category,
            R.drawable.ic_favourite,
            R.drawable.ic_home,
            R.drawable.ic_cart,
            R.drawable.ic_user_center
    };

    int[] mTabImageIdsSelected = {
            R.drawable.ic_category_selected,
            R.drawable.ic_favourite_selected,
            R.drawable.ic_home_selected,
            R.drawable.ic_cart_selected,
            R.drawable.ic_user_center_selected
    };

    HomeFragment homeFragment;
    FavoriteFragment favoriteFragment;
    CateFragment cateFragment;
    CartFragment shoppingCartFragment;
    UserFragment profileFragment;

    public TabsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.tf_toolbar, container, false);

        ButterKnife.inject(this, mainView);

        selectTab(R.id.tab_three);

        return mainView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onActivityCreated(Bundle paramBundle) {
        super.onActivityCreated(paramBundle);
        setRetainInstance(true);
    }

    @OnClick({
            R.id.tab_one,
            R.id.tab_two,
            R.id.tab_three,
            R.id.tab_four,
            R.id.tab_five})
    public void onSelectTab(View view) {
        final int tabId = view.getId();

        ButterKnife.apply(mTabTexts, new ButterKnife.Action<TextView>() {
            @Override
            public void apply(TextView view, int index) {
                boolean isSelected = ((ViewGroup) view.getParent()).getId() == tabId;
                if (isSelected) {
                    mTabImages.get(index).setImageResource(mTabImageIdsSelected[index]);
                    mTabTexts.get(index).setTextColor(getActivity().getResources().getColor(R.color.bg_Main));
                } else {
                    mTabImages.get(index).setImageResource(mTabImageIds[index]);
                    mTabTexts.get(index).setTextColor(getActivity().getResources().getColor(R.color.black));
                }
            }
        });

        selectTab(tabId);
    }

    void selectTab(int tabId) {
        FragmentTransaction localFragmentTransaction;
        switch (tabId) {
            case R.id.tab_one:
                if (null == cateFragment) {
                    cateFragment = new CateFragment();
                }

                localFragmentTransaction = getFragmentManager().beginTransaction();
                localFragmentTransaction.replace(R.id.fragment_container, cateFragment, "tab_one");
                localFragmentTransaction.commit();
                break;
            case R.id.tab_two:
                favoriteFragment = FavoriteFragment.newInstance(FavoriteFragment.SHOW_IN_MAIN);

                localFragmentTransaction = getFragmentManager().beginTransaction();
                localFragmentTransaction.replace(R.id.fragment_container, favoriteFragment, "tab_two");
                localFragmentTransaction.commit();
                break;
            case R.id.tab_three:
                if (null == homeFragment) {
                    homeFragment = new HomeFragment();
                }

                localFragmentTransaction = getFragmentManager().beginTransaction();
                localFragmentTransaction.replace(R.id.fragment_container, homeFragment, "tab_one");
                localFragmentTransaction.commit();
                break;
            case R.id.tab_four:
                shoppingCartFragment = new CartFragment();

                localFragmentTransaction = getFragmentManager().beginTransaction();
                localFragmentTransaction.replace(R.id.fragment_container, shoppingCartFragment, "tab_three");
                localFragmentTransaction.commit();
                break;
            case R.id.tab_five:
                profileFragment = new UserFragment();

                localFragmentTransaction = getFragmentManager().beginTransaction();
                localFragmentTransaction.replace(R.id.fragment_container, profileFragment, "tab_four");
                localFragmentTransaction.commit();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //登录成功返回到个人主页
        if (requestCode == 1) {
//			if (data != null) {
//                if (null == profileFragment)
//                {
//                    profileFragment = new E0_ProfileFragment();
//                }
//
//				FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
//				localFragmentTransaction.replace(R.id.fragment_container,profileFragment, "tab_four");
//				localFragmentTransaction.commit();
//
//				this.tab_one.setImageResource(R.drawable.tf_footer_home_icon);
//				this.tab_two.setImageResource(R.drawable.tf_footer_search_icon);
//				this.tab_three.setImageResource(R.drawable.tf_footer_shopping_cart_icon);
//				this.tab_four.setImageResource(R.drawable.tf_footer_user_active_icon);
//
//				this.tab_onebg.setVisibility(View.INVISIBLE);
//				this.tab_twobg.setVisibility(View.INVISIBLE);
//				this.tab_threebg.setVisibility(View.INVISIBLE);
//				this.tab_fourbg.setVisibility(View.VISIBLE);
//			}
        } else if (requestCode == 2) {
//            if (null != data)
//            {
//                if (null == shoppingCartFragment)
//                {
//                    shoppingCartFragment = new C0_ShoppingCartFragment();
//                }
//
//                FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
//                localFragmentTransaction.replace(R.id.fragment_container, shoppingCartFragment, "tab_three");
//                localFragmentTransaction.commit();
//
//                this.tab_one.setImageResource(R.drawable.tf_footer_home_icon);
//                this.tab_two.setImageResource(R.drawable.tf_footer_search_icon);
//                this.tab_three.setImageResource(R.drawable.tf_footer_shopping_cart_active_icon);
//                this.tab_four.setImageResource(R.drawable.tf_footer_user_icon);
//
//                this.tab_onebg.setVisibility(View.INVISIBLE);
//                this.tab_twobg.setVisibility(View.INVISIBLE);
//                this.tab_threebg.setVisibility(View.VISIBLE);
//                this.tab_fourbg.setVisibility(View.INVISIBLE);
//            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ShoppingCartController.getInstance().updateShoppingCartBadge(mShoppingCartBadge);
    }
}