package com.xz.btc;


public class AppConst {
    //    public static final String AppId = "1136530ac5aacf74";
    public static final String AppId = "52653a425feb4754";
    //    public static final String AppKey = "7859dc9d12c213b3b223da8a524344a3";
    public static final String AppKey = "00d4f7310e493c3e92026f71a825c253";

    public static final String SERVER_PRODUCTION = "http://115.28.219.106/shiku/buyerApi";
    public static final String SERVER_DEVELOPMENT = "http://115.28.219.106/shiku/buyerApi";
    public static final String WAP_PAY_CALLBCK_DEVELOPMENT = "http://dev.btc.me/btc/payment/app_callback.php?";
    public static final String WAP_PAY_CALLBCK_PRODUCTION = "http://shop.btc.me/btc/payment/app_callback.php?";

    public static final String URL_SHIPPING_TRACKING_MAP = "http://www.shiku.com/wap/public/getmap.html?lon=116.417854&lat=39.921988";

    /* 支付相关    */
    public static final int PAY_TYPE_UNIONPAY = 2;
    public static final int PAY_TYPE_ALIPAY = 3;
    public static final int PAY_TYPE_WXPAY = 4;

    /* 微信SDK    */
    public static final String WX_APP_ID = "wx1bb7c1026ef3a822";
}