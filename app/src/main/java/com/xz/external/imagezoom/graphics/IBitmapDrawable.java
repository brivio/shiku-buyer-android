package com.xz.external.imagezoom.graphics;

import android.graphics.Bitmap;

import com.xz.external.imagezoom.ImageViewTouchBase;

/**
 * Base interface used in the {@link ImageViewTouchBase} view
 * @author alessandro
 *
 */
public interface IBitmapDrawable {

	Bitmap getBitmap();
}
