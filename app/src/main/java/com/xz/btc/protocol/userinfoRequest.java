
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "userinfoRequest")
public class userinfoRequest extends Model {

    @Column(name = "uid")
    public String uid;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.uid = jsonObject.optString("tele");
    }

    public JSONArray toJson() throws JSONException {
        JSONArray itemJSONArray = new JSONArray();

        itemJSONArray.put(uid);

        return itemJSONArray;
    }

}
