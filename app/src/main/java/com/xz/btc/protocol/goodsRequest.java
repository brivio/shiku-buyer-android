
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "goodsRequest")
public class goodsRequest extends Model {

    @Column(name = "goods_id")
    public int goods_id;

    @Column(name = "session")
    public SESSION session;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        JSONArray subItemArray;

        this.goods_id = jsonObject.optInt("goods_id");
        SESSION session = new SESSION();
        session.fromJson(jsonObject.optJSONObject("session"));
        this.session = session;
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        try {
            localItemObject.put("id", goods_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }

}
