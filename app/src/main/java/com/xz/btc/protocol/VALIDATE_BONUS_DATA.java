
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "VALIDATE_BONUS_DATA")
public class VALIDATE_BONUS_DATA  extends Model
{

     @Column(name = "bouns")
     public String bouns;

     @Column(name = "bonus_formated")
     public String bonus_formated;

     public void  fromJson(JSONObject jsonObject)  throws JSONException
     {
          if(null == jsonObject){
            return ;
           }

          JSONArray subItemArray;

          this.bouns = jsonObject.optString("bouns");

          this.bonus_formated = jsonObject.optString("bonus_formated");
          return ;
     }

     public JSONObject toJson() throws JSONException
     {
          JSONObject localItemObject = new JSONObject();
          JSONArray itemJSONArray = new JSONArray();
          localItemObject.put("bouns", bouns);
          localItemObject.put("bonus_formated", bonus_formated);
          return localItemObject;
     }

}
