<?php
error_reporting(E_ERROR);

include __DIR__ . '/../include/common.php';
include __DIR__ . '/../include/cls_mysql.php';

define('SRC_PATH', $_SERVER['argv'][1] . "/src/main/java/" . str_replace('.', '/', $_SERVER['argv'][2]));
define("PACKAGE_PREFIX", $_SERVER['argv'][2]);

$config = include(__DIR__ . '/../protocol.config.php');
$db = new cls_mysql($config['db']);

$db_info = $db->getInfo();

foreach ($db_info as $key => $val) {
    genBaseProtocol($val);
}
genApiModelClass();
genApiProtocol();
genModules();
genApiConfig();

function genApiProtocol()
{
    global $config;
    foreach ($config['api'] as $key => $val) {
        $apiName = getApiName($key);
        if (isset($val['request'])) {
            $genJavaParams = array(
                'name'    => $apiName . 'Request',
                'import'  => array(),
                'fields'  => array(),
                'package' => '.request',
            );
            genJava(array_merge($genJavaParams, parseRules($val['request'])), 'request');
        }
        if (isset($val['response'])) {
            genApiResponseProtocol($apiName, $val);
        }
    }
}

function genApiModelClass()
{
    global $config;
    $info = array(
        'api' => array(),
    );
    foreach ($config['api'] as $key => $val) {
        $info['api'][getApiName($key)] = array(
            'url' => $key,
        );
    }
    ob_start();
    require(__DIR__ . '/templates/ApiClient.php');
    $content = ob_get_contents();
    ob_end_clean();
    file_put_contents(SRC_PATH . "/ApiClient.java", $content);
}

function getApiName($key)
{
    $res = explode('/', $key);
    foreach ($res as $k => $v) {
        $res[$k] = ucfirst($v);
    }
    return implode("", $res);
}

//生成响应model
function genApiResponseProtocol($key, $val)
{
    $name = ucfirst($key) . 'Response';
    $info = parseResponse($val["response"], $key);
    ob_start();
    require(__DIR__ . '/templates/extraProtocol.php');
    $content = ob_get_contents();
    ob_end_clean();
    file_put_contents(SRC_PATH . "/response/$name.java", $content);
}

function genModules()
{
    global $config;
    foreach ($config['modules'] as $key => $val) {
        $genJavaParams = array_merge(array(
            'name'    => getExtraClass(ucfirst($key)),
            'import'  => array(),
            'fields'  => array(),
            'package' => '.extra',
        ), parseRules($val));

        genJava($genJavaParams, 'extra');
    }
}

function getInstanceInfo($str)
{
    if (strpos($str, '/') === 0) {
        $import = 'table';
        $class = getBaseClass($str);
        $field = trim($str, '/');
    } elseif (strpos($str, '@') === 0) {
        $import = 'extra';
        $class = getExtraClass($str);
        $field = trim($str, '@');
    } else {
        return $str;
    }
    return compact('import', 'class', 'field');
}

function genJava($info, $path = "/")
{
    if (empty($info)) {
        return;
    }
    ob_start();
    require(__DIR__ . "/templates/extraProtocol.php");
    $content = ob_get_contents();
    ob_end_clean();
    if ($path != "/") {
        $path = "/$path/";
    }
    file_put_contents(SRC_PATH . "$path$info[name].java", $content);
}

function parseRules($rules)
{
    $result = array(
        'import' => array(),
        'fields' => array(),
    );
    if (is_array($rules)) {
        foreach ($rules as $key => $val) {
            $instanceInfo = getInstanceInfo($val);
            if (strpos($key, '?') > 0) {
                if (is_string($instanceInfo)) {
                    $result['fields'][$key] = $instanceInfo;
                } else {
                    $result['fields'][$key] = $instanceInfo['class'];
                    $result['import'][$instanceInfo['class']] = $instanceInfo['import'];
                }
                continue;
            }
            if (is_numeric($key)) {
                if (is_string($instanceInfo)) {
                    if (strpos($instanceInfo, '[]') > 0) {
                        $result['fields'][$instanceInfo] = "ArrayList";
                    } else {
                        $result['fields'][$instanceInfo] = "String";
                    }

                } else {
                    $result['fields'][$instanceInfo['field']] = $instanceInfo['class'];
                    $result['import'][$instanceInfo['class']] = $instanceInfo['import'];
                }
            } else {
                $result['fields'][$key] = $instanceInfo['class'];
                $result['import'][$instanceInfo['class']] = $instanceInfo['import'];
            }
        }
    }
    if (is_string($rules)) {
        $res = getInstanceInfo($rules);
        $result['import'][$res['class']] = $res['import'];
        $result['fields']['data'] = $res['class'];
    }
    return $result;
}

function parseResponse($params, $apiName)
{
    $result = array(
        'name'    => ucfirst($apiName) . 'Response',
        'import'  => array(),
        'fields'  => array(),
        'package' => '.response',
    );
    //列表结果
    if (is_array($params) && !array_key_exists('data?', $params)) {
        if (empty($params)) {
            $result['fields']['data'] = "String";
        } else {
            $genJavaParams = parseRules($params);
            $genJavaParams['name'] = getExtraClass(ucfirst($apiName));
            $genJavaParams['package'] = '.extra';

            genJava($genJavaParams, 'extra');
            $result['import'][$genJavaParams['name']] = 'extra';
            $result['fields']['data'] = $genJavaParams['name'];
        }
    }

    //单例结果
    if (is_string($params)) {
        $res = getInstanceInfo($params);
        $result['import'][$res['class']] = $res['import'];
        $result['fields']['data'] = $res['class'];
    }
    if (array_key_exists('data?', $params)) {
        $instanceInfo = getInstanceInfo($params['data?']);
        $result['import'][$instanceInfo['class']] = $instanceInfo['import'];
        $result['fields']['data?'] = $instanceInfo['class'];
    }
    $result['fields']['status'] = "String";
    $result['fields']['result'] = "String";
    return $result;
}

function genBaseProtocol($info)
{
    global $config;
    $_name = substr($info['Name'], 4);
    $baseName = ucfirst($_name);
    $name = getBaseClass($baseName);

    $genJavaParams = array(
        'package' => '.table',
        'name'    => $name,
        'import'  => array(),
        'fields'  => array(),
    );
    foreach ($info['Fields'] as $key => $val) {
        $genJavaParams['fields'][$key] = 'String';
    }
    foreach ($config['tables'][$_name] as $key => $val) {
        if (is_numeric($key)) {
            $genJavaParams['fields'][$val] = 'String';
        } else {
            $res = getInstanceInfo($val);
            $genJavaParams['fields'][$key] = $res['class'];
            if ($res['class'] != $name) {
                $genJavaParams['import'][$res['class']] = $res['import'];
            }
        }
    }
    genJava($genJavaParams, 'table');
}

function genApiConfig()
{
    global $config;
    ob_start();
    require(__DIR__ . '/templates/ApiConfig.php');
    $content = ob_get_contents();
    ob_end_clean();
    file_put_contents(SRC_PATH . "/ApiConfig.java", $content);
}