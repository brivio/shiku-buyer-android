package com.xz.shiku.adapter.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xz.shiku.R;
import com.xz.btc.protocol.CATEGORY;

import java.util.List;

/**
 * Created by txj on 15/2/17.
 */
public class Category1Adapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context mContext;
    private int mCurrentPosition = 0;
    private List<CATEGORY> mCategories;
    private boolean layout2 = false;

    public void setSelection(int position) {
        mCurrentPosition = position;
        notifyDataSetChanged();
    }

    public void updateAdapter(List<CATEGORY> categories) {
        mCategories = categories;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView text;
        View vindetor;
    }

    public Category1Adapter(Context context, List<CATEGORY> categories) {
        this.layout2 = false;
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mCategories = categories;
    }

    public Category1Adapter(Context context, List<CATEGORY> hotcatelist, boolean _layout2) {
        this.layout2 = true;
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mCategories = hotcatelist;
    }

    @Override
    public int getCount() {
        return mCategories.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = this.mInflater.inflate(R.layout.item_category, null);
            holder.text = (TextView) view.findViewById(R.id.category1_item_text);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.text.setText(mCategories.get(position).name);

        if (mCurrentPosition == position) {
            if (layout2) {
                holder.vindetor.setBackgroundResource(R.color.bg_Main);
                holder.text.setTextColor(mContext.getResources().getColor(R.color.bg_Main));

            } else {
                view.setBackgroundResource(R.color.white);
                holder.text.setTextColor(mContext.getResources().getColor(R.color.bg_Main));
            }
        } else {
            if (layout2) {
                holder.vindetor.setBackgroundResource(R.color.white);
                holder.text.setTextColor(mContext.getResources().getColor(R.color.text_color));
            } else {
                view.setBackgroundResource(R.color.bg_Main4);
                holder.text.setTextColor(mContext.getResources().getColor(R.color.text_color));
            }
        }
        return view;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }
}
