package com.xz.shiku.controller;

import android.content.Context;

import com.xz.btc.protocol.ADDRESS;
import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.PAGINATED;
import com.xz.btc.protocol.PAGINATION;
import com.xz.btc.protocol.SESSION;
import com.xz.btc.protocol.addressdeleteRequest;
import com.xz.btc.protocol.addresslistRequest;
import com.xz.btc.protocol.addresslistResponse;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.view.ToastView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by XH on 2015/6/13.
 */
public class AddressController extends BaseModel {
    public static final int PER_PAGE = 10;

    private static AddressController mInstance;

    public static AddressController getInstance() {
        if (mInstance == null) {
            mInstance = new AddressController(TFrameworkApp.getInstance());
        }

        return mInstance;
    }

    private List<ADDRESS> mAddresses;
    private PAGINATED mLastSearchPagination;
    private Object mTag;

    protected AddressController(Context context) {
        super(context);
    }

    public List<ADDRESS> getAddressesResult() {
        return mAddresses;
    }

    public boolean getIsFirstPage() {
        return (mLastSearchPagination != null &&
                mLastSearchPagination.page == 1);
    }

    public boolean getHasMore() {
        boolean hasMore = false;

        if (mLastSearchPagination != null) {
            hasMore = mLastSearchPagination.more > 0;
        }

        return hasMore;
    }

    public void getAdresses(Context context) {
        mContext = context;

        addresslistRequest request = new addresslistRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = 1;
        pagination.count = PER_PAGE;

        request.country = "cn";
        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.ADDRESS_LIST, params, true);
    }

    public void getMoreAdresses(Context context) {
        if (!getHasMore()) {
            return;
        }

        mContext = context;

        addresslistRequest request = new addresslistRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = mLastSearchPagination.page + 1;
        pagination.count = PER_PAGE;

        request.country = "cn";
        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.ADDRESS_LIST, params, false);
    }

    public void addOrUpdate(Context context, ADDRESS address) {
        mContext = context;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", address.toJson().toString());

        ajax(ApiInterface.ADDRESS_UPDATE, params, true);
    }

    public void delete(Context context, int addressId) {
        mContext = context;

        addressdeleteRequest request = new addressdeleteRequest();
        request.address_id = String.format("%d", addressId);

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        mTag = addressId;

        ajax(ApiInterface.ADDRESS_DELETE, params, true);
    }

    @Override
    protected void onSuccess(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.ADDRESS_LIST)) {
            addresslistResponse response = new addresslistResponse();
            try {
                response.fromJson(jo);

                mLastSearchPagination = response.paginated;

                if (mLastSearchPagination.page == 1) {
                    if (mAddresses == null) {
                        mAddresses = new ArrayList<>();
                    } else {
                        mAddresses.clear();
                    }
                }

                if (response.data != null && response.data.size() > 0) {
                    mAddresses.addAll(response.data);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (url.endsWith(ApiInterface.ADDRESS_DELETE)) {
            int addressId = (int) mTag;
            for (Iterator<ADDRESS> iterator = mAddresses.iterator(); iterator.hasNext(); ) {
                ADDRESS address = iterator.next();
                if (address.id == addressId) {
                    iterator.remove();
                    break;
                }
            }
        } else if (url.endsWith(ApiInterface.ADDRESS_ADD)) {

        } else if (url.endsWith(ApiInterface.ADDRESS_UPDATE)) {

        }

        super.onSuccess(url, jo, status);
    }

    @Override
    protected void onFail(String url, JSONObject jo, AjaxStatus status) {
        ToastView.showMessage(mContext, getLastResponseStatus().getErrorMessage());
    }
}
