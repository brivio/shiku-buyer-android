package com.xz.tframework.activity;

/**
 * Created by txj on 15/4/14.
 */

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.xz.tframework.adapter.DebugListAdapter;
import com.xz.tframework.model.DebugMessageModel;
import com.xz.shiku.R;


public class DebugMessageListActivity extends BaseActivity{

	private TextView title;
    private ListView messageListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tf_debug_message_list);
        
        title = (TextView) findViewById(R.id.navigationbar_title);
        Resources resource = (Resources) getBaseContext().getResources();
        String tit=getString(R.string.debughome_log);
        title.setText(tit);

        
        messageListView = (ListView)findViewById(R.id.debugMessageList);

        DebugListAdapter debugAdapter = new DebugListAdapter(this);
        messageListView.setAdapter(debugAdapter);
        messageListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				 
				
				int size = DebugMessageModel.messageList.size();
				
				Intent intent = new Intent(DebugMessageListActivity.this, DebugDetailActivity.class);
				intent.putExtra("position", size-1-arg2);
				startActivity(intent);
				
			}
		});
        
    }
}
