package com.xz.shiku;

import android.content.Context;

import java.util.HashMap;

import cn.sharesdk.onekeyshare.OnekeyShare;

public class ShareHelper {
    private Context context;

    public ShareHelper(Context context) {
        this.context = context;
    }

    public void share(String title, String content, String url) {
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();
        oks.setSilent(true);
        // 分享时Notification的图标和文字
//        oks.setNotification(R.drawable.ic_launcher, context.getResources().getString(R.string.app_name));
        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
        oks.setTitle(title);
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        oks.setTitleUrl(url);
        // text是分享文本，所有平台都需要这个字段
        oks.setText(content);
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        oks.setImageUrl("http://srz.xinyuancun.com/static/images/sshare.png");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setUrl(url);
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        oks.setComment(content);
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite(context.getResources().getString(R.string.app_name));
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        oks.setSiteUrl(url);
        oks.setOnShareListener(new OnekeyShare.OnShareListener() {
            @Override
            public HashMap<String, Object> OnShare(HashMap<String, Object> data) {
                String url = data.get("url").toString() + "&platform=" + data.get("platform").toString();

                switch (data.get("platform").toString()) {
                    case "SinaWeibo":
                        data.put("text", data.get("text").toString() + " " + url);
                        break;
                }
                data.put("url", url);
                data.put("siteUrl", url);
                data.put("titleUrl", url);

                return data;
            }
        });
        // 启动分享GUI
        oks.show(context);
    }
}
