package com.xz.shiku.fragment.user;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.insuny.sdk.api.ApiClient;
import com.insuny.sdk.api.request.UserLoginRequest;
import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.USER;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.controller.UserController;
import com.xz.shiku.fragment.BaseShikuFragment;
import com.xz.tframework.utils.MD5;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class UserLoginFragment extends BaseShikuFragment {
    static UserLoginListener userLoginListener;

    private boolean hadIntercept;

    @InjectView(R.id.username)
    EditText username;

    @InjectView(R.id.password)
    EditText password;

    public static UserLoginFragment newInstance(UserLoginListener _userLoginListener) {
        PopActivity.gShowNavigationBar = true;
        UserLoginFragment.titleResId = R.string.title_fragment_user_login;
        UserLoginFragment.topRightText = "";
        UserLoginFragment.topLeftTextResId = 0;

        UserLoginFragment fragment = new UserLoginFragment();
        userLoginListener = _userLoginListener;
        return fragment;
    }

    public UserLoginFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_user_login, container, false);
        ButterKnife.inject(this, v);
        UserController.getInstance().addResponseListener(this);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        UserController.getInstance().removeResponseListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick({R.id.loginbtn, R.id.btnforgetpsw, R.id.btnregister})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginbtn:
                UserController.getInstance().signin(getActivity(), username.getText().toString(), password.getText().toString());
                break;
            case R.id.btnregister:
                UserRegisterStep1Fragment userRegisterStep1Fragment = UserRegisterStep1Fragment.newInstance(
                        UserRegisterStep1Fragment.ACTION_REGISTER);
                startActivityWithFragment(userRegisterStep1Fragment);
                break;
            case R.id.btnforgetpsw:
                UserRegisterStep1Fragment resetPswFragment = UserRegisterStep1Fragment.newInstance(
                        UserRegisterStep1Fragment.ACTION_RESET_PASSWORD
                );
                startActivityWithFragment(resetPswFragment);
                break;
        }
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_SIGNIN)) {
            if (UserController.getInstance().isUserReady()) {
                if (UserLoginFragment.userLoginListener != null) {
                    UserLoginFragment.userLoginListener.loginSuccess(UserController.getInstance().getUser());
                }

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().finish();
                    }
                }, 1000);
            } else {
                ToastView.showMessage(getActivity(), UserController.getInstance()
                        .getLastResponseStatus()
                        .getErrorMessage());
            }
        }
    }

    @Override
    public void onLoadMore() {
    }

    @Override
    public void onRefresh() {
    }

    public interface UserLoginListener {
        void loginSuccess(USER user);
    }
}
