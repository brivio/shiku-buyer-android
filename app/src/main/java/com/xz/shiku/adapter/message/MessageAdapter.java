package com.xz.shiku.adapter.message;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.xz.btc.protocol.MESSAGE;
import com.xz.shiku.R;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    public List<MESSAGE> mMessages;
    public int flag;
    private int mRightWidth = 0;

    private ArrayList<String> items = new ArrayList<String>();

    public Handler parentHandler;

    private SharedPreferences shared;
    private SharedPreferences.Editor editor;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    private AbOnItemClickListener abOnItemClickListener;

    public MessageAdapter(Context context, List<MESSAGE> list, int rightWidth, AbOnItemClickListener l) {
        mContext = context;
        mMessages = list;
        mInflater = LayoutInflater.from(context);
        mRightWidth = rightWidth;
        abOnItemClickListener = l;
    }

    @Override
    public int getCount() {
        return mMessages != null ? mMessages.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mMessages != null ? mMessages.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        OnMessageActionListener listener;
        ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            listener = new OnMessageActionListener();
            view = this.mInflater.inflate(R.layout.item_message, null);

            holder.rl_left = (RelativeLayout) view.findViewById(R.id.rl_left);
            holder.rl_right = (RelativeLayout) view.findViewById(R.id.rl_right);

            holder.icon = (ImageView) view.findViewById(R.id.imageview);
            holder.username = (TextView) view.findViewById(R.id.username);
            holder.shopname = (TextView) view.findViewById(R.id.shopname);
            holder.yjhl = (TextView) view.findViewById(R.id.yjhl);
            holder.delbtn = (TextView) view.findViewById(R.id.activity_favorite_item_delbtn);
            holder.delbtn.setOnClickListener(listener);

            LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            holder.rl_left.setLayoutParams(lp1);
            LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(mRightWidth,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            holder.rl_right.setLayoutParams(lp2);

            view.setTag(holder);
            view.setTag(holder.delbtn.getId(), listener);
        } else {
            holder = (ViewHolder) view.getTag();
            listener = (OnMessageActionListener) view.getTag(holder.delbtn.getId());

            if (view.getScrollX() > 0) {
                view.scrollTo(0, 0);
            }
        }

        MESSAGE item = mMessages.get(position);

        holder.username.setText(item.title);
        holder.shopname.setText(item.date);
        holder.yjhl.setText(item.info);
        listener.setPosition(position);

        return view;
    }

    class OnMessageActionListener implements View.OnClickListener {

        int mPosition;

        @Override
        public void onClick(View view) {
            if (abOnItemClickListener != null) {
                abOnItemClickListener.onClick(mPosition);
            }
        }

        public void setPosition(int position) {
            mPosition = position;
        }
    }

    static class ViewHolder {
        RelativeLayout rl_left;
        RelativeLayout rl_right;

        ImageView icon;
        TextView username;
        TextView shopname;
        TextView yjhl;
        TextView mcl;
        TextView delbtn;
    }
}
