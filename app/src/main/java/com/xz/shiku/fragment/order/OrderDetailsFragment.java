package com.xz.shiku.fragment.order;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.BTCManager;
import com.xz.btc.activity.AlixPayActivity;
import com.xz.btc.protocol.ADDRESS;
import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.BONUS;
import com.xz.btc.protocol.GOODORDER;
import com.xz.btc.protocol.ORDER_INFO;
import com.xz.btc.protocol.PAYMENT;
import com.xz.btc.protocol.SHIPPING;
import com.xz.btc.protocol.STATUS;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.external.view.XListViewCart;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.adapter.order.OrderDetailsAdapter;
import com.xz.shiku.controller.OrderController;
import com.xz.shiku.fragment.address.AddressListFragment;
import com.xz.shiku.fragment.payment.PaymentFragment;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.MyDialog;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderDetailsFragment extends BackHandledFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    public List<OrderController.OrderListViewItem> mOrderListViewItems;

    @InjectView(R.id.tv_order_status)
    TextView mOrderStatus;
    @InjectView(R.id.tv_order_total)
    TextView mOrderTotal;
    @InjectView(R.id.tv_order_shipping_rates)
    TextView mOrderShippingRates;

    @InjectView(R.id.item_address_user)
    TextView mAddressName;
    @InjectView(R.id.item_address_tele)
    TextView mAddressMobile;
    @InjectView(R.id.item_address_address)
    TextView mAddressDetails;
    @InjectView(R.id.lv_order_details)
    ListView mOrderDetails;
    @InjectView(R.id.tv_order_shipping_carrier)
    TextView mOrderShippingCarrier;

    @InjectView(R.id.tv_order_id)
    TextView mOrderId;
    @InjectView(R.id.tv_order_transaction_id)
    TextView mOrderTransactionId;
    @InjectView(R.id.tv_order_placed_date)
    TextView mOrderPlacedDate;
    @InjectView(R.id.tv_order_paid_date)
    TextView mOrderPaidDate;
    @InjectView(R.id.tv_order_confirmed_date)
    TextView mOrderConfirmedDate;
    @InjectViews({R.id.action_to_close, R.id.action_to_pay, R.id.action_to_remind, R.id.action_to_track, R.id.action_to_confirm})
    List<TextView> mActions;

    private GOODORDER mOrder;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrderDetailsFragment newInstance(String param1, String param2) {
        PopActivity.gShowNavigationBar = true;

        titleResId = R.string.title_fragment_order_details;
        topRightText = "";
        topRightTextResId = 0;

        OrderDetailsFragment fragment = new OrderDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    public OrderDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_orderdetails, null);

        ButterKnife.inject(this, view);

        OrderDetailsAdapter adapter = new OrderDetailsAdapter(getActivity(), mOrderListViewItems);
        mOrderDetails.setAdapter(adapter);

        mOrder = mOrderListViewItems.size() > 0 ? mOrderListViewItems.get(0).mOrder : null;
        initUI(mOrder);

        return view;
    }

    private void initUI(GOODORDER order) {
        if (order != null) {
            OrderController.OrderStatus orderStatus = OrderController.OrderStatus.mapStringToValue(
                    order.order_info.status);
            mOrderStatus.setText(orderStatus.getDescription());
            mOrderTotal.setText(String.format("订单金额（含运费）：￥%.2f 元", order.order_info.total));
            mOrderShippingRates.setText(String.format("运费：￥%.2f 元", order.order_info.shipping_rates));

            mAddressName.setText(String.format("%s", order.order_info.name));
            mAddressMobile.setText(order.order_info.mobile);
            mAddressDetails.setText(String.format("%s%s%s%s，%s",
                    order.order_info.province,
                    order.order_info.city,
                    order.order_info.county,
                    order.order_info.address,
                    order.order_info.zipcode));

            mOrderShippingCarrier.setText(String.format("%s：%s",
                    order.order_info.shipping_carrier,
                    order.order_info.shipping_tracking_number));

            mOrderId.setText(String.format("订单编号：%s", order.order_info.order_sn));
            mOrderTransactionId.setText(String.format("支付交易号：%s", order.order_info.payment_tid));
            mOrderPlacedDate.setText(String.format("成交时间：%s", order.order_info.checkout_date));
            mOrderPaidDate.setText(String.format("付款时间：%s", order.order_info.payment_date));
            mOrderConfirmedDate.setText(String.format("确认时间：%s", order.order_info.confirmed_date));

            int[] actions = null;
            switch (orderStatus) {
                case UNPAID:
                    actions = new int[]{R.id.action_to_close, R.id.action_to_pay};
                    break;
                case PAID:
                    actions = new int[]{R.id.action_to_remind};
                    break;
                case SHIPPED:
                    actions = new int[]{R.id.action_to_track, R.id.action_to_confirm};
                    break;
                case DELIVERED:
                case CLOSED:
                    actions = new int[]{R.id.action_to_track};
                    break;
            }
            if (actions != null) {
                final int[] supportedActions = Arrays.copyOf(actions, actions.length);
                ButterKnife.apply(mActions, new ButterKnife.Action<TextView>() {
                    @Override
                    public void apply(TextView view, int index) {
                        if (isSupported(view.getId())) {
                            view.setVisibility(View.VISIBLE);
                        }
                    }

                    boolean isSupported(int viewId) {
                        for (int i = 0; i < supportedActions.length; i++) {
                            if (supportedActions[i] == viewId) {
                                return true;
                            }
                        }

                        return false;
                    }
                });
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and mAddressName
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.action_to_close)
    public void onClose() {

    }

    @OnClick(R.id.action_to_pay)
    public void onPay() {
        PaymentFragment paymentFragment = PaymentFragment.newInstance(mOrder.order_info);
        startActivityWithFragment(paymentFragment);
    }

    @OnClick(R.id.action_to_remind)
    public void onRemind() {

    }

    @OnClick(R.id.action_to_track)
    public void onTrack() {
        OrderTrackFragment orderTrackFragment = OrderTrackFragment.newInstance("", "", mOrderListViewItems);
        startActivityWithFragment(orderTrackFragment);
    }

    @OnClick(R.id.action_to_confirm)
    public void onConfirm() {

    }
}
