
package com.xz.btc.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetFarmersResponse {

    public STATUS status;

    public List<Farmer> data;

    public PAGINATED paginated;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        JSONArray subItemArray;

        STATUS status = new STATUS();
        status.fromJson(jsonObject);
        this.status = status;

        subItemArray = jsonObject.optJSONObject("data").optJSONArray("list");
        data = new ArrayList<>();
        if (null != subItemArray && subItemArray.length() > 0) {
            for (int i = 0; i < subItemArray.length(); i++) {
                JSONObject subItemObject = subItemArray.getJSONObject(i);
                Farmer subItem = new Farmer();
                subItem.fromJson(subItemObject);
                this.data.add(subItem);
            }
        }

        PAGINATED paginated = new PAGINATED();
        paginated.fromJson(jsonObject.optJSONObject("data").optJSONObject("pageInfo"));
        this.paginated = paginated;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }
}
