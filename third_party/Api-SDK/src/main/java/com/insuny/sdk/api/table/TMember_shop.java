package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TMember_shop{
    public String id;
    public String mid;
    public String title;
    public String is_company;
    public String company;
    public String voice;
    public String voice_file;
    public String pid;
    public String cate_id;
    public String years;
    public String areas;
    public String exp;
    public String organic;
    public String service_id;
    public String tech_id;
    public String province;
    public String city;
    public String area;
    public String town;
    public String province_id;
    public String city_id;
    public String area_id;
    public String rates;
    public String add_time;
    public String status;
    public String img;
    public String item_count;
    public String item_stock;
    public String follows;
    public String is_follow;
}