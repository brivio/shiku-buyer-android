package com.xz.btc.protocol;

/**
 * Created by txj on 15/3/6.
 */

import org.json.JSONObject;

public class TasteItem {

    public int id;
    public String image;
    public String title;
    public boolean isHot;
    public int status;
    public String url;

    public void fromJson(JSONObject jsonObject) {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optInt("id");
        this.image = jsonObject.optString("img");
        this.title = jsonObject.optString("name");
        this.isHot = jsonObject.optInt("is_hot", 0) == 1;
        this.status = jsonObject.optInt("status");
        this.url = jsonObject.optString("url");
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }
}
