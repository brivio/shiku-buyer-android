package com.xz.tframework.fragment;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.external.view.XListView;
import com.xz.shiku.R;
import com.xz.tframework.model.BusinessResponse;
import com.xz.tframework.utils.Utils;
import com.xz.tframework.view.RoundedWebImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class BaseFragment extends Fragment implements BusinessResponse,
        XListView.IXListViewListener,
        View.OnClickListener {
    protected View myView;

    //实现BusinessResponse
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
            throws JSONException {

    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onLoadMore() {
    }

    @Override
    public void onClick(View v) {

    }

    protected String getEditText(EditText et) {
        return Utils.getEditText(et);
    }

    protected String getEditText(EditText et, String def) {
        return Utils.getEditText(et, def);
    }

    protected String getEditText(int id, String def) {
        return Utils.getEditText(myView, id, def);
    }

    protected String getEditText(int id) {
        return Utils.getEditText(myView, id);
    }

    protected void setTextView(int id, String text) {
        Utils.setTextView(myView, id, text);
    }

    protected void setEditHint(int id, String text) {
        Utils.setEditHint(myView, id, text);
    }

    protected void setTextView(HashMap<Integer, String> map) {
        for (int key : map.keySet()) {
            Utils.setTextView(myView, key, map.get(key));
        }
    }

    protected void setEditText(int id, String text) {
        Utils.setEditText(myView, id, text);
    }

    protected void setEditText(int id, String format, Object... args) {
        Utils.setEditText(myView, id, format, args);
    }

    protected void setImageViewSrc(int id, int resource) {
        Utils.setImageViewSrc(myView, id, resource);
    }

    protected void setImageView(int id, String url) {
        Utils.setImageView(myView, getActivity(), id, url);
    }

    protected TextView getTextView(int id) {
        return Utils.getTextView(myView, id);
    }

    protected Bitmap setImageView(int id, Bitmap src) {
        return Utils.setImageView(myView, id, src);
    }

    protected Bitmap setImageViewLocal(int id, String path) {
        return Utils.setImageViewLocal(myView, id, path);
    }

    protected void removeAllViews(int id) {
        ViewGroup viewGroup = (ViewGroup) myView.findViewById(id);
        if (viewGroup != null) {
            viewGroup.removeAllViews();
        }
    }

    protected void showView(int id) {
        Utils.showView(myView, id);
    }

    protected void hideView(int id) {
        myView.findViewById(id).setVisibility(View.GONE);
    }

    protected void setOnClickListeners(int[] ids) {
        Utils.setOnClickListeners(myView, ids, this);
    }

    protected void setOnClickListener(int id, View.OnClickListener l) {
        Utils.setOnClickListener(myView, id, l);
    }

    protected void setOnClickListeners(Integer[] ids) {
        Utils.setOnClickListeners(myView, ids, this);
    }

    protected void setRoundedWebImageView(int id, String url) {
        ((RoundedWebImageView) myView.findViewById(id)).setImageWithURL(getActivity(), url);
    }
}
