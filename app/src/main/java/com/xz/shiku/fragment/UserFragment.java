package com.xz.shiku.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.USER;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.controller.OrderController;
import com.xz.shiku.controller.UserController;
import com.xz.shiku.fragment.address.AddressListFragment;
import com.xz.shiku.fragment.coupon.CouponFragment;
import com.xz.shiku.fragment.farmer.FarmerFragment;
import com.xz.shiku.fragment.message.MessageFragment;
import com.xz.shiku.fragment.order.OrderListFragment;
import com.xz.shiku.fragment.search.SearchFragment;
import com.xz.shiku.fragment.setting.SettingFragment;
import com.xz.shiku.fragment.taste.TasteFragment;
import com.xz.shiku.fragment.user.UserEditInfoFragment;
import com.xz.shiku.fragment.user.UserLoginFragment;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.view.PicturePicker.PicturePicker;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.xz.shiku.fragment.UserFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.xz.shiku.fragment.UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends BackHandledFragment implements PicturePicker.OnPickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    @InjectView(R.id.ll_usrlogin)
    LinearLayout mLayoutWithUserSignedin;
    @InjectView(R.id.ll_usrwithoutlogin)
    LinearLayout mLayoutWithUserSignedout;
    @InjectView(R.id.username)
    TextView mNickname;
    @InjectView(R.id.user_avatar)
    ImageView mAvatar;
    @InjectView(R.id.user_level)
    TextView mLevel;
    @InjectView(R.id.tv_jfsl)
    TextView mScore;
    @InjectView(R.id.tv_yhq)
    TextView mCoupon;
    @InjectView(R.id.tv_fav)
    TextView mFavourite;
    @InjectView(R.id.shopping_cart_num_bg)
    LinearLayout mOrderUnpaidLayout;
    @InjectView(R.id.shopping_cart_num)
    TextView mOrderUnpaidBadge;
    @InjectView(R.id.dfh_num_bg)
    LinearLayout mOrderPaidLayout;
    @InjectView(R.id.dfh_num)
    TextView mOrderPaidBadge;
    @InjectView(R.id.dsh_num_bg)
    LinearLayout mOrderShippedLayout;
    @InjectView(R.id.dsh_cart_num)
    TextView mOrderShippedBadge;

    PicturePicker mPickturePicker;
    boolean mNeedRefresh = true;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserFragment newInstance(String param1, String param2) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public UserFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_user, container, false);

        ButterKnife.inject(this, v);

        mPickturePicker = new PicturePicker(this, true);

        UserController.getInstance().addResponseListener(this);

        return v;
    }

    private void initUI(boolean isUserReady, USER user) {
        if (!isUserReady) {
            mLayoutWithUserSignedin.setVisibility(View.GONE);
            mLayoutWithUserSignedout.setVisibility(View.VISIBLE);
            mAvatar.setImageResource(R.drawable.product);
            mOrderUnpaidLayout.setVisibility(View.GONE);
            mOrderPaidLayout.setVisibility(View.GONE);
            mOrderShippedLayout.setVisibility(View.GONE);
        } else {
            mLayoutWithUserSignedin.setVisibility(View.VISIBLE);
            mLayoutWithUserSignedout.setVisibility(View.GONE);
            mNickname.setText(user.name);
            UILUtil.getInstance().getImage(getActivity(), mAvatar, user.avatar,
                    UILUtil.getInstance().getAlternativeDisplayImageOptions(),
                    false);
            mLevel.setText(String.format("%d", user.level));
            mScore.setText(String.format("%d", user.score));
            mCoupon.setText(String.format("%d", user.coupon));
            mFavourite.setText(String.format("%d", user.fav));

            if (user.order_fukuan > 0) {
                mOrderUnpaidBadge.setText(String.format("%d", user.order_fukuan));
                mOrderUnpaidLayout.setVisibility(View.VISIBLE);
            } else {
                mOrderUnpaidLayout.setVisibility(View.GONE);
            }
            if (user.order_fahuo > 0) {
                mOrderPaidBadge.setText(String.format("%d", user.order_fahuo));
                mOrderPaidLayout.setVisibility(View.VISIBLE);
            } else {
                mOrderPaidLayout.setVisibility(View.GONE);
            }
            if (user.order_shouhuo > 0) {
                mOrderShippedBadge.setText(String.format("%d", user.order_shouhuo));
                mOrderShippedLayout.setVisibility(View.VISIBLE);
            } else {
                mOrderShippedLayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        UserController.getInstance().removeResponseListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mNeedRefresh) {
            initUI(UserController.getInstance().isUserReady(), UserController.getInstance().getUser());
            mNeedRefresh = false;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_AVATAR)) {
            if (UserController.getInstance().getLastResponseStatus().getIsSuccess()) {
                UILUtil.getInstance().getImage(getActivity(), mAvatar,
                        UserController.getInstance().getUser().avatar,
                        false);
            } else {
                ToastView.showMessage(getActivity(), UserController.getInstance()
                        .getLastResponseStatus().getErrorMessage());
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPickturePicker.onActivityResult(requestCode, resultCode, data);
    }

    // 分类
    @OnClick(R.id.tv_menu)
    public void showCategory() {
        CateFragment cateFragment = CateFragment.newInstance(CateFragment.POPUP, "");
        startActivityWithFragment(cateFragment);
    }

    // 搜索
    @OnClick(R.id.index_search_edit)
    public void search() {
        SearchFragment searchFragment = new SearchFragment();
        startActivityWithFragment(searchFragment);
    }

    // 消息
    @Optional
    @OnClick(R.id.rightbtn)
    public void showMyMessages() {
        MessageFragment messageFragment = MessageFragment.newInstance("", "");
        startActivityWithFragment(messageFragment);
    }

    // 上传头像
    @OnClick(R.id.user_avatar)
    public void changeAvatar() {
        mPickturePicker.show();
    }

    // 编辑用户信息
    @OnClick(R.id.rl_usrlogin_top)
    public void editUser() {
        UserEditInfoFragment userEditInfoFragment = new UserEditInfoFragment();
        startActivityWithFragment(userEditInfoFragment);
        mNeedRefresh = true;
    }

    // 登录
    @OnClick(R.id.loginbtn)
    public void signin() {
        UserLoginFragment userLoginFragment = UserLoginFragment.newInstance(null);
        startActivityWithFragment(userLoginFragment);
        mNeedRefresh = true;
    }

    // 积分
    @OnClick({R.id.ll_jf, R.id.tv_jfsl, R.id.tv_jfsl_title})
    public void showMyScores() {
    }

    // 优惠券
    @OnClick({R.id.tv_yhq, R.id.tv_yhq_title})
    public void showMyCoupons() {
        if (UserController.getInstance().isUserReady()) {
            CouponFragment couponFragment = CouponFragment.newInstance(CouponFragment.MODE_VIEW, "");
            startActivityWithFragment(couponFragment);
        } else {
            signin();
        }
    }

    // 收藏
    @OnClick({R.id.tv_fav, R.id.tv_fav_title})
    public void showMyFavourites() {
        //TODO: Make sure what the user wants when this item is clicked.
        //FavoriteFragment favoriteFragment = FavoriteFragment.newInstance(FavoriteFragment.SHOW_IN_POPUP);
        //startActivityWithFragment(favoriteFragment);
    }

    // 待付款
    @OnClick(R.id.tv_dfk)
    public void showUnpaidOrders() {
        if (UserController.getInstance().isUserReady()) {
            OrderListFragment orderListFragment = OrderListFragment.newInstance(
                    OrderController.OrderStatus.UNPAID.getStringValue());
            startActivityWithFragment(orderListFragment);
        } else {
            signin();
        }
    }

    // 待发货
    @OnClick(R.id.tv_dfh)
    public void showUnshippedOrders() {
        if (UserController.getInstance().isUserReady()) {
            OrderListFragment orderListFragment = OrderListFragment.newInstance(
                    OrderController.OrderStatus.PAID.getStringValue());
            startActivityWithFragment(orderListFragment);
        } else {
            signin();
        }
    }

    // 待收货
    @OnClick(R.id.tv_dsh)
    public void showShippedOrders() {
        if (UserController.getInstance().isUserReady()) {
            OrderListFragment orderListFragment = OrderListFragment.newInstance(
                    OrderController.OrderStatus.SHIPPED.getStringValue());
            startActivityWithFragment(orderListFragment);
        } else {
            signin();
        }
    }

    // 我的订单
    @OnClick(R.id.wddd)
    public void showMyOrders() {
        if (UserController.getInstance().isUserReady()) {
            OrderListFragment orderListFragment = OrderListFragment.newInstance(
                    OrderController.OrderStatus.ALL.getStringValue());
            startActivityWithFragment(orderListFragment);
        } else {
            signin();
        }
    }

    // 喜欢
    @OnClick(R.id.ll_xh)
    public void showMyLikes() {
        FavoriteFragment favoriteFragment = FavoriteFragment.newInstance(FavoriteFragment.SHOW_IN_POPUP);
        startActivityWithFragment(favoriteFragment);
    }

    // 收货地址
    @OnClick(R.id.ll_shdz)
    public void showMyAddresses() {
        if (UserController.getInstance().isUserReady()) {
            AddressListFragment addressListFragment = AddressListFragment.newInstance( true);
            startActivityWithFragment(addressListFragment);
        } else {
            signin();
        }
    }

    // 农户
    @OnClick(R.id.ll_farm)
    public void showMyFavouriteFarms() {
        if (UserController.getInstance().isUserReady()) {
            FarmerFragment farmFragment = FarmerFragment.newInstance("", "");
            startActivityWithFragment(farmFragment);
        } else {
            signin();
        }
    }

    @OnClick(R.id.ll_taste)
    public void showMyTaste() {
        if (UserController.getInstance().isUserReady()) {
            TasteFragment tasteFragment = TasteFragment.newInstance("", "");
            startActivityWithFragment(tasteFragment);
        } else {
            signin();
        }
    }

    // 其他设置
    @OnClick(R.id.ll_qtsz)
    public void onSettings() {
        if (UserController.getInstance().isUserReady()) {
            SettingFragment settingFragment = SettingFragment.newInstance("", "");
            startActivityWithFragment(settingFragment);
        } else {
            signin();
        }
    }

    @Override
    public void onPick(Bitmap photo) {
        if (photo != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte b[] = baos.toByteArray();
            String base64EncodingImage = Base64.encodeToString(b, Base64.DEFAULT);

            UserController.getInstance().updateAvatar(getActivity(), base64EncodingImage);
            baos.reset();
        }
    }
}
