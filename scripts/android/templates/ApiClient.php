package <?=PACKAGE_PREFIX?>;
/*
该文件由脚本自动生成，不要手动修改
 */
import android.app.Dialog;
import android.content.Context;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;

import <?=PACKAGE_PREFIX?>.request.*;
import <?=PACKAGE_PREFIX?>.response.*;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ApiClient{
    protected boolean showProgress = true;
    protected OnApiClientListener apiClientListener;

    private AQuery aq;
    private Context context;

    public ApiClient(Context context, OnApiClientListener apiClientListener) {
        this.context = context;
        aq = new AQuery(context);
        this.apiClientListener = apiClientListener;
    }

    public ApiClient hideProgress() {
        showProgress = false;
        return this;
    }

    public interface OnApiClientListener {
        void beforeAjaxPost(Map<String, ?> params);

        void afterAjaxPost(String url, JSONObject jsonObject, AjaxStatus ajaxStatus);

        String getToken();

        Dialog getProgressDialog();
    }

    public interface OnSuccessListener {
        void callback(Object object);
    }

    public interface OnFailListener {
        void callback(Object object);
    }

    private <T> T jsonDecode(JSONObject object, Class<T> cls) {
        T result;
        Gson gson = new Gson();
        result = gson.fromJson(object.toString(), cls);
        return result;
    }

    private String jsonEncode(Object object) {
        Gson gson = new Gson();
        gson.toJson(object);
        return gson.toJson(object);
    }

    private void ajaxPost(String url, String request, AjaxCallback<JSONObject> callback) {
        if (showProgress) {
            aq.progress(apiClientListener.getProgressDialog());
        } else {
            aq.progress(null);
            showProgress = true;
        }
        Map<String, ?> params = new HashMap<>();
        try {
            JSONObject jsonObject = new JSONObject(request);
            params = getBeeQueryParams(jsonObject);
            callback.params(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        apiClientListener.beforeAjaxPost(params);

        aq.ajax(url, JSONObject.class, callback);
    }

    private Map<String, ?> getBeeQueryParams(JSONObject request) {
        Map<String, String> params = new HashMap<>();
        if (request == null) request = new JSONObject();
        params.put("data", request.toString());
        params.put("token", apiClientListener.getToken());
        return params;
    }

    private boolean callback(String url, JSONObject jsonObject, AjaxStatus ajaxStatus) {
        boolean result = false;
        apiClientListener.afterAjaxPost(url, jsonObject, ajaxStatus);

        try {
            result = jsonObject != null && jsonObject.getInt("status") != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }   
<?foreach($info['api'] as $key=>$val){?>
    
    public static final String <?=$key?> =ApiConfig.API_URL+"/<?=$val['url']?>";
    public void do<?=$key?>(<?=$key?>Request request, final OnSuccessListener successListener){
        do<?=$key?>(request,successListener,null);
    }    
    public void do<?=$key?>(<?=$key?>Request request, final OnSuccessListener successListener,final OnFailListener failListener) {
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                Object object = jsonDecode(jo, <?=$key?>Response.class);
                if (ApiClient.this.callback(url, jo, status)){
                    if (successListener != null) {
                        successListener.callback(object);
                    }
                }else{
                    if (failListener != null) {
                        failListener.callback(object);
                    }
                }
            }
        };
        ajaxPost(<?=$key?>, jsonEncode(request), cb);
    }
<?}?>
}
