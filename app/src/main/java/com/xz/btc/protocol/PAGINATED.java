
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "PAGINATED")
public class PAGINATED extends Model {

    @Column(name = "page")
    public int page;

    @Column(name = "total")
    public int total;

    @Column(name = "count")
    public int count;

    @Column(name = "more")
    public int more;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.page = jsonObject.optInt("page");
        this.total = jsonObject.optInt("totalPage");
        this.count = jsonObject.optInt("perPage");
        this.more = total - page;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("total", total);
        localItemObject.put("more", more);
        localItemObject.put("count", count);

        return localItemObject;
    }
}
