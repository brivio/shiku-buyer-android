package com.xz.external.activeandroid.query;

/**
 * Created by txj on 15/4/14.
 */

import com.xz.external.activeandroid.Model;

public final class Delete implements Sqlable {
	public Delete() {
	}

	public From from(Class<? extends Model> table) {
		return new From(table, this);
	}

	@Override
	public String toSql() {
		return "DELETE ";
	}
}