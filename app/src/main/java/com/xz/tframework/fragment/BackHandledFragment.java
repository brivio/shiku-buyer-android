package com.xz.tframework.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.xz.shiku.APP;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.tframework.tinterface.BackHandledInterface;

public abstract class BackHandledFragment extends BaseFragment {

    public static int titleResId;
    public static int topLeftTextResId;
    public static int topRightTextResId;

    public static String title;
    public static String topLeftText;
    public static String topRightText;

    protected BackHandledInterface mBackHandledInterface;

    /**
     * 所有继承BackHandledFragment的子类都将在这个方法中实现物理Back键按下后的逻辑
     * FragmentActivity捕捉到物理返回键点击事件后会首先询问Fragment是否消费该事件
     * 如果没有Fragment消息时FragmentActivity自己才会消费该事件
     */
    public abstract boolean onBackPressed();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!(getActivity() instanceof BackHandledInterface)) {
            throw new ClassCastException("Hosting Activity must implement BackHandledInterface");
        } else {
            this.mBackHandledInterface = (BackHandledInterface) getActivity();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //告诉FragmentActivity，当前Fragment在栈顶
        mBackHandledInterface.setSelectedFragment(this);
    }

    public void startActivityWithFragment(BackHandledFragment fragment) {
        PopActivity.gCurrentFragment = fragment;
        Intent intent = new Intent(getActivity(), PopActivity.class);
        startActivity(intent);
    }

    public void startActivityForResultWithFragment(BackHandledFragment fragment, int requestCode) {
        PopActivity.gCurrentFragment = fragment;
        Intent intent = new Intent(getActivity(), PopActivity.class);
        startActivityForResult(intent, requestCode);
    }

    /**
     * 添加Fragment *
     */
    public void addFragment(FragmentActivity activity, BackHandledFragment fragment, Fragment oldfragment) {
        APP.getInstance().getFragments().push(fragment);
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.add(R.id.container, fragment);
        ft.hide(oldfragment);
        ft.commitAllowingStateLoss();

        showFragment(fragment);
    }

    /**
     * 显示Fragment *
     */
    public void showFragment(Fragment fragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        // 设置Fragment的切换动画
        ft.setCustomAnimations(R.anim.tf_push_right_in, R.anim.tf_push_left_out);
        ft.show(fragment);
        ft.commitAllowingStateLoss();
    }

    /**
     * 删除Fragment *
     */
    public void removeFragment(Fragment fragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.remove(fragment);
        ft.commit();
    }
}
