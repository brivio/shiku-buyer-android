
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "MESSAGE")
public class MESSAGE extends Model {
    @Column(name = "message_id")
    public int id;

    @Column(name = "title")
    public String title;

    @Column(name = "info")
    public String info;

    @Column(name = "add_time")
    public String date;

    @Column(name = "status")
    public int status;

    /**
     * Unused fields below.
     */
    @Column(name = "content")
    public String content;

    @Column(name = "action")
    public String action;

    @Column(name = "parameter")
    public String parameter;

    @Column(name = "time")
    public String time;

    @Column(name = "custom_data")
    public String custom_data;

    @Column(name = "created_at")
    public String created_at;

    @Column(name = "img")
    public PHOTO img;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optInt("id");
        this.title = jsonObject.optString("title");
        this.info = jsonObject.optString("info");
        this.date = jsonObject.optString("add_time");
        this.status = jsonObject.optInt("status");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("content", content);
        localItemObject.put("action", action);
        localItemObject.put("parameter", parameter);
        localItemObject.put("time", time);
        localItemObject.put("id", id);
        localItemObject.put("custom_data", custom_data);
        localItemObject.put("created_at", created_at);
        if (null != img) {
            localItemObject.put("img", img.toJson());
        }

        return localItemObject;
    }
}
