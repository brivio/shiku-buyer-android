package com.xz.tframework;

public class TFrameworkConst {

    public static final String TAG = "xz";
    public static final int BALL_COLOR_RED = 0;
    public static final int BALL_COLOR_BLUE = 0;
    public static final Boolean DEBUG = true;
    /** 下载图片保存目录 */
    public static final String PIC_DIR_PATH = "/xz/TFramework/pic";
    /** 允许发送的最大字数 */
    public static final int MAX_CONTENT_LEN = 140;

    public static final int RESULT_PHOTO_PREVIEW = 2;


    /** 程序运行期间产生的文件，缓存根目录 */
    public static final String ROOT_DIR_PATH = "/xz/TFramework/cache";
    /** 缓存文件保存的根目录 */
    public static final String CACHE_DIR_PATH = ROOT_DIR_PATH + "/file";
    public static final String LOG_DIR_PATH = ROOT_DIR_PATH + "log";
    

}
