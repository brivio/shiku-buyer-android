package com.xz.shiku.fragment.search;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.activity.SearchResultActivity;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.utils.SharedPrefsUtil;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnItemClick;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class SearchFragment extends BackHandledFragment {

    @InjectView(R.id.index_search_edit)
    EditText mKeywords;
    @InjectView(R.id.recently_searched_keywords)
    ListView mSearchHistory;

    public SearchFragment() {
        // Required empty public constructor
        SearchFragment.topLeftTextResId = R.string.title_fragment_search;
        SearchFragment.topRightText = "";
        PopActivity.gShowNavigationBar = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tf_search, container, false);

        ButterKnife.inject(this, view);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1,
                SharedPrefsUtil.getInstance(getActivity()).getSearchHistory());
        mSearchHistory.setAdapter(adapter);


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @OnClick(R.id.cancel_search)
    public void onCancelSearch() {
        getActivity().finish();
    }

    @OnEditorAction(R.id.index_search_edit)
    public boolean onSearch(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            searchBy(v.getText().toString());

            return true;
        }
        return false;
    }

    @OnItemClick(R.id.recently_searched_keywords)
    void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        searchBy((String)parent.getItemAtPosition(position));
    }

    private void searchBy(String keyword) {
        SharedPrefsUtil.getInstance(getActivity()).addSearchHistory(keyword);

        Intent intent = new Intent(getActivity(), SearchResultActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(SearchResultActivity.SEARCH_ARG_BY, SearchResultActivity.KEYWORD);
        bundle.putString(SearchResultActivity.SEARCH_ARG_KEYWORD, keyword);
        intent.putExtras(bundle);
        startActivity(intent);

        getActivity().finish();
    }
}
