package com.xz.btc.protocol;

/**
 * Created by txj on 15/3/6.
 */

import org.json.JSONObject;

public class Farmer {

    public int id;
    public int userId;
    public int followerId;
    public String image;
    public String name;
    public String farmName;
    public double organicContent;
    public double yield;
    public String yieldUnit;
    public String address;
    public String productName;

    public void fromJson(JSONObject jsonObject) {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optInt("id");
        this.userId = jsonObject.optInt("uid");
        this.followerId = jsonObject.optInt("follow_uid");
        this.image = jsonObject.optString("img", null);
        this.name = jsonObject.optString("mname");
        this.farmName = jsonObject.optString("shop_title");
        this.organicContent = jsonObject.optDouble("organic");
        this.yield = jsonObject.optDouble("areas");
        this.yieldUnit = jsonObject.optString("unit_areas");
        this.address = jsonObject.optString("address");
        this.productName = jsonObject.optString("pname");
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }
}
