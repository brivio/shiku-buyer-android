
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "cartupdateRequest")
public class cartupdateRequest extends Model {

    @Column(name = "new_number")
    public int new_number;

    @Column(name = "rec_id")
    public int rec_id;

    /**
     * Unused fields.
     */
    @Column(name = "session")
    public SESSION session;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.new_number = jsonObject.optInt("new_number");
        this.rec_id = jsonObject.optInt("rec_id");

        SESSION session = new SESSION();
        session.fromJson(jsonObject.optJSONObject("session"));
        this.session = session;
    }

    public JSONArray toJson() {
        JSONArray jsonArray = new JSONArray();
        JSONObject localItemObject = new JSONObject();

        try {
            localItemObject.put("item_id", rec_id);
            localItemObject.put("num", new_number);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonArray.put(localItemObject);

        return jsonArray;
    }
}
