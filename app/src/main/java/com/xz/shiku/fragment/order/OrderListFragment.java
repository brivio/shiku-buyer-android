package com.xz.shiku.fragment.order;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.GOODORDER;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.external.view.XListView;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.adapter.order.OrderListAdapter;
import com.xz.shiku.controller.OrderController;
import com.xz.shiku.controller.UserController;
import com.xz.shiku.fragment.payment.PaymentFragment;
import com.xz.tframework.fragment.BackHandledFragment;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.xz.shiku.fragment.order.OrderListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.xz.shiku.fragment.order.OrderListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderListFragment extends BackHandledFragment implements OrderListAdapter.OnOrderActionListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ORDER_STATUS = "arg_order_status";

    private OrderController.OrderStatus mOrderStatus;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    OrderListAdapter mOrderListAdapter;

    @InjectView(R.id.swlist)
    XListView mOrders;
    @InjectView(R.id.ll_empty)
    LinearLayout mEmptyLayout;
    @InjectView(R.id.ll_empty_icon)
    TextView mEmptyIcon;
    @InjectView(R.id.ll_empty_text)
    TextView mEmptyText;
    @InjectView(R.id.ll_empty_subtext)
    TextView mEmptySubtext;
    @InjectViews({R.id.tv_gp1, R.id.tv_gp2, R.id.tv_gp3, R.id.tv_gp4, R.id.tv_gp5})
    List<TextView> mOrderStatusViews;
    @InjectViews({R.id.tv_indicator1, R.id.tv_indicator2, R.id.tv_indicator3, R.id.tv_indicator4, R.id.tv_indicator5})
    List<View> mOrderStatusViewIndicators;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param orderStatus The order status.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrderListFragment newInstance(String orderStatus) {
        PopActivity.gShowNavigationBar = true;
        titleResId = R.string.title_fragment_order_list;
        topRightText = "";

        OrderListFragment fragment = new OrderListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ORDER_STATUS, orderStatus);
        fragment.setArguments(args);
        return fragment;
    }

    public OrderListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mOrderStatus = OrderController.OrderStatus.mapStringToValue(
                    getArguments().getString(ARG_ORDER_STATUS));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_orderlist, container, false);

        ButterKnife.inject(this, v);

        mEmptyIcon.setText(getResources().getString(R.string.icon_star));
        mEmptyText.setText("您还没有订单");
        mEmptySubtext.setText("主人快给我挑点宝贝吧");

        if (UserController.getInstance().isUserReady()) {
            OrderController.getInstance().addResponseListener(this);
            OrderController.getInstance().getOrder(getActivity(), mOrderStatus.getStringValue());
        } else {
            initUI(false);
        }

        return v;
    }

    private void initUI(boolean showOrderLayout) {
        if (showOrderLayout) {
            mEmptyLayout.setVisibility(View.GONE);

            mOrders.setPullRefreshEnable(true);
            mOrders.setPullLoadEnable(true);
            mOrders.setXListViewListener(this);
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
        }

        final int orderStatusViewId;
        switch (mOrderStatus) {
            case UNPAID:
                orderStatusViewId = R.id.tv_gp2;
                break;
            case PAID:
                orderStatusViewId = R.id.tv_gp3;
                break;
            case SHIPPED:
                orderStatusViewId = R.id.tv_gp4;
                break;
            case DELIVERED:
                orderStatusViewId = R.id.tv_gp5;
                break;
            default:
                orderStatusViewId = R.id.tv_gp1;
                break;
        }

        ButterKnife.apply(mOrderStatusViews, new ButterKnife.Action<TextView>() {
            @Override
            public void apply(TextView view, int index) {
                if (view.getId() == orderStatusViewId) {
                    view.setTextColor(getActivity().getResources().getColor(R.color.bg_Main));
                    mOrderStatusViewIndicators.get(index).setBackgroundResource(R.color.bg_Main);
                } else {
                    view.setTextColor(getActivity().getResources().getColor(R.color.text_color));
                    mOrderStatusViewIndicators.get(index).setBackgroundResource(R.color.white);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        OrderController.getInstance().removeResponseListener(this);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onView(int position, GOODORDER order) {
        OrderDetailsFragment fragment = OrderDetailsFragment.newInstance("", "");
        int count = order.goods_list.size() + 2;
        fragment.mOrderListViewItems = mOrderListAdapter.getOrderItems(position - count + 1,
                count);
        startActivityWithFragment(fragment);
    }

    @Override
    public void onClose(int position, GOODORDER order) {
        if (order != null && order.order_info.order_id > 0) {
            int header = position - order.goods_list.size() - 1;
            mOrders.setTag(header);
            OrderController.getInstance().close(getActivity(), order.order_info.order_id);
        }
    }

    @Override
    public void onPay(int position, GOODORDER order) {
        PaymentFragment paymentFragment = PaymentFragment.newInstance(order.order_info);
        startActivityWithFragment(paymentFragment);
    }

    @Override
    public void onRemind(int position, GOODORDER order) {

    }

    @Override
    public void onTrack(int position, GOODORDER order) {
        int count = order.goods_list.size() + 2;

        OrderTrackFragment fragment = OrderTrackFragment.newInstance("", "",
                mOrderListAdapter.getOrderItems(position - count + 1, count));
        startActivityWithFragment(fragment);
    }

    @Override
    public void onConfirm(int position, GOODORDER order) {
        if (order != null && order.order_info.order_id > 0) {
            int header = position - order.goods_list.size() - 1;
            mOrders.setTag(header);
            OrderController.getInstance().confirm(getActivity(), order.order_info.order_id);
        }
    }

    @Override
    public void onArchive(int position, GOODORDER order) {

    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.ORDER_LIST)) {
            if (OrderController.getInstance().getLastResponseStatus().getIsSuccess()) {
                List<OrderController.OrderListViewItem> orders = OrderController.getInstance().getOrderListViewData();
                if (OrderController.getInstance().getIsFirstPage()) {
                    if (orders != null && orders.size() > 0) {
                        initUI(true);

                        mOrders.stopRefresh();
                        mOrders.setRefreshTime();

                        mOrderListAdapter = new OrderListAdapter(getActivity(), orders);
                        mOrderListAdapter.setOnOrderActionListener(this);
                        mOrders.setAdapter(mOrderListAdapter);
                    } else {
                        initUI(false);
                    }
                } else {
                    mOrders.stopLoadMore();
                    mOrderListAdapter.notifyDataSetChanged();
                }

                if (!OrderController.getInstance().getHasMore()) {
                    mOrders.setPullLoadEnable(false);
                }
            }
        } else if (url.endsWith(ApiInterface.ORDER_CANCEL) ||
                url.endsWith(ApiInterface.ORDER_AFFIRMRECEIVED)) {
            if (OrderController.getInstance().getLastResponseStatus().getIsSuccess()) {
                if (mOrderStatus == OrderController.OrderStatus.ALL) {
                    OrderController.getInstance().getOrder(getActivity(), mOrderStatus.getStringValue());
                } else {
                    int postion = (int) mOrders.getTag();
                    mOrderListAdapter.updateOrderStatus(postion);
                }
            }
        }
    }

    public void onRefresh() {
        OrderController.getInstance().getOrder(getActivity(), mOrderStatus.getStringValue());
    }

    @Override
    public void onLoadMore() {
        if (OrderController.getInstance().getHasMore()) {
            OrderController.getInstance().getMoreOrder(getActivity(), mOrderStatus.getStringValue());
        }
    }

    @OnClick({R.id.tv_gp1, R.id.tv_gp2, R.id.tv_gp3, R.id.tv_gp4, R.id.tv_gp5})
    public void onChangeOrderStatus(View view) {
        switch (view.getId()) {
            case R.id.tv_gp2:
                mOrderStatus = OrderController.OrderStatus.UNPAID;
                break;
            case R.id.tv_gp3:
                mOrderStatus = OrderController.OrderStatus.PAID;
                break;
            case R.id.tv_gp4:
                mOrderStatus = OrderController.OrderStatus.SHIPPED;
                break;
            case R.id.tv_gp5:
                mOrderStatus = OrderController.OrderStatus.DELIVERED;
                break;
            default:
                mOrderStatus = OrderController.OrderStatus.ALL;
                break;
        }

        if (UserController.getInstance().isUserReady()) {
            OrderController.getInstance().getOrder(getActivity(), mOrderStatus.getStringValue());
        } else {
            initUI(false);
        }
    }

    @OnItemClick(R.id.swlist)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        OrderController.OrderListViewItem item = (OrderController.OrderListViewItem) parent.
                getItemAtPosition(position);
        if (item != null && item.mItemType == OrderController.ItemType.HEADER) {
            OrderDetailsFragment fragment = OrderDetailsFragment.newInstance("", "");
            fragment.mOrderListViewItems = mOrderListAdapter.getOrderItems(position - 1,
                    item.mOrder.goods_list.size() + 2);
            startActivityWithFragment(fragment);
        }
    }
}
