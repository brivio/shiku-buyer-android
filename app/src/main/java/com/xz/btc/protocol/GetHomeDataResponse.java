
package com.xz.btc.protocol;

import org.json.JSONException;
import org.json.JSONObject;

public class GetHomeDataResponse {

    public HomeData data;
    public STATUS status;

    public void fromJson(JSONObject jsonObject) {
        if (null == jsonObject) {
            return;
        }

        STATUS status = new STATUS();
        try {
            status.fromJson(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.status = status;

        HomeData data = new HomeData();
        data.fromJson(jsonObject);
        this.data = data;
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }
}
