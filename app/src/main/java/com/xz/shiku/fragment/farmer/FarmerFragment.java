package com.xz.shiku.fragment.farmer;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.Farmer;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.adapter.farmer.FarmerAdapter;
import com.xz.shiku.controller.FarmerController;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.SwipeListView;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FarmerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FarmerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FarmerFragment extends BackHandledFragment implements AbOnItemClickListener, AdapterView.OnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private boolean hadIntercept;

    @InjectView(R.id.collect_list)
    SwipeListView mFarmers;
    @InjectView(R.id.ll_empty)
    LinearLayout mEmptyLayout;
    @InjectView(R.id.ll_empty_icon)
    TextView mEmptyIcon;
    @InjectView(R.id.ll_empty_text)
    TextView mEmptyText;
    @InjectView(R.id.ll_empty_subtext)
    TextView mEmptySubtext;

    FarmerAdapter mFarmerAdapter;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FarmerFragment newInstance(String param1, String param2) {
        PopActivity.gShowNavigationBar = true;
        titleResId = R.string.title_fragment_farmer;
        topRightText = "";

        FarmerFragment fragment = new FarmerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FarmerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_farmer, container, false);

        ButterKnife.inject(this, v);

        mEmptyIcon.setText(getResources().getString(R.string.iconlove));
        mEmptyText.setText("您还没有关注的农户");
        mEmptySubtext.setText("快去关注感兴趣的农户吧");

        FarmerController.getInstance().addResponseListener(this);
        FarmerController.getInstance().getFarmers(getActivity());

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        FarmerController.getInstance().removeResponseListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.FARMER_LIST)) {
            List<Farmer> farmers = FarmerController.getInstance().getFarmersResult();
            if (FarmerController.getInstance().getIsFirstPage()) {
                if (farmers != null && farmers.size() > 0) {
                    initUI(true);

                    mFarmers.setRefreshTime();
                    mFarmers.stopRefresh();
                    if (FarmerController.getInstance().getHasMore()) {
                        mFarmers.setPullLoadEnable(true);
                    }
                    mFarmerAdapter = new FarmerAdapter(getActivity(), farmers, mFarmers.getRightViewWidth(), this);
                    mFarmers.setAdapter(mFarmerAdapter);
                } else {
                    initUI(false);
                }
            } else {
                mFarmers.stopLoadMore();
                mFarmerAdapter.notifyDataSetChanged();
            }
        } else if (url.endsWith(ApiInterface.FARMER_DELETE)) {
            List<Farmer> farmers = FarmerController.getInstance().getFarmersResult();
            if (farmers != null && farmers.size() > 0) {
                mFarmerAdapter.notifyDataSetChanged();
            } else {
                initUI(false);
            }
        }
    }

    private void initUI(boolean showFarmerLayout) {
        if (showFarmerLayout) {
            mEmptyLayout.setVisibility(View.GONE);

            mFarmers.setPullRefreshEnable(true);
            mFarmers.setPullLoadEnable(false);
            mFarmers.setXListViewListener(this);
            mFarmers.setOnItemClickListener(this);
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        FarmerController.getInstance().getFarmers(getActivity());
    }

    @Override
    public void onLoadMore() {
        if (FarmerController.getInstance().getHasMore()) {
            FarmerController.getInstance().getMoreFarmers(getActivity());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //TODO: Not sure what's to do here.
    }

    @Override
    public void onClick(int position) {
        Farmer farmer = (Farmer) mFarmerAdapter.getItem(position);
        if (farmer != null) {
            FarmerController.getInstance().delete(getActivity(), farmer.followerId);
        }
    }
}
