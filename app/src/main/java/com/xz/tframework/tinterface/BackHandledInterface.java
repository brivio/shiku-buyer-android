package com.xz.tframework.tinterface;

import com.xz.tframework.fragment.BackHandledFragment;

/**
 * Created by txj on 15/4/16.
 */
public interface BackHandledInterface {

    public abstract void setSelectedFragment(BackHandledFragment selectedFragment);
}
