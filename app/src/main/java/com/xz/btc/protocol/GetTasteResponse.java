
package com.xz.btc.protocol;

import org.json.JSONException;
import org.json.JSONObject;

public class GetTasteResponse {

    public STATUS status;
    public TasteResult data;
    public int resultType;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        STATUS status = new STATUS();
        status.fromJson(jsonObject);
        this.status = status;

        TasteResult result = new TasteResult();
        switch (resultType) {
            case TasteResult.RESULT_LISTS:
                result.fromJson(jsonObject.optJSONObject("data"), resultType);
                break;
            case TasteResult.RESULT_CADIDATE:
                result.fromJson(jsonObject, resultType);
                break;
        }
        this.data = result;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }
}
