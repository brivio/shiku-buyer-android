
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "userinfoResponse")
public class userinfoResponse extends Model {

    @Column(name = "status")
    public STATUS status;

    @Column(name = "data")
    public USER data;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        STATUS status = new STATUS();
        status.fromJson(jsonObject.optJSONObject("status"));
        this.status = status;

        USER data = new USER();
        data.fromJson(jsonObject.optJSONObject("data"));
        this.data = data;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        if (null != status) {
            localItemObject.put("status", status.toJson());
        }
        if (null != data) {
            localItemObject.put("data", data.toJson());
        }

        return localItemObject;
    }
}
