package com.xz.btc.model;

/**
 * Created by txj on 15/4/14.
 */

import android.content.Context;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.INTEGRAL;
import com.xz.btc.protocol.PAGINATED;
import com.xz.btc.protocol.PAGINATION;
import com.xz.btc.protocol.SESSION;
import com.xz.btc.protocol.userintegrallistRequest;
import com.xz.btc.protocol.userintegrallistResponse;
import com.xz.btc.protocol.usermessagedeleteRequest;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.external.androidquery.util.AQUtility;
import com.xz.shiku.R;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.model.BeeCallback;
import com.xz.tframework.view.MyProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class IntegralModel extends BaseModel {

    public ArrayList<INTEGRAL> mList = new ArrayList<INTEGRAL>();

    public PAGINATED paginated;

    public IntegralModel(Context context) {
        super(context);

    }

    public void getList() {
        userintegrallistRequest request = new userintegrallistRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                IntegralModel.this.callback(url, jo, status);

                try {
                    userintegrallistResponse response = new userintegrallistResponse();
                    response.fromJson(jo);
                    if (jo != null) {
                        if (response.status.succeed == 1) {
                            ArrayList<INTEGRAL> data = response.data;
                            mList.clear();
                            if (null != data && data.size() > 0) {
                                mList.clear();
                                mList.addAll(data);

                            }
                            paginated = response.paginated;
                            IntegralModel.this.OnMessageResponse(url, jo, status);
                        }
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

        };

        SESSION session = SESSION.getInstance();
        PAGINATION pagination = new PAGINATION();
        pagination.page = 1;
        pagination.count = 10;

        request.session = session;
        request.pagination = pagination;
        request.rec_id = 0;

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("json", request.toJson().toString());
        } catch (JSONException e) {
            // TODO: handle exception
        }
        cb.url(ApiInterface.USER_INTEGRAL_LIST).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext, mContext.getResources().getString(R.string.hold_on));
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }
    }


    public void getListMore() {
        userintegrallistRequest request = new userintegrallistRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                IntegralModel.this.callback(url, jo, status);

                try {
                    userintegrallistResponse response = new userintegrallistResponse();
                    response.fromJson(jo);
                    if (response.status.succeed == 1) {
                        ArrayList<INTEGRAL> data = response.data;
                        if (null != data && data.size() > 0) {
                            mList.addAll(data);

                        }
                        paginated = response.paginated;
                        IntegralModel.this.OnMessageResponse(url, jo, status);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

        };

        SESSION session = SESSION.getInstance();
        PAGINATION pagination = new PAGINATION();
        pagination.page = 1;
        pagination.count = 10;
        request.session = session;
        request.pagination = pagination;
        //request.rec_id= Integer.parseInt(mList.get(mList.size() - 1).rec_id);

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("json", request.toJson().toString());
        } catch (JSONException e) {
            // TODO: handle exception
        }
        cb.url(ApiInterface.USER_INTEGRAL_LIST).type(JSONObject.class).params(params);
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }


    // 删除消息
    public void itemDelete(String rec_id) {
        usermessagedeleteRequest request = new usermessagedeleteRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                IntegralModel.this.callback(url, jo, status);

                try {
                    userintegrallistResponse response = new userintegrallistResponse();
                    response.fromJson(jo);
                    if (jo != null) {
                        if (response.status.succeed == 1) {
                            IntegralModel.this.OnMessageResponse(url, jo, status);
                        }
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };
        request.rec_id = rec_id;
        request.session = SESSION.getInstance();
        Map<String, String> params = new HashMap<String, String>();
        params.put("json", request.toJson().toString());
        cb.url(ApiInterface.USER_INTEGRAL_DELETE).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext, mContext.getResources().getString(R.string.hold_on));
        aq.progress(pd.mDialog).ajax(cb);

    }

}
