
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@Table(name = "Shop")
public class Shop extends Model {

    @Column(name = "shop_id")
    public String id;

    @Column(name = "shop_name")
    public String name;

    @Column(name = "shop_image")
    public String image;

    @Column(name = "shop_rating")
    public int rating;

    @Column(name = "total_products")
    public int total_product;

    @Column(name = "total_stock")
    public int total_stock;

    @Column(name = "total_followers")
    public int total_followers;

    @Column(name = "address_province")
    public String address_province;

    @Column(name = "address_city")
    public String address_city;

    @Column(name = "address_county")
    public String address_county;

    @Column(name = "address_details")
    public String address_details;

    public List<ProductImage> images;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optString("id");
        this.name = jsonObject.optString("title");
        this.image = jsonObject.optString("img");
        this.rating = jsonObject.optInt("rates");
        this.total_product = jsonObject.optInt("item_count");
        this.total_stock = jsonObject.optInt("item_stock");
        this.total_followers = jsonObject.optInt("follows");
        this.address_province = jsonObject.optString("province");
        this.address_city = jsonObject.optString("city");
        this.address_county = jsonObject.optString("area");
        this.address_details = jsonObject.optString("address");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();
        JSONArray itemJSONArray = new JSONArray();

        return localItemObject;
    }
}
