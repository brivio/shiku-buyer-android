package com.xz.shiku.fragment.payment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.xz.btc.AppConst;
import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.ORDER_INFO;
import com.xz.btc.protocol.ORDER_PAY_DATA;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.alipay.PayResult;
import com.xz.shiku.controller.OrderController;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class PaymentFragment extends BackHandledFragment {
    @InjectView(R.id.tv_order_id)
    TextView mOrderId;
    @InjectView(R.id.tv_order_total)
    TextView mOrderTotal;
    @InjectView(R.id.tv_order_shipping_rates)
    TextView mOrderShippingRates;

    ORDER_INFO mOrderInfo;

    /**
     * 支付宝支付接口
     */
    private static final int SDK_PAY_FLAG = 1;
    private static final int SDK_CHECK_FLAG = 2;

    Handler mAlipayHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((String) msg.obj);

                    // 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
                    String resultInfo = payResult.getResult();

                    String resultStatus = payResult.getResultStatus();

                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        ToastView.showMessage(getActivity(), "支付成功");
                    } else {
                        // 判断resultStatus 为非“9000”则代表可能支付失败
                        // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(getActivity(), "支付结果确认中",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            ToastView.showMessage(getActivity(), "支付失败");
                        }
                    }
                    break;
                }
                case SDK_CHECK_FLAG: {
                    Toast.makeText(getActivity(), "检查结果为：" + msg.obj,
                            Toast.LENGTH_SHORT).show();
                    break;
                }
                default:
                    break;
            }
        }
    };

    /**
     * 微信支付接口
     */
    IWXAPI mWxApi;

    public static PaymentFragment newInstance(ORDER_INFO orderInfo) {
        PopActivity.gShowNavigationBar = true;

        titleResId = R.string.title_fragment_payment;
        topRightText = "";

        PaymentFragment fragment = new PaymentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.mOrderInfo = orderInfo;

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_payment, container, false);

        ButterKnife.inject(this, view);

        if (mOrderInfo != null) {
            mOrderId.setText(String.format("订单编号：%s", mOrderInfo.order_sn));
            mOrderTotal.setText(String.format("订单金额（含运费）：￥%.2f 元", mOrderInfo.total));
            mOrderShippingRates.setText(String.format("运费：￥%.2f 元", mOrderInfo.shipping_rates));
        }
        // WXPay API
        mWxApi = WXAPIFactory.createWXAPI(getActivity(), null);

        OrderController.getInstance().addResponseListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        OrderController.getInstance().removeResponseListener(this);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.ORDER_PAY)) {
            if (OrderController.getInstance().getLastResponseStatus().getIsSuccess()) {
                ORDER_PAY_DATA result = OrderController.getInstance().getPayResult();
                switch (result.payWith) {
                    case AppConst.PAY_TYPE_ALIPAY:
                        final String orderString = result.aliPayResult.orderString.replace("\\", "");
                        Runnable aliPayTask = new Runnable() {

                            @Override
                            public void run() {
                                // 构造PayTask 对象
                                PayTask payTask = new PayTask(getActivity());

                                // 调用支付接口，获取支付结果
                                String payResult = payTask.pay(orderString);

                                Message msg = new Message();
                                msg.what = SDK_PAY_FLAG;
                                msg.obj = payResult;
                                mAlipayHandler.sendMessage(msg);
                            }
                        };

                        // 必须异步调用
                        new Thread(aliPayTask)
                                .start();
                        break;
                    case AppConst.PAY_TYPE_WXPAY:
                        PayReq req = new PayReq();

                        req.appId = result.wxPayResult.appId;
                        req.partnerId = result.wxPayResult.partnerId;
                        req.prepayId = result.wxPayResult.prepayId;
                        req.packageValue = result.wxPayResult.packageName;
                        req.nonceStr = result.wxPayResult.noncestr;
                        req.timeStamp = String.format("%d", result.wxPayResult.timeStamp);
                        req.sign = result.wxPayResult.sign;
                        mWxApi.registerApp(AppConst.WX_APP_ID);
                        mWxApi.sendReq(req);
                        break;
                }
            }
        }
    }

    @OnClick(R.id.ll_zffs1)
    public void onPayWithAlipay() {
        if (mOrderInfo != null && mOrderInfo.order_id > 0) {
            OrderController.getInstance().payWith(getActivity(),
                    mOrderInfo.order_id,
                    AppConst.PAY_TYPE_ALIPAY);
        }
    }

    @OnClick(R.id.ll_zffs2)
    public void onPayWithWXpay() {
        if (mOrderInfo != null && mOrderInfo.order_id > 0) {
            OrderController.getInstance().payWith(getActivity(),
                    mOrderInfo.order_id,
                    AppConst.PAY_TYPE_WXPAY);
        }
    }
}
