package com.insuny.sdk.api.response;
/*
该文件由脚本自动生成，不要手动修改
 */
import com.insuny.sdk.api.table.TItem;
import java.util.ArrayList;

public class ItemGetResponse{
    public TItem data;
    public String status;
    public String result;
}