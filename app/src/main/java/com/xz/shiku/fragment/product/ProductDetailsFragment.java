package com.xz.shiku.fragment.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.insuny.sdk.api.ApiClient;
import com.insuny.sdk.api.extra.ExLogs_class_item;
import com.insuny.sdk.api.request.ItemFollowRequest;
import com.insuny.sdk.api.request.ItemGetRequest;
import com.insuny.sdk.api.request.ItemUnfollowRequest;
import com.insuny.sdk.api.request.LogsGetRequest;
import com.insuny.sdk.api.request.User_likeAddRequest;
import com.insuny.sdk.api.request.User_likeDeleteRequest;
import com.insuny.sdk.api.response.ItemGetResponse;
import com.insuny.sdk.api.response.LogsGetResponse;
import com.insuny.sdk.api.table.TItem;
import com.insuny.sdk.api.table.TItem_img;
import com.insuny.sdk.api.table.TLogs;
import com.xz.shiku.R;
import com.xz.shiku.ShareHelper;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.activity.ProductOptionsActivity;
import com.xz.shiku.controller.FavoriteController;
import com.xz.shiku.controller.SearchController;
import com.xz.shiku.controller.ShoppingCartController;
import com.xz.shiku.fragment.BaseShikuFragment;
import com.xz.shiku.fragment.CartFragment;
import com.xz.shiku.fragment.search.SearchFragment;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.utils.Utils;
import com.xz.tframework.view.MyScrollView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ProductDetailsFragment extends BaseShikuFragment {

    private static final int REQUEST_TO_ADD_TO_CART = 1;
    private static final String ARG_PRODCUT_ID = "arg_product_id";
    private String mProductId;
    ViewPagerAdapter mViewPagerAdapter;
    private TItem mItem;

    //region 控件声明
    @InjectView(R.id.shopping_cart_badge)
    LinearLayout mShoppingCartBadge;
    @InjectView(R.id.iv_baby)
    ViewPager mProductImages;
    @InjectView(R.id.indicatorContainer)
    LinearLayout mIndicatorContainer;
    @InjectView(R.id.activity_product_details_title)
    TextView mProductName;
    @InjectView(R.id.add_to_fav)
    ImageView mAddToFav;
    @InjectView(R.id.activity_product_details_total_favs)
    TextView mProductTotalFavs;
    @InjectView(R.id.activity_product_details_price)
    TextView mProductPrice;
    @InjectView(R.id.activity_product_details_shipping_rates)
    TextView mProductShippingRates;
    @InjectView(R.id.activity_product_details_total_sales)
    TextView mProductTotalSales;
    @InjectView(R.id.activity_product_details_origin)
    TextView mProductOrigin;

    @InjectView(R.id.user_avatar)
    ImageView mShopAvatar;
    @InjectView(R.id.activity_product_details_shop_name)
    TextView mShopName;
    @InjectView(R.id.activity_product_details_shop_total_products)
    TextView mShopTotalProducts;
    @InjectView(R.id.activity_product_details_shop_total_stock)
    TextView mShopTotalStock;
    @InjectView(R.id.activity_product_details_shop_total_followers)
    TextView mShopTotalFollowers;
    @InjectView(R.id.activity_product_details_shop_rating)
    RatingBar mShopRating;
    @InjectView(R.id.add_to_cart)
    Button mAddToCart;

    @InjectView(R.id.activity_product_details)
    MyScrollView msvProductDetails;
    @InjectView(R.id.tvSplitStart)
    TextView tvSplitStart;
    @InjectView(R.id.llSplitEnd)
    LinearLayout llSplitEnd;

    @InjectView(R.id.item_detail_img_list)
    LinearLayout item_detail_img_list_ll;
    @InjectView(R.id.item_detail_pack_img_list)
    LinearLayout item_detail_pack_img_list_ll;
    //endregion

    PullToRefreshScrollView detailScrollView;

    public static ProductDetailsFragment newInstance(String productId) {
        PopActivity.gShowNavigationBar = false;

        ProductDetailsFragment fragment = new ProductDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PRODCUT_ID, productId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProductId = getArguments().getString(ARG_PRODCUT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.layout_product_details, container, false);

        ButterKnife.inject(this, myView);

        SearchController.getInstance().addResponseListener(this);
        SearchController.getInstance().getProductDetails(getActivity(), Utils.tryParseInteger(mProductId, 0));
        FavoriteController.getInstance().addResponseListener(this);

        ItemGetRequest request = new ItemGetRequest();
        request.id = mProductId;
        apiClient.doItemGet(request, new ApiClient.OnSuccessListener() {
            @Override
            public void callback(Object object) {
                ItemGetResponse response = (ItemGetResponse) object;
                initUI(response.data);
            }
        });
        detailScrollView = (PullToRefreshScrollView) myView.findViewById(R.id.activity_product_details_pullScrollView);
        return myView;
    }

    private void initUI(TItem info) {
        mItem = info;
        LayoutInflater inflater = getActivity().getLayoutInflater();
        List<View> images = new ArrayList<>();
        List<View> indicators = new ArrayList<>();

        for (int i = 0; i < info.item_img.size(); i++) {
            View view = inflater.inflate(R.layout.tf_item_pic_centercrop, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.pic_item);
            UILUtil.getInstance().getImage(getActivity(), imageView, info.item_img.get(i).img);
            images.add(view);

            View indicator = new View(getActivity());
            indicator.setBackgroundResource(i == 0 ?
                    R.drawable.tf_layoutwithstorkemainroundfill :
                    R.drawable.tf_layoutwithstorkeroundwhitefill);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(20, 20);
            lp.setMargins(0, 0, 10, 0);
            indicator.setLayoutParams(lp);
            indicator.setTag(i);

            indicators.add(indicator);
            mIndicatorContainer.addView(indicator);
        }
        mViewPagerAdapter = new ViewPagerAdapter(images, indicators);
        mProductImages.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int mLastPosition = 0;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (mLastPosition != position) {
                    mViewPagerAdapter.getIndicator(mLastPosition).setBackgroundResource(R.drawable.tf_layoutwithstorkeroundwhitefill);
                    mViewPagerAdapter.getIndicator(position).setBackgroundResource(R.drawable.tf_layoutwithstorkemainroundfill);
                    mLastPosition = position;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mProductImages.setAdapter(mViewPagerAdapter);

        mProductName.setText(info.title);
        mAddToFav.setImageResource(info.is_like.equals("1") ?
                R.drawable.ic_favourite_selected :
                R.drawable.ic_favourite_outline);

        mProductTotalFavs.setText(String.format("%s人喜欢", info.favs));
        mProductPrice.setText(info.price);

        mProductShippingRates.setText(Double.parseDouble(info.express) < 1E-5 ?
                "卖家包邮" : String.format("运费：￥%s", info.express));
        mProductTotalSales.setText(String.format("%s人已购买", info.sales));
        mProductOrigin.setText(info.member_shop.province);

        if (info.member_shop.is_follow.equals("1")) {
            setTextView(R.id.shop_follow, "取消关注");
        } else {
            setTextView(R.id.shop_follow, "关注");
        }

        UILUtil.getInstance().getImage(getActivity(), mShopAvatar, info.member_shop.img);
        mShopName.setText(info.member_shop.title);
        mShopTotalProducts.setText(info.member_shop.item_count);
        mShopTotalStock.setText(info.member_shop.item_stock);
        mShopTotalFollowers.setText(info.member_shop.follows);

        mShopRating.setRating(Utils.parseFloat(info.member_shop.rates));
        mAddToCart.setBackgroundResource(Utils.parseInt(info.stock) > 0 ? R.color.bg_Main : R.color.bg_Main_split);
        mAddToCart.setEnabled(Utils.parseInt(info.stock) > 0);

        msvProductDetails.setSplitView(tvSplitStart, llSplitEnd);

        detailScrollView.setOnPullEventListener(new PullToRefreshBase.OnPullEventListener<ScrollView>() {
            @Override
            public void onPullEvent(PullToRefreshBase<ScrollView> refreshView, PullToRefreshBase.State state, PullToRefreshBase.Mode direction) {
                if (state == PullToRefreshBase.State.PULL_TO_REFRESH) {
                    refreshView.getLoadingLayoutProxy().setPullLabel("下拉回到“商品详情”");
                    refreshView.getLoadingLayoutProxy().setReleaseLabel("释放回到“商品详情”");
                    refreshView.getLoadingLayoutProxy().setRefreshingLabel("");
                    msvProductDetails.scrollToSplitEnd();
                }
            }
        });
        detailScrollView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
                refreshView.onRefreshComplete();
                msvProductDetails.scrollToSplitStart();
            }
        });
        showTab(0);
        //产品信息
        setTextView(R.id.item_detail_title, info.title);
        setTextView(R.id.item_detail_cate_name, info.item_cate.name);
        setTextView(R.id.item_detail_brand, info.brand);
        setTextView(R.id.item_detail_storage, info.storage_label);
        setTextView(R.id.item_detail_net_content, info.net_content + "斤");
        setTextView(R.id.item_detail_license, info.license);
        item_detail_img_list_ll.removeAllViews();
        item_detail_pack_img_list_ll.removeAllViews();

        for (TItem_img img : info.item_img) {
            ImageView iv = new ImageView(getActivity());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    Utils.getWindowWidth(getActivity()),
                    Utils.getWindowWidth(getActivity()) / 2);
            lp.setMargins(0, 20, 0, 0);
            iv.setLayoutParams(lp);
            iv.setScaleType(ImageView.ScaleType.FIT_XY);

            Utils.getImage(getActivity(), iv, img.img);
            if (img.type.equals("1")) {
                item_detail_img_list_ll.addView(iv);
            } else {
                item_detail_pack_img_list_ll.addView(iv);
            }
        }
        //产地
        setTextView(R.id.member_shop_address, info.member_shop.province + info.member_shop.city + info.member_shop.area + info.member_shop.town);
        setTextView(R.id.member_shop_areas, info.member_shop.areas + "亩");
        setTextView(R.id.member_shop_years, info.member_shop.years + "年");
        setTextView(R.id.member_shop_organic, info.member_shop.organic);
        //店铺评分
        setRoundedWebImageView(R.id.member_shop_img, info.member_shop.img);
        setTextView(R.id.member_shop_title, info.member_shop.title);
        if (Utils.is_url(info.qual_safe)) {
            showView(R.id.item_qual_safe_wrap);
            setItemImgSize(R.id.item_qual_safe);
        }
        if (Utils.is_url(info.qual_char)) {
            showView(R.id.item_qual_char_wrap);
            setItemImgSize(R.id.item_qual_char);
        }
    }

    private int[] tab_content_ids = new int[]{
            R.id.product_detail_tab_content_detail,
            R.id.product_detail_tab_content_log,
            R.id.product_detail_tab_content_address,
            R.id.product_detail_tab_content_rate,
    };

    private void showTab(int index) {
        int[] tab_img_ids = new int[]{
                R.drawable.ic_product_details,
                R.drawable.ic_production_process,
                R.drawable.ic_product_origin,
                R.drawable.ic_shop_rates,
        };
        int[] tab_gray_img_ids = new int[]{
                R.drawable.ic_product_details_gray,
                R.drawable.ic_production_process_gray,
                R.drawable.ic_product_origin_gray,
                R.drawable.ic_shop_rates_gray,
        };

        int[] img_ids = new int[]{
                R.id.product_detail_tab_detail_img,
                R.id.product_detail_tab_log_img,
                R.id.product_detail_tab_address_img,
                R.id.product_detail_tab_rate_img,
        };
        int[] title_ids = new int[]{
                R.id.product_detail_tab_detail_title,
                R.id.product_detail_tab_log_title,
                R.id.product_detail_tab_address_title,
                R.id.product_detail_tab_rate_title,
        };
        int[] line_ids = new int[]{
                R.id.product_detail_tab_detail_line,
                R.id.product_detail_tab_log_line,
                R.id.product_detail_tab_address_line,
                R.id.product_detail_tab_rate_line,
        };
        for (int i = 0; i < 4; i++) {
            ImageView iv = (ImageView) myView.findViewById(img_ids[i]);
            TextView tv = (TextView) myView.findViewById(title_ids[i]);
            View v = myView.findViewById(line_ids[i]);
            LinearLayout ll = (LinearLayout) myView.findViewById(tab_content_ids[i]);
            if (i == index) {
                iv.setBackgroundResource(tab_img_ids[i]);
                tv.setTextColor(getResources().getColor(R.color.bg_Main));
                v.setBackgroundResource(R.color.bg_Main);
                ll.setVisibility(View.VISIBLE);
            } else {
                iv.setBackgroundResource(tab_gray_img_ids[i]);
                tv.setTextColor(getResources().getColor(R.color.black));
                v.setBackgroundResource(R.color.white);
                ll.setVisibility(View.GONE);
            }
        }
        //设置生产过程
        if (index == 1) {
            LogsGetRequest request = new LogsGetRequest();
            request.item_id = mProductId;
            request.perPage = "1000";
            apiClient.doLogsGet(request, new ApiClient.OnSuccessListener() {
                @Override
                public void callback(Object object) {
                    updateLogsList((LogsGetResponse) object);
                }
            });
        }
    }

    private ProductDetailsFragment updateLogsList(LogsGetResponse response) {
        removeAllViews(R.id.logs_top_class_list);
        removeAllViews(R.id.logs_list_ll);

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        //设置生产日志统计信息
        for (ExLogs_class_item item : response.data.class_list) {
            View view = inflater.inflate(R.layout.item_logs_list_top_class_item, null);
            Utils.setTextView(view, R.id.class_title, item.name);
            Utils.setTextView(view, R.id.class_info, item.info);
            if (response.data.class_list.indexOf(item) == response.data.class_list.size() - 1) {
                Utils.getView(view, R.id.sep).setVisibility(View.GONE);
            }
            Utils.addView(myView, R.id.logs_top_class_list, view);
        }

        //显示日志列表
        for (TLogs log : response.data.logs_list.list) {
            int index = response.data.logs_list.list.indexOf(log);

            View view = inflater.inflate(R.layout.item_logs_list, null);
            Utils.setTextView(view, R.id.logs_class_name, log.title_name);
            Utils.setTextView(view, R.id.logs_add_time, log.add_time);
            Utils.setTextView(view, R.id.logs_detail, String.format("%s %s：%s", log.class_name, log.data_name, log.data));

            if (log.info.trim().length() == 0) {
                Utils.hideView(view, R.id.logs_mark);
            } else {
                Utils.setTextView(view, R.id.logs_mark, log.info);
            }

            if (log.img.equals("")) {
                Utils.hideView(view, R.id.logs_img);
            } else {
                Utils.setImageView(view, getActivity(), R.id.logs_img, log.img);
            }

            View timeline_sphere = view.findViewById(R.id.logs_list_timeline_sphere);
            LinearLayout timeline_top_line = (LinearLayout) view.findViewById(R.id.logs_list_timeline_top_line);
            LinearLayout timeline_bottom_line = (LinearLayout) view.findViewById(R.id.logs_list_timeline_bottom_line);
            if (index == 0) {
                timeline_top_line.setVisibility(View.GONE);
            }
            if (log.status.equals("0")) {
                timeline_sphere.setBackground(getResources().getDrawable(R.drawable.timeline_sphere));
                timeline_top_line.setBackgroundColor(getResources().getColor(R.color.bg_Main));
                timeline_bottom_line.setBackgroundColor(getResources().getColor(R.color.bg_Main));
            } else {
                timeline_sphere.setBackground(getResources().getDrawable(R.drawable.timeline_sphere_gray));
                timeline_top_line.setBackgroundColor(getResources().getColor(R.color.gray));
                timeline_bottom_line.setBackgroundColor(getResources().getColor(R.color.gray));
            }
            Utils.addView(myView, R.id.logs_list_ll, view);
        }
        return this;
    }

    @OnClick({R.id.product_detail_tab_detail, R.id.product_detail_tab_log, R.id.product_detail_tab_address, R.id.product_detail_tab_rate})
    protected void tabClick(LinearLayout ll) {
        int[] ids = new int[]{
                R.id.product_detail_tab_detail,
                R.id.product_detail_tab_log,
                R.id.product_detail_tab_address,
                R.id.product_detail_tab_rate,
        };
        for (int i = 0; i < ids.length; i++) {
            if (ids[i] == ll.getId()) {
                showTab(i);
                break;
            }
        }
    }

    @OnClick({R.id.product_detail_tab_detail_click, R.id.product_detail_tab_log_click, R.id.product_detail_tab_address_click, R.id.product_detail_tab_rate_click})
    protected void tabGoToClick(LinearLayout ll) {
        int[] ids = new int[]{
                R.id.product_detail_tab_detail_click,
                R.id.product_detail_tab_log_click,
                R.id.product_detail_tab_address_click,
                R.id.product_detail_tab_rate_click,
        };

        for (int i = 0; i < ids.length; i++) {
            if (ids[i] == ll.getId()) {
                msvProductDetails.scrollToSplitEnd();
                showTab(i);
                break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ShoppingCartController.getInstance().updateShoppingCartBadge(mShoppingCartBadge);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        SearchController.getInstance().removeResponseListener(this);
        FavoriteController.getInstance().removeResponseListener(this);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PopActivity.gShowNavigationBar = true;
    }

    @OnClick(R.id.backbtn)
    public void onBack() {
        getActivity().finish();
    }

    @OnClick(R.id.gotoCart)
    public void onGoToCart() {
        CartFragment fragment = CartFragment.newInstance(CartFragment.SHOW_IN_POPUP);
        startActivityWithFragment(fragment);
    }

    @OnClick(R.id.index_search_edit)
    public void onSearch() {
        SearchFragment searchFragment = new SearchFragment();
        startActivityWithFragment(searchFragment);
    }

    @OnClick(R.id.add_to_cart)
    public void onAddToCart() {
        Intent intent = new Intent(getActivity(), ProductOptionsActivity.class);

        intent.putExtra(ProductOptionsActivity.ARG_PRODUCT_OPTIONS,
                SearchController.getInstance()
                        .getProductDetailsResult()
                        .getOptions()
                        .toJson().toString());
        startActivityForResult(intent, REQUEST_TO_ADD_TO_CART);
    }

    @OnClick(R.id.add_to_fav_wrap)
    public void onAddToFav() {
        if (mItem.is_like.equals("1")) {
            User_likeDeleteRequest request = new User_likeDeleteRequest();
            request.item_id = mItem.id;
            apiClient.doUser_likeDelete(request, new ApiClient.OnSuccessListener() {
                @Override
                public void callback(Object object) {
                    mItem.is_like = "0";
                    mItem.favs = Integer.toString(Integer.parseInt(mItem.favs) - 1);
                    mAddToFav.setImageResource(R.drawable.ic_favourite_outline);
                    mProductTotalFavs.setText(String.format("%s人喜欢", mItem.favs));
                }
            }, new ApiClient.OnFailListener() {
                @Override
                public void callback(Object object) {
                    mItem.is_like = "0";
                }
            });
        } else {
            User_likeAddRequest request = new User_likeAddRequest();
            request.item_id = mItem.id;
            apiClient.doUser_likeAdd(request, new ApiClient.OnSuccessListener() {
                @Override
                public void callback(Object object) {
                    mItem.is_like = "1";
                    mItem.favs = Integer.toString(Integer.parseInt(mItem.favs) + 1);
                    mAddToFav.setImageResource(R.drawable.ic_favourite_selected);
                    mProductTotalFavs.setText(String.format("%s人喜欢", mItem.favs));
                }
            }, new ApiClient.OnFailListener() {
                @Override
                public void callback(Object object) {
                    mItem.is_like = "1";
                }
            });
        }
    }

    @OnClick(R.id.shop_follow)
    protected void onShopFollow() {
        if (mItem.member_shop.is_follow.equals("1")) {
            ItemUnfollowRequest request = new ItemUnfollowRequest();
            request.id = mItem.member_shop.id;
            apiClient.doItemUnfollow(request, new ApiClient.OnSuccessListener() {
                @Override
                public void callback(Object object) {
                    mItem.member_shop.is_follow = "0";
                    setTextView(R.id.shop_follow, "关注");

                    mItem.member_shop.follows = Integer.toString(Integer.parseInt(mItem.member_shop.follows) - 1);
                    mShopTotalFollowers.setText(mItem.member_shop.follows);
                }
            });
        } else {
            ItemFollowRequest request = new ItemFollowRequest();
            request.id = mItem.member_shop.id;
            apiClient.doItemFollow(request, new ApiClient.OnSuccessListener() {
                @Override
                public void callback(Object object) {
                    mItem.member_shop.is_follow = "1";
                    setTextView(R.id.shop_follow, "取消关注");

                    mItem.member_shop.follows = Integer.toString(Integer.parseInt(mItem.member_shop.follows) + 1);
                    mShopTotalFollowers.setText(mItem.member_shop.follows);
                }
            });
        }
    }

    @OnClick(R.id.share)
    protected void onShare() {
        ShareHelper helper = new ShareHelper(getActivity());
        helper.share(mItem.title, mItem.title, "http://www.shiku.com/");
    }

    static class ViewPagerAdapter extends PagerAdapter {

        List<View> mViews;
        List<View> mIndicators;

        ViewPagerAdapter(List<View> views, List<View> indicators) {
            mViews = views == null ? new ArrayList<View>() : views;
            mIndicators = indicators == null ? new ArrayList<View>() : indicators;
        }

        public View getIndicator(int position) {
            return mIndicators.get(position);
        }

        @Override
        public int getCount() {
            return mViews.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == (View) object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = mViews.get(position);
            container.addView(view);

            return view;
        }
    }

}
