
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;
import com.xz.external.androidquery.util.AQUtility;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "STATUS")
public class STATUS extends Model {

    @Column(name = "succeed")
    public int succeed;

    @Column(name = "error_code")
    public int error_code;

    @Column(name = "error_desc")
    public String error_desc;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        if (AQUtility.isDebug()) {
            if (null == jsonObject) {
                return;
            }

            this.succeed = jsonObject.optInt("status");
            this.error_code = jsonObject.optInt("error_code");
            this.error_desc = jsonObject.optString("result");
        } else {
            this.succeed = jsonObject.optInt("status");
            this.error_desc = jsonObject.optString("result");
        }
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("succeed", succeed);
        localItemObject.put("error_code", error_code);
        localItemObject.put("error_desc", error_desc);

        return localItemObject;
    }

    public boolean getIsSuccess() {
        return succeed == 1;
    }

    public String getErrorMessage() {
        return error_desc;
    }
}
