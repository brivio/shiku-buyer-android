
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "COLLECT_LIST")
public class COLLECT_LIST extends Model {

    @Column(name = "fav_id")
    public int id;

    @Column(name = "product_id")
    public int productId;

    @Column(name = "product_name")
    public String productName;

    @Column(name = "price")
    public double price;

    @Column(name = "productImage")
    public String productImage;

    /**
     * Unused fields
     */
    @Column(name = "username")
    public String username;

    @Column(name = "shopname")
    public String shopname;

    @Column(name = "yjhl")
    public String yjhl;

    @Column(name = "mcl")
    public String mcl;

    @Column(name = "img")
    public PHOTO img;

    @Column(name = "rec_id")
    public String rec_id;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject || null == jsonObject.optJSONObject("item")) {
            return;
        }

        this.id = jsonObject.optInt("id");

        jsonObject = jsonObject.optJSONObject("item");

        this.productId = jsonObject.optInt("id");
        this.productName = jsonObject.optString("title");
        this.price = jsonObject.optDouble("price");
        this.productImage = jsonObject.optString("img");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }
}
