#!/bin/sh
function _show_help(){
    echo "选项：\n\
    build      生成android平台SDK\n\
"
}

if [[ $1 = "build" ]]; then
    DIST_PATH="../third_party/Api-SDK"
    PACKAGE="com.insuny.sdk.api"

    SRC_PATH="${DIST_PATH}/src/main/java/${PACKAGE//.//}"
    if [[ -f ${DIST_PATH}/build.gradle ]]; then
        rm -rf ${SRC_PATH}
    else
        rm -rf ${DIST_PATH}
    fi
    
    mkdir -p ${SRC_PATH}/table
    mkdir -p ${SRC_PATH}/extra
    mkdir -p ${SRC_PATH}/request
    mkdir -p ${SRC_PATH}/response
    php ./android/main.php ${DIST_PATH} ${PACKAGE}
    
    if [[ ! -f ${DIST_PATH}/build.gradle ]]; then     
        cp ./android/misc/build.gradle ${DIST_PATH}/build.gradle
    fi

    cp ./android/misc/AndroidManifest.xml ${DIST_PATH}/src/main/AndroidManifest.xml
    echo "生成位置:${DIST_PATH}"
elif [[ $1 = "help" ]]; then
    _show_help
else
    _show_help
fi
