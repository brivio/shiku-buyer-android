package com.xz.shiku.adapter.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xz.btc.protocol.ProductVariant;
import com.xz.shiku.R;

import java.util.List;

/**
 * Created by txj on 15/5/27.
 */
public class ProductVariantsAdapter extends BaseAdapter {

    List<ProductVariant> mVariants;
    int mSelection = -1;

    LayoutInflater mInflater;
    Context mContext;

    public ProductVariantsAdapter(Context context, List<ProductVariant> results) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mVariants = results;
    }

    public ProductVariant getSelectedItem() {
        return mSelection >= 0 ? mVariants.get(mSelection) : null;
    }

    @Override
    public int getCount() {
        return mVariants.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        SelectionListener listener;

        if (view == null) {
            holder = new ViewHolder();

            view = mInflater.inflate(R.layout.item_product_variant, null);
            holder.mTextView = (TextView) view.findViewById(R.id.item_text);
            listener = new SelectionListener();
            holder.mTextView.setOnClickListener(listener);


            view.setTag(holder);
            view.setTag(holder.mTextView.getId(), listener);
        } else {
            holder = (ViewHolder) view.getTag();
            listener = (SelectionListener) view.getTag(holder.mTextView.getId());
        }

        listener.setPosition(position);
        holder.mTextView.setText(mVariants.get(position).name);

        if (mSelection == position) {
            holder.mTextView.setBackgroundResource(R.drawable.tf_layoutwithstorkemainroundfill);
            holder.mTextView.setTextColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.mTextView.setBackgroundResource(R.drawable.tf_layoutwithstorkeround);
            holder.mTextView.setTextColor(mContext.getResources().getColor(R.color.black));
        }

        return view;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Object getItem(int i) {
        return mVariants == null ? null : mVariants.get(i);
    }

    static class ViewHolder {
        TextView mTextView;
    }

    class SelectionListener implements View.OnClickListener {
        int mPosition;

        @Override
        public void onClick(View v) {
            if (mSelection != mPosition) {
                mSelection = mPosition;
                notifyDataSetChanged();
            }
        }

        void setPosition(int position) {
            this.mPosition = position;
        }
    }
}
