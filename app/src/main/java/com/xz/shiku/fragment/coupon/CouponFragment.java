package com.xz.shiku.fragment.coupon;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.Coupon;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.adapter.coupon.CouponAdapter;
import com.xz.shiku.controller.CouponController;
import com.xz.shiku.controller.UserController;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.SwipeListView;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CouponFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CouponFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CouponFragment extends BackHandledFragment implements AdapterView.OnItemClickListener {
    public static final int MODE_VIEW = 1;
    public static final int MODE_SELECT = 2;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_MODE = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int mMode;
    private String mParam2;

    private boolean hadIntercept;

    @InjectView(R.id.collect_list)
    SwipeListView mCoupons;
    @InjectView(R.id.ll_empty)
    LinearLayout mEmptyLayout;
    @InjectView(R.id.ll_empty_icon)
    TextView mEmptyIcon;
    @InjectView(R.id.ll_empty_text)
    TextView mEmptyText;
    @InjectView(R.id.ll_empty_subtext)
    TextView mEmptySubtext;

    CouponAdapter mCouponAdapter;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param mode   Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CouponFragment newInstance(int mode, String param2) {
        PopActivity.gShowNavigationBar = true;

        titleResId = R.string.title_fragment_coupon;
        topRightText = "";

        CouponFragment fragment = new CouponFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_MODE, mode);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CouponFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMode = getArguments().getInt(ARG_MODE);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_coupon, container, false);

        ButterKnife.inject(this, v);

        mEmptyIcon.setText(getResources().getString(R.string.iconlove));
        mEmptyText.setText("您尚未领取优惠券");
        mEmptySubtext.setText("");

        if (UserController.getInstance().isUserReady()) {
            CouponController.getInstance().addResponseListener(this);
            CouponController.getInstance().getCoupons(getActivity());
        } else {
            initUI(false);
        }

        return v;
    }

    private void initUI(boolean showCouponLayout) {
        if (showCouponLayout) {
            mEmptyLayout.setVisibility(View.GONE);

            mCoupons.setPullRefreshEnable(true);
            mCoupons.setPullLoadEnable(false);
            mCoupons.setXListViewListener(this);
            mCoupons.setOnItemClickListener(this);
        } else {
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        CouponController.getInstance().removeResponseListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.COUPON_LIST)) {
            List<Coupon> coupons = CouponController.getInstance().getCouponsResult();
            if (CouponController.getInstance().getIsFirstPage()) {
                if (coupons != null && coupons.size() > 0) {
                    initUI(true);

                    mCoupons.setRefreshTime();
                    mCoupons.stopRefresh();
                    if (CouponController.getInstance().getHasMore()) {
                        mCoupons.setPullLoadEnable(true);
                    }

                    mCouponAdapter = new CouponAdapter(getActivity(), coupons, mCoupons.getRightViewWidth());
                    mCoupons.setAdapter(mCouponAdapter);
                } else {
                    initUI(false);
                }
            } else {
                mCoupons.stopLoadMore();
                mCouponAdapter.notifyDataSetChanged();
            }
        }
    }

    public void onRefresh() {
        CouponController.getInstance().getCoupons(getActivity());
    }

    @Override
    public void onLoadMore() {
        if (CouponController.getInstance().getHasMore()) {
            CouponController.getInstance().getMoreCoupons(getActivity());
        }
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mMode == MODE_SELECT) {
            Coupon coupon = (Coupon) mCouponAdapter.getItem(position - 1);
            if (coupon.getIsValid()) {
                Intent data = new Intent();
                data.putExtra("coupon", coupon);

                getActivity().setResult(Activity.RESULT_OK, data);
                getActivity().finish();
            }
        }
    }
}
