
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "FILTER")
public class FILTER extends Model {

    @Column(name = "keywords")
    public String keywords;

    @Column(name = "sort_by")
    public String sort_by;

    @Column(name = "sort_order")
    public String sort_order;

    @Column(name = "brand_id")
    public String brand_id;

    @Column(name = "category_id")
    public String category_id;

    @Column(name = "price_range")
    public PRICE_RANGE price_range;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.keywords = jsonObject.optString("keywords");
        this.sort_by = jsonObject.optString("sort_by");
        this.brand_id = jsonObject.optString("brand_id");
        this.category_id = jsonObject.optString("category_id");
        PRICE_RANGE price_range = new PRICE_RANGE();
        price_range.fromJson(jsonObject.optJSONObject("price_range"));
        this.price_range = price_range;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("keyword", keywords);
        localItemObject.put("order", sort_by);
        localItemObject.put("order_by", sort_order);
        localItemObject.put("cate_id", category_id);

        return localItemObject;
    }
}
