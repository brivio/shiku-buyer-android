package com.xz.btc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;

import com.xz.tframework.activity.BaseActivity;
import com.xz.shiku.R;

public class AppOutActivity extends BaseActivity {

	private ImageView bg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tf_appout);
		
		bg = (ImageView) findViewById(R.id.bg);
		
		Intent intent = getIntent();
		int flag = intent.getIntExtra("flag", 0);
		if(flag == 1) {
			bg.setBackgroundResource(R.drawable.tf_closed);
		} else if(flag == 2) {
			bg.setBackgroundResource(R.drawable.tf_expired_568h);
		}
		
	}
	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return true;
    }
}
