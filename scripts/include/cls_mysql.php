<?php

class cls_mysql
{

    var $link_id = null;
    var $opt = array();
    var $error_message = array();
    var $version = '';
    var $sql_create_info;
    static $_cache;
    var $last_sql = "";

    function cls_mysql($opt)
    {

        $res = array();
        foreach ($opt as $key => $val) {
            $res[strtolower($key)] = $val;
        }
        $this->opt = array_merge(array('charset' => 'utf8', 'pconnect' => 0, 'quiet' => true), $res);
        $this->connect();

        $this->sql_create_info = "-- ---------------------------------------
--  创建时间:" . Date("Y-m-d H:i:s", time()) . "
-- ---------------------------------------    
" . PHP_EOL;
    }

    function table($table)
    {
        return $this->opt['db_prefix'] . $table;
    }

    function connect()
    {
        if (PHP_VERSION >= '4.2') {
            $this->link_id = mysql_connect($this->opt['db_host'], $this->opt['db_user'], $this->opt['db_pwd'], true);
        } else {
            $this->link_id = @mysql_connect($this->opt['db_host'], $this->opt['db_user'], $this->opt['db_pwd']);

            mt_srand((double)microtime() * 1000000); // 对 PHP 4.2 以下的版本进行随机数函数的初始化工作
        }
        if (!$this->link_id) {
            $this->ErrorMsg("Can't Connect MySQL Server(" . $this->opt['db_host'] . ")!");
            return false;
        }

        $this->version = mysql_get_server_info($this->link_id);

        /* 如果mysql 版本是 4.1+ 以上，需要对字符集进行初始化 */
        if ($this->version > '4.1') {
            if ($this->opt['charset'] != 'latin1') {
                mysql_query("SET character_set_connection=" . $this->opt['charset'] . ", character_set_results=" . $this->opt['charset'] . ", character_set_client=binary", $this->link_id);
            }
            if ($this->version > '5.0.1') {
                mysql_query("SET sql_mode=''", $this->link_id);
            }
        }

        /* 选择数据库 */
        if ($this->opt['db_name']) {
            if (mysql_select_db($this->opt['db_name'], $this->link_id) === false) {
                $this->ErrorMsg("Can't select MySQL database(" . $this->opt['db_name'] . ")!");
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    function query($sql, $type = '')
    {
        if ($this->link_id === null) {
            $this->connect();
        }
        $this->last_sql = $sql;

        /* 当当前的时间大于类初始化时间的时候，自动执行 ping 这个自动重新连接操作 */
        if (PHP_VERSION >= '4.3') {
            mysql_ping($this->link_id);
        }
        if (!($query = mysql_query($sql, $this->link_id)) && $type != 'SILENT' && !$this->opt['quiet']) {
            $this->error_message[]['message'] = 'MySQL Query Error';
            $this->error_message[]['sql'] = $sql;
            $this->error_message[]['error'] = mysql_error($this->link_id);
            $this->error_message[]['errno'] = mysql_errno($this->link_id);

            $this->ErrorMsg();

            return false;
        }
        return $query;
    }

    function insert_id()
    {
        return mysql_insert_id($this->link_id);
    }

    function escape_string($unescaped_string)
    {
        if (PHP_VERSION >= '4.3') {
            return mysql_real_escape_string($unescaped_string);
        } else {
            return mysql_escape_string($unescaped_string);
        }
    }

    function close()
    {
        return mysql_close($this->link_id);
    }

    function ErrorMsg($message = '', $sql = '')
    {
        if ($message) {
            exit($message . PHP_EOL);
        } else {
            exit($this->error_message . PHP_EOL);
        }
    }

    /* 仿真 Adodb 函数 */

    function selectLimit($sql, $num, $start = 0)
    {
        if ($start == 0) {
            $sql .= ' LIMIT ' . $num;
        } else {
            $sql .= ' LIMIT ' . $start . ', ' . $num;
        }

        return $this->query($sql);
    }

    function getOne($sql, $limited = false)
    {
        if ($limited == true) {
            $sql = trim($sql . ' LIMIT 1');
        }

        $res = $this->query($sql);
        if ($res !== false) {
            $row = mysql_fetch_row($res);

            if ($row !== false) {
                return $row[0];
            } else {
                return '';
            }
        } else {
            return false;
        }
    }

    function getAll($sql)
    {
        $res = $this->query($sql);
        if ($res !== false) {
            $arr = array();
            while ($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
            }

            return $arr;
        } else {
            return false;
        }
    }

    function getRow($sql, $limited = false)
    {
        if ($limited == true) {
            $sql = trim($sql . ' LIMIT 1');
        }

        $res = $this->query($sql);
        if ($res !== false) {
            return mysql_fetch_assoc($res);
        } else {
            return false;
        }
    }

    function getCol($sql)
    {
        $res = $this->query($sql);
        if ($res !== false) {
            $arr = array();
            while ($row = mysql_fetch_row($res)) {
                $arr[] = $row[0];
            }

            return $arr;
        } else {
            return false;
        }
    }

    function autoExecute($table, $field_values, $mode = 'INSERT', $where = '', $querymode = '')
    {
        $field_names = $this->getCol('DESC ' . $table);

        $sql = '';
        if ($mode == 'INSERT') {
            $fields = $values = array();
            foreach ($field_names as $value) {
                if (array_key_exists($value, $field_values) == true) {
                    $fields[] = $value;
                    $values[] = "'" . $field_values[$value] . "'";
                }
            }

            if (!empty($fields)) {
                $sql = 'INSERT INTO ' . $table . ' (' . implode(', ', $fields) . ') VALUES (' .
                    implode(', ', $values) . ')';
            }
        } else {
            $sets = array();
            foreach ($field_names as $value) {
                if (array_key_exists($value, $field_values) == true) {
                    $sets[] = $value . " = '" . $field_values[$value] . "'";
                }
            }

            if (!empty($sets)) {
                $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $sets) . ' WHERE ' . $where;
            }
        }
        //print_r($sql);exit();
        if ($sql) {
            return $this->query($sql, $querymode);
        } else {
            return false;
        }
    }

    function getInfo()
    {
        $cache_key = 'db_info_' . $this->opt['db_name'];
        if (cls_mysql::$_cache[$cache_key]) {
            return cls_mysql::$_cache[$cache_key];
        }

        $info = $this->getAll("SHOW TABLE STATUS FROM " . $this->opt['db_name']);
        $i = 0;
        foreach ($info as $key => $val) {
            $i++;
            $info[$key]['Id'] = $i;
            $info[$key]['Fields'] = $this->getAll("show full fields from " . $val['Name']);
            foreach ($info[$key]['Fields'] as $k => $v) {
                $type = explode(' ', $v['Type']);

                $info[$key]['Fields'][$k]['Data_type'] = preg_replace("/\((\d*)\)/", '', $type[0]);
                $info[$key]['Fields'][$k]['Data_length'] = rtrim(preg_replace("/(\w*)\(/", '', $type[0]), ')');

                if (in_array($v['Field'], array('img', 'info', 'extimg', 'intro', 'content'))) {
                    $info[$key]['Attach_fields'][] = $v['Field'];
                }

                $info[$key]['Fields'][$v['Field']] = $info[$key]['Fields'][$k];
                unset($info[$key]['Fields'][$k]);
            }
            $info[$key]['Create'] = $this->get_table_create($val['Name']);
            $info[$key]['PRI'] = $this->getOne("SHOW COLUMNS FROM $val[Name] where `key`='PRI'");
            $info[$key]['select_fields'] = "*";
            $info[$val['Name']] = $info[$key];

            unset($info[$key]);

            $percent = round(($i / count($info)) * 100);
            // show_progress("获取数据库" . $this->opt['db_name'] . "信息", $percent);
        }
        // echo PHP_EOL;
        cls_mysql::$_cache[$cache_key] = $info;
        return $info;
    }

    function export($opt = [])
    {
        $db_info = $this->getInfo();

        if (!empty($opt['include'])) {
            $res = array();
            $opt_include = $opt['include'];

            preg_match_all("/\[([\s\(\)\w\,\;\:\`]*)\]/", $opt_include, $matches);
            foreach ($matches[1] as $val) {
                $val = trim($val);
                $tb_name = $tb_as_name = $val;
                if (strpos($val, "(") > 0) {
                    $tb_name = substr($val, 0, strpos($val, "("));

                    if (strpos($tb_name, " as ") > 0) {
                        preg_match("/(\w+)\s+as\s+(\w+)/", $tb_name, $matches);
                        $tb_name = $matches[1];
                        $tb_as_name = $matches[2];
                    }
                }

                $tb_opt = array();

                preg_match("/\(([\s\(\)\w\,\;\:\`]*)\)/", $val, $matches);
                $tmp = explode(';', $matches[1]);
                foreach ($tmp as $v) {
                    if (empty($v)) continue;
                    $v_res = explode(':', $v);
                    $tb_opt['select_' . $v_res[0]] = $v_res[1];
                }
                $res[$tb_name] = array_merge($db_info[$tb_name], $tb_opt);
                $res[$tb_name]['As'] = $tb_as_name;
                //解析fields
                $select_fileds = $res[$tb_name]['select_fields'];
                if (!empty($select_fileds)) {
                    $fields = array();
                    foreach (explode(',', $select_fileds) as $f) {
                        $f_name = $f_as_name = $f;
                        if (strpos($f, " as ") > 0) {
                            preg_match("/([\w\`]+)\s+as\s+([\w\`]+)/", $f, $matches);
                            $f_name = $matches[1];
                            $f_as_name = trim($matches[2], '`');
                        }
                        $field = $db_info[$tb_name]['Fields'][$f_name];

                        if (!empty($field)) {
                            $field['Field'] = $f_as_name;
                            $field['_Field'] = $f_name;
                            $fields[$f_as_name] = $field;
                        }
                    }
                    $res[$tb_name]['Fields'] = $fields;
                }
            }
            $db_info = $res;
        }

        $opt['ignore_data'] = explode(',', $opt['ignore_data']);

        $total_rows_num = 0;
        foreach ($db_info as $val) {
            if (!in_array($val, $opt['ignore_data'])) {
                $total_rows_num += $val['Rows'];
            }
        }
        $current_row = 0;

        $create_table_sql = $opt['struct_sql'];
        $initdata_sql = $opt['data_sql'];
        if (empty($initdata_sql)) {
            $initdata_sql = $create_table_sql;
        }

        @unlink($create_table_sql);
        @unlink($initdata_sql);
        file_put_contents($create_table_sql, print_r($this->sql_create_info, true));
        $create_table_sql != $initdata_sql && file_put_contents($initdata_sql, print_r($this->sql_create_info, true));
        $db_opt = explode(',', $opt['database']);

        if (in_array('drop', $db_opt)) {
            file_put_contents($create_table_sql
                , "DROP DATABASE IF EXISTS `" . $this->opt['db_name'] . "`;" . PHP_EOL
                , FILE_APPEND);
        }
        if (in_array('create', $db_opt)) {
            file_put_contents($create_table_sql
                , "CREATE DATABASE IF NOT EXISTS `" . $this->opt['db_name'] . "`;" . PHP_EOL
                , FILE_APPEND);
        }
        $insert_type = "INSERT";
        if ($opt['insert'] == 'replace') {
            $insert_type = "REPLACE";
        }
        foreach ($db_info as $key => $val) {
            if (empty($val['As'])) {
                $val['As'] = $val['Name'];
            }
            $sql_fields = "";
            foreach ($val['Fields'] as $v) {
                $sql_fields .= "`$v[Field]`,";
            }
            $sql_fields = rtrim($sql_fields, ',');
            //导结构
            if (in_array('create_table', $db_opt)) {
                file_put_contents($create_table_sql
                    , print_r($this->get_table_create($val['Name']) . PHP_EOL, true)
                    , FILE_APPEND);
            }
            //导数据                          
            if (!in_array($val['Name'], $opt['ignore_data'])) {

                if ($val['Rows'] <= 0) continue;
                $sql = "";
                if ($opt['delete'] == 'true') {
                    $sql .= "TRUNCATE `$val[As]`;" . PHP_EOL;
                }

                file_put_contents($initdata_sql, print_r($sql, true), FILE_APPEND);

                $start = 0;
                $offset = intval($opt['offset']) > 0 ? intval($opt['offset']) : 1000;
                $total_rows = 0;
                while ($start <= $val['Rows']) {
                    $res = $this->getAll("SELECT $val[select_fields] FROM $val[Name] limit $start,$offset");
                    if (empty($res)) break;
                    $total_rows += count($res);

                    $sql = "$insert_type INTO `$val[As]`($sql_fields) VALUES" . PHP_EOL;
                    foreach ($res as $row) {
                        $sql .= "\t(";
                        foreach ($val['Fields'] as $v) {
                            if (in_array($v['Data_type'], array('char', 'varchar', 'tinytext', 'text', 'mediumtext', 'longtext'))) {
                                $sql .= "'" . str_replace(array("\r", "\n"), array('\r', '\n'), addslashes($row[$v['Field']])) . "',";
                            } else {
                                $sql .= (empty($row[$v['Field']]) ? "0" : $row[$v['Field']]) . ",";
                            }
                        }
                        $sql = rtrim($sql, ',') . ")," . PHP_EOL;

                        $current_row++;
                    }
                    $start += $offset;

                    file_put_contents($initdata_sql, print_r(rtrim($sql, ',' . PHP_EOL) . ';' . PHP_EOL . PHP_EOL, true), FILE_APPEND);
                    show_progress("导出表$current_row,$total_rows_num", ($current_row / $total_rows_num) * 100);
                }
            }
        }
        echo "\n";
    }

    function compare($dest_db, $file)
    {
        @unlink($file);
        file_put_contents($file, $this->sql_create_info, FILE_APPEND);

        $src_db_info = $this->getInfo();
        $dest_db_info = $dest_db->getInfo();

        $table_index = 0;
        foreach ($src_db_info as $key => $val) {
            $sql = "";
            $table_index++;
            show_progress("表对比中", ($table_index / count($src_db_info)) * 100);

            if (empty($dest_db_info[$key])) {
                $sql .= $src_db_info[$key]['Create'];
                continue;
            }
            $alter_table_sql = "";
            $db_attr = array('Engine', 'Collation', 'Comment');
            foreach ($db_attr as $k => $v) {
                if ($val[$v] == $dest_db_info[$key][$v]) continue;
                if ($v == 'Collation') {
                    $field = "COLLATE";
                } else {
                    $field = strtoupper($v);
                }
                $alter_table_sql .= ",$field='$val[$v]'";
            }
            if (!empty($alter_table_sql)) {
                $sql .= "ALTER TABLE `$key` " . trim($alter_table_sql, ',') . ";" . PHP_EOL;
            }
            $alter_field_sql_list = array();
            $change_default_fields = array();

            foreach ($val['Fields'] as $k => $v) {
                $null = $v['Null'] == 'NO' ? 'NOT NULL' : 'NULL';
                $default = $v['Default'] === null ? '' : "DEFAULT " . var_export($v['Default'], true);

                $alter_field_sql = trim("$v[Type] $null $default $v[Extra]");
                if (empty($dest_db_info[$key]['Fields'][$k])) {
                    $alter_field_sql_list[] = "\tADD COLUMN `$k` $alter_field_sql";
                    continue;
                } elseif ($v['Default'] != $dest_db_info[$key]['Fields'][$k]['Default']) {
                    $change_default_fields[$k] = $v['Default'];
                }
                $field_attr = array('Type', 'Null', 'Default', 'Extra');
                $is_change = false;
                foreach ($field_attr as $attr) {
                    if ($v[$attr] != $dest_db_info[$key]['Fields'][$k][$attr]) {
                        $is_change = true;
                        break;
                    }
                }
                if ($is_change) {
                    $alter_field_sql_list[] = "\tCHANGE `$k` `$k` $alter_field_sql";
                }
            }

            foreach ($dest_db_info[$key]['Fields'] as $k => $v) {
                if (!array_key_exists($k, $val['Fields'])) {
                    $alter_field_sql_list[] = "\tDROP COLUMN `$k`";
                }
            }
            if (!empty($alter_field_sql_list)) {
                $sql .= "ALTER TABLE `$key`" . PHP_EOL
                    . implode("," . PHP_EOL, $alter_field_sql_list) . ";" . PHP_EOL;
            }
            foreach ($change_default_fields as $k => $v) {
                $sql .= "UPDATE `$key` set `$k`='$v' WHERE ISNULL(`$k`) or TRIM(`$k`)='';" . PHP_EOL;
            }
            if (!empty($sql)) {
                $sql_comment = "--\t表 $key" . PHP_EOL;
                file_put_contents($file, $sql_comment . $sql, FILE_APPEND);
            }
        }

        echo "\n";
    }

    function get_table_create($table)
    {
        $create_res = $this->getRow("SHOW CREATE TABLE `$table`");
        $sql = str_replace("\n", PHP_EOL, $create_res['Create Table']);

        $sql = preg_replace(
            array(/*"/CREATE\s* TABLE/",*/
                "/AUTO_INCREMENT\=(\d+)(\s*)/")
            , array(/*'CREATE TABLE IF NOT EXISTS',*/
                '')
            , $sql);
        return "DROP TABLE IF EXISTS `$table`;" . PHP_EOL . $sql . ";" . PHP_EOL;
    }

    private function _get_sql($sql_file)
    {
        $contents = file_get_contents($sql_file);
        $contents = str_replace("\r\n", "\n", $contents);
        $contents = trim(str_replace("\r", "\n", $contents));
        $return_items = $items = array();
        $items = explode(";\n", $contents);

        foreach ($items as $item) {
            $return_item = '';
            $item = trim($item);
            $lines = explode("\n", $item);
            foreach ($lines as $line) {
                if (isset($line[1]) && $line[0] . $line[1] == '--') {
                    continue;
                }
                $return_item .= $line;
            }
            if ($return_item) {
                $return_items[] = $return_item; //.";";
            }
        }
        return $return_items;
    }

    public function query_file($sql_file, $show_log = false)
    {
        $sqls = $this->_get_sql($sql_file);
        //print_r($sqls);exit();
        foreach ($sqls as $val) {
            $this->query($val);
            $show_log && _print_r($val . PHP_EOL);
        }
    }
}