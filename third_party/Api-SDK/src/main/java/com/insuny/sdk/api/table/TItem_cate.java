package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TItem_cate{
    public String id;
    public String pid;
    public String spid;
    public String name;
    public String img;
    public String remark;
    public String add_time;
    public String update_time;
    public String mid;
    public String maxs;
    public String unit_years;
    public String unit_areas;
    public String license;
    public String rprice;
    public String ordid;
    public String status;
    public String is_hots;
    public String seo_title;
    public String seo_keys;
    public String seo_desc;
}