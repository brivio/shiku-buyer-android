package com.xz.btc.model;

/**
 * Created by txj on 15/4/14.
 */
import android.content.Context;
import android.content.SharedPreferences;

import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.model.BeeCallback;
import com.xz.tframework.utils.MD5;
import com.xz.tframework.view.MyProgressDialog;
import com.xz.shiku.R;
import com.xz.btc.protocol.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginModel extends BaseModel {

    private SharedPreferences shared;
    private SharedPreferences.Editor editor;
    public STATUS responseStatus;

    public LoginModel(Context context) {
        super(context);
        shared = context.getSharedPreferences("userInfo", 0);
        editor = shared.edit();
    }

    public void login(String name, String password) {
        usersigninRequest request = new usersigninRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                LoginModel.this.callback(url, jo, status);
                try {
                    usersigninResponse response = new usersigninResponse();
                    response.fromJson(jo);
                    responseStatus=response.status;
                    if (jo != null) {
//                        if (response.status.succeed == 1) {
//                            SIGNIN_DATA data = response.data;
//                            SESSION session = data.session;
//                            SESSION.getInstance().uid=session.uid;
//                            SESSION.getInstance().sid = session.sid;
//                            USER user = data.user;
//                            user.save();
//                            editor.putString("uid", session.uid);
//                            editor.putString("sid", session.sid);
//                            editor.putString("email",user.email);
//                            editor.commit();
//                        }
                        LoginModel.this.OnMessageResponse(url, jo, status);
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        };
        request.uid = name;
        request.password = MD5.getMD5(password);
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("data", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cb.url(ApiInterface.USER_SIGNIN).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext,mContext.getResources().getString(R.string.hold_on));
        aq.progress(pd.mDialog).ajax(cb);

    }
}
