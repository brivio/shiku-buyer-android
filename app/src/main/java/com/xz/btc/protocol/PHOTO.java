
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "PHOTO")
public class PHOTO extends Model {

    @Column(name = "small")
    public String small;    //小图

    @Column(name = "thumb")
    public String thumb;    //中图

    @Column(name = "url")
    public String url;      //大图


    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }


        this.small = jsonObject.optString("small");
        this.thumb = jsonObject.optString("thumb");
        this.url = jsonObject.optString("url");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("small", small);
        localItemObject.put("thumb", thumb);
        localItemObject.put("url", url);

        return localItemObject;
    }
}
