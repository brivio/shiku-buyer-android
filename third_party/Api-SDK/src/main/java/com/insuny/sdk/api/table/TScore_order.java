package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TScore_order{
    public String id;
    public String order_sn;
    public String uid;
    public String uname;
    public String item_id;
    public String item_name;
    public String item_num;
    public String consignee;
    public String address;
    public String zip;
    public String mobile;
    public String order_score;
    public String add_time;
    public String remark;
    public String status;
}