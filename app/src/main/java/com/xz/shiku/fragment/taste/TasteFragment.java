package com.xz.shiku.fragment.taste;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.TasteResult;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.adapter.taste.TasteAdapter;
import com.xz.shiku.controller.UserController;
import com.xz.tframework.fragment.BackHandledFragment;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TasteFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TasteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TasteFragment extends BackHandledFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @InjectView(R.id.my_taste)
    GridView mMyTaste;
    @InjectView(R.id.recommended_taste)
    GridView mRecommendedTaste;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TasteFragment newInstance(String param1, String param2) {
        PopActivity.gShowNavigationBar = true;

        titleResId = R.string.title_fragment_taste;
        topRightText = "";

        TasteFragment fragment = new TasteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public TasteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_taste, container, false);

        ButterKnife.inject(this, v);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.bg_Main);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                UserController.getInstance().getMyTaste(getActivity());
            }
        });
        UserController.getInstance().addResponseListener(this);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        }, 200);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                UserController.getInstance().getMyTaste(getActivity());
            }
        }, 800);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        UserController.getInstance().removeResponseListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_TASTE_LIST)) {
            if (UserController.getInstance().getLastResponseStatus().getIsSuccess()) {
                TasteResult result = UserController.getInstance().getTasteResult();
                if (result.myTaste != null) {
                    TasteAdapter adapter = new TasteAdapter(getActivity(), result.myTaste);
                    mMyTaste.setAdapter(adapter);
                }
                if (result.recommendedTaste != null) {
                    TasteAdapter adapter = new TasteAdapter(getActivity(), result.recommendedTaste);
                    mRecommendedTaste.setAdapter(adapter);
                }
            }
            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    @OnClick(R.id.btn_add)
    public void onAddTaste() {
        TastePickFragment tastePickFragment = TastePickFragment.newInstance("", "");
        startActivityForResultWithFragment(tastePickFragment, 0x100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0x100) {
            mSwipeRefreshLayout.setRefreshing(true);
            UserController.getInstance().getMyTaste(getActivity());
        }
    }
}
