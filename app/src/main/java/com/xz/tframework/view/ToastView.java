package com.xz.tframework.view;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.shiku.R;

import java.util.Timer;
import java.util.TimerTask;

public class ToastView {

    private Toast mToast;
    private Timer timer;

    public ToastView(Context context, String text) {
        this(context, text, R.layout.tf_toast_view);
    }

    public ToastView(Context context, int resid) {
        this(context, resid, R.layout.tf_toast_view);
    }

    public ToastView(Context context, String text, int resource) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(resource, null);
        TextView t = (TextView) view.findViewById(R.id.toast_text);
        t.setText(text);

        mToast = new Toast(context);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setView(view);
    }

    public ToastView(Context context, int resid, int resource) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(resource, null);
        TextView t = (TextView) view.findViewById(R.id.toast_text);
        t.setText(resid);

        mToast = new Toast(context);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setView(view);
    }

    // 设置Toast显示位置
    public void setGravity(int gravity, int xOffset, int yOffset) {
        mToast.setGravity(gravity, xOffset, yOffset);
    }

    // 设置Toast显示时间
    public void setDuration(int duration) {
        mToast.setDuration(duration);
    }

    // 设置Toast显示时间(自定义时间)
    public void setDurationLong(int duration) {
        final int time = duration;
        timer = new Timer();
        timer.schedule(new TimerTask() {

            int mDuration = time;

            @Override
            public void run() {
                if (mDuration - 1000 >= 0) {
                    show();
                    mDuration -= 1000;
                } else {
                    timer.cancel();
                }
            }
        }, 0, 1000);
    }

    public void show() {
        mToast.show();
    }

    public void cancel() {
        mToast.cancel();
    }

    /**
     * 静态成员和方法
     */
    private static ToastView mInstance;

    public static void showMessage(Context context, String message) {
        mInstance = new ToastView(context, message);
        mInstance.setGravity(Gravity.CENTER, 0, 0);
        mInstance.show();
    }

    public static void hide() {
        if (mInstance != null) {
            mInstance.cancel();
        }
    }
}
