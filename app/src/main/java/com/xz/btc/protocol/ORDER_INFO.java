
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

@Table(name = "ORDER_INFO")
public class ORDER_INFO extends Model implements Serializable {

    @Column(name = "order_id")
    public int order_id;

    @Column(name = "order_sn")
    public String order_sn;

    @Column(name = "order_amount")
    public String order_amount;

    @Column(name = "desc")
    public String desc;

    @Column(name = "pay_code")
    public String pay_code;

    @Column(name = "subject")
    public String subject;

    @Column(name = "status")
    public String status;

    @Column(name = "shop_titile")
    public String shop_title;

    @Column(name = "shipping_rates")
    public double shipping_rates;

    @Column(name = "shipping_carrier")
    public String shipping_carrier;

    @Column(name = "shipping_tracking_number")
    public String shipping_tracking_number;

    @Column(name = "shipping_remark")
    public String shipping_remark;

    @Column(name = "product_subtotal")
    public double product_subtotal;

    @Column(name = "total")
    public double total;

    @Column(name = "shipping_address_province")
    public String province;

    @Column(name = "shipping_address_city")
    public String city;

    @Column(name = "shipping_address_county")
    public String county;

    @Column(name = "shipping_address_details")
    public String address;

    @Column(name = "shipping_address_name")
    public String name;

    @Column(name = "shipping_address_mobile")
    public String mobile;

    @Column(name = "shipping_address_zipcode")
    public String zipcode;

    @Column(name = "checkout_date")
    public String checkout_date;

    @Column(name = "payment_date")
    public String payment_date;

    @Column(name = "confirmed_date")
    public String confirmed_date;

    @Column(name = "shipping_date")
    public String shipping_date;

    @Column(name = "pays_sn")
    public String payment_tid;

    public void fromJson(String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            fromJson(jsonObject);
        } catch (JSONException e) {
        }
    }

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.order_id = jsonObject.optInt("id");
        this.order_sn = jsonObject.optString("orderid");
        this.order_amount = jsonObject.optString("order_item_count");
        this.status = jsonObject.optString("status");
        this.shop_title = jsonObject.optString("shop_title");
        this.shipping_rates = jsonObject.optDouble("express");
        this.shipping_carrier = jsonObject.optString("express_name");
        this.shipping_tracking_number = jsonObject.optString("express_sn");
        this.shipping_remark = jsonObject.optString("express_remark");
        this.product_subtotal = jsonObject.getDouble("prices");
        this.total = jsonObject.optDouble("total");
        this.province = jsonObject.optString("addr_province");
        this.city = jsonObject.optString("addr_city");
        this.county = jsonObject.optString("addr_area");
        this.address = jsonObject.optString("addr_address");
        this.name = jsonObject.optString("addr_name");
        this.mobile = jsonObject.optString("addr_tele");
        this.zipcode = jsonObject.optString("addr_zipcode");
        this.checkout_date = jsonObject.optString("add_time");
        this.payment_date = jsonObject.optString("pays_time");
        this.shipping_date = jsonObject.optString("express_time");
        this.confirmed_date = jsonObject.optString("check_time");
        this.payment_tid = jsonObject.optString("pays_sn");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("order_amount", order_amount);
        localItemObject.put("desc", desc);
        localItemObject.put("pay_code", pay_code);
        localItemObject.put("subject", subject);
        localItemObject.put("order_sn", order_sn);
        localItemObject.put("order_id", order_id);

        return localItemObject;
    }
}
