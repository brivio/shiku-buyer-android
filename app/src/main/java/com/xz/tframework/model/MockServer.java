package com.xz.tframework.model;

import com.xz.external.androidquery.callback.AjaxCallback;

import org.json.JSONObject;

/**
 * Created by txj on 15/4/14.
 */

public class MockServer
{
    private static MockServer instance;
    public static MockServer getInstance()
    {
        if (instance == null) {
            instance = new MockServer();
        }
        return instance;
    }
    
    public static <K> void ajax(AjaxCallback<K> callback)
    {
		JSONObject responseJsonObject = new JSONObject();

        ((BeeCallback)callback).callback(callback.getUrl(), responseJsonObject, callback.getStatus());
   }
}
