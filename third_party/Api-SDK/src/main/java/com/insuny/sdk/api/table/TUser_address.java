package com.insuny.sdk.api.table;
/*
该文件由脚本自动生成，不要手动修改
 */
import java.util.ArrayList;

public class TUser_address{
    public String id;
    public String uid;
    public String uname;
    public String province;
    public String city;
    public String area;
    public String province_id;
    public String city_id;
    public String area_id;
    public String address;
    public String zipcode;
    public String tele;
    public String name;
    public String remark;
    public String is_default;
    public String status;
    public String country;
    public String add_time;
}