
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;
import com.xz.tframework.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@Table(name = "GOODS")
public class GOODS extends Model {

    @Column(name = "GOODS_id")
    public String id;

    @Column(name = "goods_name")
    public String goods_name;

    @Column(name = "total_favs")
    public int total_favs;

    @Column(name = "shipping_rates")
    public double shipping_rates;

    @Column(name = "total_sales")
    public int total_sales;

    @Column(name = "price")
    public double price;

    @Column(name = "shop_info")
    public Shop shop_info;

    @Column(name = "feature_image")
    public String feature_image;

    @Column(name = "stock")
    public int stock;

    @Column(name = "collected")
    public boolean collected;

    public List<ProductImage> images;
    public List<ProductVariant> variants;

    /**
     * Unused fields below
     */
    @Column(name = "img")
    public PHOTO img;

    @Column(name = "promote_end_date")
    public String promote_end_date;

    @Column(name = "click_count")
    public String click_count;

    @Column(name = "goods_sn")
    public String goods_sn;

    @Column(name = "promote_start_date")
    public String promote_start_date;

    @Column(name = "goods_number")
    public String goods_number;

    @Column(name = "brand")
    public String brand;

    @Column(name = "goods_weight")
    public String goods_weight;

    @Column(name = "promote_price")
    public int promote_price;

    @Column(name = "formated_promote_price")
    public String formated_promote_price;

    @Column(name = "integral")
    public int integral;

    @Column(name = "cat_id")
    public String cat_id;

    @Column(name = "is_shipping")
    public String is_shipping;

    @Column(name = "shop_price")
    public String shop_price;

    @Column(name = "market_price")
    public String market_price;

    public ArrayList<PHOTO> pictures;
    public ArrayList<PROPERTY> properties;
    public ArrayList<SPECIFICATION> specification;
    public ArrayList<PRICE> rank_prices;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optString("id");
        this.goods_name = jsonObject.optString("title");
        this.brand = jsonObject.optString("brand");
        this.total_favs = jsonObject.optInt("likes");
        this.shipping_rates = jsonObject.optDouble("express");
        this.total_sales = jsonObject.optInt("sales");
        this.price = jsonObject.optDouble("price");
        this.feature_image = jsonObject.optString("img");
        this.stock = jsonObject.optInt("stock");
        this.collected = jsonObject.optInt("is_like", 0) == 1;

        JSONArray subItemArray;

        subItemArray = jsonObject.optJSONArray("item_img");
        images = new ArrayList<>();
        if (subItemArray != null && subItemArray.length() > 0) {
            for (int i = 0; i < subItemArray.length(); i++) {
                ProductImage image = new ProductImage();
                image.fromJson(subItemArray.getJSONObject(i));
                images.add(image);
            }
        }

        subItemArray = jsonObject.optJSONArray("sku_list");
        variants = new ArrayList<>();
        if (subItemArray != null && subItemArray.length() > 0) {
            for (int i = 0; i < subItemArray.length(); i++) {
                ProductVariant variant = new ProductVariant();
                variant.fromJson(subItemArray.getJSONObject(i));
                variants.add(variant);
            }
        }

        Shop shop = new Shop();
        shop.fromJson(jsonObject.optJSONObject("member_shop"));
        this.shop_info = shop;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();
        JSONArray itemJSONArray = new JSONArray();

        localItemObject.put("promote_end_date", promote_end_date);
        localItemObject.put("click_count", click_count);
        localItemObject.put("goods_sn", goods_sn);
        localItemObject.put("promote_start_date", promote_start_date);
        localItemObject.put("goods_number", goods_number);

        for (int i = 0; i < rank_prices.size(); i++) {
            PRICE itemData = rank_prices.get(i);
            JSONObject itemJSONObject = itemData.toJson();
            itemJSONArray.put(itemJSONObject);
        }
        localItemObject.put("rank_prices", itemJSONArray);
        if (null != img) {
            localItemObject.put("img", img.toJson());
        }
        localItemObject.put("brand", brand);

        for (int i = 0; i < pictures.size(); i++) {
            PHOTO itemData = pictures.get(i);
            JSONObject itemJSONObject = itemData.toJson();
            itemJSONArray.put(itemJSONObject);
        }
        localItemObject.put("pictures", itemJSONArray);
        localItemObject.put("goods_name", goods_name);

        for (int i = 0; i < properties.size(); i++) {
            PROPERTY itemData = properties.get(i);
            JSONObject itemJSONObject = itemData.toJson();
            itemJSONArray.put(itemJSONObject);
        }
        localItemObject.put("properties", itemJSONArray);
        localItemObject.put("goods_weight", goods_weight);
        localItemObject.put("promote_price", promote_price);
        localItemObject.put("formated_promote_price", formated_promote_price);
        localItemObject.put("integral", integral);
        localItemObject.put("id", id);
        localItemObject.put("cat_id", cat_id);
        localItemObject.put("is_shipping", is_shipping);
        localItemObject.put("shop_price", shop_price);
        localItemObject.put("market_price", market_price);
        localItemObject.put("collected", collected);

        for (int i = 0; i < specification.size(); i++) {
            SPECIFICATION itemData = specification.get(i);
            JSONObject itemJSONObject = itemData.toJson();
            itemJSONArray.put(itemJSONObject);
        }
        localItemObject.put("specification", itemJSONArray);

        return localItemObject;
    }

    public ProductOptions getOptions() {
        ProductOptions options = new ProductOptions();
        options.id = Utils.tryParseInteger(this.id, 0);
        options.name = this.goods_name;
        options.feature_img = this.feature_image;
        options.price = this.price;
        options.stock = this.stock;
        options.variants = this.variants;

        return options;
    }
}
