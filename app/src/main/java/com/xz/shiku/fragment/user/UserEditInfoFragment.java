package com.xz.shiku.fragment.user;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.USER;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.controller.UserController;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.view.SegmentButton;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserEditInfoFragment extends BackHandledFragment implements PopActivity.PopActivityListener {

    @InjectView(R.id.user_avatar)
    ImageView mAvatar;
    @InjectView(R.id.username)
    EditText mUserName;
    @InjectView(R.id.user_level)
    TextView mUserLevel;
    @InjectView(R.id.user_mobile)
    TextView mUserMobile;
    @InjectView(R.id.user_region)
    EditText mUserRegion;
    @InjectView(R.id.user_gender)
    SegmentButton mUserGender;
    @InjectView(R.id.signup_with_qq)
    ImageView mSignupWithQQ;
    @InjectView(R.id.signup_with_weixin)
    ImageView mSignupWithWX;
    @InjectView(R.id.signup_with_weibo)
    ImageView mSignupWithWB;

    public UserEditInfoFragment() {
        // Required empty public constructor
        PopActivity.gPopActivityListener = this;
        PopActivity.gShowNavigationBar = true;

        titleResId = R.string.title_fragment_user_edit_info;
        topRightTextResId = R.string.save;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_user_edit_info, container, false);

        ButterKnife.inject(this, view);
        initUI(UserController.getInstance().getUser());
        UserController.getInstance().addResponseListener(this);

        return view;
    }

    private void initUI(USER user) {
        if (user != null) {
            UILUtil.getInstance().getImage(getActivity(), mAvatar, user.avatar);
            mUserName.setText(user.name);
            mUserLevel.setText(String.format("LV%d", user.level));
            mUserMobile.setText(user.tele);
            mUserRegion.setText(String.format("%s\t%s\t%s", user.province, user.city, user.county));
            mUserGender.setPressedButton(user.sex == 1 ? 0 : 1);

            mSignupWithQQ.setImageResource(user.qq ? R.drawable.ic_qq : R.drawable.ic_qq_gray);
            mSignupWithWX.setImageResource(user.weixin ? R.drawable.ic_weixin : R.drawable.ic_weixin_gray);
            mSignupWithWB.setImageResource(user.weibo ? R.drawable.ic_weibo : R.drawable.ic_weibo_gray);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        UserController.getInstance().removeResponseListener(this);
        PopActivity.gPopActivityListener = null;
        topRightTextResId = 0;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_INFO_UPDATE)) {
            if (UserController.getInstance().getLastResponseStatus().getIsSuccess()) {
                getActivity().finish();
            } else {
                ToastView.showMessage(getActivity(), UserController.getInstance()
                        .getLastResponseStatus().getErrorMessage());
            }
        }
    }

    @Override
    public void onTopRightClick() {
        USER user = UserController.getInstance().getUser();
        user.name = mUserName.getText().toString();
        user.sex = mUserGender.getPosition() == 0 ? 1 : 0;
        UserController.getInstance().updateUserInfo(getActivity());
    }

    @OnClick(R.id.signout)
    public void onSignout() {
        UserController.getInstance().signout();
        getActivity().finish();
    }
}
