package com.xz.shiku.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xz.btc.protocol.CartItem;
import com.xz.btc.protocol.CartItemProduct;
import com.xz.shiku.R;
import com.xz.shiku.controller.ShoppingCartController;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.view.MyDialog;
import com.xz.tframework.view.NumberPicker;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class CartAdapter extends BaseAdapter {

    private List<ShoppingCartController.ShoppingCartListViewItem> mItems;
    private OnCartActionListener mListener;

    private Set<Integer> mInEditModePositions;
    private Set<Integer> mCheckedPositions;

    private LayoutInflater mInflater;
    private Context mContext;
    private DeleteConfirmationDialog mConfirmDialog;

    public CartAdapter(Context context, List<ShoppingCartController.ShoppingCartListViewItem> items) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mItems = items;
        mInEditModePositions = new HashSet<>();
        mCheckedPositions = new HashSet<>();
    }

    public boolean isCheck() {
        return mCheckedPositions.size() > 0;
    }

    public List<ShoppingCartController.ShoppingCartListViewItem> getCheckItems() {
        List<ShoppingCartController.ShoppingCartListViewItem> result = new ArrayList<>();
        for (Integer i : mCheckedPositions) {
            result.add(mItems.get(i));
        }
        return result;
    }

    public void checkAll(boolean checked) {
        if (checked) {
            for (int i = 0; i < mItems.size(); i++) {
                if (!mCheckedPositions.contains(i)) {
                    mCheckedPositions.add(i);
                }
            }
        } else {
            mCheckedPositions.clear();
        }
        notifyDataSetChanged();
        onCheckedChanged();
    }

    public void remove(int position) {
        int header = getHeaderPosition(position);
        ShoppingCartController.ShoppingCartListViewItem headerItem = mItems.get(header);
        int startToRemovePosition;
        int itemToRemoveCount;
        if (headerItem.mProductCount == 1) {
            startToRemovePosition = header;
            itemToRemoveCount = 3;
        } else {
            startToRemovePosition = position;
            itemToRemoveCount = 1;
        }

        for (int i = 0; i < itemToRemoveCount; i++) {
            mItems.remove(startToRemovePosition);

            if (mCheckedPositions.contains(startToRemovePosition + i)) {
                mCheckedPositions.remove(startToRemovePosition + i);
            }
            if (mInEditModePositions.contains(startToRemovePosition + i)) {
                mInEditModePositions.remove(startToRemovePosition + i);
            }
        }

        Set<Integer> newPositions = new HashSet<>();
        for (Iterator<Integer> i = mCheckedPositions.iterator(); i.hasNext(); ) {
            int pos = i.next();
            if (pos > startToRemovePosition) {
                i.remove();
                newPositions.add(pos - itemToRemoveCount);
            }
        }
        mCheckedPositions.addAll(newPositions);
        newPositions.clear();

        for (Iterator<Integer> i = mInEditModePositions.iterator(); i.hasNext(); ) {
            int pos = i.next();
            if (pos > startToRemovePosition) {
                i.remove();
                newPositions.add(pos - itemToRemoveCount);
            }
        }
        mInEditModePositions.addAll(newPositions);
        newPositions.clear();

        if (headerItem.mProductCount > 1) {
            headerItem.mProductCount--;

            int checkedCount = 0;
            for (int i = 1; i <= headerItem.mProductCount; i++) {
                if (!mCheckedPositions.contains(header + i)) {
                    break;
                }
                checkedCount++;
            }
            if (checkedCount != headerItem.mProductCount) {
                mCheckedPositions.remove(header);
                mCheckedPositions.remove(header + headerItem.mProductCount + 1);
            } else {
                mCheckedPositions.add(header);
                mCheckedPositions.add(header + headerItem.mProductCount + 1);
            }
        }

        notifyDataSetChanged();
        onCheckedChanged();
    }

    public void update(int position) {
        onCheckedChanged();
    }

    private int getHeaderPosition(int position) {
        while (mItems.get(position).mItemType != ShoppingCartController.ItemType.HEADER
                && position >= 0) {
            position--;
        }

        return position;
    }

    private boolean getIsInEditMode(int position) {
        return mInEditModePositions.contains(position);
    }

    private boolean getIsChecked(int position) {
        return mCheckedPositions.contains(position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).mItemType.getValue();
    }

    @Override
    public int getViewTypeCount() {
        return ShoppingCartController.ItemType.values().length;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ShoppingCartController.ShoppingCartListViewItem item = mItems.get(position);
        OnClickListener listener;
        ViewHolder holder;

        switch (item.mItemType) {
            case ITEM:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = mInflater.inflate(R.layout.item_cart, null);
                    listener = new OnClickListener();

                    holder.mItemLayout = (FrameLayout) convertView.findViewById(R.id.shop_car_item_view);
                    holder.mItemView = (LinearLayout) convertView.findViewById(R.id.shop_car_item_view1);
                    holder.mItemEditView = (FrameLayout) convertView.findViewById(R.id.shop_car_item_view2);

                    holder.mCheckbox = (CheckBox) convertView.findViewById(R.id.cb_choice);
                    holder.mCheckbox.setOnClickListener(listener);

                    holder.mProductImage = (ImageView) convertView.findViewById(R.id.shop_car_item_image);
                    holder.mProductName = (TextView) convertView.findViewById(R.id.shop_car_item_text);
                    holder.mProductPromotion = (TextView) convertView.findViewById(R.id.shop_car_item_property);
                    holder.mProductPrice = (TextView) convertView.findViewById(R.id.shop_car_item_price);
                    holder.mProductQuantity = (TextView) convertView.findViewById(R.id.shop_car_item_amount);

                    holder.mNumberPicker = (NumberPicker) convertView.findViewById(R.id.shop_car_item_number_picker);
                    holder.mNumberPicker.setOnValueChangeListener(listener);
                    holder.mRemove = (Button) convertView.findViewById(R.id.shop_car_item_remove);
                    holder.mRemove.setOnClickListener(listener);

                    convertView.setTag(R.id.tag_first, holder);
                    convertView.setTag(holder.mCheckbox.getId(), listener);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                    listener = (OnClickListener) convertView.getTag(holder.mCheckbox.getId());
                }

                holder.mCheckbox.setChecked(getIsChecked(position));
                UILUtil.getInstance().getImage(mContext, holder.mProductImage, item.mProduct.image);

                if (getIsInEditMode(position)) {
                    holder.mItemEditView.setVisibility(View.VISIBLE);
                    holder.mNumberPicker.setValue(item.mProduct.quantity);
                } else {
                    holder.mItemEditView.setVisibility(View.GONE);
                    holder.mProductName.setText(item.mProduct.name);
                    holder.mProductPromotion.setText("促销文字信息");
                    holder.mProductPrice.setText(String.format("￥%.2f", item.mProduct.price));
                    holder.mProductQuantity.setText(String.format("x%d", item.mProduct.quantity));
                }
                listener.setPosition(position);
                break;
            case HEADER:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = mInflater.inflate(R.layout.item_cart_header, null);
                    listener = new OnClickListener();

                    holder.mCheckbox = (CheckBox) convertView.findViewById(R.id.cb_choice);
                    holder.mCheckbox.setOnClickListener(listener);
                    holder.mShopName = (TextView) convertView.findViewById(R.id.shop_car_item_text);
                    holder.mEdit = (Button) convertView.findViewById(R.id.shop_car_item_change);
                    holder.mEdit.setOnClickListener(listener);

                    convertView.setTag(R.id.tag_first, holder);
                    convertView.setTag(holder.mEdit.getId(), listener);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                    listener = (OnClickListener) convertView.getTag(holder.mEdit.getId());
                }

                holder.mCheckbox.setChecked(getIsChecked(position));
                holder.mShopName.setText(item.mCartItem.shop_name);
                holder.mEdit.setText(getIsInEditMode(position) ?
                        R.string.shopcaritem_done :
                        R.string.shopcaritem_modification);
                listener.setPosition(position);
                break;
            case FOOTER:
                if (convertView == null) {
                    convertView = mInflater.inflate(R.layout.item_cart_footer, null);
                }
                break;
        }

        return convertView;
    }

    static class ViewHolder {
        CheckBox mCheckbox;
        TextView mShopName;
        Button mEdit;

        FrameLayout mItemLayout;
        LinearLayout mItemView;
        FrameLayout mItemEditView;

        ImageView mProductImage;
        TextView mProductName;
        TextView mProductPromotion;
        TextView mProductPrice;
        TextView mProductQuantity;

        NumberPicker mNumberPicker;
        Button mRemove;
    }

    public interface OnCartActionListener {
        void onCheckedChanged(double subtotal, boolean isAllChecked);

        void onRemoveCartItem(int position, CartItemProduct product);

        void onUpdateCartItem(int position, CartItemProduct product, int newQuantity);
    }

    public void setOnCartActionListener(OnCartActionListener l) {
        mListener = l;
    }

    private void onCheckedChanged() {
        if (mListener != null) {
            double subtotal = 0.0;
            for (int i : mCheckedPositions) {
                if (mItems.get(i).mItemType == ShoppingCartController.ItemType.ITEM) {
                    CartItemProduct product = mItems.get(i).mProduct;
                    subtotal += product.price * product.quantity;
                }
            }
            boolean isAllChecked = mItems.size() == mCheckedPositions.size() && mItems.size() > 0;
            mListener.onCheckedChanged(subtotal, isAllChecked);
        }
    }

    class OnClickListener implements View.OnClickListener, NumberPicker.OnValueChangeListener {
        int mPosition;

        void setPosition(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View v) {
            int vId = v.getId();
            CartItem cartItem = mItems.get(mPosition).mCartItem;
            switch (vId) {
                case R.id.shop_car_item_change:
                    if (getIsInEditMode(mPosition)) {
                        for (int i = 0; i < cartItem.products.size() + 2; i++) {
                            mInEditModePositions.remove(mPosition + i);
                        }
                    } else {
                        for (int i = 0; i < cartItem.products.size() + 2; i++) {
                            mInEditModePositions.add(mPosition + i);
                        }
                    }
                    notifyDataSetChanged();
                    break;
                case R.id.cb_choice:
                    boolean isChecked = ((CheckBox) v).isChecked();
                    switch (mItems.get(mPosition).mItemType) {
                        case HEADER:
                            if (isChecked) {
                                for (int i = 0; i < cartItem.products.size() + 2; i++) {
                                    if (!getIsChecked(mPosition + i)) {
                                        mCheckedPositions.add(mPosition + i);
                                    }
                                }
                            } else {
                                for (int i = 0; i < cartItem.products.size() + 2; i++) {
                                    if (getIsChecked(mPosition + i)) {
                                        mCheckedPositions.remove(mPosition + i);
                                    }
                                }
                            }
                            notifyDataSetChanged();
                            break;
                        case ITEM:
                            int header = getHeaderPosition(mPosition);
                            if (isChecked) {
                                mCheckedPositions.add(mPosition);

                                if (!mCheckedPositions.contains(header)) {
                                    int checkedCount = 0;
                                    for (int i = 1; i < cartItem.products.size() + 1; i++) {
                                        if (mCheckedPositions.contains(header + i)) {
                                            checkedCount++;
                                        }
                                    }
                                    if (checkedCount == cartItem.products.size()) {
                                        mCheckedPositions.add(header);
                                        mCheckedPositions.add(header + cartItem.products.size() + 1);
                                        notifyDataSetChanged();
                                    }
                                }
                            } else {
                                mCheckedPositions.remove(mPosition);
                                if (mCheckedPositions.contains(header)) {
                                    mCheckedPositions.remove(header);
                                    mCheckedPositions.remove(header + cartItem.products.size() + 1);
                                    notifyDataSetChanged();
                                }
                            }
                            break;
                    }
                    onCheckedChanged();
                    break;
                case R.id.shop_car_item_remove:
                    CartItemProduct product = mItems.get(mPosition).mProduct;
                    if (product != null) {
                        if (mConfirmDialog == null) {
                            mConfirmDialog = new DeleteConfirmationDialog(mContext);
                        }
                        mConfirmDialog.setPositionAndProduct(mPosition, product);
                        mConfirmDialog.show();
                    }
                    break;
            }
        }

        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            CartItemProduct product = mItems.get(mPosition).mProduct;
            if (product.quantity != newVal) {
                if (mListener != null) {
                    mListener.onUpdateCartItem(mPosition, product, newVal);
                }
            }
        }
    }

    class DeleteConfirmationDialog extends MyDialog {

        CartItemProduct mProduct;
        int mPosition;

        DeleteConfirmationDialog(Context context) {
            super(context, context.getString(R.string.shopcaritem_remove),
                    context.getString(R.string.delete_confirm));

            negative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            positive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();

                    if (mListener != null) {
                        mListener.onRemoveCartItem(mPosition, mProduct);
                    }
                }
            });
        }

        void setPositionAndProduct(int position, CartItemProduct product) {
            mPosition = position;
            mProduct = product;
        }
    }

}

