package com.insuny.sdk.api.response;
/*
该文件由脚本自动生成，不要手动修改
 */
import com.insuny.sdk.api.extra.ExLogs_get;
import java.util.ArrayList;

public class LogsGetResponse{
    public ExLogs_get data;
    public String status;
    public String result;
}