
package com.xz.btc.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CartList {

    public List<CartItem> items;

    public void fromJson(JSONArray jsonArray) throws JSONException {
        items = new ArrayList<>();

        if (null != jsonArray) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject subItemObject = jsonArray.getJSONObject(i);
                CartItem item = new CartItem();
                item.fromJson(subItemObject);
                items.add(item);
            }
        }
    }

    public JSONObject toJson() throws JSONException {
        return new JSONObject();
    }

}
