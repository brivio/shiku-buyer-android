package com.xz.tframework.utils;

import android.widget.AbsListView;

import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

/**
 * Created by Maimez on 2015/5/22.
 */
public class GridViewScrollListener extends MultiScrollListener {
    private static final int VISIBLE_THRESHOLD = 2;

    private OnLoadMoreListener mListener;

    private AbsListView.OnScrollListener mScrollToLoadMoreListener = new EndlessScrollListener(VISIBLE_THRESHOLD) {
        @Override
        public void onLoadMore(int page, int totalItemsCount) {
            if (mListener != null) {
                mListener.loadMore(page, totalItemsCount);
            }
        }
    };

    public GridViewScrollListener() {

    }

    public GridViewScrollListener(boolean addPauseOnScrollListener) {
        this(addPauseOnScrollListener, false, null);
    }

    public GridViewScrollListener(boolean addPauseOnScrollListener, boolean addScrollToLoadMoreListener, OnLoadMoreListener l) {
        if (addPauseOnScrollListener) {
            addScrollListener(new PauseOnScrollListener(UILUtil.getInstance().getImageLoader(), true, true));
        }
        if (addScrollToLoadMoreListener) {
            addScrollListener(mScrollToLoadMoreListener);
            mListener = l;
        }
    }

    public void reset() {
        if (mScrollToLoadMoreListener != null) {
            ((EndlessScrollListener)mScrollToLoadMoreListener).reset();
        }
    }

    public interface OnLoadMoreListener {
        public void loadMore(int page, int totalItemsCount);
    }
}
