package com.xz.tframework.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.TabHost;

import com.xz.shiku.R;

public class DebugTabActivity extends TabActivity {

    private TabHost tabHost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.tf_debug_home_tab);

        tabHost = getTabHost();


        TabHost.TabSpec spec_tab1 = tabHost.newTabSpec("spec_tab1").setIndicator("spec_tab1")
                .setContent(new Intent(DebugTabActivity.this, DebugMessageListActivity.class));
        tabHost.addTab(spec_tab1);

        TabHost.TabSpec spec_tab2 = tabHost.newTabSpec("spec_tab2").setIndicator("spec_tab2")
                .setContent(new Intent(DebugTabActivity.this, ActivityLifeCycleActivity.class));
        tabHost.addTab(spec_tab2);

        TabHost.TabSpec spec_tab3 = tabHost.newTabSpec("spec_tab3").setIndicator("spec_tab3")
                .setContent(new Intent(DebugTabActivity.this, CrashLogActivity.class));
        tabHost.addTab(spec_tab3);

        TabHost.TabSpec spec_tab4 = tabHost.newTabSpec("spec_tab4").setIndicator("spec_tab4")
                .setContent(new Intent(DebugTabActivity.this, FloatViewSettingActivity.class));
        tabHost.addTab(spec_tab4);


        RadioGroup group = (RadioGroup) this.findViewById(R.id.tab_group);
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch(checkedId) {
                    case R.id.tab_one:
                        tabHost.setCurrentTabByTag("spec_tab1");
                        break;
                    case R.id.tab_two:
                        tabHost.setCurrentTabByTag("spec_tab2");
                        break;
                    case R.id.tab_three:
                        tabHost.setCurrentTabByTag("spec_tab3");
                        break;
                    case R.id.tab_four:
                        tabHost.setCurrentTabByTag("spec_tab4");
                        break;

                }
            }
        });
    }
}

