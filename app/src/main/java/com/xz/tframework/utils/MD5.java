package com.xz.tframework.utils;

import java.security.MessageDigest;

/**
 * Created by txj on 15/3/5.
 */
//public class MD5 {
//    public static String getMD5(String val) throws NoSuchAlgorithmException {
//        MessageDigest md5 = MessageDigest.getInstance("MD5");
//        md5.update(val.getBytes());
//        byte[] m = md5.digest();//加密
//        return getString(m);
//    }
//    private static String getString(byte[] b){
//        StringBuffer sb = new StringBuffer();
//        for(int i = 0; i < b.length; i ++){
//            sb.append(b[i]);
//        }
//        return sb.toString().replace("-","");
//    }
//}

public class MD5 {
    public final static String getMD5(String s) {
        char hexDigits[] = { '0', '1', '2', '3', '4',
                '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F' };
        try {
            byte[] btInput = s.getBytes();
            //获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            //使用指定的字节更新摘要
            mdInst.update(btInput);
            //获得密文
            byte[] md = mdInst.digest();
            //把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return (new String(str)).toLowerCase();
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}