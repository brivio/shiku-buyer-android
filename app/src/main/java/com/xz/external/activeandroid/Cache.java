package com.xz.external.activeandroid;

/**
 * Created by txj on 15/4/14.
 */

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.xz.external.activeandroid.serializer.TypeSerializer;
import com.xz.external.activeandroid.util.Log;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class Cache {
	//////////////////////////////////////////////////////////////////////////////////////
	// PRIVATE MEMBERS
	//////////////////////////////////////////////////////////////////////////////////////

	private static Context sContext;

	private static ModelInfo sModelInfo;
	private static DatabaseHelper sDatabaseHelper;

	private static Set<Model> sEntities;

	private static boolean sIsInitialized = false;

	//////////////////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS
	//////////////////////////////////////////////////////////////////////////////////////

	private Cache() {
	}

	//////////////////////////////////////////////////////////////////////////////////////
	// PUBLIC METHODS
	//////////////////////////////////////////////////////////////////////////////////////

	public static synchronized void initialize(Application application) {
		if (sIsInitialized) {
			Log.v("ActiveAndroid already initialized.");
			return;
		}

		sContext = application;

		sModelInfo = new ModelInfo(application);
		sDatabaseHelper = new DatabaseHelper(sContext);

		sEntities = new HashSet<Model>();

		openDatabase();

		sIsInitialized = true;

		Log.v("ActiveAndroid initialized succesfully.");
	}

	public static synchronized void clear() {
		sEntities = new HashSet<Model>();
		Log.v("Cache cleared.");
	}

	public static synchronized void dispose() {
		closeDatabase();
		
		sEntities = null;
		sModelInfo = null;
		sDatabaseHelper = null;

		sIsInitialized = false;

		Log.v("ActiveAndroid disposed. Call initialize to use library.");
	}

	// Database access

	public static synchronized SQLiteDatabase openDatabase() {
		return sDatabaseHelper.getWritableDatabase();
	}

	public static synchronized void closeDatabase() {
		sDatabaseHelper.close();
	}

	// Context access

	public static Context getContext() {
		return sContext;
	}

	// Entity cache

	public static synchronized void addEntity(Model entity) {
		sEntities.add(entity);
	}

	public static synchronized Model getEntity(Class<? extends Model> type, long id) {
		for (Model entity : sEntities) {
			if (entity != null && entity.getClass() != null && entity.getClass() == type && entity.getId() != null
					&& entity.getId() == id) {

				return entity;
			}
		}

		return null;
	}

	public static synchronized void removeEntity(Model entity) {
		sEntities.remove(entity);
	}

	// Model cache

	public static synchronized Collection<TableInfo> getTableInfos() {
		return sModelInfo.getTableInfos();
	}

	public static synchronized TableInfo getTableInfo(Class<? extends Model> type) {
		return sModelInfo.getTableInfo(type);
	}

	public static synchronized TypeSerializer getParserForType(Class<?> type) {
		return sModelInfo.getTypeSerializer(type);
	}

	public static synchronized String getTableName(Class<? extends Model> type) {
		return sModelInfo.getTableInfo(type).getTableName();
	}
}