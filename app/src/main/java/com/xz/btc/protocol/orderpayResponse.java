
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "orderpayResponse")
public class orderpayResponse extends Model {

    @Column(name = "ORDER_PAY_DATA")
    public ORDER_PAY_DATA data;

    @Column(name = "status")
    public STATUS status;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        STATUS status = new STATUS();
        status.fromJson(jsonObject);
        this.status = status;

        ORDER_PAY_DATA data = new ORDER_PAY_DATA();
        data.fromJson(jsonObject.optJSONObject("data"));
        this.data = data;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        if (null != status) {
            localItemObject.put("status", status.toJson());
        }
        if (null != data) {
            localItemObject.put("data", data.toJson());
        }

        return localItemObject;
    }
}
