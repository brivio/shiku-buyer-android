
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "usercollectcreateRequest")
public class usercollectcreateRequest extends Model {

    @Column(name = "session")
    public SESSION session;

    @Column(name = "goods_id")
    public int goods_id;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        SESSION session = new SESSION();
        session.fromJson(jsonObject.optJSONObject("session"));
        this.session = session;
        this.goods_id = jsonObject.optInt("goods_id");
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        try {
            localItemObject.put("item_id", goods_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }
}
