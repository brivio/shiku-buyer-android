package com.xz.shiku.fragment.address;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xz.btc.protocol.ADDRESS;
import com.xz.btc.protocol.ApiInterface;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.activity.PopActivity;
import com.xz.shiku.controller.AddressController;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.ToastView;
import com.xz.tframework.view.iosdialog.wheel.wheelcity.OnWheelChangedListener;
import com.xz.tframework.view.iosdialog.wheel.wheelcity.WheelView;
import com.xz.tframework.view.iosdialog.wheel.wheelcity.adapters.AbstractWheelTextAdapter;
import com.xz.tframework.view.iosdialog.wheel.wheelcity.adapters.AddressData;
import com.xz.tframework.view.iosdialog.wheel.wheelcity.adapters.ArrayWheelAdapter;
import com.xz.tframework.view.iosdialog.widget.MyAlertDialog;

import org.json.JSONObject;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.xz.shiku.fragment.address.AddressEditFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.xz.shiku.fragment.address.AddressEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddressEditFragment extends BackHandledFragment implements PopActivity.PopActivityListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ADDRESS = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ADDRESS mAddress;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    @InjectView(R.id.et_consignee)
    EditText et_username;
    @InjectView(R.id.et_tele)
    EditText et_tele;
    @InjectView(R.id.et_zipcode)
    EditText et_zipcode;
    @InjectView(R.id.et_address)
    EditText et_address;
    @InjectView(R.id.tv_area)
    TextView tv_area;
    @InjectView(R.id.tv_delbtn)
    TextView tv_submit;

    String mProvince, mCity, mCounty;
    boolean mIsValid = false;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param address Parameter 1.
     * @param param2  Parameter 2.   ==1编辑，不等于1 新增
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddressEditFragment newInstance(ADDRESS address, String param2) {
        PopActivity.gShowNavigationBar = true;

        titleResId = address != null && address.id > 0 ?
                R.string.title_fragment_address_edit :
                R.string.title_fragment_address_edit_add;
        topRightText = "保存";

        AddressEditFragment fragment = new AddressEditFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ADDRESS, address);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        PopActivity.gPopActivityListener = fragment;

        return fragment;
    }

    public AddressEditFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mAddress = getArguments().getParcelable(ARG_ADDRESS);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_addressedit, container, false);

        ButterKnife.inject(this, v);

        if (mAddress != null && mAddress.id > 0) {
            mProvince = mAddress.province;
            mCity = mAddress.city;
            mCounty = mAddress.district;

            et_username.setText(mAddress.consignee);
            et_tele.setText(mAddress.tel);
            et_zipcode.setText(mAddress.zipcode);
            tv_area.setText(String.format("%s  %s  %s", mProvince, mCity, mCounty));
            et_address.setText(mAddress.address);
        }

        tv_submit.setEnabled(false);
        tv_submit.setBackgroundResource(R.drawable.tf_layoutwithstorkeroundgrayfill);
        tv_submit.setTextColor(getResources().getColor(R.color.bg_Main_split));

        AddressController.getInstance().addResponseListener(this);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        AddressController.getInstance().removeResponseListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.ADDRESS_UPDATE)) {
            ToastView.showMessage(getActivity(), "操作成功");
            getActivity().finish();
        }
    }

    //保存修改
    @Override
    public void onTopRightClick() {
        if (mIsValid) {
            ADDRESS address = getAddress();

            AddressController.getInstance().addOrUpdate(getActivity(), address);
        }
    }

    @OnClick(R.id.tv_delbtn)
    public void onSetDefault() {
        ADDRESS address = getAddress();
        address.is_default = true;

        AddressController.getInstance().addOrUpdate(getActivity(), address);
    }

    @OnTextChanged({R.id.et_consignee, R.id.et_tele, R.id.et_address})
    public void onTextChanged(CharSequence text) {
        mIsValid = et_username.length() > 0 && et_tele.length() > 0 && et_address.length() > 0;
        if (!mIsValid) {
            if (tv_submit.isEnabled()) {
                tv_submit.setEnabled(false);
                tv_submit.setBackgroundResource(R.drawable.tf_layoutwithstorkeroundgrayfill);
                tv_submit.setTextColor(getResources().getColor(R.color.bg_Main_split));
            }
        } else {
            if (!tv_submit.isEnabled()) {
                tv_submit.setEnabled(true);
                tv_submit.setBackgroundResource(R.drawable.tf_layoutwithstorkemainroundfill);
                tv_submit.setTextColor(getResources().getColor(R.color.white));
            }
        }
    }

    private ADDRESS getAddress() {
        ADDRESS address = new ADDRESS();
        address.id = mAddress != null ? mAddress.id : 0;
        address.consignee = et_username.getText().toString();
        address.tel = et_tele.getText().toString();
        address.zipcode = et_zipcode.getText().toString();
        address.province = mProvince;
        address.city = mCity;
        address.district = mCounty;
        address.address = et_address.getText().toString();
        address.is_default = mAddress != null && mAddress.is_default;

        return address;
    }

    @OnClick(R.id.tv_area)
    public void onSelectRegion() {
        View view = getSelectRegionDialog();
        MyAlertDialog dialog1 = new MyAlertDialog(getActivity())
                .builder()
                .setTitle("选择地区")
                .setView(view)
                .setNegativeButton("取消", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).setPositiveButton("确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tv_area.setText(String.format("%s  %s  %s", mProvince, mCity, mCounty));
                    }
                });
        dialog1.show();
    }

    private View getSelectRegionDialog() {
        View contentView = LayoutInflater.from(getActivity()).inflate(
                R.layout.wheelcity_cities_layout, null);
        final WheelView country = (WheelView) contentView
                .findViewById(R.id.wheelcity_country);
        // 省级选择
        country.setVisibleItems(3);
        CountryAdapter adapter = new CountryAdapter(getActivity());
        adapter.setTextColor(getResources().getColor(R.color.bg_Main4));
        country.setViewAdapter(adapter);

        final String cities[][] = AddressData.CITIES;
        final String ccities[][][] = AddressData.COUNTIES;
        // 地市选择
        final WheelView city = (WheelView) contentView
                .findViewById(R.id.wheelcity_city);
        city.setVisibleItems(0);

        // 县市选择
        final WheelView ccity = (WheelView) contentView
                .findViewById(R.id.wheelcity_ccity);
        ccity.setVisibleItems(0);// 不限城市

        country.addChangingListener(new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                updateCities(city, cities, newValue);

                mProvince = AddressData.PROVINCES[country.getCurrentItem()];
                mCity = AddressData.CITIES[country.getCurrentItem()][city.getCurrentItem()];
                mCounty = AddressData.COUNTIES[country.getCurrentItem()][city.getCurrentItem()][ccity.getCurrentItem()];
            }
        });

        city.addChangingListener(new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                updatecCities(ccity, ccities, country.getCurrentItem(),
                        newValue);

                mProvince = AddressData.PROVINCES[country.getCurrentItem()];
                mCity = AddressData.CITIES[country.getCurrentItem()][city.getCurrentItem()];
                mCounty = AddressData.COUNTIES[country.getCurrentItem()][city.getCurrentItem()][ccity.getCurrentItem()];
            }
        });

        ccity.addChangingListener(new OnWheelChangedListener() {
            public void onChanged(WheelView wheel, int oldValue, int newValue) {

                mProvince = AddressData.PROVINCES[country.getCurrentItem()];
                mCity = AddressData.CITIES[country.getCurrentItem()][city.getCurrentItem()];
                mCounty = AddressData.COUNTIES[country.getCurrentItem()][city.getCurrentItem()][ccity.getCurrentItem()];
            }
        });

        int countryIndex = 11;
        int cityIndex = 1;
        int countyIndex = 1;
        if (mProvince != null && mCity != null && mCounty != null) {
            try {
                countryIndex = Arrays.asList(AddressData.PROVINCES).indexOf(mProvince);
                cityIndex = Arrays.asList(AddressData.CITIES[countryIndex]).indexOf(mCity);
                countyIndex = Arrays.asList(AddressData.COUNTIES[countryIndex][cityIndex]).indexOf(mCounty);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        country.setCurrentItem(countryIndex > 0 ? countryIndex : 11);
        city.setCurrentItem(cityIndex > 0 ? cityIndex : 1);
        ccity.setCurrentItem(countyIndex > 0 ? countyIndex : 1);

        return contentView;
    }

    /**
     * Updates the city wheel
     */
    private void updateCities(WheelView city, String cities[][], int index) {
        ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<String>(getActivity(),
                cities[index]);

        adapter.setTextSize(18);
        adapter.setTextColor(getResources().getColor(R.color.actionsheet_gray));
        city.setViewAdapter(adapter);
        city.setCurrentItem(0);
    }

    /**
     * Updates the ccity wheel
     */
    private void updatecCities(WheelView city, String ccities[][][], int index,
                               int index2) {
        ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<String>(getActivity(),
                ccities[index][index2]);
        adapter.setTextSize(18);
        adapter.setTextColor(getResources().getColor(R.color.actionsheet_gray));
        city.setViewAdapter(adapter);
        city.setCurrentItem(0);
    }

    /**
     * Adapter for countries
     */
    private class CountryAdapter extends AbstractWheelTextAdapter {
        // Countries names
        private String countries[] = AddressData.PROVINCES;

        /**
         * Constructor
         */
        protected CountryAdapter(Context context) {
            super(context, R.layout.wheelcity_country_layout, NO_RESOURCE);
            setItemTextResource(R.id.wheelcity_country_name);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);
            return view;
        }

        @Override
        public int getItemsCount() {
            return countries.length;
        }

        @Override
        protected CharSequence getItemText(int index) {
            return countries[index];
        }
    }
}