package com.xz.tframework.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.xz.shiku.BuildConfig;
import com.xz.shiku.R;
import com.xz.tframework.TFrameworkApp;

/**
 * Created by XH on 2015/5/20.
 */
public class UILUtil {
    private static final int UIL_MEMORY_CACHE_SIZE = 10 * 1024 * 1024;  // 10MB
    private static final int UIL_DISK_CACHE_SIZE = 50 * 1024 * 1024;    // 50MB

    private static UILUtil mInstance;
    private ImageLoader mImageLoader;
    private DisplayImageOptions mAlternativeDisplayImageOptions;

    private UILUtil() {
        mImageLoader = ImageLoader.getInstance();
    }

    public static UILUtil getInstance() {
        if (mInstance == null) {
            mInstance = new UILUtil();
        }

        return mInstance;
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public void init() {
        if (mImageLoader == null || mImageLoader.isInited()) {
            return;
        }

        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        // ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(TFrameworkApp.getInstance())
                .threadPriority(Thread.NORM_PRIORITY - 2)                                   // UIL的默认设置
                .denyCacheImageMultipleSizesInMemory()
                .memoryCacheSize(UIL_MEMORY_CACHE_SIZE)
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(UIL_DISK_CACHE_SIZE)
                .defaultDisplayImageOptions(new DisplayImageOptions.Builder()
                                .showImageOnLoading(R.drawable.tf_placeholderimg)           // 设置图片下载期间显示的图片
                                .showImageForEmptyUri(R.drawable.tf_placeholderimg)         // 设置图片Uri为空或是错误的时候显示的图片
                                .showImageOnFail(R.drawable.tf_placeholderimg)              // 设置图片加载或解码过程中发生错误显示的图片
                                .cacheInMemory(true)                                        // 设置下载的图片是否缓存在内存中
                                .cacheOnDisk(true)                                          // 设置下载的图片是否缓存在
                                .bitmapConfig(Bitmap.Config.RGB_565)
                                .build()
                );
        if (BuildConfig.DEBUG) {
            builder.writeDebugLogs();
        }

        // Initialize ImageLoader with configuration.
        mImageLoader.init(builder.build());
    }

    public void getImage(Context context, ImageView imageView, String uri) {
        getImage(context, imageView, uri, null, true);
    }

    public void getImage(Context context, ImageView imageView, String uri, boolean fromCache) {
        getImage(context, imageView, uri, null, fromCache);
    }


    public void getImage(Context context, ImageView imageView, String uri, DisplayImageOptions options, boolean fromCache) {
        if (!fromCache) {
            MemoryCacheUtils.removeFromCache(uri, mImageLoader.getMemoryCache());
            DiskCacheUtils.removeFromCache(uri, mImageLoader.getDiskCache());
        }

        if (options != null) {
            mImageLoader.displayImage(uri, imageView, options);
        } else {
            mImageLoader.displayImage(uri, imageView);
        }
    }

    public DisplayImageOptions getAlternativeDisplayImageOptions() {
        if (mAlternativeDisplayImageOptions == null) {
            mAlternativeDisplayImageOptions = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.tf_profile_no_avarta_icon)       // 设置图片下载期间显示的图片
                    .showImageForEmptyUri(R.drawable.tf_profile_no_avarta_icon)     // 设置图片Uri为空或是错误的时候显示的图片
                    .showImageOnFail(R.drawable.tf_profile_no_avarta_icon)          // 设置图片加载或解码过程中发生错误显示的图片
                    .cacheInMemory(true)                                            // 设置下载的图片是否缓存在内存中
                    .cacheOnDisk(true)                                              // 设置下载的图片是否缓存在SD卡中
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();
        }

        return mAlternativeDisplayImageOptions;
    }
}
