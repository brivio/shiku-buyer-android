package com.xz.shiku.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xz.btc.protocol.COLLECT_LIST;
import com.xz.shiku.R;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import java.util.List;

public class FavoriteAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context mContext;

    private List<COLLECT_LIST> mFavorites;
    private int mRightWidth = 0;

    private AbOnItemClickListener abOnItemClickListener;

    public FavoriteAdapter(Context context, List<COLLECT_LIST> favorites, int rightWidth, AbOnItemClickListener listener) {
        mContext = context;
        mFavorites = favorites;
        mInflater = LayoutInflater.from(context);
        mRightWidth = rightWidth;
        abOnItemClickListener = listener;
    }

    @Override
    public int getCount() {
        return mFavorites != null ? mFavorites.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mFavorites != null ? mFavorites.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        OnClickListener listener;
        ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            listener = new OnClickListener();
            view = this.mInflater.inflate(R.layout.item_favorite, parent, false);

            holder.rl_left = (RelativeLayout) view.findViewById(R.id.rl_left);
            holder.rl_right = (RelativeLayout) view.findViewById(R.id.rl_right);
            holder.icon = (ImageView) view.findViewById(R.id.imageview);
            holder.username = (TextView) view.findViewById(R.id.username);
            holder.shopname = (TextView) view.findViewById(R.id.shopname);
            holder.yjhl = (TextView) view.findViewById(R.id.yjhl);
            holder.mcl = (TextView) view.findViewById(R.id.mcl);
            holder.delbtn = (TextView) view.findViewById(R.id.activity_favorite_item_delbtn);
            holder.delbtn.setOnClickListener(listener);

            LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            holder.rl_left.setLayoutParams(lp1);
            LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(
                    mRightWidth,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            holder.rl_right.setLayoutParams(lp2);

            view.setTag(holder);
            view.setTag(holder.delbtn.getId(), listener);
        } else {
            holder = (ViewHolder) view.getTag();
            listener = (OnClickListener) view.getTag(holder.delbtn.getId());

            if (view.getScrollX() > 0) {
                view.scrollTo(0, 0);
            }
        }

        COLLECT_LIST item = mFavorites.get(position);

        UILUtil.getInstance().getImage(mContext, holder.icon, item.productImage);

        holder.username.setText(item.productName);
        holder.yjhl.setText(String.format("￥%.2f", item.price));

        listener.setPosition(position);

        return view;
    }

    class OnClickListener implements View.OnClickListener {

        int mPosition;

        @Override
        public void onClick(View v) {
            if (abOnItemClickListener != null) {
                abOnItemClickListener.onClick(mPosition);
            }
        }

        public void setPosition(int position) {
            mPosition = position;
        }
    }

    static class ViewHolder {
        RelativeLayout rl_left;
        RelativeLayout rl_right;
        ImageView icon;
        TextView username;
        TextView shopname;
        TextView yjhl;
        TextView mcl;
        TextView delbtn;
    }
}
