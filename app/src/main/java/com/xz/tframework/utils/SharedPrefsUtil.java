package com.xz.tframework.utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by XH on 2015/5/10.
 */
public class SharedPrefsUtil {
    private static int MAX_SEARCH_HISTORY_COUNT = 20;

    private static final String PREF_NAME = "userInfo";
    private static final String IS_FIRST_RUN = "isFirstRun";
    private static final String SESSION_TOKEN = "token";
    private static final String SEARCH_HISTROY = "searchHistory";

    private static SharedPrefsUtil instance;
    private Context mContext;

    private SharedPrefsUtil(Context context) {
        mContext = context;
    }

    public static synchronized SharedPrefsUtil getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPrefsUtil(context);
        }

        return instance;
    }

    public boolean getIsFirstRun() {
        SharedPreferences prefs = getSharedPreferences();
        return prefs.getBoolean(IS_FIRST_RUN, true);
    }

    public void setFirstRunComplete() {
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        editor.putBoolean(IS_FIRST_RUN, false);
        editor.commit();
    }

    public void setSession(String token) {
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        editor.putString(SESSION_TOKEN, token);
        editor.commit();
    }

    public String getSession() {
        SharedPreferences prefs = getSharedPreferences();
        return prefs.getString(SESSION_TOKEN, null);
    }

    public void addSearchHistory(String keyword) {
        if (keyword == null || keyword.length() < 1) {
            return;
        }
        
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        List<String> keywords = getSearchHistory();
        if (keywords.indexOf(keyword) == 0) {
            return;
        } else if (keywords.indexOf(keyword) > 0) {
            keywords.remove(keyword);
        } else if (keywords.size() == MAX_SEARCH_HISTORY_COUNT) {
            keywords.remove(MAX_SEARCH_HISTORY_COUNT - 1);
        }
        keywords.add(0, keyword);

        JSONArray jsonArray = new JSONArray(keywords);
        editor.putString(SEARCH_HISTROY, jsonArray.toString());
        editor.commit();
    }

    public List<String> getSearchHistory() {
        SharedPreferences prefs = getSharedPreferences();
        String history = prefs.getString(SEARCH_HISTROY, "[]");
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(history);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonArray = new JSONArray();
        }

        List<String> list = new ArrayList<>(jsonArray.length());
        for (int i = 0; i < jsonArray.length(); i++) {
            list.add(jsonArray.optString(i));
        }

        return list;
    }

    private SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getSharedPreferencesEditor() {
        return getSharedPreferences().edit();
    }
}
