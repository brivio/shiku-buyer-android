package com.xz.btc.model;
/**
 * Created by txj on 15/4/14.
 */

import android.content.Context;

import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.external.androidquery.util.AQUtility;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.model.BeeCallback;
import com.xz.tframework.view.MyProgressDialog;
import com.xz.btc.BTCManager;
import com.xz.shiku.R;
import com.xz.btc.protocol.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderModel extends BaseModel {

    private int page = 1;
    public PAGINATED paginated;

    public ArrayList<GOODORDER> order_list = new ArrayList<GOODORDER>();
    public ArrayList<EXPRESS> express_list = new ArrayList<EXPRESS>();
    public String pay_wap = "";
    public String pay_online = "";
    public String upop_tn = "";
    public String shipping_name;

    public OrderModel(Context context) {
        super(context);

    }

    public void getOrder(String type) {
        page = 1;
        orderlistRequest request = new orderlistRequest();

        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                OrderModel.this.callback(url, jo, status);
                try {
                    orderlistResponse response = new orderlistResponse();
                    response.fromJson(jo);
                    if (jo != null) {

                        if (response.status.succeed == 1) {
                            order_list.clear();
                            ArrayList<GOODORDER> data = response.data;
                            if (null != data && data.size() > 0) {
                                order_list.addAll(data);
                            }
                            paginated = response.paginated;
                        }
                        OrderModel.this.OnMessageResponse(url, jo, status);
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

        };

        SESSION session = SESSION.getInstance();
        PAGINATION pagination = new PAGINATION();
        pagination.page = 1;
        pagination.count = 10;

        request.session = session;
        request.pagination = pagination;
        request.type = type;

        Map<String, String> params = new HashMap<String, String>();

        params.put("json", request.toJson().toString());

        cb.url(ApiInterface.ORDER_LIST).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext, mContext.getResources().getString(R.string.hold_on));
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }

    public void getOrderMore(String type) {

        final orderlistRequest request = new orderlistRequest();

        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                OrderModel.this.callback(url, jo, status);

                try {
                    orderlistResponse response = new orderlistResponse();
                    response.fromJson(jo);
                    if (jo != null) {

                        if (response.status.succeed == 1) {

                            ArrayList<GOODORDER> data = response.data;

                            if (null != data && data.size() > 0) {

                                for (int i = 0; i < data.size(); i++) {
                                    order_list.addAll(data);
                                }
                            }

                            paginated = response.paginated;

                        }
                    }
                    OrderModel.this.OnMessageResponse(url, jo, status);

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

        };

        SESSION session = SESSION.getInstance();
        PAGINATION pagination = new PAGINATION();
        pagination.page = order_list.size() / 10 + 1;
        pagination.count = 10;

        request.session = session;
        request.pagination = pagination;
        request.type = type;

        Map<String, String> params = new HashMap<String, String>();

        params.put("json", request.toJson().toString());

        cb.url(ApiInterface.ORDER_LIST).type(JSONObject.class).params(params);
        if (AQUtility.isDebug()) {
            MockServer.getInstance().ajax(cb);
        } else {
            aq.ajax(cb);
        }

    }

    public void orderPay(int order_id) {

        orderpayRequest request = new orderpayRequest();

        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                OrderModel.this.callback(url, jo, status);
                try {
                    orderpayResponse response = new orderpayResponse();
                    response.fromJson(jo);

                    if (jo != null) {
                        pay_online = response.data.pay_online;
                        pay_wap = response.data.pay_wap;
                        upop_tn = response.data.upop_tn;
                        OrderModel.this.OnMessageResponse(url, jo, status);
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        };

        request.session = SESSION.getInstance();
        request.order_id = order_id;

        Map<String, String> params = new HashMap<String, String>();
        params.put("json", request.toJson().toString());

        cb.url(ApiInterface.ORDER_PAY).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext, mContext.getResources().getString(R.string.hold_on));
        aq.progress(pd.mDialog).ajax(cb);
    }

    // 取消订单
    public void orderCancle(int order_id) {

        ordercancelRequest request = new ordercancelRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                OrderModel.this.callback(url, jo, status);
                try {
                    ordercancelResponse response = new ordercancelResponse();
                    response.fromJson(jo);
                    if (jo != null) {

                        if (response.status.succeed == 1) {

                            OrderModel.this.OnMessageResponse(url, jo, status);
                        }
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

        };

        request.session = SESSION.getInstance();
        request.order_id = order_id;

        Map<String, String> params = new HashMap<String, String>();
        params.put("json", request.toJson().toString());

        cb.url(ApiInterface.ORDER_CANCEL).type(JSONObject.class).params(params);
        aq.ajax(cb);
    }


    // 确认收货
    public void affirmReceived(int order_id) {
        orderconfirmReceivedRequest request = new orderconfirmReceivedRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                OrderModel.this.callback(url, jo, status);
                try {
                    orderconfirmReceivedResponse response = new orderconfirmReceivedResponse();
                    response.fromJson(jo);
                    if (jo != null) {
                        if (response.status.succeed == 1) {
                            OrderModel.this.OnMessageResponse(url, jo, status);
                        }
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };

        request.session = SESSION.getInstance();
        request.order_id = order_id;

        Map<String, String> params = new HashMap<String, String>();
        params.put("json", request.toJson().toString());

        cb.url(ApiInterface.ORDER_AFFIRMRECEIVED).type(JSONObject.class).params(params);
        aq.ajax(cb);
    }

    // 查看物流
    public void orderExpress(String order_id) {

        orderexpressRequest request = new orderexpressRequest();
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {

                OrderModel.this.callback(url, jo, status);

                try {
                    orderexpressResponse response = new orderexpressResponse();
                    response.fromJson(jo);
                    if (jo != null) {
                        if (response.status.succeed == 1) {

                            ORDER_EXPRESS_DATA data = response.data;

                            shipping_name = data.shipping_name;
                            ArrayList<EXPRESS> content = data.content;
                            if (null != content && content.size() > 0) {
                                express_list.clear();
                                express_list.addAll(content);

                            }
                        }
                        OrderModel.this.OnMessageResponse(url, jo, status);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        };

        request.session = SESSION.getInstance();
        request.order_id = order_id;
        request.app_key = BTCManager.getKuaidiKey(mContext);
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("json", request.toJson().toString());
        } catch (JSONException e) {
            // TODO: handle exception
        }
        ;
        ;

        cb.url(ApiInterface.ORDER_EXPRESS).type(JSONObject.class).params(params);
        MyProgressDialog pd = new MyProgressDialog(mContext, mContext.getResources().getString(R.string.hold_on));
        aq.progress(pd.mDialog).ajax(cb);
    }
}
