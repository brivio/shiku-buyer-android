package com.xz.tframework.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xz.tframework.protocol.CrashMessage;
import com.xz.shiku.R;

import java.util.ArrayList;

/**
 * Created by txj on 15/4/14.
 */
public class CrashLogAdapter extends TBaseAdapter {
    public CrashLogAdapter(Context c, ArrayList dataList)
    {
        super(c, dataList);
    }

    public class LogCellHolder extends TCellHolder
    {
        TextView logTimeTextView;
        TextView logContentTextView;
    }

    @Override
    protected TCellHolder createCellHolder(View cellView)
    {
        LogCellHolder holder = new LogCellHolder();
        holder.logTimeTextView = (TextView)cellView.findViewById(R.id.crash_time);
        holder.logContentTextView = (TextView)cellView.findViewById(R.id.crash_content);
        return holder;
    }

    @Override
    protected View bindData(int position, View cellView, ViewGroup parent, TCellHolder h)
    {
    	int size =dataList.size();
        CrashMessage message = (CrashMessage)dataList.get(size-1-position);
        LogCellHolder holder = (LogCellHolder)h;
        holder.logTimeTextView.setText(message.crashTime);
        holder.logContentTextView.setText(message.crashContent);

        return cellView;
    }

    @Override
    public View createCellView()
    {
        return mInflater.inflate(R.layout.tf_crash_log_item,null);
    }
}
