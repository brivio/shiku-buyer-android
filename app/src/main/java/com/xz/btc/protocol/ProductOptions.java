
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@Table(name = "ProductOptions")
public class ProductOptions extends Model {

    @Column(name = "product_id")
    public int id;

    @Column(name = "name")
    public String name;

    @Column(name = "feature_img")
    public String feature_img;

    @Column(name = "price")
    public double price;

    @Column(name = "stock")
    public int stock;

    public List<ProductVariant> variants;

    public void fromJson(String jsonString) {
        if (jsonString == null || jsonString.length() < 1) {
            return;
        }

        try {
            JSONObject localJsonObject = new JSONObject(jsonString);

            this.id = localJsonObject.optInt("id");
            this.name = localJsonObject.optString("name");
            this.feature_img = localJsonObject.optString("feature_img");
            this.price = localJsonObject.optDouble("price");
            this.stock = localJsonObject.optInt("stock");

            JSONArray jsonArray = localJsonObject.optJSONArray("variants");
            variants = new ArrayList<>();
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    ProductVariant variant = new ProductVariant();
                    variant.fromJson(jsonArray.getJSONObject(i));
                    variants.add(variant);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        try {
            localItemObject.put("id", id);
            localItemObject.put("name", name);
            localItemObject.put("feature_img", feature_img);
            localItemObject.put("price", price);
            localItemObject.put("stock", stock);

            if (variants != null) {
                JSONArray jsonArray = new JSONArray();
                for (ProductVariant variant : variants) {
                    jsonArray.put(variant.toJson());
                }
                localItemObject.put("variants", jsonArray);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }
}
