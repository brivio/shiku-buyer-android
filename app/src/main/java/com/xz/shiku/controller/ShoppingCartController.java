package com.xz.shiku.controller;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.CartAddRequest;
import com.xz.btc.protocol.CartFillResponse;
import com.xz.btc.protocol.CartItem;
import com.xz.btc.protocol.CartItemProduct;
import com.xz.btc.protocol.CartList;
import com.xz.btc.protocol.ProductVariant;
import com.xz.btc.protocol.SESSION;
import com.xz.btc.protocol.cartdeleteRequest;
import com.xz.btc.protocol.cartupdateRequest;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCartController extends BaseModel {

    private static ShoppingCartController mInstance;

    public static ShoppingCartController getInstance() {
        if (mInstance == null) {
            mInstance = new ShoppingCartController(TFrameworkApp.getInstance());
        }

        return mInstance;
    }

    CartList mShoppingCart;
    List<ShoppingCartListViewItem> mListViewItems;
    List<Integer> mRemovingItems;
    Map<Integer, Integer> mUpdatingItems;

    protected ShoppingCartController(Context context) {
        super(context);
    }

    public void updateShoppingCartBadge(LinearLayout badge) {
        int count = getProductCount();
        if (count > 0) {
            ((TextView) badge.getChildAt(0)).setText(String.format("%d", count));
            badge.setVisibility(View.VISIBLE);
        } else {
            badge.setVisibility(View.GONE);
        }
    }

    public void clear() {
        if (mShoppingCart != null) {
            mShoppingCart.items.clear();
            mShoppingCart = null;
        }
        if (mListViewItems != null) {
            mListViewItems.clear();
            mListViewItems = null;
        }
        if (mRemovingItems != null) {
            mRemovingItems.clear();
            mRemovingItems = null;
        }
        if (mUpdatingItems != null) {
            mUpdatingItems.clear();
            mUpdatingItems = null;
        }
    }

    public List<ShoppingCartListViewItem> getShoppingCart() {
        if (mListViewItems == null) {
            mListViewItems = new ArrayList<>();
        } else {
            mListViewItems.clear();
        }

        if (mShoppingCart != null && mShoppingCart.items.size() > 0) {
            for (CartItem item : mShoppingCart.items) {
                mListViewItems.add(new ShoppingCartListViewItem(ItemType.HEADER, null, item));
                for (CartItemProduct product : item.products) {
                    mListViewItems.add(new ShoppingCartListViewItem(ItemType.ITEM, product, item));
                }
                mListViewItems.add(new ShoppingCartListViewItem(ItemType.FOOTER, null));
            }
        }

        return mListViewItems;
    }

    public int getProductCount() {
        int count = 0;
        if (mShoppingCart != null) {
            for (CartItem item : mShoppingCart.items) {
                for (CartItemProduct product : item.products) {
                    count += product.quantity;
                }
            }
        }

        return count;
    }

    public void getItems(Context context) {
        getItems(context, true);
    }

    public void getItems(Context context, boolean showProgress) {
        mContext = context;

        Map<String, String> params = new HashMap<>();
        params.put("token", SESSION.getInstance().token);

        ajax(ApiInterface.CART_LIST, params, showProgress);
    }

    public void add(Context context, int productId, int quantity, ProductVariant variant) {
        mContext = context;

        CartAddRequest request = new CartAddRequest();

        request.goods_id = productId;
        request.number = quantity;
        request.variant_name = variant == null ? "" : variant.name;

        Map<String, String> params = new HashMap<>();
        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.CART_ADD, params, true);
    }

    public void update(Context context, int productId, int quantity) {
        mContext = context;

        cartupdateRequest request = new cartupdateRequest();

        request.rec_id = productId;
        request.new_number = quantity;

        Map<String, String> params = new HashMap<>();
        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        if (mUpdatingItems == null) {
            mUpdatingItems = new HashMap<>();
        }
        mUpdatingItems.put(productId, quantity);

        ajax(ApiInterface.CART_UPDATE, params, true);
    }

    public void remove(Context context, int cartItemProductId) {
        mContext = context;

        cartdeleteRequest request = new cartdeleteRequest();

        request.product_ids = String.format("%d", cartItemProductId);

        Map<String, String> params = new HashMap<>();
        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        if (mRemovingItems == null) {
            mRemovingItems = new ArrayList<>();
        }
        mRemovingItems.add(cartItemProductId);

        ajax(ApiInterface.CART_REMOVE, params, true);
    }

    @Override
    protected void onSuccess(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.CART_LIST)) {
            CartFillResponse response = new CartFillResponse();
            response.fromJson(jo);

            mShoppingCart = response.data;
        } else if (url.endsWith(ApiInterface.CART_UPDATE)) {
            CartItemProduct product;
            for (Map.Entry<Integer, Integer> entry : mUpdatingItems.entrySet()) {
                product = null;
                for (CartItem item : mShoppingCart.items) {
                    for (CartItemProduct p : item.products) {
                        if (p.item_id == entry.getKey()) {
                            product = p;
                            break;
                        }
                    }
                    if (product != null) {
                        break;
                    }
                }
                if (product != null) {
                    product.quantity = entry.getValue();
                }
            }
            mUpdatingItems.clear();
        } else if (url.endsWith(ApiInterface.CART_REMOVE)) {
            CartItem cartItem = null;
            CartItemProduct product = null;

            for (int i = 0; i < mRemovingItems.size(); i++) {
                for (CartItem item : mShoppingCart.items) {
                    for (CartItemProduct p : item.products) {
                        if (p.id == mRemovingItems.get(i)) {
                            cartItem = item;
                            product = p;
                            break;
                        }
                    }
                    if (cartItem != null) {
                        break;
                    }
                }

                if (cartItem != null) {
                    if (cartItem.products.size() == 1) {
                        mShoppingCart.items.remove(cartItem);
                    } else {
                        cartItem.products.remove(product);
                    }
                }

                cartItem = null;
                product = null;
            }
            mRemovingItems.clear();
        }

        super.onSuccess(url, jo, status);
    }

    @Override
    protected void onFail(String url, JSONObject jo, AjaxStatus status) {
        ToastView.showMessage(mContext, getLastResponseStatus().getErrorMessage());

        if (url.endsWith(ApiInterface.CART_REMOVE)) {
            if (mRemovingItems != null) {
                mRemovingItems.clear();
            }
        } else if (url.endsWith(ApiInterface.CART_UPDATE)) {
            if (mUpdatingItems != null) {
                mUpdatingItems.clear();
            }
        }

        super.onFail(url, jo, status);
    }

    public enum ItemType {
        HEADER(0x00),
        ITEM(0x01),
        FOOTER(0x02);

        private int mIntValue;

        ItemType(int typeInt) {
            mIntValue = typeInt;
        }

        public int getValue() {
            return mIntValue;
        }
    }

    public static class ShoppingCartListViewItem {
        public ItemType mItemType;
        public CartItemProduct mProduct;
        public CartItem mCartItem;
        public int mProductCount;

        ShoppingCartListViewItem(ItemType type, CartItemProduct product) {
            this(type, product, null);
        }

        ShoppingCartListViewItem(ItemType type, CartItemProduct product, CartItem cartItem) {
            mItemType = type;
            mProduct = product;
            mCartItem = cartItem;
            mProductCount = cartItem == null ? 0 : cartItem.products.size();
        }
    }
}
