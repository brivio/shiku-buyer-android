package com.xz.tframework.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xz.tframework.activity.BaseActivity;
import com.xz.shiku.R;

import java.util.ArrayList;

/**
 * Created by txj on 15/4/14.
 */
public class ActivitylifeCycleAdapter extends TBaseAdapter
{
    public ActivitylifeCycleAdapter(Context c, ArrayList dataList)
    {
        super(c, dataList);
    }

    public class LifeCycleHolder extends TCellHolder
    {
        public TextView activityItem;
    }

    @Override
    protected TCellHolder createCellHolder(View cellView)
    {
        LifeCycleHolder holder = new LifeCycleHolder();
        holder.activityItem = (TextView)cellView.findViewById(R.id.activity_name);
        return holder;
    }

    @Override
    protected View bindData(int position, View cellView, ViewGroup parent, TCellHolder h)
    {
        BaseActivity activity_name = (BaseActivity)dataList.get(position);
        LifeCycleHolder holder = (LifeCycleHolder)h;

        String activity_name_str =  activity_name.getClass().toString();
        holder.activityItem.setText(activity_name_str);
        return null;
    }

    @Override
    public View createCellView()
    {
        return mInflater.inflate(R.layout.tf_activity_lifecycle_item,null);
    }
}
