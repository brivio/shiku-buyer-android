package com.xz.tframework.utils;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * Created by XH on 2015/6/13.
 */
public class PropertiesUtil {
    public static final String DEBUG = "debug";
    public static final String DEBUG_SHOW_GALLERY = "debug.showgallery";

    private static final String PROPERTIES_BASE = "assets";
    private static final String PROPERTIES_FILE = "settings.properties";

    /**
     * Singleton pattern
     */
    private static PropertiesUtil mInstance;

    private Properties mProperties;

    private PropertiesUtil() {
        mProperties = new Properties();

        try {
            URL url = getClass().getResource(String.format("/%s/%s",
                    PROPERTIES_BASE, PROPERTIES_FILE));
            mProperties.load(url.openStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static PropertiesUtil getInstance() {
        if (mInstance == null) {
            mInstance = new PropertiesUtil();
        }

        return mInstance;
    }

    public String getProperty(String name) {
        return mProperties.getProperty(name);
    }

    /**
     * @param name
     * @param defaultValue
     * @return the named property value, or defaultValue if it can't be found.
     */
    public String getStringProperty(String name, String defaultValue) {
        String value = getProperty(name);
        return value != null ? value : defaultValue;
    }

    /**
     * @param name
     * @param defaultValue
     * @return the named property value, or defaultValue if it can't be found.
     */
    public Integer getIntegerProperty(String name, Integer defaultValue) {
        String value = getProperty(name);
        return value != null ? Integer.valueOf(value) : defaultValue;
    }

    /**
     * @param name
     * @param defaultValue
     * @return the named property value, or defaultValue if it can't be found.
     */
    public Boolean getBooleanProperty(String name, Boolean defaultValue) {
        String value = getProperty(name);
        return value != null ? Boolean.valueOf(value) : defaultValue;
    }

    public boolean getIsDebugEnabled() {
        return getBooleanProperty(DEBUG, false);
    }

    public boolean getShowGallery() {
        return getBooleanProperty(DEBUG_SHOW_GALLERY, false);
    }
}
