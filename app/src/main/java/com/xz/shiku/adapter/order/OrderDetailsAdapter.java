package com.xz.shiku.adapter.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xz.btc.protocol.ORDER_GOODS_LIST;
import com.xz.shiku.R;
import com.xz.shiku.controller.OrderController;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.utils.Utils;

import java.util.List;

public class OrderDetailsAdapter extends BaseAdapter {

    private List<OrderController.OrderListViewItem> mOrderListViewItems;
    private LayoutInflater mInflater;
    private Context mContext;

    public OrderDetailsAdapter(Context context, List<OrderController.OrderListViewItem> orderItems) {
        this.mContext = context;
        this.mOrderListViewItems = orderItems;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mOrderListViewItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mOrderListViewItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return mOrderListViewItems.get(position).mItemType.getValue();
    }

    @Override
    public int getViewTypeCount() {
        return OrderController.ItemType.values().length;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        OrderController.OrderListViewItem item = mOrderListViewItems.get(position);
        ViewHolder holder;

        switch (item.mItemType) {
            case ITEM:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = mInflater.inflate(R.layout.item_checkout, parent, false);

                    holder.mProductImage = (ImageView) convertView.findViewById(R.id.shop_car_item_image);
                    holder.mProductTitle = (TextView) convertView.findViewById(R.id.shop_car_item_text);
                    holder.mProductPromotion = (TextView) convertView.findViewById(R.id.shop_car_item_property);
                    holder.mProductPrice = (TextView) convertView.findViewById(R.id.shop_car_item_price);
                    holder.mProductQuantity = (TextView) convertView.findViewById(R.id.shop_car_item_amount);

                    convertView.setTag(R.id.tag_first, holder);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                }

                ORDER_GOODS_LIST goods = (ORDER_GOODS_LIST) item.mTag;

                UILUtil.getInstance().getImage(mContext, holder.mProductImage, goods.goods_info.picture);

                holder.mProductTitle.setText(goods.goods_info.name);
                holder.mProductPromotion.setText(goods.goods_info.origin);
                holder.mProductPrice.setText(String.format("￥%.2f", Utils.tryParseDouble(goods.goods_info.shop_price, 0D)));
                holder.mProductQuantity.setText(String.format("×%d", Utils.tryParseInteger(goods.goods_number, 0)));
                break;
            case HEADER:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = mInflater.inflate(R.layout.item_orderlist_header, parent, false);

                    holder.mShopTitle = (TextView) convertView.findViewById(R.id.shop_car_item_text);
                    holder.mOrderStatus = (TextView) convertView.findViewById(R.id.shop_car_item_change);

                    convertView.setTag(R.id.tag_first, holder);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                }

                holder.mShopTitle.setText(item.mOrder.order_info.shop_title);
                holder.mOrderStatus.setVisibility(View.INVISIBLE);
                break;
            case FOOTER:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = mInflater.inflate(R.layout.item_checkout_footer, parent, false);

                    holder.mShippingRates = (TextView) convertView.findViewById(R.id.tv_total_yf);
                    holder.mOrderTotal = (TextView) convertView.findViewById(R.id.tv_real_pay);
                    holder.mOrderAmount = (TextView) convertView.findViewById(R.id.tv_total_count);

                    convertView.setTag(R.id.tag_first, holder);
                } else {
                    holder = (ViewHolder) convertView.getTag(R.id.tag_first);
                }

                int orderAmount = 0;
                for (ORDER_GOODS_LIST g : item.mOrder.goods_list) {
                    orderAmount += Utils.tryParseInteger(g.goods_number, 0);
                }

                holder.mOrderAmount.setText(String.format("共%d件商品", orderAmount));
                holder.mShippingRates.setText(String.format("￥%.2f", item.mOrder.order_info.shipping_rates));
                holder.mOrderTotal.setText(String.format("￥%.2f", item.mOrder.order_info.total));
                break;
        }

        return convertView;
    }


    static class ViewHolder {
        private ImageView mProductImage;
        private TextView mShopTitle;
        private TextView mOrderStatus;
        private TextView mProductTitle;
        private TextView mProductPromotion;
        private TextView mProductPrice;
        private TextView mProductQuantity;
        private TextView mOrderAmount;
        private TextView mShippingRates;
        private TextView mOrderTotal;
    }
}
