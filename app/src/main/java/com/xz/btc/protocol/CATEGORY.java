
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@Table(name = "CATEGORY")
public class CATEGORY extends Model {

    @Column(name = "CATEGORY_id")
    public String id;

    @Column(name = "name")
    public String name;

    @Column(name = "img")
    public String img;

    public ArrayList<CATEGORY> subcategories = new ArrayList<>();

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        JSONArray subItemArray;

        this.id = jsonObject.optString("id");
        this.name = jsonObject.optString("name");
        this.img = jsonObject.optString("img");

        subItemArray = jsonObject.optJSONArray("items");
        if (null != subItemArray) {
            for (int i = 0; i < subItemArray.length(); i++) {
                JSONObject subItemObject = subItemArray.getJSONObject(i);
                CATEGORY subItem = new CATEGORY();
                subItem.fromJson(subItemObject);
                this.subcategories.add(subItem);
            }
        }
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();
        JSONArray itemJSONArray = new JSONArray();
        localItemObject.put("id", id);
        localItemObject.put("name", name);
        localItemObject.put("img", img);

        for (int i = 0; i < subcategories.size(); i++) {
            CATEGORY itemData = subcategories.get(i);
            JSONObject itemJSONObject = itemData.toJson();
            itemJSONArray.put(itemJSONObject);
        }
        localItemObject.put("items", itemJSONArray);

        return localItemObject;
    }
}
