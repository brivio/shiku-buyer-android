
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "cartdeleteRequest")
public class cartdeleteRequest extends Model {

    @Column(name = "product_ids")
    public String product_ids;

    /**
     * Unused fields.
     */
    @Column(name = "session")
    public SESSION session;

    @Column(name = "rec_id")
    public int rec_id;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        SESSION session = new SESSION();
        session.fromJson(jsonObject.optJSONObject("session"));
        this.session = session;
        this.rec_id = jsonObject.optInt("rec_id");
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        try {
            localItemObject.put("id", product_ids);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }
}
