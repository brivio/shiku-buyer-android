package com.xz.external.activeandroid.app;

/**
 * Created by txj on 15/4/14.
 */

import com.xz.external.activeandroid.ActiveAndroid;

public class Application extends android.app.Application {
	@Override
	public void onCreate() {
		super.onCreate();
		ActiveAndroid.initialize(this);
	}
	
	@Override
	public void onTerminate() {
		super.onTerminate();
		ActiveAndroid.dispose();
	}
}