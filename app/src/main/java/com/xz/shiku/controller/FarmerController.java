package com.xz.shiku.controller;

import android.content.Context;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.Farmer;
import com.xz.btc.protocol.GetFarmersRequest;
import com.xz.btc.protocol.GetFarmersResponse;
import com.xz.btc.protocol.PAGINATED;
import com.xz.btc.protocol.PAGINATION;
import com.xz.btc.protocol.SESSION;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.view.ToastView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by XH on 2015/6/13.
 */
public class FarmerController extends BaseModel {
    public static final int PER_PAGE = 10;

    private static FarmerController mInstance;

    public static FarmerController getInstance() {
        if (mInstance == null) {
            mInstance = new FarmerController(TFrameworkApp.getInstance());
        }

        return mInstance;
    }

    private List<Farmer> mFarmers;
    private PAGINATED mLastSearchPagination;
    private Object mTag;

    protected FarmerController(Context context) {
        super(context);
    }

    public List<Farmer> getFarmersResult() {
        return mFarmers;
    }

    public boolean getIsFirstPage() {
        return (mLastSearchPagination != null &&
                mLastSearchPagination.page == 1);
    }

    public boolean getHasMore() {
        boolean hasMore = false;

        if (mLastSearchPagination != null) {
            hasMore = mLastSearchPagination.more > 0;
        }

        return hasMore;
    }

    public void getFarmers(Context context) {
        mContext = context;

        GetFarmersRequest request = new GetFarmersRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = 1;
        pagination.count = PER_PAGE;

        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.FARMER_LIST, params, true);
    }

    public void getMoreFarmers(Context context) {
        if (!getHasMore()) {
            return;
        }

        mContext = context;

        GetFarmersRequest request = new GetFarmersRequest();

        PAGINATION pagination = new PAGINATION();
        pagination.page = mLastSearchPagination.page + 1;
        pagination.count = PER_PAGE;

        request.pagination = pagination;

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());

        ajax(ApiInterface.FARMER_LIST, params, true);
    }

    public void add(Context context, int farmerId) {
        mContext = context;

        JSONObject data = new JSONObject();

        try {
            data.put("follow_uid", farmerId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", data.toString());

        ajax(ApiInterface.FARMER_ADD, params, true);
    }

    public void delete(Context context, int farmerId) {
        mContext = context;

        JSONObject data = new JSONObject();

        try {
            data.put("follow_uid", farmerId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> params = new HashMap<>();

        params.put("token", SESSION.getInstance().token);
        params.put("data", data.toString());

        mTag = farmerId;

        ajax(ApiInterface.FARMER_DELETE, params, true);
    }

    @Override
    protected void onSuccess(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.FARMER_LIST)) {
            GetFarmersResponse response = new GetFarmersResponse();
            try {
                response.fromJson(jo);

                mLastSearchPagination = response.paginated;

                if (mLastSearchPagination.page == 1) {
                    if (mFarmers == null) {
                        mFarmers = new ArrayList<>();
                    } else {
                        mFarmers.clear();
                    }
                }

                if (response.data != null && response.data.size() > 0) {
                    mFarmers.addAll(response.data);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (url.endsWith(ApiInterface.FARMER_DELETE)) {
            int farmerId = (int) mTag;
            for (Iterator<Farmer> iterator = mFarmers.iterator(); iterator.hasNext(); ) {
                Farmer farmer = iterator.next();
                if (farmer.followerId == farmerId) {
                    iterator.remove();
                    break;
                }
            }
        }

        super.onSuccess(url, jo, status);
    }

    @Override
    protected void onFail(String url, JSONObject jo, AjaxStatus status) {
        ToastView.showMessage(mContext, getLastResponseStatus().getErrorMessage());
    }
}
