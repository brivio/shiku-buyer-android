package com.xz.tframework.model;

/**
 * Created by txj on 15/4/14.
 */

import android.content.Context;
import android.content.res.Resources;
import android.view.Gravity;

import com.xz.btc.ErrorCodeConst;
import com.xz.btc.protocol.STATUS;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.tframework.view.MyProgressDialog;
import com.xz.tframework.view.ToastView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class BaseModel implements BusinessResponse {

    protected ArrayList<BusinessResponse> mOnCallbackListeners;
    protected BeeQuery aq;
    protected MockServer ms;
    protected Context mContext;
    protected STATUS mResponseStatus;
    protected tfCallback mCallback;

    public BaseModel() {

    }

    public BaseModel(Context context) {
        aq = new BeeQuery(context);
        mContext = context;
    }


    public STATUS getLastResponseStatus() {
        return mResponseStatus;
    }

    public void addResponseListener(BusinessResponse listener) {
        if (mOnCallbackListeners == null) {
            mOnCallbackListeners = new ArrayList<>();
            mOnCallbackListeners.add(listener);
        } else {
            if (!mOnCallbackListeners.contains(listener)) {
                mOnCallbackListeners.add(listener);
            }
        }
    }

    public void removeResponseListener(BusinessResponse listener) {
        if (mOnCallbackListeners != null) {
            mOnCallbackListeners.remove(listener);
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) throws JSONException {
        if (mOnCallbackListeners != null) {
            for (BusinessResponse listener : mOnCallbackListeners) {
                listener.OnMessageResponse(url, jo, status);
            }
        }
    }

    protected void ajax(String url, Map<String, String> params) {
        ajax(url, params, false);
    }

    protected void ajax(String url, Map<String, String> params, boolean showProgress) {
        ajax(url, params, showProgress, null, null);
    }

    protected void ajax(String url, Map<String, String> params, boolean showProgress, String cookieKey, String cookieValue) {
        if (showProgress) {
            aq.progress(getProgressDialog().mDialog);
        }

        tfCallback tf;
        if (cookieKey != null) {
            tf = new tfCallback();
            tf.type(JSONObject.class).cookie(cookieKey, cookieValue);
        } else {
            tf = getAjaxCallback();
        }

        tf.url(url).params(params);

        aq.ajax(tf);
    }

    protected tfCallback getAjaxCallback() {
        if (mCallback == null) {
            mCallback = new tfCallback();
            mCallback.type(JSONObject.class);
        }

        return mCallback;
    }

    protected MyProgressDialog getProgressDialog() {
        return new MyProgressDialog(mContext,
                mContext.getResources().getString(R.string.hold_on));
    }

    protected void onSuccess(String url, JSONObject jo, AjaxStatus status) {
        onComplete(url, jo, status);
    }

    protected void onFail(String url, JSONObject jo, AjaxStatus status) {
        onComplete(url, jo, status);
    }

    protected void onComplete(String url, JSONObject jo, AjaxStatus status) {
        try {
            aq.dismiss();

            OnMessageResponse(url, jo, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // 公共的错误处理
    protected void callback(String url, JSONObject jo, AjaxStatus status) {
        if (null == jo) {
            mResponseStatus = new STATUS();
            mResponseStatus.succeed = 0;
            mResponseStatus.error_desc = "网络错误，请检查网络设置";

            onFail(url, jo, status);

            return;
        }

        try {
            mResponseStatus = new STATUS();
            mResponseStatus.fromJson(jo);
        } catch (JSONException e) {
            e.printStackTrace();
            onFail(url, jo, status);

            return;
        }

        if (mResponseStatus.succeed == ErrorCodeConst.ResponseSucceed) {
            onSuccess(url, jo, status);
        } else {
            Resources resource = mContext.getResources();
            String way = resource.getString(R.string.delivery);
            String col = resource.getString(R.string.collected);
            String und = resource.getString(R.string.understock);
            String been = resource.getString(R.string.been_used);
            String sub = resource.getString(R.string.submit_the_parameter_error);
            String fai = resource.getString(R.string.failed);
            String pur = resource.getString(R.string.error_10008);
            String noi = resource.getString(R.string.no_shipping_information);
            String error = resource.getString(R.string.username_or_password_error);

            if (mResponseStatus.error_code == ErrorCodeConst.InvalidSession) {
                ToastView toast = new ToastView(mContext, mContext.getString(R.string.session_expires_tips));
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            if (mResponseStatus.error_code == ErrorCodeConst.SelectedDeliverMethod) {
                ToastView toast = new ToastView(mContext, way);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            if (mResponseStatus.error_code == ErrorCodeConst.AlreadyCollected) {
                //Toast.makeText(mContext, "您已收藏过此商品", Toast.LENGTH_SHORT).show();
                ToastView toast = new ToastView(mContext, col);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            if (mResponseStatus.error_code == ErrorCodeConst.InventoryShortage) {
                ToastView toast = new ToastView(mContext, und);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            if (mResponseStatus.error_code == ErrorCodeConst.UserOrEmailExist) {
                ToastView toast = new ToastView(mContext, been);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            if (mResponseStatus.error_code == ErrorCodeConst.InvalidParameter) {
                ToastView toast = new ToastView(mContext, sub);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            if (mResponseStatus.error_code == ErrorCodeConst.ProcessFailed) {
                ToastView toast = new ToastView(mContext, fai);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            if (mResponseStatus.error_code == ErrorCodeConst.BuyFailed) {
                ToastView toast = new ToastView(mContext, pur);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            if (mResponseStatus.error_code == ErrorCodeConst.NoShippingInformation) {
                ToastView toast = new ToastView(mContext, noi);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            if (mResponseStatus.error_code == ErrorCodeConst.InvalidUsernameOrPassword) {
                ToastView toast = new ToastView(mContext, error);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            if (mResponseStatus.error_code == ErrorCodeConst.UserOrEmailExist) {
                ToastView toast = new ToastView(mContext, resource.getString(R.string.been_used));
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            onFail(url, jo, status);
        }
    }

    class tfCallback extends BeeCallback<JSONObject> {

        @Override
        public void callback(String url, JSONObject jo, AjaxStatus status) {
            BaseModel.this.callback(url, jo, status);
        }
    }
}
