package com.xz.shiku.adapter.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xz.btc.protocol.SIMPLEGOODS;
import com.xz.shiku.R;
import com.xz.shiku.ShareHelper;
import com.xz.tframework.utils.UILUtil;
import com.xz.tframework.utils.Utils;

import java.util.List;

public class SearchResultAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    List<SIMPLEGOODS> mResults;
    Context mContext;

    public SearchResultAdapter(Context context, List<SIMPLEGOODS> results) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mResults = results;
    }

    @Override
    public int getCount() {
        return mResults.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = mInflater.inflate(R.layout.item_search_result, null);
            holder.mImage = (ImageView) view.findViewById(R.id.product_list_item_image);
            holder.mShare = (ImageView) view.findViewById(R.id.share);
            holder.mTitle = (TextView) view.findViewById(R.id.product_list_item_title);
            holder.mPrice = (TextView) view.findViewById(R.id.product_list_item_price);
            holder.mSales = (TextView) view.findViewById(R.id.product_list_item_sales);
            holder.mOrigin = (TextView) view.findViewById(R.id.product_list_item_origin);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        SIMPLEGOODS good = mResults.get(position);
        UILUtil.getInstance().getImage(mContext, holder.mImage, good.picture);

        holder.mPrice.setText(String.format("%.2f", Utils.tryParseDouble(good.shop_price, 0D)));
        holder.mTitle.setText(good.name);
        holder.mSales.setText(String.format("%d", Utils.tryParseInteger(good.sales, 0)));
        holder.mOrigin.setText(good.origin);
        holder.mShare.setTag(good);
        holder.mShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SIMPLEGOODS tag = (SIMPLEGOODS) v.getTag();
                ShareHelper helper = new ShareHelper(mContext);
                helper.share(tag.name, tag.name, "http://www.shiku.com");
            }
        });
        return view;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Object getItem(int i) {
        return mResults == null ? null : mResults.get(i);
    }

    static class ViewHolder {
        ImageView mImage;
        TextView mTitle;
        TextView mPrice;
        TextView mSales;
        TextView mOrigin;
        ImageView mShare;
    }
}
