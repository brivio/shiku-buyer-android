package com.xz.btc.protocol;

/**
 * Created by txj on 15/3/6.
 */

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TasteResult {

    public static final int RESULT_LISTS = 1;
    public static final int RESULT_CADIDATE = 2;
    private static final String MY_TASTE = "我的品味";
    private static final String RECOMMENDED_TASTE = "热门推荐";

    public List<TasteItem> myTaste;
    public List<TasteItem> recommendedTaste;
    public List<TasteItem> candidate;

    public void fromJson(JSONObject jsonObject, int resultType) {
        if (null == jsonObject) {
            return;
        }

        JSONArray jsonArray;
        if (resultType == RESULT_LISTS) {
            myTaste = new ArrayList<>();
            recommendedTaste = new ArrayList<>();

            JSONArray resultArray = jsonObject.optJSONArray("list");
            if (resultArray != null && resultArray.length() == 2) {
                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject resultObject = resultArray.optJSONObject(i);
                    String resultName = resultObject.optString("name");
                    jsonArray = resultObject.optJSONArray("items");
                    List<TasteItem> target;

                    // The code below ignores the resultName.
                    //target = i == 0 ? myTaste : recommendedTaste;
                    if (resultName == null || resultName.length() < 1) {
                        break;
                    } else if (resultName.equals(MY_TASTE)) {
                        target = myTaste;
                    } else if (resultName.equals(RECOMMENDED_TASTE)) {
                        target = recommendedTaste;
                    } else {
                        break;
                    }

                    if (jsonArray != null && jsonArray.length() > 0) {
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject jsonItem = jsonArray.optJSONObject(j);
                            TasteItem subitem = new TasteItem();
                            subitem.fromJson(jsonItem);
                            target.add(subitem);
                        }
                    }
                }
            }
        } else if (resultType == RESULT_CADIDATE) {
            candidate = new ArrayList<>();

            jsonArray = jsonObject.optJSONArray("data");

            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.optJSONObject(i);
                    TasteItem subitem = new TasteItem();
                    subitem.fromJson(jsonItem);
                    candidate.add(subitem);
                }
            }
        }
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }
}
