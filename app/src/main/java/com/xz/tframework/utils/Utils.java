package com.xz.tframework.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.xz.shiku.BuildConfig;
import com.xz.shiku.R;
import com.xz.tframework.view.ToastView;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Utils {
    public static Object parseResponse(String responseBody) throws JSONException {
        Object result = null;
        //trim the string to prevent start with blank, and test if the string is valid JSON, because the parser don't do this :(. If Json is not valid this will return null
        responseBody = responseBody.trim();
        if (responseBody.startsWith("{") || responseBody.startsWith("[")) {
            result = new JSONTokener(responseBody).nextValue();
        }
        if (result == null) {
            result = responseBody;
        }
        return result;
    }

    public static Bitmap scaleBitmap(Bitmap bm, int pixel) {
        int srcHeight = bm.getHeight();
        int srcWidth = bm.getWidth();


        if (srcHeight > pixel || srcWidth > pixel) {
            float scale_y = 0;
            float scale_x = 0;
            if (srcHeight > srcWidth) {
                scale_y = ((float) pixel) / srcHeight;
                scale_x = scale_y;
            } else {
                scale_x = ((float) pixel) / srcWidth;
                scale_y = scale_x;
            }

            Matrix matrix = new Matrix();
            matrix.postScale(scale_x, scale_y);
            Bitmap dstbmp = Bitmap.createBitmap(bm, 0, 0, srcWidth, srcHeight, matrix, true);
            return dstbmp;
        } else {
            return Bitmap.createBitmap(bm);
        }
    }

    public static Bitmap scaleBitmap(Bitmap bm, int dstHeight, int dstWidth) {
        if (bm == null) return null;//java.lang.NullPointerException
        int srcHeight = bm.getHeight();
        int srcWidth = bm.getWidth();
        if (srcHeight > dstHeight || srcWidth > dstWidth) {
            float scale_y = ((float) dstHeight) / srcHeight;
            float scale_x = ((float) dstWidth) / srcWidth;
            float scale = scale_y < scale_x ? scale_y : scale_x;
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            Bitmap dstbmp = Bitmap.createBitmap(bm, 0, 0, srcWidth, srcHeight, matrix, true);
            return dstbmp;
        } else {
            return Bitmap.createBitmap(bm);
        }
    }

    public static String getAppVersionName(Context context) {
        String versionName = "";
        try {
            // ---get the package info---
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionName = pi.versionName;
            if (versionName == null || versionName.length() <= 0) {
                return "";
            }
        } catch (Exception e) {
        }
        return versionName;
    }

    public static ImageLoader imageLoader = ImageLoader.getInstance();

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        // ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder configBuilder = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)                                   // UIL的默认设置
                .denyCacheImageMultipleSizesInMemory()
                .memoryCacheSize(10 * 1024 * 1024)                                          // 10MB内存缓存
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024)                                            // 50MB磁盘缓存
                .defaultDisplayImageOptions(new DisplayImageOptions.Builder()
                                .showImageOnLoading(R.drawable.tf_placeholderimg)           // 设置图片下载期间显示的图片
                                .showImageForEmptyUri(R.drawable.tf_placeholderimg)         // 设置图片Uri为空或是错误的时候显示的图片
                                .showImageOnFail(R.drawable.tf_placeholderimg)              // 设置图片加载或解码过程中发生错误显示的图片
                                .cacheInMemory(true)                                        // 设置下载的图片是否缓存在内存中
                                .cacheOnDisk(true)                                          // 设置下载的图片是否缓存在
                                .bitmapConfig(Bitmap.Config.RGB_565)
                                .build()
                );

        if (BuildConfig.DEBUG) {
            configBuilder.writeDebugLogs();
        }

        // Initialize ImageLoader with configuration.
        imageLoader.init(configBuilder.build());
    }

    public static void getImage(Context context, ImageView imageView, String uri) {
        getImage(context, imageView, uri, true);
    }

    public static void getImage(Context context, ImageView imageView, String uri, boolean fromCache) {
        if (!fromCache) {
            MemoryCacheUtils.removeFromCache(uri, imageLoader.getMemoryCache());
            DiskCacheUtils.removeFromCache(uri, imageLoader.getDiskCache());
        }

        imageLoader.displayImage(uri, imageView);
    }


    public static void startActivity(Activity activity) {

    }

    /**
     * * Method for Setting the Height of the ListView dynamically.
     * *** Hack to fix the issue of not showing all the items of the ListView
     * *** when placed inside a ScrollView  ***
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    /**
     * 根据手机的分辨率从dp转成为px
     */
    public static int pxFromDip(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从px转成为 dp
     */
    public static int dipFromPx(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int tryParseInteger(String value, int def) {
        int i;

        try {
            i = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            i = def;
        }

        return i;
    }

    public static double tryParseDouble(String value, double def) {
        double d;

        try {
            d = Double.parseDouble(value);
        } catch (NumberFormatException ex) {
            d = def;
        }

        return d;
    }

    public static Date tryParseDate(String value) {
        return tryParseDate(value, "yyyy-MM-dd HH:mm:ss");
    }

    public static Date tryParseDate(String value, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date d;
        try {
            d = sdf.parse(value);
        } catch (ParseException ex) {
            d = null;
        }

        return d;
    }


    public static String getEditText(EditText et) {
        if (et != null) {
            return et.getText().toString().trim();
        }
        return "";
    }

    public static String getEditText(EditText et, String def) {
        String result = getEditText(et);
        if (result.equals("")) {
            result = def;
        }
        return result;
    }

    public static String getEditText(View view, int id, String def) {
        return getEditText((EditText) view.findViewById(id), def);
    }

    public static String getEditText(View view, int id) {
        return getEditText((EditText) view.findViewById(id), "");
    }

    public static void setTextView(View view, int id, Object... args) {
        TextView textView = (TextView) view.findViewById(id);
        if (textView != null) {
            textView.setText(String.format(textView.getText().toString(), args));
        }
    }

    public static void setTextView(TextView tv, Object... args) {
        if (tv != null) {
            tv.setText(String.format(tv.getText().toString(), args));
        }
    }

    public static void setTextView(View view, int id, String text) {
        TextView textView = (TextView) view.findViewById(id);
        if (textView != null) {
            String format = textView.getText().toString();
            if (format.contains("%s")) {
                text = String.format(format, text);
            }
            textView.setText(text);
        }
    }

    public static void setEditHint(View view, int id, String text) {
        TextView textView = (TextView) view.findViewById(id);
        if (textView != null) {
            textView.setHint(text);
        }
    }

    public static void setEditText(View view, int id, String text) {
        EditText editText = (EditText) view.findViewById(id);
        if (editText != null) {
            editText.setText(text);
        }
    }

    public static void setEditText(EditText et, String text) {
        if (et != null) {
            et.setText(text);
        }
    }

    public static void setEditText(View view, int id, String format, Object... args) {
        EditText editText = (EditText) view.findViewById(id);
        if (editText != null) {
            editText.setText(String.format(format, args));
        }
    }

    public static void setImageViewSrc(View view, int id, int resource) {
        ((ImageView) view.findViewById(id)).setImageResource(resource);
    }

    public static void setImageView(View view, Activity activity, int id, String url) {
        ImageView imageView = (ImageView) view.findViewById(id);
        if (imageView != null) {
            Utils.getImage(activity, imageView, url);
        }
    }

    public static Bitmap setImageView(View view, int id, Bitmap src) {
        ImageView imageView = (ImageView) view.findViewById(id);
        if (imageView != null) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(src);
        }
        return src;
    }

    public static Bitmap setImageViewLocal(View view, int id, String path) {
        return Utils.setImageView(view, id, BitmapFactory.decodeFile(path));
    }

    public static String getLocalImageBase64(String uri) {
        return Utils.bitmapToBase64(BitmapFactory.decodeFile(uri));
    }

    public static LinearLayout getLinearLayout(View view, int id) {
        return (LinearLayout) view.findViewById(id);
    }

    public static void addView(View view, int id, View child) {
        ((ViewGroup) view.findViewById(id)).addView(child);
    }

    public static View getView(View view, int id) {
        return view.findViewById(id);
    }

    public static void hideView(View view, int id) {
        view.findViewById(id).setVisibility(View.GONE);
    }

    public static void showView(View view, int id) {
        View view1 = view.findViewById(id);
        if (view1 != null) {
            view1.setVisibility(View.VISIBLE);
        }
    }

    public static TextView getTextView(View view, int id) {
        return (TextView) view.findViewById(id);
    }


    public static void setOnClickListeners(View view, int[] ids, View.OnClickListener l) {
        for (int i : ids) {
            View v = view.findViewById(i);
            if (v != null) {
                v.setOnClickListener(l);
            }
        }
    }

    public static void setOnClickListener(View view, int id, View.OnClickListener l) {
        View v = view.findViewById(id);
        if (v != null) {
            v.setOnClickListener(l);
        }
    }

    public static void setOnClickListeners(View view, Integer[] ids, View.OnClickListener l) {
        for (int i : ids) {
            View v = view.findViewById(i);
            if (v != null) {
                v.setOnClickListener(l);
            }
        }
    }

    public static <T> boolean in_array(T needle, T[] haystack) {
        return new ArrayList<T>(Arrays.asList(haystack)).indexOf(needle) >= 0;
    }

    public static void toast(Activity activity, String msg) {
        ToastView toast = new ToastView(activity, msg);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static String bitmapToBase64(Bitmap bitmap) {

        String result = "";
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                baos.flush();
                baos.close();

                byte[] bitmapBytes = baos.toByteArray();
                result = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static float parseFloat(String data) {
        float result = 0;
        try {
            result = Float.parseFloat(data);
        } catch (Exception e) {
            result = 0;
        }
        return result;
    }

    public static int parseInt(String data) {
        int result = 0;
        try {
            result = Integer.parseInt(data);
        } catch (Exception e) {
            result = 0;
        }
        return result;
    }

    public static int getWindowWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();//屏幕宽度
        return width;
    }

    public static boolean is_url(String str) {
        if (str == null) return false;
        return str.trim().startsWith("http://") || str.trim().startsWith("https://");
    }

    public static <T> T jsonDecode(JSONObject object, Class<T> cls) {
        T result;
        Gson gson = new Gson();
        result = gson.fromJson(object.toString(), cls);
        return result;
    }

    public static String jsonEncode(Object object) {
        Gson gson = new Gson();
        gson.toJson(object);
        return gson.toJson(object);
    }
}
