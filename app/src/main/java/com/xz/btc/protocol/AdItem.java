package com.xz.btc.protocol;

/**
 * Created by txj on 15/3/6.
 */

import com.xz.tframework.utils.Utils;

import org.json.JSONObject;

import java.util.Date;

public class AdItem {

    public int id;
    public String image;
    public String title;
    public String description;
    public String keywords;
    public String categoryId;
    public String productId;
    public int adClickType;
    public Date startTime;
    public Date endTime;
    public String html;

    public void fromJson(JSONObject jsonObject) {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.optInt("id");
        this.description = jsonObject.optString("desc");
        this.image = jsonObject.optString("img");
        this.title = jsonObject.optString("name");
        this.keywords = jsonObject.optString("keywords");
        this.categoryId = jsonObject.optString("cateid");
        this.productId = jsonObject.optString("productid");
        this.adClickType = jsonObject.optInt("adclicktype", -1);
        this.startTime = Utils.tryParseDate(jsonObject.optString("start_time"));
        this.endTime = Utils.tryParseDate(jsonObject.optString("end_time"));
        this.html = jsonObject.optString("html");
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();

        return localItemObject;
    }

}
