package com.xz.tframework.adapter;

/**
 * Created by txj on 15/4/14.
 */

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xz.external.androidquery.callback.AjaxCallback;
import com.xz.tframework.model.DebugMessageModel;

public class DebugMessageListAdapter extends BaseAdapter {
    private Context context;

    public DebugMessageListAdapter( Context context)
    {
        this.context = context;
    }

    @Override
    public int getCount() {
         
        return DebugMessageModel.messageList.size();
    }

    @Override
    public Object getItem(int position) {
         
        return null;
    }

    @Override
    public long getItemId(int position) {
         
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int size = DebugMessageModel.messageList.size();
        AjaxCallback msg = DebugMessageModel.messageList.get(size -1 -position);
        String msgDesc = msg.toString();
        TextView itemView = new TextView(this.context);
        itemView.setText(msgDesc);
        return itemView;
    }
}
