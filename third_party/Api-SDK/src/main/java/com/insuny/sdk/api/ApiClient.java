package com.insuny.sdk.api;
/*
该文件由脚本自动生成，不要手动修改
 */
import android.app.Dialog;
import android.content.Context;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;

import com.insuny.sdk.api.request.*;
import com.insuny.sdk.api.response.*;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ApiClient{
    protected boolean showProgress = true;
    protected OnApiClientListener apiClientListener;

    private AQuery aq;
    private Context context;

    public ApiClient(Context context, OnApiClientListener apiClientListener) {
        this.context = context;
        aq = new AQuery(context);
        this.apiClientListener = apiClientListener;
    }

    public ApiClient hideProgress() {
        showProgress = false;
        return this;
    }

    public interface OnApiClientListener {
        void beforeAjaxPost(Map<String, ?> params);

        void afterAjaxPost(String url, JSONObject jsonObject, AjaxStatus ajaxStatus);

        String getToken();

        Dialog getProgressDialog();
    }

    public interface OnSuccessListener {
        void callback(Object object);
    }

    public interface OnFailListener {
        void callback(Object object);
    }

    private <T> T jsonDecode(JSONObject object, Class<T> cls) {
        T result;
        Gson gson = new Gson();
        result = gson.fromJson(object.toString(), cls);
        return result;
    }

    private String jsonEncode(Object object) {
        Gson gson = new Gson();
        gson.toJson(object);
        return gson.toJson(object);
    }

    private void ajaxPost(String url, String request, AjaxCallback<JSONObject> callback) {
        if (showProgress) {
            aq.progress(apiClientListener.getProgressDialog());
        } else {
            aq.progress(null);
            showProgress = true;
        }
        Map<String, ?> params = new HashMap<>();
        try {
            JSONObject jsonObject = new JSONObject(request);
            params = getBeeQueryParams(jsonObject);
            callback.params(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        apiClientListener.beforeAjaxPost(params);

        aq.ajax(url, JSONObject.class, callback);
    }

    private Map<String, ?> getBeeQueryParams(JSONObject request) {
        Map<String, String> params = new HashMap<>();
        if (request == null) request = new JSONObject();
        params.put("data", request.toString());
        params.put("token", apiClientListener.getToken());
        return params;
    }

    private boolean callback(String url, JSONObject jsonObject, AjaxStatus ajaxStatus) {
        boolean result = false;
        apiClientListener.afterAjaxPost(url, jsonObject, ajaxStatus);

        try {
            result = jsonObject != null && jsonObject.getInt("status") != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }   
    
    public static final String ItemFollow =ApiConfig.API_URL+"/item/follow";
    public void doItemFollow(ItemFollowRequest request, final OnSuccessListener successListener){
        doItemFollow(request,successListener,null);
    }    
    public void doItemFollow(ItemFollowRequest request, final OnSuccessListener successListener,final OnFailListener failListener) {
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                Object object = jsonDecode(jo, ItemFollowResponse.class);
                if (ApiClient.this.callback(url, jo, status)){
                    if (successListener != null) {
                        successListener.callback(object);
                    }
                }else{
                    if (failListener != null) {
                        failListener.callback(object);
                    }
                }
            }
        };
        ajaxPost(ItemFollow, jsonEncode(request), cb);
    }
    
    public static final String ItemGet =ApiConfig.API_URL+"/item/get";
    public void doItemGet(ItemGetRequest request, final OnSuccessListener successListener){
        doItemGet(request,successListener,null);
    }    
    public void doItemGet(ItemGetRequest request, final OnSuccessListener successListener,final OnFailListener failListener) {
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                Object object = jsonDecode(jo, ItemGetResponse.class);
                if (ApiClient.this.callback(url, jo, status)){
                    if (successListener != null) {
                        successListener.callback(object);
                    }
                }else{
                    if (failListener != null) {
                        failListener.callback(object);
                    }
                }
            }
        };
        ajaxPost(ItemGet, jsonEncode(request), cb);
    }
    
    public static final String ItemUnfollow =ApiConfig.API_URL+"/item/unfollow";
    public void doItemUnfollow(ItemUnfollowRequest request, final OnSuccessListener successListener){
        doItemUnfollow(request,successListener,null);
    }    
    public void doItemUnfollow(ItemUnfollowRequest request, final OnSuccessListener successListener,final OnFailListener failListener) {
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                Object object = jsonDecode(jo, ItemUnfollowResponse.class);
                if (ApiClient.this.callback(url, jo, status)){
                    if (successListener != null) {
                        successListener.callback(object);
                    }
                }else{
                    if (failListener != null) {
                        failListener.callback(object);
                    }
                }
            }
        };
        ajaxPost(ItemUnfollow, jsonEncode(request), cb);
    }
    
    public static final String LogsGet =ApiConfig.API_URL+"/logs/get";
    public void doLogsGet(LogsGetRequest request, final OnSuccessListener successListener){
        doLogsGet(request,successListener,null);
    }    
    public void doLogsGet(LogsGetRequest request, final OnSuccessListener successListener,final OnFailListener failListener) {
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                Object object = jsonDecode(jo, LogsGetResponse.class);
                if (ApiClient.this.callback(url, jo, status)){
                    if (successListener != null) {
                        successListener.callback(object);
                    }
                }else{
                    if (failListener != null) {
                        failListener.callback(object);
                    }
                }
            }
        };
        ajaxPost(LogsGet, jsonEncode(request), cb);
    }
    
    public static final String OrderAdd =ApiConfig.API_URL+"/order/add";
    public void doOrderAdd(OrderAddRequest request, final OnSuccessListener successListener){
        doOrderAdd(request,successListener,null);
    }    
    public void doOrderAdd(OrderAddRequest request, final OnSuccessListener successListener,final OnFailListener failListener) {
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                Object object = jsonDecode(jo, OrderAddResponse.class);
                if (ApiClient.this.callback(url, jo, status)){
                    if (successListener != null) {
                        successListener.callback(object);
                    }
                }else{
                    if (failListener != null) {
                        failListener.callback(object);
                    }
                }
            }
        };
        ajaxPost(OrderAdd, jsonEncode(request), cb);
    }
    
    public static final String UserLogin =ApiConfig.API_URL+"/user/login";
    public void doUserLogin(UserLoginRequest request, final OnSuccessListener successListener){
        doUserLogin(request,successListener,null);
    }    
    public void doUserLogin(UserLoginRequest request, final OnSuccessListener successListener,final OnFailListener failListener) {
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                Object object = jsonDecode(jo, UserLoginResponse.class);
                if (ApiClient.this.callback(url, jo, status)){
                    if (successListener != null) {
                        successListener.callback(object);
                    }
                }else{
                    if (failListener != null) {
                        failListener.callback(object);
                    }
                }
            }
        };
        ajaxPost(UserLogin, jsonEncode(request), cb);
    }
    
    public static final String User_likeAdd =ApiConfig.API_URL+"/user_like/add";
    public void doUser_likeAdd(User_likeAddRequest request, final OnSuccessListener successListener){
        doUser_likeAdd(request,successListener,null);
    }    
    public void doUser_likeAdd(User_likeAddRequest request, final OnSuccessListener successListener,final OnFailListener failListener) {
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                Object object = jsonDecode(jo, User_likeAddResponse.class);
                if (ApiClient.this.callback(url, jo, status)){
                    if (successListener != null) {
                        successListener.callback(object);
                    }
                }else{
                    if (failListener != null) {
                        failListener.callback(object);
                    }
                }
            }
        };
        ajaxPost(User_likeAdd, jsonEncode(request), cb);
    }
    
    public static final String User_likeDelete =ApiConfig.API_URL+"/user_like/delete";
    public void doUser_likeDelete(User_likeDeleteRequest request, final OnSuccessListener successListener){
        doUser_likeDelete(request,successListener,null);
    }    
    public void doUser_likeDelete(User_likeDeleteRequest request, final OnSuccessListener successListener,final OnFailListener failListener) {
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                Object object = jsonDecode(jo, User_likeDeleteResponse.class);
                if (ApiClient.this.callback(url, jo, status)){
                    if (successListener != null) {
                        successListener.callback(object);
                    }
                }else{
                    if (failListener != null) {
                        failListener.callback(object);
                    }
                }
            }
        };
        ajaxPost(User_likeDelete, jsonEncode(request), cb);
    }
}
