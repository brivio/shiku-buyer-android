package com.xz.shiku.adapter.address;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xz.btc.protocol.ADDRESS;
import com.xz.shiku.R;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import java.util.List;

public class AddressAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context mContext;
    private List<ADDRESS> mAddresses;
    private int mRightWidth = 0;
    private AbOnItemClickListener abOnItemClickListener;

    public AddressAdapter(Context context, List<ADDRESS> list, int rightWidth, AbOnItemClickListener l) {
        mContext = context;
        mAddresses = list;
        mInflater = LayoutInflater.from(context);
        mRightWidth = rightWidth;
        abOnItemClickListener = l;
    }

    @Override
    public int getCount() {
        return mAddresses != null ? mAddresses.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mAddresses != null ? mAddresses.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        OnAddressActionListener listener;
        ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            listener = new OnAddressActionListener();
            view = this.mInflater.inflate(R.layout.item_address, parent, false);

            holder.rl_left = (RelativeLayout) view.findViewById(R.id.rl_left);
            holder.rl_right = (RelativeLayout) view.findViewById(R.id.rl_right);
            holder.item_address_default_marker = (ImageView) view.findViewById(R.id.item_address_default_marker);
            holder.item_address_user = (TextView) view.findViewById(R.id.item_address_user);
            holder.item_address_tele = (TextView) view.findViewById(R.id.item_address_tele);
            holder.item_address_address = (TextView) view.findViewById(R.id.item_address_address);
            holder.delbtn = (TextView) view.findViewById(R.id.activity_favorite_item_delbtn);
            holder.delbtn.setOnClickListener(listener);

            LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            holder.rl_left.setLayoutParams(lp1);
            LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(mRightWidth,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            holder.rl_right.setLayoutParams(lp2);

            view.setTag(holder);
            view.setTag(holder.delbtn.getId(), listener);
        } else {
            holder = (ViewHolder) view.getTag();
            listener = (OnAddressActionListener) view.getTag(holder.delbtn.getId());

            if (view.getScrollX() > 0) {
                view.scrollTo(0, 0);
            }
        }

        ADDRESS item = mAddresses.get(position);

        holder.item_address_user.setText(item.consignee);
        holder.item_address_tele.setText(item.tel);
        holder.item_address_address.setText(String.format("%s%s%s%s，%s",
                item.province, item.city, item.district, item.address, item.zipcode));
        if (item.is_default) {
            holder.rl_left.setBackgroundResource(R.color.bg_Main);
            holder.item_address_default_marker.setImageResource(R.drawable.ic_map_marker_white);
            holder.item_address_default_marker.setVisibility(View.VISIBLE);
            holder.item_address_user.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.item_address_tele.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.item_address_address.setTextColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.rl_left.setBackgroundResource(R.color.white);
            holder.item_address_default_marker.setVisibility(View.INVISIBLE);
            holder.item_address_user.setTextColor(mContext.getResources().getColor(R.color.text_color));
            holder.item_address_tele.setTextColor(mContext.getResources().getColor(R.color.text_color));
            holder.item_address_address.setTextColor(mContext.getResources().getColor(R.color.bg_Main_split));
        }

        listener.setPosition(position);

        return view;
    }

    class OnAddressActionListener implements View.OnClickListener {

        int mPosition;

        @Override
        public void onClick(View v) {
            if (abOnItemClickListener != null) {
                abOnItemClickListener.onClick(mPosition);
            }
        }

        public void setPosition(int position) {
            mPosition = position;
        }
    }

    static class ViewHolder {
        RelativeLayout rl_left;
        RelativeLayout rl_right;

        ImageView item_address_default_marker;
        TextView item_address_user;
        TextView item_address_tele;
        TextView item_address_address;
        TextView delbtn;
    }
}
