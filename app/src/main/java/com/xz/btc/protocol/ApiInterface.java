
package com.xz.btc.protocol;

public class ApiInterface {
    public static final String ORDER_AFFIRMRECEIVED = "/order/orderConfirm";
    public static final String ORDER_LIST = "/order/lists";
    public static final String ORDER_CANCEL = "/order/orderClose";
    public static final String ORDER_EXPRESS = "/order/orderLogistics";
    public static final String ORDER_PAY = "/order/pay";
    public static final String SHOPHELP = "/shopHelp";
    public static final String CART_LIST = "/cart/lists";
    public static final String CART_REMOVE = "/cart/delete";
    public static final String CART_UPDATE = "/cart/update";
    public static final String CART_ADD = "/cart/add";
    public static final String PRICE_RANGE = "/price_range";
    public static final String ARTICLE = "/article";
    public static final String ADDRESS_LIST = "/user_address/lists";
    public static final String ADDRESS_ADD = "/user_address/add";
    public static final String ADDRESS_SET_DEFAULT = "/address/setDefault";
    public static final String ADDRESS_UPDATE = "/user_address/update";
    public static final String ADDRESS_INFO = "/address/info";
    public static final String ADDRESS_DELETE = "/user_address/delete";
    public static final String USER_COLLECT_CREATE = "/user/collect/create";
    public static final String FLOW_DONE = "/flow/done";
    public static final String BRAND = "/brand";
    public static final String HOME_CATEGORY = "/home/category";
    public static final String USER_COLLECT_DELETE = "/user/collect/delete";
    public static final String USER_INTEGRAL_DELETE = "/user/integral/delete";
    public static final String VALIDATE_INTEGRAL = "/validate/integral";
    public static final String HOME_DATA = "/index/get";
    public static final String USER_SIGNIN = "/user/login";
    public static final String USER_SIGNUP = "/user/register";
    public static final String USER_INFO = "/user/get";
    public static final String USER_INFO_UPDATE = "/user/update";
    public static final String USER_RESET_PASSWORD = "/user/resetPassword";
    public static final String USER_AVATAR = "/user/avatar";
    public static final String USER_TASTE_LIST = "/user_taste/lists";
    public static final String USER_TASTE_CADIDATE_LIST = "/user_taste/add_lists";
    public static final String USER_TASTE_ADD = "/user_taste/add";
    public static final String USER_TASTE_DELETE = "/user_taste/delete";
    public static final String USER_SETTINGS_UPDATE = "/user/other_set";
    public static final String GOODS_DESC = "/goods/desc";
    public static final String PRODUCT_DETAILS = "/item/get";
    public static final String USER_SIGNUPFIELDS = "/user/signupFields";
    public static final String SEARCHKEYWORDS = "/searchKeywords";
    public static final String VALIDATE_BONUS = "/validate/bonus";
    public static final String REGION = "/region";
    public static final String USER_COLLECT_LIST = "/user/collect/list";
    public static final String USER_MESSAGE_LIST = "/message/lists";
    public static final String USER_MESSAGE_DELETE = "/message/delete";
    public static final String USER_INTEGRAL_LIST = "/user/integral/list";
    public static final String CONFIG = "/config";
    public static final String COMMENTS = "/comments";
    public static final String SEARCH = "/item/lists";
    public static final String CATEGORY = "/item_cate/lists";
    public static final String FLOW_CHECKORDER = "/flow/checkOrder";
    public static final String FAVORITE_LIST = "/user_like/lists";
    public static final String FAVORITE_ADD = "/user_like/add";
    public static final String FAVORITE_REMOVE = "/user_like/delete";
    public static final String COUPON_LIST = "/quan/lists";
    public static final String FARMER_LIST = "/user_follow/lists";
    public static final String FARMER_ADD = "/user_follow/add";
    public static final String FARMER_DELETE = "/user_follow/delete";
}