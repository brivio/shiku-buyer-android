package com.xz.shiku.adapter.integral;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.xz.btc.protocol.COLLECT_LIST;
import com.xz.btc.protocol.INTEGRAL;
import com.xz.shiku.R;
import com.xz.tframework.utils.Utils;
import com.xz.tframework.view.abview.AbOnItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class IntegralAdapter extends BaseAdapter {

	private Context context;
	public List<INTEGRAL> list;
	public int flag;
	private LayoutInflater inflater;
    private int mRightWidth = 0;

	private ArrayList<String> items = new ArrayList<String>();

	public Handler parentHandler;

	private SharedPreferences shared;
	private SharedPreferences.Editor editor;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    private AbOnItemClickListener abOnItemClickListener ;

	public IntegralAdapter(Context context, List<INTEGRAL> list, int rightWidth, AbOnItemClickListener abOnItemClickListener) {
		this.context = context;
		this.list = list;
		this.flag = flag;
		inflater = LayoutInflater.from(context);
        mRightWidth = rightWidth;
        this.abOnItemClickListener=abOnItemClickListener;
	}

	@Override
	public int getCount() {

		return list.size();

	}

	@Override
	public Object getItem(int position) {

		return list.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {

        ViewHolder holder;
        if (view==null) {
            holder=new ViewHolder();
            view=this.inflater.inflate(R.layout.item_integral, null);

            holder.rl_left = (RelativeLayout) view
                    .findViewById(R.id.rl_left);
            holder.rl_right = (RelativeLayout) view
                    .findViewById(R.id.rl_right);

            holder.icon=(ImageView) view.findViewById(R.id.imageview);
//            holder.username=(TextView) view.findViewById(R.id.username);
//            holder.shopname=(TextView) view.findViewById(R.id.shopname);
//            holder.yjhl=(TextView) view.findViewById(R.id.yjhl);
//            holder.mcl=(TextView) view.findViewById(R.id.mcl);
//            holder.delbtn=(TextView) view.findViewById(R.id.activity_favorite_item_delbtn);

            view.setTag(holder);
        }
        else {
            holder=(ViewHolder) view.getTag();
        }

        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        holder.rl_left.setLayoutParams(lp1);
        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(mRightWidth,
                LinearLayout.LayoutParams.MATCH_PARENT);
        holder.rl_right.setLayoutParams(lp2);

        final INTEGRAL item=list.get(position);

        Utils.getImage(context, holder.icon, item.img.small);

//        holder.username.setText(item.content);
//        holder.shopname.setText(String.format("店铺名称:%s",item.custom_data));
//        holder.yjhl.setText(String.format("有机含量:%s",item.time));
//        holder.delbtn.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//             abOnItemClickListener.onClick(item.id);
//            }
//        });


        return view;
	}
	
	class ViewHolder {

        RelativeLayout rl_left;
        RelativeLayout rl_right;

        ImageView icon;
        TextView username;
        TextView shopname;
        TextView yjhl;
        TextView mcl;

        TextView delbtn;
		
	}

}
