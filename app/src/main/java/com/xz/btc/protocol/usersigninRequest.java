
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Table(name = "usersigninRequest")
public class usersigninRequest extends Model {

    @Column(name = "uid")
    public String uid;

    @Column(name = "username")
    public String username;

    @Column(name = "password")
    public String password;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.uid = jsonObject.optString("tele");
        this.username = jsonObject.optString("username");
        this.password = jsonObject.optString("password");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();

        localItemObject.put("tele", uid);
        localItemObject.put("username", username);
        localItemObject.put("password", password);
        return localItemObject;
    }
}
