
package com.xz.btc.protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class GetFarmersRequest {

    public PAGINATION pagination;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }
    }

    public JSONObject toJson() {
        JSONObject localItemObject = new JSONObject();
        String tempKey;

        try {
            if (null != pagination) {
                JSONObject jsonPagination = pagination.toJson();
                Iterator iPagination = jsonPagination.keys();
                while (iPagination.hasNext()) {
                    tempKey = (String) iPagination.next();
                    localItemObject.put(tempKey, jsonPagination.get(tempKey));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return localItemObject;
    }
}