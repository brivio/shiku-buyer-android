package com.xz.tframework.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

/**
 * Created by Maimez on 2015/5/7.
 */
public class GridViewInHSV extends GridView {

    public GridViewInHSV(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
        super.setAdapter(adapter);

        if (adapter != null) {
            setNumColumns(adapter.getCount());
            int columnWidth = getRequestedColumnWidth();
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    columnWidth * adapter.getCount(),
                    LinearLayout.LayoutParams.MATCH_PARENT);
            setLayoutParams(params);
        }
    }
}
