package com.xz.shiku.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.xz.shiku.R;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.tinterface.BackHandledInterface;

public class SearchResultActivity extends FragmentActivity implements BackHandledInterface {

    public static final int KEYWORD = 0;
    public static final int CATEGORY = 1;

    public static final String SEARCH_ARG_BY = "search_by";
    public static final String SEARCH_ARG_KEYWORD = "keyword";
    public static final String SEARCH_ARG_CATEGORY_ID = "category_id";
    public static final String SEARCH_ARG_CATEGORY_NAME = "category_name";

    private BackHandledFragment mBackHandedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search_result);
    }

    @Override
    public void setSelectedFragment(BackHandledFragment selectedFragment) {
        mBackHandedFragment = selectedFragment;
    }
}
