package com.xz.shiku.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.androidquery.callback.AjaxStatus;
import com.insuny.sdk.api.ApiClient;
import com.xz.btc.protocol.SESSION;
import com.xz.shiku.R;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.utils.Utils;
import com.xz.tframework.view.MyProgressDialog;
import com.xz.tframework.view.ToastView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class BaseShikuFragment extends BackHandledFragment {
    protected ApiClient apiClient;

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        apiClient = new ApiClient(getActivity(), new ApiClient.OnApiClientListener() {
            @Override
            public void beforeAjaxPost(Map<String, ?> params) {

            }

            @Override
            public void afterAjaxPost(String url, JSONObject jsonObject, AjaxStatus ajaxStatus) {
                try {
                    if (jsonObject == null) {
                        toast("网络错误，请检查网络设置");
                        return;
                    }
                    if (jsonObject.getInt("status") == 0) {
                        toast(jsonObject.getString("result"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public String getToken() {
                return SESSION.getInstance().token;
            }

            @Override
            public Dialog getProgressDialog() {
                return new MyProgressDialog(getActivity(),
                        getActivity().getResources().getString(R.string.hold_on)).mDialog;
            }
        });
    }

    protected BaseShikuFragment setItemImgSize(View rootView, int id) {
        View view;
        if (id == 0) {
            view = rootView;
        } else {
            view = rootView.findViewById(id);
        }
        if (view != null)
            view.setLayoutParams(new LinearLayout.LayoutParams(
                    Utils.getWindowWidth(getActivity()),
                    Utils.getWindowWidth(getActivity()) / 2
            ));
        return this;
    }

    protected BaseShikuFragment setItemImgSize(int id) {
        return setItemImgSize(myView, id);
    }

    protected BaseShikuFragment setItemImgSize(View view) {
        return setItemImgSize(view, 0);
    }

    protected void toast(String content) {
        ToastView toast = new ToastView(getActivity(), content);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
