/**
 * Created by txj on 15/4/14.
 */

package com.xz.external.androidquery.callback;


public interface Transformer{

	public <T> T transform(String url, Class<T> type, String encoding, byte[] data, AjaxStatus status);

	
}
