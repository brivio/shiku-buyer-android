package com.xz.shiku.fragment.user;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.xz.btc.protocol.ApiInterface;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.shiku.R;
import com.xz.shiku.controller.UserController;
import com.xz.tframework.fragment.BackHandledFragment;
import com.xz.tframework.view.ToastView;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserRestPswFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserRestPswFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserRestPswFragment extends BackHandledFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_MOBILE = "mobile";

    private String mMobile;

    private OnFragmentInteractionListener mListener;

    private boolean hadIntercept;

    @InjectView(R.id.password)
    EditText mPassword;
    @InjectView(R.id.validcode)
    EditText mConfirmedPassword;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param mobile The mobile used to register..
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserRestPswFragment newInstance(String mobile) {
        titleResId = R.string.title_fragment_user_reset_password;
        topRightText = "";

        UserRestPswFragment fragment = new UserRestPswFragment();
        Bundle args = new Bundle();
        args.putString(ARG_MOBILE, mobile);
        fragment.setArguments(args);

        return fragment;
    }

    public UserRestPswFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMobile = getArguments().getString(ARG_MOBILE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.layout_user_resetpsw, container, false);
        ButterKnife.inject(this, v);
        UserController.getInstance().addResponseListener(this);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        UserController.getInstance().removeResponseListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @OnClick(R.id.btnnext)
    public void onNext() {
        if (mPassword.length() < 1 || mConfirmedPassword.length() < 1) {
            ToastView.showMessage(getActivity(), "您输入的密码为空，请输入后再进行操作～");
            return;
        }

        String password = mPassword.getText().toString();
        String confirmedPassword = mConfirmedPassword.getText().toString();
        if (!password.equals(confirmedPassword)) {
            ToastView.showMessage(getActivity(), "您前后两次输入的密码不一致，请重新输入");
            return;
        }

        UserController.getInstance().resetPassword(getActivity(), mMobile, password);
    }

    @Override
    public boolean onBackPressed() {
        if (hadIntercept) {
            return false;
        } else {
            Toast.makeText(getActivity(), "Click From MyFragment", Toast.LENGTH_SHORT).show();
            hadIntercept = true;
            return true;
        }
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_RESET_PASSWORD)) {
            if (UserController.getInstance().getLastResponseStatus().getIsSuccess()) {
                getActivity().finish();
            } else {
                ToastView.showMessage(getActivity(), UserController.getInstance()
                        .getLastResponseStatus()
                        .getErrorMessage());
            }
        }
    }
}
