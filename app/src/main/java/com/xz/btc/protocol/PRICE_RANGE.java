package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class PRICE_RANGE extends Model
{

    @Column(name = "price_min")
    public int price_min;

    @Column(name = "price_max")
    public int price_max;

    public void fromJson(JSONObject jsonObject)  throws JSONException
    {
        if(null == jsonObject){
            return ;
        }

        JSONArray subItemArray;

        this.price_min = jsonObject.optInt("price_min");

        this.price_max = jsonObject.optInt("price_max");
        return ;
    }

    public JSONObject toJson() throws JSONException
    {
        JSONObject localItemObject = new JSONObject();
        JSONArray itemJSONArray = new JSONArray();
        localItemObject.put("price_min", price_min);
        localItemObject.put("price_max", price_max);
        return localItemObject;
    }
}