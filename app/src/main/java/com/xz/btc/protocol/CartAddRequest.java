
package com.xz.btc.protocol;

import com.xz.external.activeandroid.Model;
import com.xz.external.activeandroid.annotation.Column;
import com.xz.external.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@Table(name = "CartAddRequest")
public class CartAddRequest extends Model {

    @Column(name = "goods_id")
    public int goods_id;

    @Column(name = "number")
    public int number;

    @Column(name = "variant_name")
    public String variant_name;

    /**
     * Unused fields below
     */
    public SESSION session;
    public ArrayList<Integer> spec;


    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }
    }

    public JSONArray toJson() {
        JSONObject localItemObject = new JSONObject();
        JSONArray itemJSONArray = new JSONArray();

        try {
            localItemObject.put("item_id", goods_id);
            localItemObject.put("num", number);
            localItemObject.put("attr", variant_name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        itemJSONArray.put(localItemObject);

        return itemJSONArray;
    }
}
