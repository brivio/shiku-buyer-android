package com.xz.shiku.controller;

import android.content.Context;

import com.xz.btc.protocol.ApiInterface;
import com.xz.btc.protocol.GetTasteResponse;
import com.xz.btc.protocol.SESSION;
import com.xz.btc.protocol.TasteResult;
import com.xz.btc.protocol.USER;
import com.xz.btc.protocol.userEditInfoRequest;
import com.xz.btc.protocol.userEditInfoResponse;
import com.xz.btc.protocol.userResetPasswordRequest;
import com.xz.btc.protocol.userUpdateAvatarRequest;
import com.xz.btc.protocol.userUpdateSettingsRequest;
import com.xz.btc.protocol.userinfoResponse;
import com.xz.btc.protocol.usersigninRequest;
import com.xz.btc.protocol.usersigninResponse;
import com.xz.external.androidquery.callback.AjaxStatus;
import com.xz.tframework.TFrameworkApp;
import com.xz.tframework.model.BaseModel;
import com.xz.tframework.utils.MD5;
import com.xz.tframework.utils.SharedPrefsUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UserController extends BaseModel {

    private static UserController mInstance;

    public static UserController getInstance() {
        if (mInstance == null) {
            mInstance = new UserController(TFrameworkApp.getInstance());
        }

        return mInstance;
    }

    private USER mUser;

    protected UserController(Context context) {
        super(context);
    }

    public USER getUser() {
        return mUser;
    }

    public boolean isUserReady() {
        return mUser != null;
    }

    public void getUser(Context context) {
        mContext = context;

        Map<String, String> params = new HashMap<>();
        params.put("token", SESSION.getInstance().token);

        ajax(ApiInterface.USER_INFO, params, false);
    }

    public void signin(Context context, String userId, String userPassword) {
        mContext = context;

        usersigninRequest request = new usersigninRequest();
        request.uid = userId;
        request.password = MD5.getMD5(userPassword);

        Map<String, String> params = new HashMap<>();
        try {
            params.put("data", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ajax(ApiInterface.USER_SIGNIN, params, true);
    }

    public void signout() {
        mUser = null;
        mInstance = null;
        ShoppingCartController.getInstance().clear();
        SharedPrefsUtil.getInstance(mContext).setSession(null);
        SESSION.getInstance().token = null;
    }

    public void signup(Context context, String mobile, String password, String nickname) {
        mContext = context;

        usersigninRequest request = new usersigninRequest();
        request.uid = mobile;
        request.username = nickname;
        request.password = MD5.getMD5(password);

        Map<String, String> params = new HashMap<>();
        try {
            params.put("data", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ajax(ApiInterface.USER_SIGNUP, params, true);
    }

    public void resetPassword(Context context, String mobile, String newPassword) {
        updatePassword(context, mobile, null, newPassword);
    }

    public void updatePassword(Context context, String mobile, String oldPassword, String newPassword) {
        mContext = context;

        userResetPasswordRequest request = new userResetPasswordRequest();
        request.uid = mobile;
        request.newpassword = MD5.getMD5(newPassword);
        request.oldpassword = oldPassword == null ? "" : MD5.getMD5(oldPassword);
        request.type = oldPassword == null ? 1 : 2;

        Map<String, String> params = new HashMap<>();
        try {
            params.put("data", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ajax(ApiInterface.USER_RESET_PASSWORD, params, true);
    }

    public void updateAvatar(Context context, String base64EncodingImage) {
        mContext = context;

        userUpdateAvatarRequest request = new userUpdateAvatarRequest();
        request.file = base64EncodingImage;

        Map<String, String> params = new HashMap<>();
        try {
            params.put("token", SESSION.getInstance().token);
            params.put("data", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ajax(ApiInterface.USER_AVATAR, params, true);
    }

    public void updateUserInfo(Context context) {
        mContext = context;

        userEditInfoRequest request = new userEditInfoRequest();
        request.user = mUser;

        Map<String, String> params = new HashMap<>();
        try {
            params.put("token", SESSION.getInstance().token);
            params.put("data", request.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ajax(ApiInterface.USER_INFO_UPDATE, params, true);
    }

    public static final int SETTING_TRACKING_INFORMATION = 1;
    public static final int SETTING_PROMOTIONS = 2;
    public static final int SETTING_SYSTEM_NOTIFICATION = 3;
    public static final int SETTING_FEEDBACK = 4;

    public void updateUserSettings(Context context, int type, String info) {
        mContext = context;

        userUpdateSettingsRequest request = new userUpdateSettingsRequest();
        request.type = type;
        request.info = info;

        Map<String, String> params = new HashMap<>();
        params.put("token", SESSION.getInstance().token);
        params.put("data", request.toJson().toString());
        ajax(ApiInterface.USER_SETTINGS_UPDATE, params, true);
    }

    TasteResult mTasteResult;

    public TasteResult getTasteResult() {
        return mTasteResult;
    }

    public void getMyTaste(Context context) {
        mContext = context;

        Map<String, String> params = new HashMap<>();
        params.put("token", SESSION.getInstance().token);

        ajax(ApiInterface.USER_TASTE_LIST, params, false);
    }

    public void getTasteCadidate(Context context) {
        mContext = context;

        Map<String, String> params = new HashMap<>();
        params.put("token", SESSION.getInstance().token);

        ajax(ApiInterface.USER_TASTE_CADIDATE_LIST, params, false);
    }

    public void toggleTaste(Context context, int tasteId, boolean isAdded) {
        mContext = context;

        String url = isAdded ? ApiInterface.USER_TASTE_DELETE : ApiInterface.USER_TASTE_ADD;

        JSONObject data = new JSONObject();
        try {
            data.put("taste_id", tasteId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> params = new HashMap<>();
        params.put("token", SESSION.getInstance().token);
        params.put("data", data.toString());

        ajax(url, params, true);
    }

    @Override
    protected void onSuccess(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_SIGNIN) || url.endsWith(ApiInterface.USER_SIGNUP)) {
            usersigninResponse response = new usersigninResponse();
            try {
                response.fromJson(jo);
                mUser = response.data.user;
            } catch (JSONException e) {
                e.printStackTrace();
                mUser = null;
            }
            if (mUser != null) {
                ShoppingCartController.getInstance().getItems(mContext, false);
                SharedPrefsUtil.getInstance(mContext).setSession(SESSION.getInstance().token);
            }
        } else if (url.endsWith(ApiInterface.USER_INFO)) {
            userinfoResponse response = new userinfoResponse();
            try {
                response.fromJson(jo);
                mUser = response.data;
            } catch (JSONException e) {
                e.printStackTrace();
                mUser = null;
            }
        } else if (url.endsWith(ApiInterface.USER_AVATAR)) {
            mUser.avatar = jo.optString("data");
        } else if (url.endsWith(ApiInterface.USER_INFO_UPDATE) ||
                url.endsWith(ApiInterface.USER_SETTINGS_UPDATE)) {
            userEditInfoResponse response = new userEditInfoResponse();
            try {
                response.fromJson(jo);
                // Generally it returns the updated user info except when
                // the user submits some feedback.
                if (response.data.id > 0) {
                    mUser = response.data;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (url.endsWith(ApiInterface.USER_TASTE_LIST)) {
            GetTasteResponse response = new GetTasteResponse();
            response.resultType = TasteResult.RESULT_LISTS;
            try {
                response.fromJson(jo);
                mTasteResult = response.data;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (url.endsWith(ApiInterface.USER_TASTE_CADIDATE_LIST)) {
            GetTasteResponse response = new GetTasteResponse();
            response.resultType = TasteResult.RESULT_CADIDATE;
            try {
                response.fromJson(jo);
                mTasteResult = response.data;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        super.onSuccess(url, jo, status);
    }

    @Override
    protected void onFail(String url, JSONObject jo, AjaxStatus status) {
        if (url.endsWith(ApiInterface.USER_SIGNIN)) {
            mUser = null;
        }

        super.onFail(url, jo, status);
    }
}
